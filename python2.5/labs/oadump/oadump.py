

#! /usr/bin/env python2.5

#-------------------------------------------------------------------------------
#
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use
# this file except in compliance with the License. You may obtain a copy of the
# License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed
# under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
# CONDITIONS OF ANY KIND, either express or implied. See the License for the
# specific language governing permissions and limitations under the License.
#
#-------------------------------------------------------------------------------
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   08/26/10  1.0      bpfeil, Si2      Si2 Tutorial 10thEd labs - Python version
#
#-------------------------------------------------------------------------------
#
# Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
#-------------------------------------------------------------------------------

import sys
import os
here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '../..'))

from optparse import OptionParser

import oa.design

class Dumper:
    def __init__(self, cellstr, viewstr):
        self.cell = cellstr
        self.view = viewstr
       
        dumpname = "%s%s.dump" % (cellstr, viewstr)
        self.dumpname = dumpname
        self.FILE=open(dumpname,'w')
       

    def log(self, formatstr, logargs):
        if not len(logargs):
            message = str(formatstr)
        else:
            message = str(formatstr) % (logargs)

        self.FILE.write(message)

    def closelog(self):
        self.FILE.close()

    def sortlog(self):
        filename=self.dumpname
        command = "sort --ignore-case -o %s %s" % (filename, filename) 
        os.system(command)        



def dumpCV(block, design, lib, dumper):
    global ns

    vtstr = oa.oaString()
    ctstr = oa.oaString()
    lstr  = oa.oaString()

    lstr = design.getLibName (ns)
    cstr = design.getCellName(ns)
    vstr = design.getViewName(ns)
    dumper.log("Design %s/%s/%s ",  (lstr, cstr, vstr))

    ctype = design.getCellType()
    vtype = design.getViewType()

    vtstr = oa.oaString()
    vtype.getName(vtstr)
    ctstr = oa.oaString()
    ctstr = ctype.getName()
    dumper.log("%s %s ", (vtstr, ctstr))

    bbox = oa.oaBox()
    block.getBBox(bbox);
    dumper.log( "BBOX ((%d, %d) (%d, %d))\n", tuple(bbox) )


def dumpInst(block, dumper):
    global ns

    for inst in block.getInsts():
        iStr = oa.oaString()
        libStr  = oa.oaString()
        cellStr = oa.oaString()
        viewStr = oa.oaString()

        inst.getName(ns, iStr)
        origin = oa.oaPoint()
        inst.getOrigin(origin)
        orient = oa.oaOrient()
        orient = inst.getOrient()
        bbox   = oa.oaBox()
        inst.getBBox(bbox);
        inst.getLibName (ns, libStr )
        inst.getCellName(ns, cellStr)
        inst.getViewName(ns, viewStr)
        orientname = oa.oaString()
        orientname = orient.getName()

        dumper.log( "Inst %s OF %s/%s/%s ", (iStr, libStr, cellStr, viewStr) )
        dumper.log("%s ", (orientname))
        dumper.log("LOC (%d, %d) ", tuple(origin) )
        dumper.log("BBOX ((%d, %d), (%d, %d))\n", tuple(bbox) )

        for iterm in inst.getInstTerms():
            tStr = oa.oaString()
            nStr = oa.oaString()
            iterm.getTermName(ns, tStr)
            net  = iterm.getNet()
            if net:
                net.getName(ns, nStr)
            else:
                nStr = "<none>"

            dumper.log( "InstTerm %s of %s is on %s\n", (iStr, tStr, nStr))



def dumpNet(block, dumper):
    global ns

    nStr = oa.oaString()
    tStr = oa.oaString()
    stypeStr = oa.oaString()
    ttypeStr = oa.oaString()

    for net in block.getNets():
        net.getName(ns, nStr)
        stype    = net.getSigType()
        stypeStr=stype.getName()
        dumper.log( "Net %s %s\n", (nStr, stypeStr) )

        for term in net.getTerms():
            ttype = term.getTermType()
            term.getName(ns, tStr)
            ttypeStr = ttype.getName()
            dumper.log( "Term %s on %s type %s", (tStr, nStr, ttypeStr) )
            
            collectPins = term.getPins()
            if (not collectPins):
                dumper.log("%s\n", (""))
            for pin in term.getPins():
                pin.getName(tStr)
                dumper.log( " Pin=%s", (tStr) )
                
            dumper.log( "\n"   , ())



def dumpShape(block, design, dumper):

    typeStr = oa.oaString()
    sStr = oa.oaString()
    pStr = oa.oaString()
    nStr = oa.oaString()      

    tech = design.getTech()

    for shape in block.getShapes():
        bbox = oa.oaBox()
        shape.getBBox(bbox)
        type = oa.oaType()
        type = shape.getType()
        typeStr = type.getName()

        dumper.log( "Shape %s", (typeStr) )
        if (typeStr == "Text"):
           sStr=shape.getText()
           dumper.log( "\"%s\"", (sStr) )

        dumper.log( " at ((%d, %d) (%d,%d))", tuple(bbox) )

        lnum = shape.getLayerNum()
        pnum = shape.getPurposeNum()

        lStr  = oa.oaString()
        lStr=lnum
        pStr  = oa.oaString()
        pStr=pnum

        if tech: 
           layer = oa.oaLayer.find(tech, lnum)
           purp  = oa.oaPurpose.find(tech, pnum)

           if layer:
              lStr=layer.getName()

           if purp:
              pStr=purp.getName()
              dumper.log( " LPP %s/%s", (lStr, pStr) )
        else:
            dumper.log( " LPP %d/%d", (lnum, pnum) )

        if shape.hasPin():
            pin = shape.getPin()
            pStr= pin.getName()
            dumper.log( " on pin %s", (pStr) )

        if shape.hasNet():
            net = shape.getNet()
            nStr= net.getName()
            dumper.log( " on net %s", (nStr) )

        if (int(type) == oa.oacPathSegType and shape.hasRoute()) :
            dumper.log( " ROUTE", () )

        dumper.log( "\n", ())



def dumpRtPoint(pt, key, dumper):
    global ns

    if (not pt):
        dumper.log( "%s NULL ", (key) )
        return 0

    type = pt.getType()
    tStr = oa.oaString()
    tStr = type.getName()

    dumper.log( "%s %s", (key, tStr ))

    aStr = oa.oaString()

    if int(type) == oa.oacInstTermType:
        pt.getInst().getName(ns, aStr)
        dumper.log( " [%s",  (aStr) )
        pt.getTerm().getName(ns, aStr)
        dumper.log( "/%s] ", (aStr) )
    elif int(type) == oa.oacSteinerType:
        nop=1
    elif int(type) == oa.oacPinType:
        pt.getName(aStr);
        dumper.log( " %s",  (aStr) )
        pt.getTerm().getName(ns, aStr)
        dumper.log( "[%s] ", (aStr) )
    else:
        bbox = oa.oaBox()
        pt.getBBox(bbox)
        lnum = pt.getLayerNum()
        pnum = pt.getPurposeNum()
        dumper.log( " at ((%d, %d) (%d,%d))", tuple(bbox) )
        dumper.log( " on %d/%d ", (lnum, pnum) )


def dumpRtElement(relem, dumper):

    type = relem.getType()

    #
    # Error Code: - no getPoints for oaFig object
    # 
    if (int(type) == oa.oacPathSegType):
        ptSt = oa.oaPoint()
        ptEn = oa.oaPoint()
        relem.getPoints(ptSt, ptEn)
        dumper.log( "SEG at ((%d,%d)", tuple(ptSt) )
        dumper.log( " (%d,%d)) ", tuple(ptEn) )
        dumper.log( "on %d ", (relem.getLayerNum(),) )

    elif (int(type) == oa.oacStdViaType or int(type) == oa.oacCustomViaType):
        ptOrigin = oa.Point()
        relem.getOrigin(ptOrigin)
        
        orient = oa.oaOrient(relem.getOrient())
        oStr   = oa.oaString()
        oStr   = orient.getName()
        dumper.log( "VIA %s at (%d, %d) ", (oStr) )
        dumper.log( " at (%d, %d) ",  tuple(ptOrigin) )


def dumpRoute(block, dumper):
    global ns

    for rt in block.getRoutes():
        rstat = rt.getRouteStatus()
        net   = rt.getNet()
        dumper.log( "Route ST %s ", rstat.getName() )
        if (net):
            dumper.log( "NET %s ", net.getName(ns) )

        rtfrom = rt.getBeginConn()
        rtto   = rt.getEndConn()
        dumpRtPoint(rt.getBeginConn(), "START", dumper)
        dumpRtPoint(rt.getEndConn(),   "END", dumper)    
        relems = []
        rt.getObjects(relems)
        for element in relems:
            dumpRtElement(element, dumper)

        dumper.log("\n", ())



# -------------------------------- main ------------------------------
oa.oaDesignInit()

ns = oa.oaNativeNS()

def main():

    if (len(sys.argv) < 4):
        print " Use : \n\toadump.py  libraryName cellName viewName\n"
        return 1 

    libstr  = sys.argv[1]
    cellstr = sys.argv[2]
    viewstr = sys.argv[3]

    pathLibDefs = "./lib.defs"
    if not os.path.exists(pathLibDefs):
	print "***Must have %s file.\n" % (pathLibDefs )
        return 1

    oa.oaLibDefList.openLibs()
    lib = oa.oaLib.find(libstr)
    if (not(lib)):
	print "***Either Lib name=%s or its path mapping in file=%s is invalid.\n" \
             % (libstr, pathLibDefs )
        return 1

    # for now
    cvtype = 0
    if (not cvtype):
        design = oa.oaDesign.open(libstr, cellstr, viewstr, "r")
    else:
        design = oa.oaDesign.open(libstr, cellstr, viewstr,oa.oaViewType.find(cvType), "r")

    if (not design):
        print "Can't read Design %s/%s/%s (check lib.defs)\n" % (libstr,cellstr,viewstr)
        return 1

    dumper = Dumper(cellstr,viewstr)

    print "Writing dump to %s%s.dump\n" % (cellstr,viewstr)
    block = design.getTopBlock()
    design.openHier(200)

    print "Reading Design %s/%s/%s\n" % (libstr, cellstr, viewstr)
    dumpCV(block, design, lib, dumper)
    dumpInst(block, dumper)
    dumpNet(block, dumper)
    dumpShape(block, design, dumper)
    dumpRoute(block, dumper)

    dumper.closelog()

    dumper.sortlog()


if (__name__ == "__main__"):
    sys.exit(main())

