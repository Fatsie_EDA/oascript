#! /usr/bin/env python2.5

#-------------------------------------------------------------------------------
#
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use
# this file except in compliance with the License. You may obtain a copy of the
# License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed
# under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
# CONDITIONS OF ANY KIND, either express or implied. See the License for the
# specific language governing permissions and limitations under the License.
#
#-------------------------------------------------------------------------------
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   08/16/10  1.0      bpfeil, Si2      Si2 Tutorial 10thEd labs - Python version
#
#-------------------------------------------------------------------------------
#
# Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working qGroup, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
#-------------------------------------------------------------------------------

import sys
import os
here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '../..'))
import oa.design


def make_inst_term_connnect_to_net(strNameTerm, inst, net):
    # Create an InstTerm of the Term named strNameTerm
    # and connected to the Net specified by the input arguments.
    nameTerm = oa.oaName(ns , strNameTerm)
    oa.oaInstTerm.create(net, inst, nameTerm)
    return

def get_term_name(term):
    global ns
    name = oa.oaString()
    if not term:
        return name
    # Return a String representation for the term's name.
    term.getName(ns, name)
    return name

def get_inst_term_name(instTerm):
    global ns
    name = oa.oaString()
    if not instTerm:
         return name

    # Return a name of the form, Instname.Termname, that is, the name
    # of InstTerm's Inst, followed by a ".", followed by the Term's name.
    # Use the NameSpace from the global_data singleton.
    # begin_ske
    instname = oa.oaString()
    instTerm.getInst().getName(ns, instname)
    termname = oa.oaString()
    instTerm.getTerm().getName(ns, termname)
    name = "%s.%s" % (instname, termname)
    return name

def make_net_and_term(block, chNameTerm, termType): 
    # Create a Net allowing the database to assign a name to it.
    # Create a Term connected to that Net with the name and type passed in
    # as an args. 
    net = oa.oaScalarNet.create(block)
    termName = oa.oaScalarName(ns, chNameTerm)
    oa.oaScalarTerm.create(net, termName, termType)
    return net

def print_nets(block):
    global ns
    # Print the names of all Nets in the specified Block. For each Net,
    # print the Term names of all Terms and InstTerms attached to it.
    if not block:
        return 1
    # get the Cell name of the Block's owner Design in the 
    # cellName variable.
    cellName = oa.oaString()
    block.getDesign().getCellName(ns, cellName)
    print ""
    print "The following Nets exist in Cell: %s" % (cellName)

    # Create an iterator for the Nets in the Block
    nets = block.getNets();

    # Create a while statement that gets the next Net in the iterator. 
    for nextNet in block.getNets():

    # Get the name of the Net into the netName variable.
        netName= oa.oaString()
        nextNet.getName(ns,netName)
        termNames = oa.oaString()

        # Create an if condition to test that there is at least one Term on the Net.
        terms = nextNet.getTerms()
        if terms:

            # Create an iterator for the Terms connected to nextNet.
            termNames = ""
            for nextTerm in nextNet.getTerms():
                termNames = "%s\t%s" % (termNames , get_term_name(nextTerm))

            # Create an if condition to test that there is at least one 
            # InstTerm connected to the Net.
            for nextInstTerm in nextNet.getInstTerms():
                termNames = "%s\t%s" % (termNames , get_inst_term_name(nextInstTerm))

        print "Net: %s is connected to Term:%s" % (netName, termNames )
    return


def make_leaf_cell(strNameCell):
    global ns
    global lib_name_str
    global chp_name_view

    # Create a netlist type Design with the mode that will overwrite any
    # existing Design of that type and names.
    scNameCell = oa.oaScalarName(ns, strNameCell)
    scNameLib  = oa.oaScalarName(ns, lib_name_str)
    scNameView = oa.oaScalarName(ns, chp_name_view)
    view_type  = oa.oaViewType.get(oa.oaReservedViewType('netlist'))
    design = oa.oaDesign.open(scNameLib, scNameCell, scNameView, view_type, 'w')

    # Create the top Block in the Design to own the design objects.
    block = oa.oaBlock.create(design)

    # Create the Nets and Terms for the leaf gate.
    termTypeInput =  oa.oaTermType(oa.oacInputTermType)
    make_net_and_term(block, "A", termTypeInput)
    make_net_and_term(block, "B", termTypeInput)

    termTypeOutput =  oa.oaTermType(oa.oacOutputTermType)
    make_net_and_term(block, "Y", termTypeOutput) 

    print_nets(block)
    design.save()
    design.close()
    return

def make_half_adder():
    global ns
    global lib_name_str
    global chp_name_view
    global mode_write

    # Open a netlist type Design and name "HalfAdder" with the mode that will
    # overwrite any existing Design of that type and name.
    scNameCell =  oa.oaScalarName(ns, "HalfAdder")
    scNameLib  =  oa.oaScalarName(ns, lib_name_str)
    scNameView =  oa.oaScalarName(ns, chp_name_view)
    view_type  =  oa.oaViewType.get(oa.oaReservedViewType('netlist'))
    mode = mode_write
    designHA = oa.oaDesign.open(scNameLib, scNameCell, scNameView, view_type, mode)

    # Create a top Block for HalfAdder Design.
    blockHA = oa.oaBlock.create(designHA);

    # Create the 4 Terms on this halfadder: A and B are inputs, C and S are outputs.
    termTypeInput  =  oa.oaTermType(oa.oacInputTermType)
    termTypeOutput =  oa.oaTermType(oa.oacOutputTermType)

    A_Net=make_net_and_term(blockHA, "A", termTypeInput)
    B_Net=make_net_and_term(blockHA, "B", termTypeInput)

    C_Net=make_net_and_term(blockHA, "C", termTypeOutput) 
    S_Net=make_net_and_term(blockHA, "S", termTypeOutput) 

    # oaTransform trans is necessary for and Inst even though we don't
    # care about placement at this point in the netlist.
    origin_x  = 0;
    origin_y  = 0;
    transform = oa.oaTransform(origin_x, origin_y, oa.oaOrient(oa.oacR0))

    # Create a scalar Inst named "And1" with the master "And" using the
    # method that takes lib/cell/view names of the master.
    scNameAnd  =   oa.oaScalarName(ns,  "And")
    scNameAnd1 =   oa.oaScalarName(ns,  "And1");
    and1 = oa.oaScalarInst.create(blockHA, scNameLib, scNameAnd, scNameView, scNameAnd1, transform)

    # Create by names a scalar Inst named "Xor1" with the master "Xor" of the master.
    scNameXor  =  oa.oaScalarName(ns, 'Xor')
    scNameXor1 =  oa.oaScalarName(ns, "Xor1")
    xor1 =  oa.oaScalarInst.create(blockHA, scNameLib, scNameXor, scNameView, scNameXor1, transform)

    # Create and hook the InstTerms of And1 and Xor1 to the right Nets.
    make_inst_term_connnect_to_net("A", and1, A_Net)
    make_inst_term_connnect_to_net("B", and1, B_Net)
    make_inst_term_connnect_to_net("Y", and1, C_Net)
    make_inst_term_connnect_to_net("A", xor1, A_Net)
    make_inst_term_connnect_to_net("B", xor1, B_Net)
    make_inst_term_connnect_to_net("Y", xor1, S_Net)

    print_nets(blockHA)
    designHA.save()
    designHA.close()
    return

def make_full_adder():
    global ns
    global lib_name_str
    global chp_name_view
    global mode_write
    # Create a 'netlist' type Design and name "FullAdder" with the mode that
    # will overwrite any existing Design of that type and name.
    scNameCell = oa.oaScalarName(ns, "FullAdder")
    scNameLib  = oa.oaScalarName(ns, lib_name_str)
    scNameView = oa.oaScalarName(ns, chp_name_view)
    view_type  = oa.oaViewType.get(oa.oaReservedViewType('netlist'))
    mode       = mode_write
    designFA   = oa.oaDesign.open(scNameLib, scNameCell, scNameView, view_type, mode)

    # Create a top Block for FullAdder Design.
    blockFA = oa.oaBlock.create(designFA)
    print "Creating FullAdder with two HalfAdder Insts, one Or Inst"

    termTypeInput =  oa.oaTermType(oa.oacInputTermType)
    termTypeOutput =  oa.oaTermType(oa.oacOutputTermType)

    A_Net  = make_net_and_term (blockFA, "A", termTypeInput)
    B_Net  = make_net_and_term (blockFA, "B", termTypeInput)
    Ci_Net = make_net_and_term (blockFA, "Ci",termTypeInput)
    Co_Net = make_net_and_term (blockFA, "Co", termTypeOutput)
    S_Net  = make_net_and_term (blockFA, "S",  termTypeOutput)

    # Create internal Nets SA_Net, CA_Net, CB_Net, using the Net create
    # method that lets the implementation assign the Net name.
    SA_Net = oa.oaScalarNet.create(blockFA) 
    CA_Net = oa.oaScalarNet.create(blockFA)
    CB_Net = oa.oaScalarNet.create(blockFA)

    # oaTransform trans is necessary to create the Inst even though
    # placement at this netlist stage is irrelevant. Create a null
    # Transform that neither translates nor rotates.
    origin_x = 0
    origin_y = 0
    transform = oa.oaTransform(origin_x, origin_y, oa.oaOrient(oa.oacR0))

    # Create two ScalarInsts "Ha1" and "Ha2" of master "HalfAdder"
    # and one ScalarInst "Or1" of master "Or" using the create method that 
    # takes the lib/cell/view names of the master.
    scNameInstCellHA  =  oa.oaScalarName(ns, "HalfAdder")
    scNameInstNameHa1 =  oa.oaScalarName (ns, "Ha1")
    instHa1 = oa.oaScalarInst.create(blockFA, scNameLib, scNameInstCellHA, scNameView, scNameInstNameHa1, transform)

    scNameInstNameHa2 =  oa.oaScalarName(ns, "Ha2")
    instHa2 = oa.oaScalarInst.create(blockFA, scNameLib, scNameInstCellHA, scNameView, scNameInstNameHa2, transform)

    scNameInstCellOr  =  oa.oaScalarName(ns, "Or")
    scNameInstNameOr1 =  oa.oaScalarName(ns, "Or1")
    instOr1 = oa.oaScalarInst.create(blockFA, scNameLib, scNameInstCellOr, scNameView, scNameInstNameOr1, transform)

    # Create and hook the InstTerms to the right Nets.
    make_inst_term_connnect_to_net("A", instHa1, A_Net)
    make_inst_term_connnect_to_net("B", instHa1, B_Net)
    make_inst_term_connnect_to_net("C", instHa1, CA_Net)
    make_inst_term_connnect_to_net("S", instHa1, SA_Net)
    
    make_inst_term_connnect_to_net("A", instHa2, SA_Net)
    make_inst_term_connnect_to_net("B", instHa2, Ci_Net)
    make_inst_term_connnect_to_net("C", instHa2, CB_Net)
    make_inst_term_connnect_to_net("S", instHa2, S_Net)
    
    make_inst_term_connnect_to_net("A", instOr1, CA_Net)
    make_inst_term_connnect_to_net("B", instOr1, CB_Net)
    make_inst_term_connnect_to_net("Y", instOr1, Co_Net)
    
    print_nets(blockFA)
    designFA.save()
    designFA.close()
    return
 
   
# -------------------------------- main ------------------------------


oa.oaDesignInit()

ns = oa.oaNativeNS()

mode_write='w'
lib_name_str='Lib'
chp_path_lib='./Lib'
chp_name_view='netlist'
mode_write='w'

def main():

    # No point in trying oaLib.find() since in this simple program there
    # are no other threads or functions that might have opened the Lib already.
    # However, this program may have executed already, in which case a
    # Lib directory will exist and attempting create again on it will hurl.
    sclibname  = oa.oaScalarName(ns, lib_name_str)
    viewName = oa.oaScalarName(ns, chp_name_view)
    lib = oa.oaLib.exists(chp_path_lib)

    if lib :
        print "Opening existing Library name=%s mapped to filesystem path=%s" % (lib_name_str, chp_path_lib)

        # Open an existing Lib using the scNameLib and chp_path_lib.
        lib = oa.oaLib.open(sclibname, chp_path_lib)
    else:
        print "Creating new Library for Lib name=%s mapped to filesystem path=%s" % (lib_name_str, chp_path_lib)

        # Create a  Lib in shared mode using the DMFileSys PlugIn.
        lib = oa.oaLib.create(sclibname, chp_path_lib , oa.oaLibMode(oa.oacSharedLibMode), 'oaDMFileSys')
        print "Overwriting lib.defs file."

    # Create/overwrite a lib.defs file in the current directory,
    ldl = oa.oaLibDefList.get("lib.defs", "w")
    oa.oaLibDef.create(ldl, sclibname, chp_path_lib)
    ldl.save()
# end_skel

# Create the "cell library".
#

    make_leaf_cell("And")
    make_leaf_cell("Or")
    make_leaf_cell("Xor")
        
# Create the design hierarchy.
#
    make_half_adder()

    make_full_adder()

    print ""
    return 0

if (__name__ == "__main__"):
    sys.exit(main())
