/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%typecheck(0) (OpenAccess_4::oaComplex<OpenAccess_4::oaFloat>)
{
    $1 = PyComplex_Check($input);
}

%typecheck(0) OpenAccess_4::oaComplex<OpenAccess_4::oaFloat> & = OpenAccess_4::oaComplex<OpenAccess_4::oaFloat>;
%typecheck(0) OpenAccess_4::oaComplex<OpenAccess_4::oaFloat> * = OpenAccess_4::oaComplex<OpenAccess_4::oaFloat>;

%typemap(in) OpenAccess_4::oaComplex<OpenAccess_4::oaFloat>
{
    if (!PyComplex_Check($input)) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    $1.real() = PyComplex_RealAsDouble($input);
    $1.imag() = PyComplex_ImagAsDouble($input);
}

%typemap(in) OpenAccess_4::oaComplex<OpenAccess_4::oaFloat> & (oaComplex<oaFloat> tmpComplex)
{
    if (!PyComplex_Check($input)) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    tmpComplex.real() = PyComplex_RealAsDouble($input);
    tmpComplex.imag() = PyComplex_ImagAsDouble($input);
    $1 = &tmpComplex;
}

%typemap(in) OpenAccess_4::oaComplex<OpenAccess_4::oaFloat> * = OpenAccess_4::oaComplex<OpenAccess_4::oaFloat> &;

