#!/usr/bin/env python
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

import sys
import os
here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '..'))

import oa.design

oa.oaDesignInit()

# Test each oa*AppDef by creating it and then returning the name.  The
# getSession() method is called to test inheritance which was a problem
# previously on the oa*AppDef proxies.

## Boolean

boolean = oa.oaBooleanAppDef(oa.oaNet).get("boolean")
print "name: %s, default: %s" % (boolean.getName(), boolean.getDefault())
boolean.getSession()
oa.oaBooleanAppDef(oa.oaNet).find("boolean")

## Data

#data = oa.oaDataAppDef(oa.oaNet).get("data", 3, 'foo')
#print "name: %s, default: %s" % (data.getName(), data.getDefault())
#data.getSession()
#oa.oaDataAppDef(oa.oaNet).find("data")

## Double

double = oa.oaDoubleAppDef(oa.oaNet).get("double")
print "name: %s, default: %s" % (double.getName(), double.getDefault())
double.getSession()
oa.oaDoubleAppDef(oa.oaNet).find("double")

## Float

float = oa.oaFloatAppDef(oa.oaNet).get("float")
print "name: %s, default: %f" % (float.getName(), float.getDefault())
float.getSession()
oa.oaFloatAppDef(oa.oaNet).find("float")

## Int

int = oa.oaIntAppDef(oa.oaNet).get("int")
print "name: %s, default: %s" % (int.getName(), int.getDefault())
int.getSession()
oa.oaIntAppDef(oa.oaNet).find("int")

## InterPointer

interPointer = oa.oaInterPointerAppDef(oa.oaNet).get("interPointer")
print "name: %s, no default" % (interPointer.getName(),)
interPointer.getSession()
oa.oaInterPointerAppDef(oa.oaNet).find("interPointer")

## IntraPointer

intraPointer = oa.oaIntraPointerAppDef(oa.oaNet).get("intraPointer")
print "name: %s, no default" % (intraPointer.getName(),)
intraPointer.getSession()
oa.oaIntraPointerAppDef(oa.oaNet).find("intraPointer")

## String

string = oa.oaStringAppDef(oa.oaNet).get("string", "foobar")
print "name: %s, default: %s" % (string.getName(), string.getDefault())
string.getSession()
oa.oaStringAppDef(oa.oaNet).find("string")

## Time

time = oa.oaTimeAppDef(oa.oaNet).get("time")
print "name: %s, default: %d" % (time.getName(), time.getDefault())
time.getSession()
oa.oaTimeAppDef(oa.oaNet).find("time")

## VarData

#varData = oa.oaVarDataAppDef(oa.oaNet).get("varData")
#print "name: %s, default: %s" % (varData.getName(), varData.getDefault())
#varData.getSession()
#oa.oaVarDataAppDef(oa.oaNet).find("varData")

## VoidPointer

voidPointer = oa.oaVoidPointerAppDef(oa.oaNet).get("voidPointer")
print "name: %s, no default" % (voidPointer.getName(),)
voidPointer.getSession()
oa.oaVoidPointerAppDef(oa.oaNet).find("voidPointer")
