#!/usr/bin/env python
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

import sys
import ctypes
flags = sys.getdlopenflags() | ctypes.RTLD_GLOBAL
ctypes.CDLL("liboaCommon.so", flags)
ctypes.CDLL("liboaBase.so", flags)
ctypes.CDLL("liboaPlugIn.so", flags)
ctypes.CDLL("liboaDM.so", flags)
ctypes.CDLL("liboaTech.so", flags)
ctypes.CDLL("liboaDesign.so", flags)
ctypes.CDLL("liboaWafer.so", flags)
import openaccess22
from openaccess22 import *
from optparse import OptionParser

def main():
    here = os.path.abspath(sys._getframe().f_code.co_filename)
    parser = OptionParser()
    parser.add_option("--libdefs", action="store", type="string", dest="libdefs", metavar="FILE",
                      help="Location of OA library definition file.")
    parser.add_option("--lib", action="store", type="string", dest="lib", metavar="<libname>", help="libname of design")
    parser.add_option("--cell", action="store", type="string", dest="cell", metavar="<cellname>", help="cellname of design")
    parser.add_option("--view", action="store", type="string", dest="view", metavar="<viewname>", help="viewname of design")
    (options, args) = parser.parse_args()

    if (not options.lib or not options.cell or not options.view) :
        print "Missing -lib, -cell, or -view."
        return 1

    oaDesignInit()
    if (options.libdefs) :
        oaLibDefList.static_openLibs(options.libdefs)
    else :
        oaLibDefList.static_openLibs()

    ns = oaNativeNS()

    lib_scname = oaScalarName(ns, options.lib)
    lib = oaLib.static_find(lib_scname)
    if not lib:
        print "Couldn't find lib %s" % options.lib
        return 1
    accessEnum = oaLibAccessLevelEnum(oacReadLibAccessLevel)
    accessObj = oaLibAccess(accessEnum)
    if not lib.getAccess(accessObj):
        print "lib %s: No Access!" % options.lib
        return 1

    cell_scname = oaScalarName(ns, options.cell)
    view_scname = oaScalarName(ns, options.view)

    design = oaDesign.static_open(lib_scname, cell_scname, view_scname, "r")
    if not design:
        print "No design!"
        return 1

    for i in range(10000):
        topBlock = design.getTopBlock()
        if not topBlock:
            print "No top block!"
            return 1

        for shape in topBlock.getShapesIter():
            bbox = oaBox()
            shape.getBBox(bbox)
            #print "%s on layer %d with bbox %s" % (shape.getType().getName(), shape.getLayerNum(), bbox)

    return 0
    
if (__name__ == "__main__"):
    sys.exit(main())

