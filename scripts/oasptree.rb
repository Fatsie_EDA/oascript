###############################################################################
#
# Copyright 2011 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   01/10/2011: Initial copy
#
###############################################################################

require 'yaml'

module OasParseTree

  # Mixin for objects that have an access attribute (classes and functions)
  module AccessMixin
    # Is this protected? true/false
    def protected? ; @access == 'protected' ; end

    # Is this private? true/false
    def private? ; @access == 'private' ; end

    # Is this public? true/fase
    def public? ; @access == 'public' ; end    
  end

  ############################################################################

  # Stores the C++ parse tree.  The structure is a YAML structure as read in
  # from a YAML file.
  class Tree

    attr_reader :klasses

    # Create a new Tree object from a hash structure (from YAML).
    def initialize(struct)
      @klass_hash = {}
      @klasses = []
      struct['classes'].each {|kstruct| add_klass(Klass.new(self, kstruct))}
    end

    # Load the parse tree structure from a YAML file and return a Tree object.
    def Tree.load_yaml(yaml_file)
      Tree.new(YAML::load_file(yaml_file))
    end

    # Adds a class (klass) to this parse tree
    def add_klass(klass)
      @klasses.push klass
      @klass_hash[klass.name] = klass
    end

    # Finds a class (klass) in this parse tree
    def find_klass(name)
      @klass_hash[name]
    end

    # Override inspection to be more terse
    def inspect
      sprintf("#<Tree:0x%x, klasses=%d>", self.object_id, @klasses.length)
    end
    
  end

  ############################################################################

  # Stores a C++ class (named klass due to a Ruby 'class' keyword)
  class Klass

    include AccessMixin

    attr_reader :tree, :name, :access, :tuid, :inherit_names, :module, :header

    # Create a new class (klass) for a given parse tree given a hash
    # structure.
    def initialize(tree, struct)
      @tree = tree
      @name = struct['name']
      @module = struct['module']
      @header = struct['header']
      @access = struct['access']
      @tuid = struct['tuid']
      @inherit_names = struct['inherits'].map {|istruct| istruct['name']}
      @functions = struct['functions'].map {|f| Function.new(self, f)}
    end

    ## Returns a list of inherited class objects (Klass)
    #def inherits
    #  @inherit_names.map {|kname| @tree.find_klass(kname)}
    #end


    # Returns the base class(es)
    def base_klasses
      @inherit_names.map {|kname| @tree.find_klass(kname)}.compact
    end
    
    # True if this class only inherits from one base
    def single_base? ; self.base_klasses.length == 1 ; end
    
    # True if this class has a base class
    def inherits? ; !base_klasses.empty? ; end

    # Returns the inheritance tree
    def inherits(stack=[])
      if not self.inherits?
        return stack
      else
        stack.push self
        if self.single_base?
          @tree.find_klass(@inherit_names.first).inherits(stack)
        else
          @inherit_names.map {|kname| @tree.find_klass(kname).inherits(stack)}
        end
      end
      stack[1..-1]
    end

    # Returns functions in this class
    def functions(options={})
      if options[:local] or !self.inherits?
        @functions
      else
        @functions + base_klasses.map {|k| k.functions}.flatten
      end
    end

    def to_s ; @name.to_s ; end

    # Override inspection to be more terse
    def inspect
      sprintf("#<Klass:0x%x, functions=%d>", self.object_id, @functions.length)
    end
    
  end

  ############################################################################

  # Stores a C++ function for a given class (klass)
  class Function

    include AccessMixin

    attr_reader :klass, :name, :tuid, :access, :return

    # Create a function object for a given class (klass) given a hash.
    def initialize(klass, struct)
      @klass = klass
      @name = struct['name']
      @tuid = struct['tuid']
      @access = struct['access']
      @args = struct['args'].map {|a| Arg.new(self, a)}
      @return = Arg.new(self, struct['return'])
    end

    # Returns the parse tree for this function.
    def tree ; @klass.tree ; end

    # Cuts out the "this" object pointer
    def args
      first = @args.first
      if first and first.const? and first.ptr? and first.name == 'this'
        @args[1..-1]
      else
        @args
      end
    end
    
    def fqn
      "#{@klass}::#{@name}"
    end
    
    def arg_str
      self.args.map {|a| a.to_s}.join(', ')
    end

    def to_s
      "#{self.return} #{fqn}(#{arg_str})"
    end

  end
  
  ############################################################################

#   # Stores a C++ return value for a given function
#   class Return
#
#     attr_reader :function, :type
#
#     # Creates a return object for a given function.
#     def initialize(function, struct)
#       @function = function
#       @type = struct['type']
#     end
#
#     def to_s ; @type.to_s ; end
#
#   end

  ############################################################################

  # Stores a C++ argument (or return value)
  class Arg

    attr_reader :function, :tuid, :name, :type

    # Creates a new C++ argument for a function or return value.
    def initialize(function, struct)
      @function = function
      @tuid = struct['tuid']
      @name = struct['name']
      @type = struct['type']
      @ptr = struct['ptr']
      @ref = struct['ref']
      @const = struct['const']
    end

    # Is this argument a pointer? true/false
    def ptr? ; !!@ptr ; end

    # Is this argument a reference? true/false
    def ref? ; !!@ref ; end

    # Is this argument constant? true/false
    def const? ; !!@const ; end

    def to_s
      ((const?) ? 'const ' : '') + @type.to_s + ' ' + ((ref?) ? '&' : '') + ((ptr?) ? '*' : '') + @name.to_s
    end

  end
  
end
