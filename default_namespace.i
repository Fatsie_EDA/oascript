/* -*- mode: c++ -*- */
/*
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%{

#include <stack>

#if defined(OA_WINDOWS)
#define OASCRIPT_DLL_API __declspec(dllexport)
#else 
#define OASCRIPT_DLL_API 
#endif

class OASCRIPT_DLL_API defaultNameSpaceManager
{
public:
        defaultNameSpaceManager() : mStack()
        {
        }

        ~defaultNameSpaceManager()
        {
        }

        oaNameSpace* top()
        {
                if (mStack.empty()) {
                        return &mDefault;
                } else {
                        return mStack.top();
                }
        }

        void push(oaNameSpace* ns)
        {
                mStack.push(ns);
        }

        void pop()
        {
                if (!mStack.empty()) {
                        mStack.pop();
                }
        }

        bool isEmpty()
        {
            return mStack.empty();
        }

        void clear()
        {
			std::stack<oaNameSpace*> empty;
			std::swap(mStack, empty);
        }

private:
        oaNativeNS mDefault;
        std::stack<oaNameSpace*> mStack;
};

static defaultNameSpaceManager sDefaultNameSpaceManager;

OASCRIPT_DLL_API oaNameSpace* getDefaultNameSpace()
{
        return sDefaultNameSpaceManager.top();
}

%}

// C# does not need downcasting
#if !defined(SWIGCSHARP)

%define OANAMESPACECLASS(typename, swigtype)
%header %{
    } else if (dynamic_cast<typename*>(ns)) {
        return SWIG_MangledTypeQuery(#swigtype);
%}
%enddef

%header %{
static swig_type_info* oaNameSpace_dcast (void **obj)
{
    oaNameSpace* ns = reinterpret_cast<oaNameSpace*>(*obj);
    if (!ns) {
        return NULL;
%}
%include <macros/oa_namespaces.i>
%header %{
    } else {
        return SWIG_MangledTypeQuery("OpenAccess_4::oaNameSpace");
    }
}
%}

%init %{
    _swigt__p_OpenAccess_4__oaNameSpace.dcast = &oaNameSpace_dcast;
%}

#endif   // !defined(SWIGCSHARP)
