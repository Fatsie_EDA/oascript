/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%ignore OpenAccess_4::oaTransform::oaTransform(); // skip uninitializing constructor
%ignore OpenAccess_4::oaTransform::offset();
%ignore OpenAccess_4::oaTransform::xOffset();
%ignore OpenAccess_4::oaTransform::yOffset();
%ignore OpenAccess_4::oaTransform::orient();

%typemap(in) OpenAccess_4::oaTransform& (OpenAccess_4::oaTransform tmpTransform)
{
    // typemap(in) oaTransform&
    AV *arr;
    SV **arr0, **arr1, **arr2;
    int x, y, o;
    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 2 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y)) &&
        (arr2 = av_fetch(arr, 2, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr2, &o))) {
        tmpTransform.set(x, y, (oaOrientEnum) o);
    } else if ($input != &PL_sv_undef && !SvOK($input)) {
        // allow undef
        tmpTransform.set(0, 0, oacR0);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpTransform;
}    

%typemap(in) OpenAccess_4::oaTransform* (OpenAccess_4::oaTransform tmpTransform)
{
    // typemap(in) oaTransform*
    AV *arr;
    SV **arr0, **arr1, **arr2;
    int x, y, o;
    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 2 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y)) &&
        (arr2 = av_fetch(arr, 2, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr2, &o))) {
        tmpTransform.set(x, y, (oaOrientEnum) o);
    } else if ($input != &PL_sv_undef && !SvOK($input)) {
        // allow undef
        tmpTransform.set(0, 0, oacR0);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpTransform;
}    

%typemap(in) const OpenAccess_4::oaTransform& (OpenAccess_4::oaTransform tmpTransform)
{
    // typemap(in) const oaTransform&
    AV *arr;
    SV **arr0, **arr1, **arr2;
    int x, y, o;
    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 2 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y)) &&
        (arr2 = av_fetch(arr, 2, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr2, &o))) {
        tmpTransform.set(x, y, (oaOrientEnum) o);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpTransform;
}

%typemap(in) const OpenAccess_4::oaTransform* (OpenAccess_4::oaTransform tmpTransform)
{
    // typemap(in) const oaTransform*
    AV *arr;
    SV **arr0, **arr1, **arr2;
    int x, y, o;
    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 2 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y)) &&
        (arr2 = av_fetch(arr, 2, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr2, &o))) {
        tmpTransform.set(x, y, (oaOrientEnum) o);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpTransform;
}

%typemap(in) OpenAccess_4::oaTransform, const OpenAccess_4::oaTransform
{
    // typemap(in) oaTransform, const oaTransform
    AV *arr;
    SV **arr0, **arr1, **arr2;
    int x, y, o;
    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 2 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y)) &&
        (arr2 = av_fetch(arr, 2, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr2, &o))) {
        $1.set(x, y, (oaOrientEnum) o);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
}

%typecheck(0) OpenAccess_4::oaTransform&, OpenAccess_4::oaTransform*
{
    // typecheck(0) oaTransform&, oaTransform*
    AV *arr;
    $1 = ($input != &PL_sv_undef && !SvOK($input))
         || (SvROK($input) &&
             SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
             av_len(arr) == 2 &&
             !SvROK(*(av_fetch((AV*) SvRV($input), 0, 0))));
}

%typecheck(0) const OpenAccess_4::oaTransform&, const oaTransform*, OpenAccess_4::oaTransform, const OpenAccess_4::oaTransform
{
    // typecheck(0) const oaTransform&, const oaTransform*, oaTransform, const oaTransform
    AV *arr;
    $1 = (SvROK($input) &&
          SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
          av_len(arr) == 2 &&
          !SvROK(*(av_fetch((AV*) SvRV($input), 0, 0))));
}

%typemap(argout) OpenAccess_4::oaTransform&
{
    // typemap(argout) oaTransform&
    SV *coords[3];
    coords[0] = sv_2mortal(newSViv((IV) $1->xOffset()));
    coords[1] = sv_2mortal(newSViv((IV) $1->yOffset()));
    coords[2] = sv_2mortal(newSViv((IV) (oaOrientEnum) $1->orient()));
    AV *arr = av_make(3, coords);
    sv_setsv($input, sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaTransform*)->clientdata)));
}

%typemap(argout) OpenAccess_4::oaTransform*
{
    // typemap(argout) oaTransform*
    SV *coords[3];
    coords[0] = sv_2mortal(newSViv((IV) $1->xOffset()));
    coords[1] = sv_2mortal(newSViv((IV) $1->yOffset()));
    coords[2] = sv_2mortal(newSViv((IV) (oaOrientEnum) $1->orient()));
    AV *arr = av_make(3, coords);
    sv_setsv($input, sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaTransform*)->clientdata)));
}

%typemap(argout) const OpenAccess_4::oaTransform&
{
    // typemap(argout) const oaTransform&
}

%typemap(argout) const OpenAccess_4::oaTransform*
{
    // typemap(argout) const oaTransform*
}

%typemap(out) OpenAccess_4::oaTransform&
{
    // typemap(out) oaTransform&
    if (argvi >= items) EXTEND(sp,1);
    SV *coords[3];
    coords[0] = sv_2mortal(newSViv((IV) $1->xOffset()));
    coords[1] = sv_2mortal(newSViv((IV) $1->yOffset()));
    coords[2] = sv_2mortal(newSViv((IV) (oaOrientEnum) $1->orient()));
    AV *arr = av_make(3, coords);
    $result = sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaTransform*)->clientdata));
    argvi++;
}

%typemap(out) OpenAccess_4::oaTransform*
{
    // typemap(out) oaTransform*
    if (argvi >= items) EXTEND(sp,1);
    SV *coords[3];
    coords[0] = sv_2mortal(newSViv((IV) $1->xOffset()));
    coords[1] = sv_2mortal(newSViv((IV) $1->yOffset()));
    coords[2] = sv_2mortal(newSViv((IV) (oaOrientEnum) $1->orient()));
    delete $1; // Note: this typemap is only used in constructors, and since we copy the data to perl SVs we need to free the constructed oaTransform
    AV *arr = av_make(3, coords);
    $result = sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaTransform*)->clientdata));
    argvi++;
}

%typemap(out) OpenAccess_4::oaTransform
{
    // typemap(out) oaTransform
    if (argvi >= items) EXTEND(sp,1);
    SV *coords[3];
    coords[0] = sv_2mortal(newSViv((IV) $1.xOffset()));
    coords[1] = sv_2mortal(newSViv((IV) $1.yOffset()));
    coords[2] = sv_2mortal(newSViv((IV) (oaOrientEnum) $1.orient()));
    AV *arr = av_make(3, coords);
    $result = sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaTransform*)->clientdata));
    argvi++;
}

%apply (OpenAccess_4::oaTransform) { oaTransform }

