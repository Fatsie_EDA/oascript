#!/usr/bin/perl
################################################################################
#  Copyright {c} 2002-2010  Si2, Inc.  DUNS 62-191-1718#
#  Licensed under the Apache License, Version 2.0 {the "License" you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   04/27/11  10.0     bpfeil, Si2     Tutorial 10th Edition 
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction {CAI} projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library {UNIX man pages}
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation {www.swig.org}
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

# This is a simplified EMH lister.
# Changes from C++ version:
#     This does not print object IDs
#     This only deals with the 5-box model for each Domain:
#         Block/Module/Occ, Nets, Terms, Insts, InstTerms
#     The following are cut:
#          Pcell Params, Assignments, MustJoin
#
our ( $expand_OccDesignInstMasters, $dump_block_domain, $dump_mod_domain);
our ( $dump_occ_domain, $skipIfZero, $dontSkipIfZero);
# Global indent value for prettier log printing
our $indent_inc;
our $indent;
our $ns;

# global array  for Occurrence Headers, flag for a null or unusable array
our @oiHeaderArr;
our $oih_null_status = 0;
# ----------------------------------- globals setup ----------------------------
eval {

    $ns = new oa::oaNativeNS();
    $indent = 0;
    $indent_inc = 2;
    $expand_OccDesignInstMasters=0;
    $dump_block_domain = 1;
    $dump_mod_domain   = 1;
    $dump_occ_domain   = 1;
    $skipIfZero      = 1;
    $dontSkipIfZero  = 0;
};

# ----------------------------------- globals setup ----------------------------
eval {

    $ns = new oa::oaNativeNS();
    $indent = 0;
    $indent_inc = 2;
    $expandOccDesignInstMasters=0;
    $dump_block_domain = 1;
    $dump_mod_domain   = 1;
    $dump_occ_domain   = 1;
    $skipIfZero      = 1;
    $dontSkipIfZero  = 0;
};



# -----------------------------------    subs      -----------------------------
sub assertq($) {
    my $statement = shift;
    if ( ! ($statement) ) {
        printf( "\n***ASSERT\[FAIL\]  %s \n",$statement);
    }
}


sub add_indent () {
        $indent += $indent_inc;
}

sub sub_indent() {
    $indent -= $indent_inc;
}

sub do_indent() {
    my $ix;
    for ($ix = 0; $ix < $indent; $ix++) {
        printf(" ");
    }
}

sub log_it ($){
    my $mesg = shift;
    &do_indent();
    printf("%s", $mesg );
}


sub print_obj_type ($) {
    my $obj = shift;
    my $type = $obj->getType();
    my $name = (new oa::oaType($type))->getName();
    printf (" %s", $name );
}


sub new_obj_baseID($) {
    my $obj = shift;
    printf( "\n" );
    &do_indent();
    &print_obj_type($obj);
}

sub bound_status($){
    my $ref = shift;
    if ( $ref->isBound() ) {
        printf(" BOUND ");
    } else {
        printf(" UNBOUND ");
    }
}



sub print_obj_name_scalar($$) {
    my $obj = shift;
    my $prepend = shift;
    # This is used for those classes that don't have a simple getName func.
    # Now try the more complex way

    my $scalarName;
    my $simpleStr= "";

    $obj->getName($ns, $scalarName);
    $simpleStr = (new oa::oaScalarName($scalarName))->get();# $ns simpleStr
    printf ("%s\"%s\"", $prepend, $simpleStr);
}

sub print_obj_name($$) {
    my $obj = shift;
    my $prepend = shift;

    if ( $obj->isOccObject() ) {
        my $pathName;

        if ( $obj->isOccNet() || $obj->isOccInst()) {
            $pathName = $obj->getPathName();
            printf( "%s\"%s\"",$prepend, $pathName );
        } else {
            printf( "%s\"%s\"", $prepend, $obj->getName() );
        }
    } else {
        printf( "%s\"%s\"", $prepend, $obj->getName() );
    }
}


sub print_transform($) {
     my $inst = shift;
     my $tranny = $inst->getTransform();

     my $orient = $tranny->[2];
     my $tranny_orient= (new oa::oaOrient($orient));
     my $orientname = $tranny_orient->getName();
     printf( " %s", $orientname);

     printf("(%d,%d)", $tranny->[0],$tranny->[1] );
 }


sub print_only_LCV($) {
    my $objContainer = shift;
    my $sc_libname  = $objContainer->getLibName();
    my $sc_cellname = $objContainer->getCellName();
    my $sc_viewname = $objContainer->getViewName();
    printf("\"%s", (new oa::oaScalarName($sc_libname))->get());
    printf("|%s",  (new oa::oaScalarName($sc_cellname))->get());
    printf("|%s\"",(new oa::oaScalarName($sc_viewname))->get());
}


sub get_obj_name($) {
    my $obj = shift;
    my $obj_name;
    $obj->getName($ns, $obj_name);
    return $obj_name;
}

sub print_orig_LCV($) {
    my $mod = shift;

    my $scNameOrig= oa::oaScalarName($ns,"");
    my $scNameLib = oa::oaScalarName($ns,"");
    my $scNameCell= oa::oaScalarName($ns,"");
    my $scNameView= oa::oaScalarName($ns,"");

    $mod->getOrig($scNameOrig, $scNameLib, $scNameCell, $scNameView);

    &do_indent();
    printf( "ORIG=%s[%s|%s|%s]", $scNameOrig,$scNameLib,$scNameCell,$scNameView);
}



sub print_header_line {
    my @objects  = @_;
    my $count    = $objects[0];
    my $category = $objects[1];
    my $skipIfZero = 1;
    my $len_arr = length(@objects);
    if ( $len_arr == 3) {
         $skipIfZero = $objects[2];
    }
    if ($count == 0 && $skipIfZero==1  ) {return 0;}
    # force a linefeed
    printf( "\n");
    &do_indent();
    printf("[%d]%s", $count,$category);
    if ( $count != 1 ) {
         printf( "s" );
    }
    return 1;
}


#>>> Can't figure how to do the template oaIter class setup 
#>>> Currently only 2 calls to dumpTermDefs - for oaModule and oaBlock
#>>> 2 subroutines for the time being.
sub dump_ModuleBusTermDefs ($$){
    my $objContainer= shift;
    my $descrip = shift;

    my $busTermDefs = $objContainer->getBusTermDefs();
    my $count = $busTermDefs->getCount();
    if ( print_header_line( $count,$descrip) ) {
         return;
    }

    my $itIter = new oa::oaIter::oaTerm($objContainer->getBusTermDefs());
    while (my $def = $itIter->getNext()) {

         printf( ": BITS %d", $def->getNumBits() );
         printf( " MINIX=%d", $def->getMinIndex());
         printf( " MAXIX=%d", $def->getMaxIndex());
         printf( "  BASENAME=%s", $def->getName());
         printf( "  BITS:");

         my $newitIter = new oa::oaIter::oaModBusTermDef($def->getBusTermBits());
         while( my $bit = $newitIter->getNext()) {
             printf( " %d", $bit->getBitIndex());
         }
    }
}


sub dump_BlockBusTermDefs ($$){
    my $objContainer= shift;
    my $descrip = shift;

    my $busTermDefs = $objContainer->getBusTermDefs();
    my $count = $busTermDefs->getCount();
    if ( print_header_line( $count,$descrip) ) {
         return;
    }

    my $itIter = new oa::oaIter::oaTerm($objContainer->getBusTermDefs());
    while (my $def = $itIter->getNext()) {

         printf( ": BITS %d", $def->getNumBits() );
         printf( " MINIX=%d", $def->getMinIndex());
         printf( " MAXIX=%d", $def->getMaxIndex());
         printf( "  BASENAME=%s", $def->getName());
         printf( "  BITS:");

         my $newitIter = new oa::oaIter::oaModBusTermDef($def->getBusTermBits());
         while( my $bit = $newitIter->getNext()) {
             printf( " %d", $bit->getBitIndex());
         }
    }
}

sub print_SSS ($){
    my $bus = shift;
    printf( "%d",  $bus->getStart() );
    printf(  ":%d", $bus->getStop() );
    printf( ":%d",  $bus->getStep() );
}


sub dump_bits($) {
    my $obj = shift;
    new_obj_baseID( $obj );
    print_obj_name( $obj, "" );
    if ( $obj->isImplicit() ) {
         printf(" IM" );
    } else {
         printf( " EX" );
    }
    printf( "BITS=%d", $obj->getNumBits() );

}


sub print_net_summary($) { 
    my $net = shift;
    dump_bits($net);
    if ( $net->getType() ==  $oa::oacBusNetType ) {
	 print_SSS($net);
    }
    my $itIter;
    if ( $net->isBitNet() ) {
	 # The following is programmed to more closely follow the C++ lab,
	 # in which you have to downcast the Net
	 my $bitNet =  $net;
	 if ( $bitNet->isPreferredEquivalent() ) {
	     printf( " ISPREF" );
	 }
	 my $getEquiv = $bitNet->getEquivalentNets();
	 if ( ! $getEquiv->isEmpty() ) {
	     printf( " EQUIVALENTS:");

	     $itIter = new  oa::oaIter::oaNet($bitNet->getEquivalentNets());
	     while (my $eqNet = $itIter->getNext()) {
		 printf( " %s", $eqNet->getName() );
	     }
	 }
    }
    my $net_terms = $net->getTerms();
    if ( !  $net_terms->isEmpty() ) {
	 printf( "\n" );
	 add_indent();
	 log_it("CONNECTS Terms:");

	 $itIter = new  oa::oaIter::oaNet($net->getTerms());
	 while (my $term = $itIter->getNext()) {
	     print_obj_name($term, " ");
	 }
	 sub_indent();
    }
    my $inst_terms =  $net->getInstTerms();
    if ( ! $inst_terms->isEmpty() ) {
	 printf( "\n" );
	 add_indent();
	 log_it("CONNECTS InstTerms:");

	 $itIter = new  oa::oaIter::oaNet($net->getInstTerms());
	 while (my $instTerm = $itIter->getNext()) {
	     $instTerm->getTerm();
	     if ( $instTerm->isBound() ) {
		 printf( " " );
	     } else {
		 printf( " CANTBIND" );
	     }
	     my $inst = $instTerm->getInst();
	     printf( "\"%s", $inst->getName() );
	     printf( "|%s\"", $instTerm->getTermName() );
	 }
	 sub_indent();
    }
}

sub print_domain_ref($) { 
    my $ref = shift;
    if ( ($ref) && $ref->isValid() ) {
	 printf( "  ->" );
	 print_obj_type( $ref );
	 return 1;
    }
    return 0;
}


sub print_OccNet_summary($) { 
    my $net = shift;
    dump_bits( $net );
    if ( $net->getType() == $oa::oacOccBusNetType ) {
	 print_SSS( $net );
    }
    my $itIter;
    if ( $net->getNumBits() == 1 ) {
	 my $obNet = $net;
	 my $getEquiv = $obNet->getEquivalentNets();
	 if  ( ! $getEquiv->isEmpty() ) {
	     printf( " EQUIVALENTS:" );

	     $itIter = new  oa::oaIter::oaOccNet($obNet->getEquivalentNets());
	     while (my $eqNet = $itIter->getNext()) {
		 printf( " " );
		 print_obj_name($eqNet, " " );
	     }
	 }
    }
    my $getTerms = $net->getTerms();
    if ( ! $getTerms->isEmpty() ) {
	 printf( "\n" );
	 add_indent();
	 log_it("CONNECTS OccTerms:");

	 $itIter = new  oa::oaIter::oaOccNet($net->getTerms());
	 while (my $term = $itIter->getNext()) {
	     print_obj_name($term, " ");
	 }
	 sub_indent();
    }
    my $instTerms = $net->getInstTerms();
    if ( ! $instTerms->isEmpty() ) {
	 printf( "\n" );
	 add_indent();
	 log_it("CONNECTS OccInstTerms:" );

	 $itIter = new  oa::oaIter::oaOccNet($net->getInstTerms());
	 while (my $instTerm = $itIter->getNext()) {
	     my $inst = $instTerm->getInst();
	     printf( "\"%s", $inst->getName() );
	     printf( "|%s\"", $instTerm->getTermName() );
	 }
	 sub_indent();
    }
    # Cross-domain references
    #
    printf( "\n" );
    do_indent();
    if ( print_domain_ref( $net->getNet() ) ) {
	 print_obj_name($net->getNet(), "" );
    }
    if ( print_domain_ref( $net->getModNet() ) )  {
	 print_obj_name( $net->getModNet(), "" );
    }
}




sub print_ModNet_summary($) { 
    my $net = shift;
    dump_bits( $net );
    
    my $type = $net->getType();
    my $itIter;
    if ( $type == $oa::oacModBusNetType ) {
	 print_SSS( $net );
    }
    if ( $net->getNumBits() == 1 ) {
	 my $mbNet = $net;
	 my $equivNets = $mbNet->getEquivalentNets();
	 if ( ! $equivNets->isEmpty() ) {
	     printf( " EQUIVALENTS:" );

	 $itIter = new  oa::oaIter::oaNet($mbNet->getEquivalentNets());
	 while (my $eqNet = $itIter->getNext()) {
		 printf( " %s", $eqNet->getName() );
	     }
	 }
    }
    my $terms = $net->getTerms();
    if ( ! $terms->isEmpty() ) {
	 printf( "\n" );
	 add_indent();
	 log_it( "CONNECTS ModTerms:" );

	 $itIter = new  oa::oaIter::oaNet($net->getTerms());
	 while (my $term = $itIter->getNext()) {
	     print_obj_name( $term, " " );
	 }
	 sub_indent();
    }
    my $instTerms = $net->getInstTerms();
    if ( ! $instTerms->isEmpty() ) {
	 printf( "\n" );
	 add_indent();
	 log_it(  "CONNECTS ModInstTerms:" );

	 $itIter = new  oa::oaIter::oaNet($net->getInstTerms());
	 while (my $instTerm = $itIter->getNext()) {
	     my $Inst = $instTerm->getInst();
	     printf( "\"%s", $Inst->getName() );
	     printf( "|%s\"",  $instTerm->getTermName() );
	 }
	 sub_indent();
    }
}



sub dump_NetMembers($) { 
    my $netCollection = shift;

    my $itIter = new  oa::oaIter::oaNet($netCollection);
    while (my $net = $itIter->getNext()) {

	 print_net_summary( $net );
	 my $type = $net->getType();
	 if ($type  == $oa::oacScalarNetType) {

	 } elsif ($type == $oa::oacBusNetBitType) {

	 } elsif ($type == $oa::oacBusNetType ) {
	     add_indent();
	     &dump_NetMembers( $net->getSingleBitMembers() );
	     sub_indent();
	 } elsif ($type == $oa::oacBundleNetType ) {
	     add_indent();
	     &dump_NetMembers( $net->getMembers() );
	     sub_indent();
	 } else {
	     my $msg = sprintf("Unrecognized net type: %s\n", $type->getName() );
	     log_it( $msg );
	 }
    }
}

sub dump_ModNetMembers($)  { 
    my $netCollection = shift;

    my $itIter = new  oa::oaIter::oaModNet($netCollection);
    while (my $net = $itIter->getNext()) {

	 print_ModNet_summary( $net );
	 my $type = $net->getType();

	 my $x;
	 if ($type  == $oa::oacModScalarNetType) {
	     $x=0;
	 } elsif ($type == $oa::oacModBusNetBitType) {
	     $x=0;
	 } elsif ($type == $oa::oacModBusNetType ) {
	     add_indent();
	     &dump_ModNetMembers( $net->getSingleBitMembers() );
	     sub_indent();
	 } elsif ($type == $oa::oacModBundleNetType ) {
	     add_indent();
	     &dump_ModNetMembers( $net->getMembers() );
	     sub_indent();
	 } else {
	     my $type_name = (new oa::oaType($type))->getName();
	     my $msg = sprintf("Unrecognized net type: %s\n", $type_name);
	     log_it( $msg );
	 }
    }
}

   
sub dump_OccNetMembers($) { 
    my $netCollection = shift;

    my $itIter = new  oa::oaIter::oaOccNet($netCollection);
    while (my $net = $itIter->getNext()) {
	 print_OccNet_summary( $net );
	 my $type = $net->getType();

	 my $i;
	 if ($type  == $oa::oacOccScalarNetType) {
	     $i = 0;
	 } elsif ($type == $oa::oacOccBusNetBitType) {
	     $i = 0;
	 } elsif ($type == $oa::oacOccBusNetType ) {
	     add_indent();
	     &dump_OccNetMembers( $net->getSingleBitMembers() );
	     sub_indent();
	 } elsif ($type == $oa::oacOccBundleNetType ) {
	     add_indent();
	     &dump_OccNetMembers( $net->getMembers() );
	     sub_indent();
	 } else {
	     my $msg = sprintf("Unrecognized net type: %s\n", $type->getName());
	     log_it( $msg );
	 }
    }
}    


sub print_Master_info($$) { 
    my $ref = shift; 
    my $nameType = shift;
    if ( $ref->isBound ) {
	 my $master = $ref->getMaster();
	 printf( " BOUND=>" );
	 print_obj_type( $master );
	 print_only_LCV( $master );
    } else {
	 printf(" UNBOUND=> %s", $nameType );
    }
}

# Specialization: Occ[Mod]Module masters have just a name, no LCV.
# Plus the hier traversal name for oaModModuleInstHeader
# is getMasterModule, not getMaster!
#
sub print_OccMaster_info($) { 
    my $ref = shift;
    if ( $ref->isBound() )  {
	 my $modMaster = $ref->getMaster();
	 printf( " BOUND=>" );
	 print_obj_type( $modMaster );
	 print_obj_name( $modMaster, "" );
    } else {
	 printf( " UNBOUND=>MODULE" );
    }
}



sub print_ModMaster_info($) { 
    my $ref = shift;
    if ( $ref->isBound() ) {
	 my $modMaster  = $ref->getMasterModule();
	 printf( " BOUND=>" );
	 print_obj_type( $modMaster );
	 print_obj_name( $modMaster, "" );
    } else {
	 printf( " UNBOUND=>MODULE" );
    }
}

sub dump_MasterOcc($) { 
    my $inst = shift;

    our $expand_OccDesignInstMasters;

    my $wasBound = $inst->isBound();
    printf( "\n" );
    add_indent();
    if ( $wasBound ) {
	 log_it( "MASTEROCC=BOUND2=" );
    } else {
	 log_it( "MASTEROCC=UNBOUND" );
    }

    if ( $inst->getType() ==  $oa::oacOccVectorInstType)  {
	my $x=0;
    } elsif ( $inst->getType() == $oa::oacOccVectorInstBitType ) {
	 printf( "NOMASTER]" );
    } else { 
	 my $masterOcc = $inst->getMasterOccurrence();
	 if ( $masterOcc->isValid() ) {
	     my $masterType = $masterOcc->getType();
	     if (!  $masterType == $oa::oacOccurrenceType  ) {
		 my $msg = sprintf("masterType->typeEnum == oa::oacOccurrenceType");
		 printf( "\n***ASSERT\[FAIL\]  %s at line %d\n", $msg, __LINE__);
	     }
		 #
		 # Always expand OccModuleInsts, since they are part of this Design.
		 # But only expand beyond the frontier into other Designs if flag is set.
		 #
	     if ( $inst->isOccModuleInst() | $expand_OccDesignInstMasters  ) {
		 if ( !$wasBound ) {
		     printf( " BINDING2=" );
		 }
		 dump_Occ( $masterOcc );
	     } 
	 } else {
	     printf( "CANTBIND" );
	 }
    }
    sub_indent();

}

sub maybe_add_new_OccInstHeader($) { 
    my $inst = shift; 
    our @oiHeaderArr;
 
    my $ih = $inst->getHeader();
    my $found=0;
    foreach $oih (@oiHeaderArr) {
	if ( $oih->getCellName() eq $ih->getCellName()) {
	    $found=1;
	    last;
	}
    }
    if ( ! $found ) {
	push (@oiHeaderArr, $ih); 
    }    
    return;
}


sub dump_OccDesignInsts($) { 
    my $occ = shift;

    our @oiHeaderArr;
    our $oih_null_status;

    my $lenArr = @oiHeaderArr;

    my @odiList;
    my $nInsts = 0;

    # Organize only the OccDesignInsts into a list
    #
    my $inst;
    my $itIter = new  oa::oaIter::oaOccInst($occ->getInsts());
    while ( $inst = $itIter->getNext()) {
	 if ( $inst->isOccDesignInst() ) {
	     push( @odiList, $inst );
	 }
    }

    $nInsts = scalar(@odiList);
    if (! print_header_line( $nInsts, "OCCDESIGNINST") ) {
	 return 0;
    }


    foreach $inst (@odiList) {
	 new_obj_baseID( $inst );
	 print_obj_name( $inst, "" );

	 # If we were NOT called [indirectly] from Dump_OccDesignInstsFromHeader

	 if ( ! $oih_null_status )  { 
             maybe_add_new_OccInstHeader( $inst );
	 }
	 # Cross-domain references
	 #
	 printf( "\n" );
	 do_indent();
	 if ( print_domain_ref( $inst->getInst() ) ) {
	     print_obj_name( $inst->getInst(), "" );
	 }
	 if ( print_domain_ref( $inst->getModInst() ) ) {
	     print_obj_name( $inst->getModInst(), "" );
	 }

	 # No XFORMS in Occ Domain!!
    }
    return;
}


sub dump_OccDesignInstsFromHeader($) { 
    my $ih = shift;
    our $oih_null_status;

    my $insts = $ih->getInsts();
    if ( ! print_header_line( $insts->getCount(), "OCCDESIGNINST" ) ) {
	 return;
    }

    my $itIter = new  oa::oaIter::oaOccDesignInst($ih->getInsts());
    while (my $inst = $itIter->getNext()) {
	 new_obj_baseID( $inst );
	 print_obj_name( $inst, "" );

	 # Cross-domain references
	 #
	 printf( "\n" );
	 do_indent();
	 if ( print_domain_ref( $inst->getInst() ) ) {
	     print_obj_name( $inst->getInst(), "" );
	 }
	 if ( print_domain_ref( $inst->getModInst() ) ) {
	     print_obj_name( $inst->getModInst(), "" );
	 }

	 # No XFORMS in Occ Domain!!

	 add_indent();
	 dump_InstTerms( $inst, "OccModInstTerm" );
	 sub_indent();

          # flag that oiHeaderArr array NOT for use by maybe_add_new_OccInstHeader
	 $oih_null_status = 1;
         dump_MasterOcc( $inst);
	 $oih_null_status = 0;
    }
  

}


sub dump_OccInstHeaders () { 
    our @oiHeaderArr;

    # Need this cheesy code because there's no getInstHeaders in oaOccurrence
    # and OccInstHeaders are scoped to the Design.
    my $lenArr = scalar(@oiHeaderArr);
    if ( ! print_header_line( $lenArr, "OCCINSTHEADER") )  {
	 return;
    }
    my $ih;

    for (my $ix =0; $ix < $lenArr; $ix++) {
	 my $ih = $oiHeaderArr[$ix];
	 # Pcell subessing is taken out of this lab version.
	 #
	if ( $ih->isSubHeader() || $ih->isSuperHeader() ) {
	    next;
	}
	
	new_obj_baseID( $ih );
	print_only_LCV( $ih );
	print_Master_info( $ih, "DESIGN" );
	printf( "\n" );
	do_indent();
	print_domain_ref( $ih->getInstHeader() );
	print_domain_ref( $ih->getModInstHeader() );
	add_indent();
	dump_OccDesignInstsFromHeader( $ih );
	sub_indent();
    }
    
    
}

sub dump_OccModuleInsts($) { 
    my $ih = shift;
    our @oiHeaderArr;

    my $insts = $ih->getInsts();
    if ( ! print_header_line( $insts->getCount(), "OCCMODULEINST") ) {
	 return;
    }

    my $itIter = new  oa::oaIter::oaOccModuleInst($ih->getInsts());
    while (my $inst = $itIter->getNext()) {
	 new_obj_baseID( $inst );
	 print_obj_name( $inst, "" );
	 add_indent();
	 dump_InstTerms( $inst, "OccModInstTerm" );
	 sub_indent();
	 dump_MasterOcc( $inst );
    }

    return;
}

sub dump_OccModuleInstHeaders($) { 
    my $occ = shift;
    our @oiHeaderArr;


    my $iHeadColl = $occ->getModuleInstHeaders();
    if ( ! print_header_line( $iHeadColl->getCount(), "OCCMODULEINSTHEADER") ) {
	 return;
    }

    my $itIter = new  oa::oaIter::oaOccModuleInstHeader($occ->getModuleInstHeaders());
    while (my $ih = $itIter->getNext()) {
	 new_obj_baseID( $ih );
	 print_obj_name( $ih, "" );
	 print_OccMaster_info( $ih );

	 # Cross-domain reference
	 #
	 my $mmIH = $ih->getModModuleInstHeader();
	 printf( "\n");
	 do_indent();
	 print_domain_ref(      $mmIH);
	 print_obj_name_scalar( $mmIH, "");

	 add_indent();
	 dump_OccModuleInsts( $ih  );

	 sub_indent();
    }
    return;
}

sub dump_OccNets($) { 
    my $occ = shift;
    my $netColl = $occ->getNets( $oa::oacNetIterAll );
    if ( ! print_header_line( $netColl->getCount(), "OCCNET") ) {
	 return;
    }
    dump_OccNetMembers( $netColl );
}

sub print_OccTerm_summary($) {
    my $term = shift;

    dump_bits( $term );
    if ( $term->getType() == $oa::oacOccBusTermType ) {
	 print_SSS( $term );
    }

    my $type = $term->getTermType();
    my $name = (new oa::oaTermType($type))->getName();
    printf(" %-6s", $name );

    my $conNet = $term->getNet();
    if ( $conNet->isValid() ) {
	 print_obj_name( $conNet, " CONN2" );
    }

    # Cross-domain references
    #
    printf ("\n");
    do_indent();

    if ( print_domain_ref( $term->getTerm() ) ) {
	 print_obj_name( $term->getTerm(), "" );
    }
    if ( print_domain_ref( $term->getModTerm() ) ) {
	 print_obj_name( $term->getModTerm(), "" );
    }
}

  
sub dump_OccTermMembers($) { 
    my $termColl = shift;

    my $itIter = new  oa::oaIter::oaOccTerm($termColl);
    while (my $term = $itIter->getNext()) {
	 my ($type, $occ, $start, $stop, $step);
	 my ($def, $baseName, $ix, $termbit, $mtype);

	 print_OccTerm_summary( $term );
	 $type = $term->getType();
	 if ( $type == $oa::oacOccScalarTermType ) {

	 } elsif ( $type == $oa::oacOccBusTermBitType ) {

	 } elsif ( $type == $oa::oacOccBusTermType ) {
	     $occ = $term->getOccurrence();
	     $start = $term->getStart();
	     $stop  = $term->getStop();
	     $step  = $term->getStep();

	     $def = $term->getDef();
	     $baseName =  $def->getName();
	     add_indent();
	     for ( $ix = $start; $ix <= $stop; $ix = $ix + $step ) {
		 my $termBit = oa::oaOccBusTermBit_find( $occ, $baseName, $ix);
		 print_OccTerm_summary( $termBit );
	     }
	     sub_indent();

	 } elsif ( $type ==  $oa::oacOccBundleTermType )  {
	     add_indent();
	     my $newitIter = new  oa::oaIter::oaOccBundleTerm($term->getMembers());
	     while (my $memTerm = $newitIter->getNext()) {
		 print_OccTerm_summary( $memTerm );
	     }
	     sub_indent();
	 } else {
	     my $msg = sprintf("Unrecognized OccTerm type: %s\n",  $type->getName());
	     log_it( $msg );
	 }
    }
}

sub dump_OccTerms($) { 
    my $occ = shift;
    my $terms = $occ->getTerms();
    if ( ! print_header_line( $terms->getCount(), "OCCTERM" ) ) {
	 return;
    }
    dump_OccTermMembers( $occ->getTerms() );
}

sub dump_Occ($) { 
    my $occ = shift;

    print_obj_type( $occ );

    # Cross-domain references
    #
    printf( "\n");
    do_indent();
    my $block = $occ->getBlock();
    if ( print_domain_ref( $block ) ) {
	 print_only_LCV( $block->getDesign() );
    }
    my $mod = $occ->getModule();
    if ( print_domain_ref( $mod ) ) {
	 print_obj_name( $mod, "" );
    }

    add_indent();
    dump_OccModuleInstHeaders( $occ );
    dump_OccDesignInsts( $occ );
    dump_OccNets( $occ );
    dump_OccTerms( $occ );
    sub_indent();

    return;
}

sub dump_Nets($) { 
    my $block = shift;

    my $netColl = $block->getNets( $oa::oacNetIterAll );
    if ( ! print_header_line(  $netColl->getCount(), "NET" ) ) {
      return:
    }
    dump_NetMembers( $netColl );
}

sub dump_ModNets($) { 
    my $mod  = shift;
    my $netColl =  $mod->getNets( $oa::oacNetIterAll );

    if ( ! print_header_line( $netColl->getCount(), "MODNET" ) ) {
	 return;
    }
    dump_ModNetMembers( $netColl );
}

sub print_Term_summary($) { 
    my $term = shift;
    dump_bits( $term );
    my $type = $term->getType();
    if ( $type == $oa::oacBusTermType ) {
	 print_SSS( $term );
    }
    my $termType = $term->getTermType();
    my $name = (new oa::oaTermType($termType))->getName();
    printf(" %-6s", $name );
    my $conNet = $term->getNet();
    if ( $conNet->isValid() ) {
	 print_obj_name( $conNet, " CONN2" );
    } 
    if ( $term->isBitTerm() ) {
	 my $pinCM = $term->getPinConnectMethod();
	 $name = (new oa::oaPinConnectMethod($pinCM))->getName();
	 printf( " PINCON=%s",$name);
    }

}

sub print_ModTerm_summary($) { 
    my $term = shift;
    dump_bits( $term );
    my $type = $term->getType();
    if ( $type  == $oa::oacModBusTermType ) {
	 print_SSS( $term );
    }
    my $termType = $term->getTermType();
    my $name =(new oa::oaTermType($termType))->getName();
    printf(" %-6s", $name );
    my $conNet = $term->getNet();
    if ( $conNet->isValid() ) {
	 print_obj_name( $conNet, " CONN2" );
    } 
}
sub dump_ModTermMembers($) { 
    my $termColl = shift;

    my $itIter = new  oa::oaIter::oaModTerm($termColl);
    while (my $term = $itIter->getNext()) {
	 my ($type, $mod, $occ, $start, $stop, $step);
	 my ($def, $baseName, $ix, $termbit, $mtype, $later);

	 print_ModTerm_summary( $term );
	 $type = $term->getType();

	 if ( $type == $oa::oacModScalarTermType ) {
	     $later = 1;
	 } elsif ( $type == $oa::oacModBusTermBitType ) {
	     $later = 1;
	 } elsif ( $type ==  $oa::oacModBusTermType ) {
	     $mod   = $term->getModule();
	     $start = $term->getStart();
	     $stop  = $term->getStop();
	     $step  = $term->getStep();

	     $def = $term->getDef();
	     $baseName =  $def->getName();
	     add_indent();
	     for ( $ix = $start; $ix <= $stop; $ix = $ix + $step ) {
		 my $termBit = oa::oaModBusTermBit_find( $mod, $baseName, $ix);
		 print_ModTerm_summary( $termBit );
	     }
	     sub_indent();
	 } elsif ( $type == $oa::oacModBundleTermType ) {
	     add_indent();
	     $itIter = new  oa::oaIter::oaModBundleTerm($term->getMembers());
	     while (my $memTerm = $itIter->getNext()) {
		 print_ModTerm_summary( $memTerm );
	     }
	     sub_indent();

	 } else {
	     my $msg = sprintf("Unrecognized OccTerm type: %s\n",  $type->getName());
	     log_it( $msg );
	 }
    }
}

sub dump_TermMembers($) { 
    my $termColl = shift;

    my $itIter = new  oa::oaIter::oaTerm($termColl);
    while (my $term = $itIter->getNext()) {
	 my ( $type, $start, $stop, $step );
	 my ( $block, $baseName, $def, $ix );
	 print_Term_summary( $term );
	 $type= $term->getType();

	 my $x;
	 if ( $type == $oa::oacScalarTermType ) {
            $x=0;
	 } elsif ( $type == $oa::oacBusTermBitType ) {
	     $x=0;
	 } elsif ( $type == $oa::oacBusTermType ) {
	     $block = $term->getBlock();
	     $start = $term->getStart();
	     $stop  = $term->getStop();
	     $step  = $term->getStep();

	     $def = $term->getDef();
	     $baseName =  $def->getName();
	     add_indent();
	     for ( $ix = $start; $ix <= $stop; $ix = $ix + $step ) {
		 my $termBit;								       
		 $termBit = oa::oaBusTermBit_find( $block, $baseName, $ix);
		 print_ModTerm_summary( $termBit );
	     }
	     sub_indent();
	 } elsif ( $type == $oa::oacBundleTermType ) {
	     add_indent();
	     
	     $itIter = new  oa::oaIter::oaBundleTerm($term->getMembers());
	     while (my $memTerm = $itIter->getNext()) {
		 print_Term_summary( $memTerm );
	     }
	     sub_indent();
	 } else {
	     my $msg = sprintf("Unrecognized Term type:%s\n", $type->getName());
	     log_it( $msg ); 
	 }
    }
}


sub dump_Terms($) { 
    my $block = shift;
    my $terms = $block->getTerms();
    if ( ! print_header_line( $terms->getCount(), "TERM" ) ) {
	return;
    }
    dump_TermMembers( $block->getTerms() );
}

sub dump_ModTerms($) { 
    my $mod = shift;
    my $terms = $mod->getTerms();
    if ( ! print_header_line( $terms->getCount(), "MODTERM" ) ) {
	return;
    }
    dump_ModTermMembers( $mod->getTerms() );
}

sub print_InstTerm($) { 
    my $instTerm = shift;
    if (! $instTerm->isValid() ) {
	printf("*** print_InstTerm ! instTerm->isValid() \n");
	exit(10);
    }
    new_obj_baseID( $instTerm );

    my $type = $instTerm->getType();
    my $name;
    $instTerm->getTermName($ns, $name);
    printf( "\" %s\"", $name );
    if ( $instTerm->isImplicit() ) {
        printf( " IM" );
    } else {
        printf( " EX" );
    }
    printf( " BITS=%d", $instTerm->getNumBits() );
    if ( $instTerm->isBound() ) {
        printf( " BOUND" );
    } else {
        printf( " UNBOUND" );
    }
    if  ( $instTerm->usesTermPosition() ) {
        printf( " POS" );
    }
    my $conNet = $instTerm->getNet();
    if ( $conNet->isValid() )  {
        print_obj_name( $conNet, " CONN2" );
    }
}

sub dump_InstTerms ($$)  { 
    my $objContainer = shift;
    my $inst_term_code = shift;

    my $instTerms = $objContainer->getInstTerms();
    if ( ! print_header_line( $instTerms->getCount(), "INSTTERM") ) {
        return;
    }

    my $itIter;
    if ($inst_term_code eq "ModModuleInstTerm" || $inst_term_code eq  "ModInstTerm"  ) {
 	$itIter = new  oa::oaIter::oaModInstTerm($objContainer->getInstTerms());
    } elsif ( $inst_term_code eq  "OccModInstTerm" ) {
	$itIter = new  oa::oaIter::oaOccInstTerm($objContainer->getInstTerms());
    } else {
	$itIter = new  oa::oaIter::oaInstTerm($objContainer->getInstTerms());
    }
    while (my $instTerm = $itIter->getNext()) {
        print_InstTerm( $instTerm );
    }
}

sub dump_one_Inst($) { 
    my $inst = shift;
    dump_InstTerms( $inst, "InstTerm" );
}


sub dump_props($) { 
    my $obj = shift;
    my $props = $obj->getProps();
    if ( ! print_header_line( $props->getCount(), "PROP" ) ) {
        return;
    }
    add_indent();

    my $itIter = new  oa::oaIter::oaProp($props->getMembers());
    while (my $prop = $itIter->getNext()) {
	my ( $type);
        printf( "\n" );
	$type = $prop->getType();
        log_it( $type->getName() );
        printf( " %s=%d", $prop->getName, $prop->getValue() );
    }
    sub_indent();
}


sub dump_Insts($) { 
    my $objContainer = shift;

    my $insts = $objContainer->getInsts();
    if  ( ! print_header_line( $insts->getCount(), "INST" ) ) {
	return;
    }

    my $itIter = new  oa::oaIter::oaInst($objContainer->getInsts());
    while (my $inst = $itIter->getNext()) {
        new_obj_baseID( $inst );
        print_obj_name( $inst, "" );
        print_transform( $inst );
	add_indent();
        dump_one_Inst( $inst );
	sub_indent();
    }
}

sub dump_ModDesignInsts($) { 
    my $objContainer = shift;
    #
    # If objContainer Type is Module the Iter has both kinds of ModInsts; so to get an
    # accurate count of only the ModDesignInsts, do an initial cycle through the iterator.
    #
    my $count = 0;
    my $inst;
    my $itIter = new  oa::oaIter::oaModDesignInst($objContainer->getInsts());
    while ($inst = $itIter->getNext()) {
        if ( $inst->isModDesignInst() ) {
            $count++;
        }
    }

    if ( ! print_header_line( $count, "MODDESIGNINST" ) ) {
	return;
    }
    #
    # Go through the iterater again, this time printing out ModInst information
    #
    $itIter = new  oa::oaIter::oaModInst($objContainer->getInsts());
    while ($inst = $itIter->getNext()) {
	my ($msg, $instDb, $instDbType, $objContainerDb);
        $instDb        = $inst->getDatabase();
        $instDbType    = $instDb->getType();
	$objContainerDb= $objContainer->getDatabase();
     
	if ( ! $instDbType == $oa::oacDesignType ) {
	        $msg = sprintf(" instDbType == oa::oacDesignType ");
		printf( "\n***ASSERT\[FAIL\]  %s at line=%d\n", $msg, __LINE__ );
	}
	if ( ! $instDb eq $objContainerDb ) {
		$msg = sprintf(" $instDb eq $objContainerDb ");
		printf( "\n***ASSERT\[FAIL\]  %s at line=%d\n", $msg, __LINE__ );
	}
        #
        # If the objContainer Type is Module the Iter has both kinds of ModInsts.
        # Skip the ModModuleInsts since they are dumped from the ModModuleInstHeader
        #
        if  ( $inst->isModModuleInst() ) {
	    next;
	}
        new_obj_baseID( $inst );
        if ( $objContainer->getType() != $oa::oacModuleType )   {
            print_obj_name( $inst->getModule(), "" );
            printf( "|" ); 
	}
	print_obj_name( $inst, "" );
        #
        # No XFORMS in Mod Domain.
        # Don't need to repeat the guts in the ModInstHeader.
        # But DO need to in each Module, since the InstTerms could be connected
        # to different Nets, have different annotations, etc.
        #
	if ( $objContainer->getType() == $oa::oacModuleType ) {
	    add_indent();
	    dump_InstTerms( $inst, "ModInstTerm" );
            sub_indent();
	} else {
	    bound_status( $inst );
            if ( $inst->isBound() ) {
                my $mod = $inst->getMasterModule();
		if ( $mod->isValid() ) {
		    print_obj_name( $mod, "" );
		    my $desMod = $mod->getDesign();
		    if ( $desMod->getTopModule()->getName() eq $mod->getName() ) {
			printf( "\[TOP\]" ); 
		    }
                } else {
                    printf( "***NULL MASTER MODULE!" );
                }
	    }
	}
    }
}

sub dump_ModModuleInsts($) { 
    my $objContainer = shift;

    add_indent();
    my $insts = $objContainer->getInsts();
    if ( ! print_header_line( $insts->getCount(), "MODMODULEINST" ) ) {
        return;
    }
    my $itIter = new  oa::oaIter::oaModModuleInst($objContainer->getInsts());
    while (my $inst = $itIter->getNext()) {
        new_obj_baseID( $inst );
        print_obj_name( $inst, "" );
        # No XFORMS in Mod Domain!!
        add_indent();
	dump_InstTerms( $inst,"ModModuleInstTerm" );
        sub_indent();
    }
    sub_indent();
}

sub dump_NetDefs($$) { 
    my $objContainer = shift;
    my $descrip = shift;
    my $defs = $objContainer->getBusNetDefs();
    print_header_line( $defs->getCount, $descrip );

    my $itIter = new  oa::oaIter::oaNet($objContainer->getBusNetDefs());
    while (my $def = $itIter->getNext()) {
        new_obj_baseID( $def );
        printf( " BITS=%d",  $def->getNumBits() );
        printf( " MINIX=%d", $def->getMinIndex() );
        printf( " MAXIX=%d", $def->getMaxIndex() );
        printf( "  BASENAME=%s", $def->getName() );
        printf( "  BITS:" );

	$itIter = new  oa::oaIter::oaBusNetDef($def->getBusNetBits());
	while (my $bit = $itIter->getNext()) {
            printf(" %d", $bit->getBitIndex());
        }
    }
}

sub dump_ModInstHeaders($) {
    my $design = shift;
    our $dontSkipIfZero;

    my $iHeadColl = $design->getModInstHeaders();
    if ( !print_header_line( $iHeadColl->getCount(), 
			   "MODINSTHEADER" ,
			   $dontSkipIfZero ) ) {
	return;
    }

    my $itIter = new oa::oaIter::oaModInstHeader($design->getModInstHeaders());
    while (my $ih = $itIter->getNext()) {
        # Pcell subessing is taken out of this tcl version.
        #
        if ( $ih->isSubHeader() || $ih->isSuperHeader() ) {
	  next:
        }
  
        new_obj_baseID( $ih );
        print_only_LCV( $ih );
        print_Master_info( $ih, "DESIGN" );
        add_indent();
        dump_ModDesignInsts( $ih );
        sub_indent();
    }
}


sub dump_ModModuleInstHeaders($) { 
    my $mod = shift;

    my $iHeadColl = $mod->getModuleInstHeaders();
    if ( ! print_header_line( $iHeadColl->getCount(), "MODMODULEINSTHEADER") ) {
	return;
    }

    my $itIter = new oa::oaIter::oaModModuleInstHeader($mod->getModuleInstHeaders());
    while (my $ih = $itIter->getNext()) {
        #
        # ModModules can't have parameters, so no Super/SubHeaders
        #
        new_obj_baseID( $ih );
        print_obj_name_scalar( $ih,"" );
        print_ModMaster_info(  $ih );
        dump_ModModuleInsts(  $ih );
    }
}


sub dump_InstHeaders($) { 
    my $block = shift;
    my $iHeadColl =  $block->getInstHeaders();
    if ( ! print_header_line($iHeadColl->getCount(), "INSTHEADER" ) ) {
	return;
    }


    my $itIter = new oa::oaIter::oaInstHeader($block->getInstHeaders());
    while (my $ih = $itIter->getNext()) {
        # Pcell subessing is taken out of this tcl version.
        #
        if ( $ih->isSubHeader() || $ih->isSuperHeader() ) {
            next;
        }
  
        new_obj_baseID( $ih );
        print_Master_info( $ih, "DESIGN");
        add_indent();
        dump_Insts( $ih );
        sub_indent();
    }
}

sub dump_modules($) { 
    my $design = shift;
    my $modTop = $design->getTopModule();

    our $dontSkipIfZero;

    # First dump the ModInstHeaders, which are scoped to the Design,
    # common across all Modules that use them.
    #
    dump_ModInstHeaders( $design );

    # Next take each Module in turn, start with its unique ModModuleInstHeaders.
    #
    my $iModColl = $design->getModules();
    printf( "\n" );
    print_header_line( $iModColl->getCount(), "MODULE", $dontSkipIfZero );

    my $itIter = new oa::oaIter::oaModule($design->getModules());
    while (my $mod = $itIter->getNext()) {
        new_obj_baseID( $mod );
        print_obj_name( $mod, "" );
        if ( $mod->getName() eq $modTop->getName() ) {
            printf( " TOP" ); 
        } elsif ( $mod->isEmbedded() ) {
            printf( " EMBEDDED" ); 
        }
        if ( $mod->isDerived() ) {
            print_orig_LCV( $mod );
        }
        add_indent();
        dump_ModModuleInstHeaders( $mod );
        dump_ModDesignInsts(       $mod );
        dump_NetDefs(  $mod, "MODBUSNETDEF" );
	dump_ModuleBusTermDefs( $mod, "MODBUSTERMDEF" );
        dump_ModNets(  $mod );
        dump_ModTerms( $mod );
        sub_indent();
    }
}


sub separator($) { 
    my $mesg = shift;
    printf( "\n");
    my $msg = sprintf("==================== %s ====================\n",$mesg); 
    log_it($msg);
}

sub dump_block($) { 
    my $block = shift;
    if ( $block->isValid() ) {
        print_header_line( 1,  "BLOCK" );
        new_obj_baseID( $block );
        add_indent();
        dump_InstHeaders( $block );
        dump_NetDefs( $block, "BUSNETDEF" );
        dump_BlockBusTermDefs( $block, "BUSTERMDEF" );
        dump_Nets( $block );
	dump_Terms( $block);
        sub_indent();
    } else {
        printf( "\n" );
        log_it( "NO BLOCK" );
    }
}


sub dump_design($) { 
    my $design = shift;

    our @oiHeaderArr;

    printf( "\n" );
    log_it( "______________________________ " );

    print_obj_type( $design );
    print_only_LCV( $design );
    add_indent();
    if ( $design->isSubMaster() ) {
        printf( "\n" );
        log_it( "SUBMASTER" ); 
    }
    sub_indent();
    dump_props( $design );
    if ( $dump_block_domain ) {
        separator( "BLOCK DOMAIN" );
        dump_block( $design->getTopBlock );
    }
    if ( $dump_mod_domain ) {
        separator( "MODULE DOMAIN" );
        dump_modules( $design );
    }
    if ( $dump_occ_domain ) {
        separator( "OCCURRENCE DOMAIN" );
        printf( "\n" );
        do_indent();
        my $occTop = $design->getTopOccurrence();
        if ( $occTop->isValid() ) {
            printf(  "[TOP]" ); 
	    @oiHeaderArr=();
            dump_Occ( $occTop );
            #
            # Dump out any OccInstHeaders we saved while iterating over
            # OccDesignInsts in the Occurrence.
            #
            # end of road for array
            dump_OccInstHeaders();
	    
        } else {
            printf( "NO OCCURRENCE" ) 
        }
    }
    printf( "\n " );

    return;
 }


sub dump_open_designs() {

    printf( "\n" );
    log_it( "Currently open Designs:" );


    my $itIter = new oa::oaIter::oaDesign(oa::oaDesign::getOpenDesigns());
    while (my $design = $itIter->getNext()) {
       dump_design( $design );
    }
    printf( "\n" );
}



1;

