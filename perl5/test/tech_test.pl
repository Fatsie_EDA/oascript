#!/usr/bin/perl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

use strict;
use warnings;

use File::Basename;
use FindBin;
use Getopt::Long;

use lib dirname($FindBin::RealBin);
use oa::tech;

my %options;
GetOptions(\%options,
	   "libdefs=s",
	   "lib=s"
	   );

eval {

    oa::oaDMInit();
    oa::oaTechInit();

    if (exists($options{libdefs})) {
	&oa::oaLibDefList::openLibs($options{libdefs});
    } else {
	&oa::oaLibDefList::openLibs();
    }

    my $ns = new oa::oaNativeNS;

    my $techlib_name = $options{lib} || die "Missing mandatory -lib argument.";

    my $techlib_scname = new oa::oaScalarName($ns, $techlib_name);
    my $techlib = oa::oaLib::find($techlib_scname);
    if (!$techlib) {
	print "Couldn't find $techlib_name.\n";
	exit 1;
    }
    if (!$techlib->getAccess(new oa::oaLibAccess($oa::oacReadLibAccess))) {
	print "lib $techlib_name: No access!\n";
	exit 1;
    }
    my $tech = oa::oaTech::open($techlib);
    if (!$tech) {
	print "No tech info!\n";
	exit 1;
    }
    my $layer_iter = new oa::oaIter::oaLayer($tech->getLayers());
    while (my $layer = $layer_iter->getNext()) {
	my $layer_name;
	$layer->getName($layer_name);
	print "layer " . $layer->getNumber() . " = $layer_name\n";
    }

};

if ($@) {
    print STDERR "Caught exception: $@\n";
}

exit 0;
