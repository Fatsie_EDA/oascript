#!/usr/bin/perl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

use strict;
use warnings;

use File::Basename;
use FindBin;
use Getopt::Long;

use lib dirname($FindBin::RealBin);
use oa::dm;

my %options;
GetOptions(\%options,
	   "libdefs=s",
	   "quiet"
	   );

my $num_calls = 0;
my $verbose = !exists($options{quiet});

eval {

    oa::oaDMInit();
    $num_calls++;

    if (exists($options{libdefs})) {
	&oa::oaLibDefList::openLibs($options{libdefs});
    } else {
	&oa::oaLibDefList::openLibs();
    }
    $num_calls++;

    my $ns = new oa::oaNativeNS;
    $num_calls++;

    my $lib_iter = new oa::oaIter::oaLib(oa::oaLib::getOpenLibs());
    $num_calls += 2;
    while (my $lib = $lib_iter->getNext()) {
	$num_calls++;
	my $libname;
	$lib->getName($ns, $libname);
	$num_calls++;
	$num_calls += 2;  # Following line
	if (!$lib->getAccess(new oa::oaLibAccess($oa::oacReadLibAccess))) {
	    printf("lib $libname: No access!\n");
	    next;
	}
	my $libpath;
	$lib->getPath($libpath);
	$num_calls++;
	print "lib $libname path=$libpath\n" if $verbose;
	my $cell_iter = new oa::oaIter::oaCell($lib->getCells());
	$num_calls += 2;
	while (my $cell = $cell_iter->getNext()) {
	    $num_calls++;
	    my $cellname;
	    $cell->getName($ns, $cellname);
	    $num_calls++;
	    print("  cell $cellname\n") if $verbose;
	    my $cv_iter = new oa::oaIter::oaCellView($cell->getCellViews());
	    $num_calls += 2;
	    while (my $cv = $cv_iter->getNext()) {
		$num_calls++;
		my $view = $cv->getView();
		$num_calls++;
		my $view_name;
		$view->getName($ns, $view_name);
		$num_calls++;
		print("    $view_name\n") if $verbose;
		my $file_iter = new oa::oaIter::oaDMFile($cv->getDMFiles());
		$num_calls += 2;
		while (my $file = $file_iter->getNext()) {
		    $num_calls++;
		    my $file_name;
		    $file->getPath($file_name);
		    $num_calls++;
		    my $size = $file->getOnDiskSize();
		    $num_calls++;
		    print("      " . basename($file_name) . " ($size bytes)\n") if $verbose;
		}
		$num_calls++;
	    }
	    $num_calls++;
	}
	$num_calls++;
    }
    $num_calls++;
};

if ($@) {
    print STDERR "Caught exception: $@\n";
}

print("number of API calls = $num_calls\n");

exit 0;
