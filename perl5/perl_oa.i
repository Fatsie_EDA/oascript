/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%header %{

#undef SWIG_SHADOW
#define SWIG_SHADOW 0

#define LANG_STRINGP(input) SvPOK_with_Magic(input)
#define LANG_STR_TO_CSTR(input) SvPV_with_Magic(input, PL_na)
#define LANG_THROW_OAEXCP(e) croak("OpenAccess Exception (ID=%d) : %s", e.getMsgId(), (const oaChar*) e.getMsg())
#define LANG_OA_TRY(action) \
try { \
    action; \
} catch (const OpenAccess_4::oaException& e) { \
    LANG_THROW_OAEXCP(e); \
}

static inline bool SvPOK_with_Magic(SV *obj)
{
    if (SvMAGICAL(obj)) {
        SV *tmp = sv_newmortal();
        SvSetSV(tmp, obj);
        obj = tmp;
    }
    return SvPOK(obj);
}

static inline const char *SvPV_with_Magic(SV *obj, STRLEN &len)
{
    if (SvMAGICAL(obj)) {
        SV *tmp = sv_newmortal();
        SvSetSV(tmp, obj);
        obj = tmp;
    }
    return SvPV(obj, len);
}

%}

/* Catch exceptions, or else they are segfaults! */
%exception
{
    try {
	$action;
    } catch (OpenAccess_4::oaException& e) {
        LANG_THROW_OAEXCP(e);
    }
}

/* Useless in perl */
%ignore OpenAccess_4::oaSwap;
%ignore OpenAccess_4::oaXYTreeRec::operator new[];
%ignore OpenAccess_4::oaXYTreeRec::operator delete[];
%ignore OpenAccess_4::oaXYTreeRec::operator new[];
%ignore OpenAccess_4::oaXYTreeNode::operator delete[];
%ignore OpenAccess_4::oaXYTreeNode::operator new[];
%ignore OpenAccess_4::operator+;
%ignore OpenAccess_4::operator-;
%ignore OpenAccess_4::operator*;
%ignore OpenAccess_4::operator/;

/* These classes are all translated to native types. */
%include "oaString.i"
%include "oaTimestamp.i"
%include "oaPoint.i"
%include "oaBox.i"
%include "oaTransform.i"

/* Generic typemaps. */
%include <generic/typemaps/oaNames.i>

/* Ignore forward declarations of these classes in oaArray.h */
%ignore OpenAccess_4::oaFeatureArray;
%ignore OpenAccess_4::oaManagedTypeArray;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaPoint>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaBox>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaString>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaFeature*>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaUInt4>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaObject*>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaArray<OpenAccess_4::oaUInt4> >;

%ignore OpenAccess_4::oaDesignInit;

%include "target_arrays.i"

#undef ENUMWRAPPER
%define ENUMWRAPPER(ns,class,enumns,enumclass)
%typemap(in) ns::class
{
    void *obj;

    if (SWIG_IsOK(SWIG_ConvertPtr($input, &obj, $&1_descriptor, $disown | %convertptr_flags))) {
	$1 = *((ns::class*) obj);
    } else if (SvIOK($input)) {
	$1 = ns::class((enumns::enumclass) SvIV($input));
    } else {
	%argument_fail(SWIG_TypeError, "$type", $symname, $argnum); 
    }
}
%typecheck(0) ns::class
{
    void *obj;
    $1 = SvIOK($input) || SWIG_IsOK(SWIG_ConvertPtr($input, &obj, $&1_descriptor, 0));
}
%typemap(out) ns::class
{
    $result = sv_2mortal(newSViv((IV) (enumns::enumclass) (ns::class&) $1));
    argvi++;
}
%enddef

%include <macros/oa_enums.i>
