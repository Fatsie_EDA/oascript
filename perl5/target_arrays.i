/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#undef INTARRAY
%define INTARRAY(safetype,paramtype)
%ignore OpenAccess_4::oaArrayBase<paramtype>;
%ignore OpenAccess_4::oaArray<paramtype>;

%typemap(in) OpenAccess_4::oaArray<paramtype>& (oaArray<paramtype> tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) const OpenAccess_4::oaArray<paramtype>& (oaArray<paramtype> tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    int intval;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	if (!SWIG_IsOK(SWIG_AsVal_int(*psv, &intval))) {
	    %argument_fail(SWIG_ERROR, #paramtype, $symname, $argnum);
	}
	tmpArr.append((paramtype) intval);
    }

    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<paramtype>&
{
    $1 = ($input == &PL_sv_undef ? 0 : 1);
}

%typecheck(0) const OpenAccess_4::oaArray<paramtype>&
{
    $1 = 0;
    if (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV) {
	AV *av = (AV*) SvRV($input);
	if (av_len(av) < 0) {
	    $1 = 1;
	} else {
	    SV **psv = av_fetch(av, 0, 0);
	    int intval;
	    if (psv && !SvROK(*psv) && SWIG_IsOK(SWIG_AsVal_int(*psv, &intval)))
		$1 = 1;
	}
    }
}

%typemap(argout) OpenAccess_4::oaArray<paramtype>&
{
    AV *arr = NULL;
    bool argIsArray = (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV);
    if (argIsArray) {
	arr = (AV*) SvRV($input);
	av_clear(arr);
    } else {
	arr = newAV();
    }
    oaUInt4 i, len;
    if ($1) {
	len = $1->getNumElements();
	for (i = 0; i < len; ++i)
	    av_push(arr, newSViv((IV) $1->get(i)));
    }
    if (!argIsArray) {
	SV *arrRef = newRV_noinc((SV*) arr);
	sv_setsv($input, arrRef);
	SvREFCNT_dec(arrRef);
    }
}

%typemap(argout) const OpenAccess_4::oaArray<paramtype>& {}

%apply OpenAccess_4::oaArray<paramtype>& { OpenAccess_4::oaArray<paramtype>* };
%apply const OpenAccess_4::oaArray<paramtype>& { const OpenAccess_4::oaArray<paramtype>* };

%enddef

/**/

#undef INTARRAYEXT
%define INTARRAYEXT(ns,class,safetype,paramtype)

%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (ns::class tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    int intval;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	if (!SWIG_IsOK(SWIG_AsVal_int(*psv, &intval))) {
	    %argument_fail(SWIG_ERROR, #paramtype, $symname, $argnum);
	}
	tmpArr.append((paramtype) intval);
    }

    $1 = &tmpArr;
}

%typecheck(0) ns::class&, ns::class *
{
    $1 = ($input == &PL_sv_undef ? 0 : 1);
}

%typecheck(0) const ns::class&, const ns::class *
{
    $1 = 0;
    if (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV) {
	AV *av = (AV*) SvRV($input);
	if (av_len(av) < 0) {
	    $1 = 1;
	} else {
	    SV **psv = av_fetch(av, 0, 0);
	    int intval;
	    if (psv && !SvROK(*psv) && SWIG_IsOK(SWIG_AsVal_int(*psv, &intval)))
		$1 = 1;
	}
    }
}

%typemap(argout) ns::class&
{
    AV *arr = NULL;
    bool argIsArray = (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV);
    if (argIsArray) {
	arr = (AV*) SvRV($input);
	av_clear(arr);
    } else {
	arr = newAV();
    }
    oaUInt4 i, len;
    if ($1) {
	len = $1->getNumElements();
	for (i = 0; i < len; ++i)
	    av_push(arr, newSViv((IV) $1->get(i)));
    }
    if (!argIsArray) {
	SV *arrRef = newRV_noinc((SV*) arr);
	sv_setsv($input, arrRef);
	SvREFCNT_dec(arrRef);
    }
}

%typemap(argout) const ns::class& {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

/**/

#undef STRINGARRAY
%define STRINGARRAY

%typemap(in) OpenAccess_4::oaArray<OpenAccess_4::oaString>& (oaArray<oaString> tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) const OpenAccess_4::oaArray<OpenAccess_4::oaString>& (oaArray<oaString> tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	if (!SvPOK(*psv)) {
	    %argument_fail(SWIG_ERROR, "oaString", $symname, $argnum);
	}
	tmpArr.append(oaString((const oaChar*) SvPV_nolen(*psv)));
    }

    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<OpenAccess_4::oaString>&
{
    $1 = ($input == &PL_sv_undef ? 0 : 1);
}

%typecheck(0) const OpenAccess_4::oaArray<OpenAccess_4::oaString>&
{
    $1 = 0;
    if (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV) {
	AV *av = (AV*) SvRV($input);
	if (av_len(av) < 0) {
	    $1 = 1;
	} else {
	    SV **psv = av_fetch(av, 0, 0);
	    if (psv && SvPOK(*psv))
		$1 = 1;
	}
    }
}

%typemap(argout) OpenAccess_4::oaArray<OpenAccess_4::oaString>&
{
    AV *arr = NULL;
    bool argIsArray = (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV);
    if (argIsArray) {
	arr = (AV*) SvRV($input);
	av_clear(arr);
    } else {
	arr = newAV();
    }
    oaUInt4 i, len;
    if ($1) {
	len = $1->getNumElements();
	for (i = 0; i < len; ++i)
	    av_push(arr, newSVpv((const oaChar*) $1->get(i), 0));
    }
    if (!argIsArray) {
	SV *arrRef = newRV_noinc((SV*) arr);
	sv_setsv($input, arrRef);
	SvREFCNT_dec(arrRef);
    }
}

%typemap(argout) const OpenAccess_4::oaArray<OpenAccess_4::oaString>& {}

%apply (OpenAccess_4::oaArray<OpenAccess_4::oaString>&) { OpenAccess_4::oaArray<OpenAccess_4::oaString>* };
%apply (const OpenAccess_4::oaArray<OpenAccess_4::oaString>&) { const OpenAccess_4::oaArray<OpenAccess_4::oaString>* };

%enddef

/**/

#undef STRINGARRAYEXT
%define STRINGARRAYEXT(ns,class)
%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (ns::class tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	if (!SvPOK(*psv)) {
	    %argument_fail(SWIG_ERROR, "oaString", $symname, $argnum);
	}
	tmpArr.append(oaString((const oaChar*) SvPV_nolen(*psv)));
    }

    $1 = &tmpArr;
}

%typecheck(0) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaString>&;
%typecheck(0) (const ns::class&) = const OpenAccess_4::oaArray<OpenAccess_4::oaString>&;
%typemap(argout) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaString>&;
%typemap(argout) (const ns::class&) = const OpenAccess_4::oaArray<OpenAccess_4::oaString>&;

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

/**/

#undef POINTARRAY
%define POINTARRAY

%typemap(in) (OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) (oaArray<oaPoint> tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) (oaArray<oaPoint> tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	AV *av2;
	SV **xPtr, **yPtr;
	int x, y;
	if (!SvROK(*psv) ||
	    SvTYPE((SV*) (av2 = (AV*) SvRV(*psv))) != SVt_PVAV ||
	    av_len(av2) != 1 ||
	    !(xPtr = av_fetch(av2, 0, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*xPtr, &x)) ||
	    !(yPtr = av_fetch(av2, 1, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*yPtr, &y))) {
	    %argument_fail(SWIG_ERROR, "oaPoint", $symname, $argnum);
	}
	tmpArr.append(oaPoint(x, y));
    }

    $1 = &tmpArr;
}

%typecheck(0) (OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&)
{
    $1 = ($input == &PL_sv_undef ? 0 : 1);
}

%typecheck(0) (const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&)
{
    $1 = 0;
    if (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV) {
	AV *av = (AV*) SvRV($input);
	if (av_len(av) < 0) {
	    $1 = 1;
	} else {
	    SV **psv = av_fetch(av, 0, 0);
	    if (psv && SvROK(*psv) && SvTYPE(SvRV(*psv)) == SVt_PVAV && av_len((AV*) SvRV(*psv)) == 1)
		$1 = 1;
	}
    }
}

%typemap(argout) (OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&)
{
    AV *arr = NULL;
    bool argIsArray = (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV);
    if (argIsArray) {
	arr = (AV*) SvRV($input);
	av_clear(arr);
    } else {
	arr = newAV();
    }
    oaUInt4 i, len;
    if ($1) {
	len = $1->getNumElements();
	for (i = 0; i < len; ++i) {
	    oaPoint pt = $1->get(i);
	    AV *arr2 = newAV();
	    av_push(arr2, newSViv(pt.x()));
	    av_push(arr2, newSViv(pt.y()));
	    av_push(arr, newRV_noinc((SV*) arr2));
	}
    }
    if (!argIsArray) {
	SV *arrRef = newRV_noinc((SV*) arr);
	sv_setsv($input, arrRef);
	SvREFCNT_dec(arrRef);
    }
}

%typemap(argout) (const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) {}

%typemap(out) OpenAccess_4::oaArray<OpenAccess_4::oaPoint>
{
    AV *arr = newAV();
    oaUInt4 len = $1.getNumElements();
    for (oaUInt4 i = 0; i < len; ++i) {
        oaPoint pt = $1.get(i);
        AV *arr2 = newAV();
        av_push(arr2, newSViv(pt.x()));
        av_push(arr2, newSViv(pt.y()));
        av_push(arr, newRV_noinc((SV*) arr2));
    }
    $result = sv_2mortal(newRV_noinc((SV*)arr));
    argvi++;
}

%typemap(out) (OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&)
{
    AV *arr = newAV();
    oaUInt4 len = $1->getNumElements();
    for (oaUInt4 i = 0; i < len; ++i) {
        oaPoint pt = $1->get(i);
        AV *arr2 = newAV();
        av_push(arr2, newSViv(pt.x()));
        av_push(arr2, newSViv(pt.y()));
        av_push(arr, newRV_noinc((SV*) arr2));
    }
    $result = sv_2mortal(newRV_noinc((SV*)arr));
    argvi++;
}

%apply (OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) { OpenAccess_4::oaArray<OpenAccess_4::oaPoint>* };
%apply (const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) { const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>* };

%enddef

/**/

#undef POINTARRAYEXT
%define POINTARRAYEXT(ns,class)
%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (ns::class tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	AV *av2;
	SV **xPtr, **yPtr;
	int x, y;
	if (!SvROK(*psv) ||
	    SvTYPE((SV*) (av2 = (AV*) SvRV(*psv))) != SVt_PVAV ||
	    av_len(av2) != 1 ||
	    !(xPtr = av_fetch(av2, 0, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*xPtr, &x)) ||
	    !(yPtr = av_fetch(av2, 1, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*yPtr, &y))) {
	    %argument_fail(SWIG_ERROR, "oaPoint", $symname, $argnum);
	}
	tmpArr.append(oaPoint(x, y));
    }

    $1 = &tmpArr;
}

%typecheck(0) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&;
%typecheck(0) (const ns::class&) = const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&;
%typemap(argout) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&;
%typemap(argout) (const ns::class&) = const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&;
%typemap(out) (ns::class) = OpenAccess_4::oaArray<OpenAccess_4::oaPoint>;
%typemap(out) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&;

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

/**/

#undef BOXARRAY
%define BOXARRAY

%typemap(in) (OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) (oaArray<oaBox> tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) (oaArray<oaBox> tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	AV *av2;
	SV **x1Ptr, **y1Ptr, **x2Ptr, **y2Ptr;
	int x1, y1, x2, y2;
	if (!SvROK(*psv) ||
	    SvTYPE((SV*) (av2 = (AV*) SvRV(*psv))) != SVt_PVAV ||
	    av_len(av2) != 3 ||
	    !(x1Ptr = av_fetch(av2, 0, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*x1Ptr, &x1)) ||
	    !(y1Ptr = av_fetch(av2, 1, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*y1Ptr, &y1)) ||
	    !(x2Ptr = av_fetch(av2, 2, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*x2Ptr, &x2)) ||
	    !(y2Ptr = av_fetch(av2, 3, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*y2Ptr, &y2))) {
	    %argument_fail(SWIG_ERROR, "oaBox", $symname, $argnum);
	}
	tmpArr.append(oaBox(x1, y1, x2, y2));
    }

    $1 = &tmpArr;
}

%typecheck(0) (OpenAccess_4::oaArray<OpenAccess_4::oaBox>&)
{
    $1 = ($input == &PL_sv_undef ? 0 : 1);
}

%typecheck(0) (const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&)
{
    $1 = 0;
    if (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV) {
	AV *av = (AV*) SvRV($input);
	if (av_len(av) < 0) {
	    $1 = 1;
	} else {
	    SV **psv = av_fetch(av, 0, 0);
	    if (psv && SvROK(*psv) && SvTYPE(SvRV(*psv)) == SVt_PVAV && av_len((AV*) SvRV(*psv)) == 3)
		$1 = 1;
	}
    }
}

%typemap(argout) (OpenAccess_4::oaArray<OpenAccess_4::oaBox>&)
{
    AV *arr = NULL;
    bool argIsArray = (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV);
    if (argIsArray) {
	arr = (AV*) SvRV($input);
	av_clear(arr);
    } else {
	arr = newAV();
    }
    oaUInt4 i, len;
    if ($1) {
	len = $1->getNumElements();
	for (i = 0; i < len; ++i) {
	    oaBox box = $1->get(i);
	    AV *arr2 = newAV();
	    av_push(arr2, newSViv(box.left()));
	    av_push(arr2, newSViv(box.bottom()));
	    av_push(arr2, newSViv(box.right()));
	    av_push(arr2, newSViv(box.top()));
	    av_push(arr, newRV_noinc((SV*) arr2));
	}
    }
    if (!argIsArray) {
	SV *arrRef = newRV_noinc((SV*) arr);
	sv_setsv($input, arrRef);
	SvREFCNT_dec(arrRef);
    }
}

%typemap(argout) (const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) {}

%apply (OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) { OpenAccess_4::oaArray<OpenAccess_4::oaBox>* };
%apply (const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) { const OpenAccess_4::oaArray<OpenAccess_4::oaBox>* };

%enddef

/**/

#undef BOXARRAYEXT
%define BOXARRAYEXT(ns,class)
%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (ns::class tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	AV *av2;
	SV **x1Ptr, **y1Ptr, **x2Ptr, **y2Ptr;
	int x1, y1, x2, y2;
	if (!SvROK(*psv) ||
	    SvTYPE((SV*) (av2 = (AV*) SvRV(*psv))) != SVt_PVAV ||
	    av_len(av2) != 3 ||
	    !(x1Ptr = av_fetch(av2, 0, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*x1Ptr, &x1)) ||
	    !(y1Ptr = av_fetch(av2, 1, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*y1Ptr, &y1)) ||
	    !(x2Ptr = av_fetch(av2, 2, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*x2Ptr, &x2)) ||
	    !(y2Ptr = av_fetch(av2, 3, 0)) ||
	    !SWIG_IsOK(SWIG_AsVal_int(*y2Ptr, &y2))) {
	    %argument_fail(SWIG_ERROR, "oaBox", $symname, $argnum);
	}
	tmpArr.append(oaBox(x1, y1, x2, y2));
    }

    $1 = &tmpArr;
}

%typecheck(0) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaBox>&;
%typecheck(0) (const ns::class&) = const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&;
%typemap(argout) (ns::class&) = OpenAccess_4::oaArray<OpenAccess_4::oaBox>&;
%typemap(argout) (const ns::class&) = const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&;

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

/**/

#undef ARRAYCLASS
%define ARRAYCLASS(ns,paramtype)

%typemap(in) (OpenAccess_4::oaArray<ns::paramtype>&) (oaArray<ns::paramtype> tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const OpenAccess_4::oaArray<ns::paramtype>&) (int res, void *argp, oaArray<ns::paramtype> tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	res = SWIG_ConvertPtr(*psv, &argp, $descriptor(ns::paramtype *), 0);
	if (!SWIG_IsOK(res)) {
	    %argument_fail(SWIG_ERROR, #paramtype, $symname, $argnum);
	}
	tmpArr.append(*(%reinterpret_cast(argp, ns::paramtype *)));
    }

    $1 = &tmpArr;
}

%typecheck(0) (OpenAccess_4::oaArray<ns::paramtype>&)
{
    $1 = ($input == &PL_sv_undef ? 0 : 1);
}

%typecheck(0) (const OpenAccess_4::oaArray<ns::paramtype>)
{
    $1 = 0;
    if (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV) {
	AV *av = (AV*) SvRV($input);
	if (av_len(av) < 0) {
	    $1 = 1;
	} else {
	    void *argp;
	    SV **psv = av_fetch(av, 0, 0);
	    if (psv && SWIG_IsOK(SWIG_ConvertPtr(*psv, &argp, $descriptor(ns::paramtype*), 0)))
		$1 = 1;
	}
    }
}

%typemap(argout) (OpenAccess_4::oaArray<ns::paramtype>&)
{
    AV *arr = NULL;
    bool argIsArray = (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV);
    if (argIsArray) {
	arr = (AV*) SvRV($input);
	av_clear(arr);
    } else {
	arr = newAV();
    }
    oaUInt4 i, len;
    if ($1) {
	len = $1->getNumElements();
	for (i = 0; i < len; ++i) {
	    ns::paramtype *el = new ns::paramtype($1->get(i));
	    SV *newObj = newSV(0);
	    SWIG_MakePtr(newObj, (void*) el, SWIG_TypeDynamicCast($descriptor(ns::paramtype *), (void**) &el), SWIG_POINTER_OWN);
	    av_push(arr, newObj);
	}
    }
    if (!argIsArray) {
	SV *arrRef = newRV_noinc((SV*) arr);
	sv_setsv($input, arrRef);
	SvREFCNT_dec(arrRef);
    }
}

%typemap(argout) (const OpenAccess_4::oaArray<ns::paramtype>&) {}

%apply (OpenAccess_4::oaArray<ns::paramtype>&) { OpenAccess_4::oaArray<ns::paramtype>* };
%apply (const OpenAccess_4::oaArray<ns::paramtype>&) { const OpenAccess_4::oaArray<ns::paramtype>* };

%enddef

/**/

#undef ARRAYEXT
%define ARRAYEXT(ns,class,paramtype)
%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (int res, void *argp, ns::class tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	res = SWIG_ConvertPtr(*psv, &argp, $descriptor(paramtype *), 0);
	if (!SWIG_IsOK(res)) {
	    %argument_fail(SWIG_ERROR, #paramtype, $symname, $argnum);
	}
	tmpArr.append(*(%reinterpret_cast(argp, paramtype *)));
    }

    $1 = &tmpArr;
}

%typemap(in) (ns::class*) = ns::class&;
%typemap(in) (const ns::class*) = const ns::class&;

%typecheck(0) (ns::class&)
{
    $1 = ($input == &PL_sv_undef ? 0 : 1);
}

%typecheck(0) (const ns::class&)
{
    $1 = 0;
    if (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV) {
	AV *av = (AV*) SvRV($input);
	if (av_len(av) < 0) {
	    $1 = 1;
	} else {
	    void *argp;
	    SV **psv = av_fetch(av, 0, 0);
	    if (psv && SWIG_IsOK(SWIG_ConvertPtr(*psv, &argp, $descriptor(paramtype*), 0)))
		$1 = 1;
	}
    }
}

%typemap(argout) (ns::class&)
{
    AV *arr = NULL;
    bool argIsArray = (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV);
    if (argIsArray) {
	arr = (AV*) SvRV($input);
	av_clear(arr);
    } else {
	arr = newAV();
    }
    oaUInt4 i, len;
    if ($1) {
	len = $1->getNumElements();
	for (i = 0; i < len; ++i) {
	    paramtype *el = new paramtype($1->get(i));
	    SV *newObj = newSV(0);
	    SWIG_MakePtr(newObj, (void*) el, SWIG_TypeDynamicCast($descriptor(paramtype *), (void**) &el), SWIG_POINTER_OWN);
	    av_push(arr, newObj);
	}
    }
    if (!argIsArray) {
	SV *arrRef = newRV_noinc((SV*) arr);
	sv_setsv($input, arrRef);
	SvREFCNT_dec(arrRef);
    }
}

%typemap(argout) (const ns::class&) {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

/**/

#undef PTRARRAYCLASS
%define PTRARRAYCLASS(ns,paramtype)

%typemap(in) (OpenAccess_4::oaArray<ns::paramtype *>&) (oaArray<ns::paramtype *> tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const OpenAccess_4::oaArray<ns::paramtype *>&) (int res, void *argp, oaArray<ns::paramtype *> tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	res = SWIG_ConvertPtr(*psv, &argp, $descriptor(ns::paramtype *), 0);
	if (!SWIG_IsOK(res)) {
	    %argument_fail(SWIG_ERROR, #paramtype "*", $symname, $argnum);
	}
	tmpArr.append(%reinterpret_cast(argp, ns::paramtype *));
    }

    $1 = &tmpArr;
}

%typecheck(0) (OpenAccess_4::oaArray<ns::paramtype *>&)
{
    $1 = ($input == &PL_sv_undef ? 0 : 1);
}

%typecheck(0) (const OpenAccess_4::oaArray<ns::paramtype *>)
{
    $1 = 0;
    if (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV) {
	AV *av = (AV*) SvRV($input);
	if (av_len(av) < 0) {
	    $1 = 1;
	} else {
	    void *argp;
	    SV **psv = av_fetch(av, 0, 0);
	    if (psv && SWIG_IsOK(SWIG_ConvertPtr(*psv, &argp, $descriptor(ns::paramtype*), 0)))
		$1 = 1;
	}
    }
}

%typemap(argout) (OpenAccess_4::oaArray<ns::paramtype *>&)
{
    AV *arr = NULL;
    bool argIsArray = (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV);
    if (argIsArray) {
	arr = (AV*) SvRV($input);
	av_clear(arr);
    } else {
	arr = newAV();
    }
    oaUInt4 i, len;
    if ($1) {
	len = $1->getNumElements();
	for (i = 0; i < len; ++i) {
	    ns::paramtype *el = $1->get(i);
	    SV *newObj = newSV(0);
	    SWIG_MakePtr(newObj, (void*) el, SWIG_TypeDynamicCast($descriptor(ns::paramtype *), (void**) &el), 0);
	    av_push(arr, newObj);
	}
    }
    if (!argIsArray) {
	SV *arrRef = newRV_noinc((SV*) arr);
	sv_setsv($input, arrRef);
	SvREFCNT_dec(arrRef);
    }
}

%typemap(argout) (const OpenAccess_4::oaArray<ns::paramtype *>&) {}

%apply (OpenAccess_4::oaArray<ns::paramtype *>&) { OpenAccess_4::oaArray<ns::paramtype *>* };
%apply (const OpenAccess_4::oaArray<ns::paramtype *>&) { const OpenAccess_4::oaArray<ns::paramtype *>* };

%enddef

/**/

#undef PTRARRAYEXT
%define PTRARRAYEXT(ns,class,basetype)
%ignore ns::class;

%typemap(in) (ns::class&) (ns::class tmpArr)
{
    $1 = &tmpArr;
}

%typemap(in) (const ns::class&) (int res, void *argp, ns::class tmpArr)
{
    if (!SvROK($input) || SvTYPE(SvRV($input)) != SVt_PVAV) {
	%argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }

    AV *av = (AV*) SvRV($input);
    I32 len = av_len(av) + 1;
    I32 i;
    SV **psv;
    for (i = 0; i < len; ++i) {
	psv = av_fetch(av, i, 0);
	res = SWIG_ConvertPtr(*psv, &argp, $descriptor(basetype *), 0);
	if (!SWIG_IsOK(res)) {
	    %argument_fail(SWIG_ERROR, #basetype "*", $symname, $argnum);
	}
	tmpArr.append(%reinterpret_cast(argp, basetype *));
    }

    $1 = &tmpArr;
}

%typecheck(0) (ns::class&)
{
    $1 = ($input == &PL_sv_undef ? 0 : 1);
}

%typecheck(0) (const ns::class&)
{
    $1 = 0;
    if (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV) {
	AV *av = (AV*) SvRV($input);
	if (av_len(av) < 0) {
	    $1 = 1;
	} else {
	    void *argp;
	    SV **psv = av_fetch(av, 0, 0);
	    if (psv && SWIG_IsOK(SWIG_ConvertPtr(*psv, &argp, $descriptor(basetype*), 0)))
		$1 = 1;
	}
    }
}

%typemap(argout) (ns::class&)
{
    AV *arr = NULL;
    bool argIsArray = (SvROK($input) && SvTYPE(SvRV($input)) == SVt_PVAV);
    if (argIsArray) {
	arr = (AV*) SvRV($input);
	av_clear(arr);
    } else {
	arr = newAV();
    }
    oaUInt4 i, len;
    if ($1) {
	len = $1->getNumElements();
	for (i = 0; i < len; ++i) {
	    basetype *el = $1->get(i);
	    SV *newObj = newSV(0);
	    SWIG_MakePtr(newObj, (void*) el, SWIG_TypeDynamicCast($descriptor(basetype *), (void**) &el), 0);
	    av_push(arr, newObj);
	}
    }
    if (!argIsArray) {
	SV *arrRef = newRV_noinc((SV*) arr);
	sv_setsv($input, arrRef);
	SvREFCNT_dec(arrRef);
    }
}

%typemap(argout) (const ns::class&) {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

/**/

#undef INTSUBSET
%define INTSUBSET(safetype,paramtype)
%enddef

#undef INTSUBSETEXT
%define INTSUBSETEXT(ns,class,safetype,paramtype)
%enddef

#undef STRINGSUBSET
%define STRINGSUBSET
%enddef

#undef STRINGSUBSETEXT
%define STRINGSUBSETEXT(ns,class)
%enddef

#undef POINTSUBSET
%define POINTSUBSET
%enddef

#undef POINTSUBSETEXT
%define POINTSUBSETEXT(ns,class)
%enddef

#undef BOXSUBSET
%define BOXSUBSET
%enddef

#undef BOXSUBSETEXT
%define BOXSUBSETEXT(ns,class)
%enddef

#undef SUBSETCLASS
%define SUBSETCLASS(ns,paramtype)
%enddef

#undef SUBSETEXT
%define SUBSETEXT(ns,class,paramtype)
%enddef

#undef PTRSUBSETCLASS
%define PTRSUBSETCLASS(ns,paramtype)
%enddef

#undef PTRSUBSETEXT
%define PTRSUBSETEXT(ns,class,basetype)
%enddef

%include <macros/oa_arrays.i>
