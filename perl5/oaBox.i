/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

// %ignore OpenAccess_4::oaBox;
%ignore OpenAccess_4::oaBox::lowerLeft();
%ignore OpenAccess_4::oaBox::upperRight();
%ignore OpenAccess_4::oaBox::left();
%ignore OpenAccess_4::oaBox::right();
%ignore OpenAccess_4::oaBox::bottom();
%ignore OpenAccess_4::oaBox::top();
%ignore OpenAccess_4::oaBox::oaBox(); // do not allow creating unitialized bbox
%ignore OpenAccess_4::oaBox::inverted; // undocumented and broken in perl
%ignore OpenAccess_4::oaBox::ptAtOrigin; // undocumented and broken in perl

%typemap(in) OpenAccess_4::oaBox& (OpenAccess_4::oaBox tmpBox)
{
    // typemap(in) oaBox&
    AV *arr;
    SV **arr0, **arr1, **arr2, **arr3;
    int x1, y1, x2, y2;

    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 3 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x1)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y1)) &&
        (arr2 = av_fetch(arr, 2, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr2, &x2)) &&
        (arr3 = av_fetch(arr, 3, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr3, &y2))) {
        tmpBox.set(x1, y1, x2, y2);
    } else if ($input != &PL_sv_undef && !SvOK($input)) {
        // allow undef
        tmpBox.set(0, 0, 0, 0);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpBox;
}

%typemap(in) OpenAccess_4::oaBox* (OpenAccess_4::oaBox tmpBox)
{
    // typemap(in) oaBox*
    AV *arr;
    SV **arr0, **arr1, **arr2, **arr3;
    int x1, y1, x2, y2;

    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 3 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x1)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y1)) &&
        (arr2 = av_fetch(arr, 2, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr2, &x2)) &&
        (arr3 = av_fetch(arr, 3, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr3, &y2))) {
        tmpBox.set(x1, y1, x2, y2);
    } else if ($input != &PL_sv_undef && !SvOK($input)) {
        // allow undef
        tmpBox.set(0, 0, 0, 0);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpBox;
}

%typemap(in) const OpenAccess_4::oaBox& (OpenAccess_4::oaBox tmpBox)
{
    // typemap(in) const oaBox&
    AV *arr;
    SV **arr0, **arr1, **arr2, **arr3;
    int x1, y1, x2, y2;

    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 3 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x1)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y1)) &&
        (arr2 = av_fetch(arr, 2, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr2, &x2)) &&
        (arr3 = av_fetch(arr, 3, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr3, &y2))) {
        tmpBox.set(x1, y1, x2, y2);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpBox;
}

%typemap(in) const OpenAccess_4::oaBox* (OpenAccess_4::oaBox tmpBox)
{
    // typemap(in) const oaBox*
    AV *arr;
    SV **arr0, **arr1, **arr2, **arr3;
    int x1, y1, x2, y2;

    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 3 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x1)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y1)) &&
        (arr2 = av_fetch(arr, 2, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr2, &x2)) &&
        (arr3 = av_fetch(arr, 3, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr3, &y2))) {
        tmpBox.set(x1, y1, x2, y2);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
    $1 = &tmpBox;
}

%typemap(in) OpenAccess_4::oaBox
{
    // typemap(in) 
    AV *arr;
    SV **arr0, **arr1, **arr2, **arr3;
    int x1, y1, x2, y2;

    if (SvROK($input) &&
        SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
        av_len(arr) == 3 &&
        (arr0 = av_fetch(arr, 0, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr0, &x1)) &&
        (arr1 = av_fetch(arr, 1, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr1, &y1)) &&
        (arr2 = av_fetch(arr, 2, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr2, &x2)) &&
        (arr3 = av_fetch(arr, 3, 0)) &&
        SWIG_IsOK(SWIG_AsVal_int(*arr3, &y2))) {
        $1.set(x1, y1, x2, y2);
    } else {
        %argument_fail(SWIG_ValueError, "$type", $symname, $argnum); 
    }
}

%typecheck(0) OpenAccess_4::oaBox&, OpenAccess_4::oaBox*
{
    // typecheck(0) oaBox&, oaBox*
    AV *arr;
    $1 = ($input != &PL_sv_undef && !SvOK($input))
         || (SvROK($input) &&
             SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
             av_len(arr) == 3 &&
             !SvROK(*(av_fetch((AV*) SvRV($input), 0, 0))));
}

%typecheck(0) const OpenAccess_4::oaBox&, const OpenAccess_4::oaBox*, OpenAccess_4::oaBox, const OpenAccess_4::oaBox
{
    // typecheck(0) const oaBox&, const oaBox*, oaBox, const oaBox
    AV *arr;
    $1 = (SvROK($input) &&
          SvTYPE((SV*) (arr = (AV*) SvRV($input))) == SVt_PVAV &&
          av_len(arr) == 3 &&
          !SvROK(*(av_fetch((AV*) SvRV($input), 0, 0))));
}

%typemap(argout) OpenAccess_4::oaBox&
{
    // typemap(argout) oaBox&
    SV *coords[4];
    coords[0] = sv_2mortal(newSViv((IV) $1->left()));
    coords[1] = sv_2mortal(newSViv((IV) $1->bottom()));
    coords[2] = sv_2mortal(newSViv((IV) $1->right()));
    coords[3] = sv_2mortal(newSViv((IV) $1->top()));
    AV *arr = av_make(4, coords);
    sv_setsv($input, sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaBox*)->clientdata)));
}

%typemap(argout) OpenAccess_4::oaBox*
{
    // typemap(argout) oaBox*
    SV *coords[4];
    coords[0] = sv_2mortal(newSViv((IV) $1->left()));
    coords[1] = sv_2mortal(newSViv((IV) $1->bottom()));
    coords[2] = sv_2mortal(newSViv((IV) $1->right()));
    coords[3] = sv_2mortal(newSViv((IV) $1->top()));
    AV *arr = av_make(4, coords);
    sv_setsv($input, sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaBox*)->clientdata)));
}

%typemap(argout) const OpenAccess_4::oaBox&
{
    // typemap(argout) const oaBox&
}

%typemap(argout) const OpenAccess_4::oaBox*
{
    // typemap(argout) const oaBox*
}

%typemap(out) OpenAccess_4::oaBox&
{
    // typemap(out) oaBox&
    if (argvi >= items) EXTEND(sp,1);
    SV *coords[4];
    coords[0] = sv_2mortal(newSViv((IV) $1->left()));
    coords[1] = sv_2mortal(newSViv((IV) $1->bottom()));
    coords[2] = sv_2mortal(newSViv((IV) $1->right()));
    coords[3] = sv_2mortal(newSViv((IV) $1->top()));
    AV *arr = av_make(4, coords);
    $result = sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaBox*)->clientdata));
    argvi++;
}

%typemap(out) OpenAccess_4::oaBox*
{
    // typemap(out) oaBox*
    if (argvi >= items) EXTEND(sp,1);
    SV *coords[4];
    coords[0] = sv_2mortal(newSViv((IV) $1->left()));
    coords[1] = sv_2mortal(newSViv((IV) $1->bottom()));
    coords[2] = sv_2mortal(newSViv((IV) $1->right()));
    coords[3] = sv_2mortal(newSViv((IV) $1->top()));
    delete $1; // Note: this typemap is only used in constructors, and since we copy the data to perl SVs we need to free the constructed oaBox
    AV *arr = av_make(4, coords);
    $result = sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaBox*)->clientdata));
    argvi++;
}

%typemap(out) OpenAccess_4::oaBox, const OpenAccess_4::oaBox
{
    // typemape(out) oaBox, const oaBox
    if (argvi >= items) EXTEND(sp,1);
    SV *coords[4];
    coords[0] = sv_2mortal(newSViv((IV) $1.left()));
    coords[1] = sv_2mortal(newSViv((IV) $1.bottom()));
    coords[2] = sv_2mortal(newSViv((IV) $1.right()));
    coords[3] = sv_2mortal(newSViv((IV) $1.top()));
    AV *arr = av_make(4, coords);
    $result = sv_2mortal(sv_bless(newRV_noinc((SV*) arr), (HV*)$descriptor(OpenAccess_4::oaBox*)->clientdata));
    argvi++;
}

%apply (OpenAccess_4::oaBox) { oaBox }

