/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

/*******************************************************************************

Macros definitions.

All of the language-specific magic is done with macros.  The following macros
define default behavior.  To customize the output, redefine any of the following
macros.  For example, to control the handling of enum wrapper classes, you
would redefine the ENUMWRAPPER macro, and then include oa_enums.i:

#undef ENUMWRAPPER
%define ENUMWRAPPER(ns,class,enumns,enumclass)
// swig code
%enddef

%include <oa_enums.i>

*******************************************************************************/

/* Need a forward class declaration of std::pair */
namespace std {
    class pair;
}

namespace OpenAccess_4 {}

// oaTime typemap (through time_t)
%apply OpenAccess_4::oaInt8 { time_t };

/* %feature("valuewrapper") is required for classes that don't have a zero-argument constructor */
%feature("valuewrapper") oaCommon::SRef<oaCommon::IString>;
%feature("valuewrapper") oaSaveRecoverType;
class oaSaveRecoverType;

/* Mapping of swig_type_info variable name to oaTypeNum, for the purpose of downcasting */
%define OATYPENUM(type, num)
%enddef

/* Typemaps for oaCollection<ns1::paramtype1, ns2::paramtype2>
 * that is performed in *_collection_pre.i, which is included in target_*_pre.i */
#undef  COLLCLASSPRE
%define COLLCLASSPRE(ns1, paramtype1, ns2, paramtype2)
%enddef

/* Template instantation of oaCollection<ns1::paramtype1, ns2::paramtype2>
 * that is performed in *_collection_post.i, which is included in target_*_post.i */
%define COLLCLASS(ns1, paramtype1, ns2, paramtype2)
%feature("valuewrapper") OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2>;
%template(oaCollection_##paramtype1##_##paramtype2##) OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2>;
%enddef

/* Template instantation of oaIter<ns::paramtype> */
%define ITERCLASS(ns, paramtype)
%template (oaIter_##paramtype) OpenAccess_4::oaIter<ns##::##paramtype>;
%enddef

/* Enums.  Default behavior is to treat them as integers. */
%define ENUM(ns, paramtype)
%apply int { paramtype, ns::paramtype };
%enddef

/* Classes that are a wrapper around an enum, e.g., oaOrient is a wrapper around oaOrientEnum. */
%define ENUMWRAPPER(ns,class,enumns,enumclass)
%enddef

/* Interface for 'oaArray<paramtype>', where paramtype is an integer type.  'safetype' is
   'paramtype' with all spaces replaced by underscores; e.g., unsigned int -> unsigned_int */
%define INTARRAY(safetype,paramtype)
%template(oaArrayBase_##safetype##) OpenAccess_4::oaArrayBase<paramtype>;
%template(oaArray_##safetype##) OpenAccess_4::oaArray<paramtype>;
%enddef

/* Interface for 'class ns::class : public oaArray<paramtype>', where paramtype is a typedef for an integer type */
%define INTARRAYEXT(ns,class,safetype,paramtype)
%enddef

/* Interface for 'oaArray<oaString>' */
%define STRINGARRAY
%template(oaArrayBase_oaString) OpenAccess_4::oaArrayBase<OpenAccess_4::oaString>;
%template(oaArray_oaString) OpenAccess_4::oaArray<OpenAccess_4::oaString>;
%enddef

/* Interface for 'class ns::class : public oaArray<oaString>' */
%define STRINGARRAYEXT(ns,class)
%enddef

/* Interface for 'oaArray<oaPoint>' */
%define POINTARRAY 
%template(oaArrayBase_oaPoint) OpenAccess_4::oaArrayBase<OpenAccess_4::oaPoint>;
%template(oaArray_oaPoint) OpenAccess_4::oaArray<OpenAccess_4::oaPoint>;
%enddef

/* Interface for 'class ns::class : public oaArray<oaPoint>' */
%define POINTARRAYEXT(ns,class)
%enddef

/* Interface for 'oaArray<oaBox>' */
%define BOXARRAY
%template(oaArrayBase_oaBox) OpenAccess_4::oaArrayBase<OpenAccess_4::oaBox>;
%template(oaArray_oaBox) OpenAccess_4::oaArray<OpenAccess_4::oaBox>;
%enddef

/* Interface for 'class : public oaArray<oaBox>' */
%define BOXARRAYEXT(ns,class)
%enddef

/* Interface for 'oaArray<ns::paramtype>' */
%define ARRAYCLASS(ns,paramtype)
%template(oaArrayBase_##paramtype##) OpenAccess_4::oaArrayBase<ns::paramtype>;
%template(oaArray_##paramtype##) OpenAccess_4::oaArray<ns::paramtype>;
%enddef

/* Interface for 'class ns::class : public oaArray<paramtype>' */
%define ARRAYEXT(ns,class,paramtype)
%enddef

/* Interface for 'oaArray<paramtype*>' */
%define PTRARRAYCLASS(ns,paramtype)
%template(oaArrayBase_p_##paramtype##) OpenAccess_4::oaArrayBase<ns::paramtype *>;
%template(oaArray_p_##paramtype##) OpenAccess_4::oaArray<ns::paramtype *>;
%enddef

/* Interface for 'class : public oaArray<basetype*>' */
%define PTRARRAYEXT(ns,class,basetype)
%enddef

/* Interface for 'oaSubset<paramtype>', where paramtype is an integer type. */
%define INTSUBSET(safetype,paramtype)
%template(oaSubset_##safetype##) OpenAccess_4::oaSubset<paramtype>;
%enddef

/* Interface for 'class ns::class : public oaSubset<paramtype>', where paramtype is a typedef for an integer type */
%define INTSUBSETEXT(ns,class,safetype,paramtype)
%enddef

/* Interface for 'oaSubset<oaString>' */
%define STRINGSUBSET
%template(oaSubset_oaString) OpenAccess_4::oaSubset<OpenAccess_4::oaString>;
%enddef

/* Interface for 'class ns::class : public oaSubset<oaString>' */
%define STRINGSUBSETEXT(ns,class)
%enddef

/* Interface for 'oaSubset<oaPoint>' */
%define POINTSUBSET 
%template(oaSubset_oaPoint) OpenAccess_4::oaSubset<OpenAccess_4::oaPoint>;
%enddef

/* Interface for 'class ns::class : public oaSubset<oaPoint>' */
%define POINTSUBSETEXT(ns,class)
%enddef

/* Interface for 'oaSubset<oaBox>' */
%define BOXSUBSET
%template(oaSubset_oaBox) OpenAccess_4::oaSubset<OpenAccess_4::oaBox>;
%enddef

/* Interface for 'class : public oaSubset<oaBox>' */
%define BOXSUBSETEXT(ns,class)
%enddef

/* Interface for 'oaSubset<ns::paramtype>' */
%define SUBSETCLASS(ns,paramtype)
%template(oaSubset_##paramtype##) OpenAccess_4::oaSubset<ns::paramtype>;
%enddef

/* Interface for 'class ns::class : public oaSubset<paramtype>' */
%define SUBSETEXT(ns,class,paramtype)
%enddef

/* Interface for 'oaSubset<paramtype*>' */
%define PTRSUBSETCLASS(ns,paramtype)
%template(oaSubset_p_##paramtype##) OpenAccess_4::oaSubset<ns::paramtype *>;
%enddef

/* Interface for 'class ns::class : public oaSubset<basetype*>' */
%define PTRSUBSETEXT(ns,class,basetype)
%enddef

/* Template instantiation of oa1DLookupTbl< param1, param2 >. 'class' is a cooked-up unique name for this instantiation. */
%define LOOKUP1D(class,param1,param2)
%enddef

/* Template instantiation of oa2DLookupTbl< param1, param2, param3 >. 'class' is a cooked-up a unique name for this instantiation. */
%define LOOKUP2D(class,param1,param2,param3)
%enddef
