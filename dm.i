/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifdef SWIGPERL
%module "oa::dm";
#endif

#ifdef SWIGRUBY
%module "oa::dm";
#endif

#ifdef SWIGTCL
%module "dm";
#endif

#ifdef SWIGPYTHON
%module "dm";
#endif

#ifdef SWIGCSHARP
%module "oasCSharpDM";
#endif

%{

#include <oa/oaDesignDB.h>

USE_OA_NAMESPACE
USE_OA_COMMON_NAMESPACE
USE_OA_PLUGIN_NAMESPACE

oaNameSpace* getDefaultNameSpace();

%}

// Basic agnostic setup
%include "oa.i"

// Optional target-language-specific code:
%include <target_dm_pre.i>

// oaLibDefList::openLibs errors (through observers)
%include <generic/extensions/dm/open_libs_errors.i>

#if !defined(oaDM_P)
#define oaDM_P



// *****************************************************************************
// Platform macros
// *****************************************************************************
#if defined(_MSC_VER)
#define OA_WINDOWS
#endif



// *****************************************************************************
// System Header Files used by DM.
// *****************************************************************************
#if !defined(OA_WINDOWS)
#include <sys/param.h>
#include <netdb.h>
#include <netinet/tcp.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <pwd.h>
#endif



// *****************************************************************************
// Imported Public Header Files.
// *****************************************************************************
%import "base.i"
%import "plugin.i"

// *****************************************************************************
// DM Public Header Files.
// *****************************************************************************
%include "oa/oaDMTypes.h"

%include "oa/oaDMModTypes.h"
%include "oa/oaDMAttribute.h"
%include "oa/oaVCCap.h"
%include "oa/oaVersionComp.h"
%include "oa/oaVCSystem.h"
%include "oa/oaVCQueryDepth.h"
%include "oa/oaDMObject.h"
%include "oa/oaLib.h"
%include "oa/oaDMException.h"
%include "oa/oaCell.h"
%include "oa/oaView.h"
%include "oa/oaCellView.h"
%include "oa/oaDMFile.h"
%include "oa/oaDMCollection.h"
%include "oa/oaLibDefList.h"
%include "oa/oaLibDefListMem.h"
%include "oa/oaDMData.h"
%include "oa/oaDMTraits.h"
%include "oa/oaDMObserver.h"
%include "oa/oaVCObserver.h"



// *****************************************************************************
// DM Public Inline Files.
// *****************************************************************************
%include "oa/oaDMEnumWrapper.inl"
%include "oa/oaDMAttribute.inl"
%include "oa/oaVCSystem.inl"
%include "oa/oaDMObject.inl"
%include "oa/oaDMException.inl"
%include "oa/oaDMCollection.inl"
%include "oa/oaDMObserver.inl"



#endif

// Optional target-language-specific code:
%include <target_dm_post.i>

%include <generic/extensions/dm.i>

#if !defined(SWIGCSHARP)
%init %{

  // Call oaDMInit automatically to avoid SEGV if the user forgets.
  // The user can choose to NOT call this in the event that an older OA DM
  // model is desired by setting $OASCRIPT_USE_INIT = 0.
  char *useinit = getenv("OASCRIPT_USE_INIT");
  if (!useinit || strcmp(useinit, "0")) {
    OpenAccess_4::oaDMInit();
  }

%}
#endif

