/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifdef SWIGPERL
%module "oa::util";
#endif

#ifdef SWIGRUBY
%module "oa::util";
#endif

#ifdef SWIGTCL
%module "util";
#endif

#ifdef SWIGPYTHON
%module "util";
#endif

#ifdef SWIGCSHARP
%module "oasCSharpUtil";
#endif

%{

#include <oa/oaDesignDB.h>
#include <oa/oaUtil.h>

USE_OA_NAMESPACE
USE_OA_COMMON_NAMESPACE
USE_OA_PLUGIN_NAMESPACE
using namespace oaUtil;

#undef SWIG_SHADOW
#define SWIG_SHADOW 0

oaNameSpace* getDefaultNameSpace();

%}

// Basic agnostic setup
%include "oa.i"

// Optional target-language-specific code:
%include <target_util_pre.i>


#if !defined(oaUtil_P)
#define oaUtil_P



// ****************************************************************************
// System Headers
// ****************************************************************************
#include <stdio.h>
#include <set>
#include <vector>
#include <map>
#include <utility>
#include <iterator>
#include <memory>
#include <algorithm>


// ****************************************************************************
// OpenAccess Headers
// ****************************************************************************
%import "design.i"


// *****************************************************************************
// Define the namespace macros
// *****************************************************************************
#define BEGIN_OA_UTIL_NAMESPACE namespace oaUtil {
#define END_OA_UTIL_NAMESPACE }



// *****************************************************************************
// oaUtil Package Headers
// *****************************************************************************
%include "oa/oaUtilMsgs.h"
%include "oa/oaUtilHashFunction.h"
%include "oa/oaUtilHashTable.h"
%include "oa/oaUtilHashSet.h"
%include "oa/oaUtilHashMap.h"
%include "oa/oaUtilMsgAdapter.h"
%include "oa/oaUtilException.h"
%include "oa/oaUtilFormatInfo.h"
%include "oa/oaUtilVersionInfo.h"
%include "oa/oaUtilArgs.h"
%include "oa/oaUtilLibMgrOptions.h"
%include "oa/oaUtilLibMgr.h"
%include "oa/oaUtilOptionParser.h"



// *****************************************************************************
// Public Inline Includes
// *****************************************************************************
%include "oa/oaUtilHashFunction.inl"
%include "oa/oaUtilHashTable.inl"
%include "oa/oaUtilHashSet.inl"
%include "oa/oaUtilHashMap.inl"
%include "oa/oaUtilMsgAdapter.inl"
%include "oa/oaUtilException.inl"
%include "oa/oaUtilFormatInfo.inl"
%include "oa/oaUtilVersionInfo.inl"
%include "oa/oaUtilArgs.inl"
%include "oa/oaUtilLibMgrOptions.inl"
%include "oa/oaUtilOptionParser.inl"



#endif


// Optional target-language-specific code:
%include <target_util_post.i>

%include <generic/extensions/util.i>

