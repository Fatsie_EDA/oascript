/* ****************************************************************************

   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     02/16/2010: Copied from Python bindings (authored by AMD) and modified by
                 Intel Corporation for Ruby.

 *************************************************************************** */

%header %{

// Convenience method to rip out "OpenAccess_4::" qualifiers in argument
// errors.  It just clutters the error message, OpenAccess_4 is not exposed
// to the language bindings, and everything is in the scope of OA.
const char* OAS_Clean_Message( const char* msg )
{
  VALUE str;
  if ( msg && *msg )
    {
      str = rb_str_new2(msg);
    }
  else
    {
      str = rb_str_new(NULL, 0);
    }

  rb_funcall(str, rb_intern("gsub!"), 2, rb_str_new2("OpenAccess_4::"), rb_str_new2(""));

  return StringValuePtr( str );
}

// Wrapper for Ruby_Format_TypeError to clean OpenAccess_4:: from the message
SWIGINTERN const char* OAS_Ruby_Format_TypeError( const char* msg,
				   const char* type, 
				   const char* name, 
				   const int argn,
				   VALUE input )
{  
  return(Ruby_Format_TypeError(OAS_Clean_Message(msg), type, name, argn, input));
}

// Wrapper for Ruby_Format_TypeError to clean OpenAccess_4:: from the message
SWIGINTERN void OAS_Ruby_Format_OverloadedError(
				 const int argc,
				 const int maxargs,
				 const char* method, 
				 const char* prototypes 
				 ) 
{
  return(Ruby_Format_OverloadedError(argc, maxargs, method, OAS_Clean_Message(prototypes)));
}

#define SWIG_OAS_Error(code, msg) SWIG_Error(code, OAS_Clean_Message(msg))
#define SWIG_OAS_exception_fail(code, msg) do { SWIG_OAS_Error(code, msg); SWIG_fail; } while(0) 

// START USING OAS MESSAGE FORMATTING -- do not alter, move, or remove this line (parsed by Perl)

  
/* Language-specific type conversion */
#define LANG_NULLP(input) (TYPE(input) == T_NIL)
#define LANG_ARRAYP(input) (TYPE(input) == T_ARRAY)
#define LANG_INTP(input) (TYPE(input) == T_FIXNUM)
#define LANG_SYMBOLP(input) (TYPE(input) == T_SYMBOL)
#define LANG_STRINGP(input) (TYPE(input) == T_STRING)
#define LANG_ARRAY_SIZE(input) RARRAY(input)->len
#define LANG_ARRAY_INDEX(input, index) rb_ary_entry(input, index)
#define LANG_NUM_TO_LONG(langobj) NUM2LONG(langobj)
#define LANG_NUM_TO_INT(langobj) NUM2INT(langobj)
#define LANG_INT_TO_NUM(cppint) INT2NUM(cppint)
#define LANG_SYM_TO_STR(langobj) rb_funcall(langobj, oarToStrID, 0)
#define LANG_STR_TO_CSTR(langstr) StringValueCStr(langstr)
#define LANG_VALUE_TYPE VALUE
#define LANG_STR_TYPE VALUE
#define LANG_APPEND_OUTPUT(result, obj) SWIG_Ruby_AppendOutput(result, obj)
#define LANG_THROW_OAEXCP(excp) rb_exc_raise(SWIG_Ruby_ExceptionType(SWIGTYPE_p_OpenAccess_4__oaException, SWIG_NewPointerObj(SWIG_as_voidptr(&excp), SWIGTYPE_p_OpenAccess_4__oaException, 0))); SWIG_fail



// Used in string argout typemap (uses rb_funcall since rb_str_replace is not public)
// TODO: Look into using someting like rb_str_replace.
static ID oarStrReplaceID = rb_intern("replace");
static ID oarToStrID = rb_intern("to_s");
static VALUE oarEnumerable = rb_define_module("Enumerable");

// Macro to try and catch OA exceptions in some typemaps
#define LANG_OA_TRY_RESUME(action, resume)\
try {\
  action;\
} catch(OpenAccess_4::oaException& excp) {\
  rb_exc_raise(SWIG_Ruby_ExceptionType(SWIGTYPE_p_OpenAccess_4__oaException, SWIG_NewPointerObj(SWIG_as_voidptr(&excp), SWIGTYPE_p_OpenAccess_4__oaException, 0)));\
  resume;\
}

#define LANG_OA_TRY(action) LANG_OA_TRY_RESUME(action,)

#define OAR_TRY(action) LANG_TRY(action)
 
%}


//////////////////////////////////////////////////////////////////////////////
// Dereference coordinates -  this is for oaPoint issues returning &x &y
// TODO: why is this not needed for the Perl and Python bindings?
//////////////////////////////////////////////////////////////////////////////
%typemap(out) OpenAccess_4::oaCoord& "$result = INT2NUM(*$1);";
%typemap(out) OpenAccess_4::oaOffset& "$result = INT2NUM(*$1);";


// TODO: Find a better way to deal with these?
%rename("guid_eq?") oaCommon::operator==;
%rename("guid_ne?") oaCommon::operator!=;
%rename("guid_gt?") oaCommon::operator>;
%rename("guid_lt?") oaCommon::operator<;

// There is some OA top-level == operator that will cause Ruby to crash... so
// ignore it for now (perhaps long-term rename it if we need it?)
%ignore OpenAccess_4::operator==;

%header %{
#include <munged_headers/oaAppDefProxy.h>
%}

// Apply boolean mapping to oaBoolean
%apply bool { OpenAccess_4::oaBoolean };

// Load time mapping typemaps from SWIG Ruby library
%include "timeval.i"

// The following typecheck is missing from timeval.i and is needed for the
// overloaded methods.
%typemap(typecheck) time_t
{
  $1 = (NIL_P($input) || TYPE(rb_funcall($input, rb_intern("respond_to?"), 1, ID2SYM(rb_intern("tv_sec")))) == T_TRUE);
}

// Apply time_t to oaTime
%apply time_t { OpenAccess_4::oaTime };


// Helper functions for primitive types
%alias toInt "to_i";
%alias toDouble "to_f";



//////////////////////////////////////////////////////////////////////////////
// NameSpace extensions
//////////////////////////////////////////////////////////////////////////////
/*
%extend OpenAccess_4::oaNameSpace {
  static OpenAccess_4::oaNameSpace* getDefault() {
    return(oarDefaultNS);
  }
  static OpenAccess_4::oaNameSpace* setDefault(OpenAccess_4::oaNameSpace* newNS) {
    return(oarDefaultNS = newNS);
  }
}
*/


//////////////////////////////////////////////////////////////////////////////
// Collection class definitions
//////////////////////////////////////////////////////////////////////////////

#undef COLLCLASS
%define COLLCLASS(ns1, paramtype1, ns2, paramtype2)
  
%feature("valuewrapper") OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2>;

%template(oaCollection_##paramtype1##_##paramtype2##) OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2>;

%extend OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2> {
  // Convert to iterator
  OpenAccess_4::oaIter<ns1::paramtype1> to_iter() {
    return(OpenAccess_4::oaIter<ns1::paramtype1>(*$self));
  }

  // Convert to iterator and then loop through the members
  OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2> each() {
    OpenAccess_4::oaIter<ns1::paramtype1> iter(*$self);
    while (ns1::paramtype1 *object = iter.getNext()) {
      rb_yield(SWIG_NewPointerObj(SWIG_as_voidptr(object), SWIG_TypeDynamicCast($descriptor(ns1::paramtype1*), (void**) &object), 0));
    }    
    return(OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2>(*$self));
  }
 }

/*
%extend OpenAccess_4::oaEnumCollection {
  // Convert to iterator
  OpenAccess_4::oaEnumPropIter to_iter() {
    return(OpenAccess_4::oaEnumPropIter((OpenAccess_4::oaEnumIterState*)*$self));
  }

  // Convert to iterator and then loop through the members
  OpenAccess_4::oaEnumCollection each() {
    OpenAccess_4::oaString &oaStr();
    OpenAccess_4::oaEnumPropIter iter((OpenAccess_4::(oaEnumIterState*)*$self);
    while (iter.getNext(oaStr)) {
      rb_yield(rb_str_new((const oaChar*) oaStr, (const unsigned int)oaStr.getLength()));
    }    
    return(OpenAccess_4::oaEnumCollection(*$self));
  }
 }
*/

%enddef

//////////////////////////////////////////////////////////////////////////////
// Iterator class definitions
//////////////////////////////////////////////////////////////////////////////

#undef ITERCLASS
%define ITERCLASS(ns, paramtype)

%template (oaIter_##paramtype) OpenAccess_4::oaIter<ns##::##paramtype>;

%extend OpenAccess_4::oaIter<ns::paramtype> {
  // Loop through the members
  OpenAccess_4::oaIter<ns::paramtype> each() {
    while (ns::paramtype *object = $self->getNext()) {
      rb_yield(SWIG_NewPointerObj(SWIG_as_voidptr(object), SWIG_TypeDynamicCast($descriptor(ns::paramtype*), (void**) &object), 0));
    }    
    return(OpenAccess_4::oaIter<ns::paramtype>(*$self));
  }
 }

%enddef

//////////////////////////////////////////////////////////////////////////////
// Wrapped ENUM definitions
//////////////////////////////////////////////////////////////////////////////

#undef ENUMWRAPPER
%define ENUMWRAPPER(ns,class,enumns,enumclass)

%rename ("to_i") ns::class::operator enumclass() const;
%alias ns::class::getName "to_s";

%typemap(in) ns::class
{
  if (LANG_INTP($input)) {
    $1 = ns::class((enumns::enumclass) LANG_NUM_TO_INT($input));
  } else if (LANG_SYMBOLP($input)) {
    VALUE rbstr = LANG_SYM_TO_STR($input);
    $1 = ns::class(oaString(RSTRING(rbstr)->ptr));
  } else {
    void *obj;
    if (SWIG_IsOK(SWIG_ConvertPtr($input, &obj, $&1_descriptor, $disown | %convertptr_flags))) {
      $1 = *((ns::class*) obj);
    } else {
      %argument_fail(SWIG_TypeError, "$type", $symname, $argnum); 
    }
  }
}

%typecheck(0) ns::class
{
  void *tmpVoidPtr;
  $1 = LANG_INTP($input) || LANG_SYMBOLP($input) || SWIG_IsOK(SWIG_ConvertPtr($input, &tmpVoidPtr, $&1_descriptor, 0));
}
%enddef

#include <macros/oa_enums.i>
