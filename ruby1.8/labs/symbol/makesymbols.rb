###############################################################################
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   09/07/10  1.0      scarver, Si2     OA Scripting Language Labs
#
###############################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
##############################################################################

# Create Shapes that represent a simple schematic symbol for the leaf cell.
#   (These are not electrical or manufacturing geometries, merely Shapes
#   that comprise the symbol on the schematic.)

def make_fa_symbol
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    label = "FullAdder"
    design = make_sch_sym_view( label )
    block = design.getTopBlock

    # Create a rectangle shape to be the full-adder symbol with the lower left at
    # the origin and the upper right at {60,52}. Use the layer and purpose from
    # the globals singleton object.
    #
    #         3,95_________________159,95
    #            |                 |
    #            |                 |
    #            |                 |
    #            |                 |
    #            |_________________|
    #         3, 3                 159, 3
    #
    box = [ 3, 3, 159, 95 ]
    OaRect.create( block, $GlobalData.num_layer_dev, $GlobalData.num_purpose_dev, box )

    # Use the make_label function to put the cell's name at the location {2,2}
    origin = [ 2, 2 ]
    make_label( block, origin )

    save_close_design( design )
end

# Create Shapes that represent a simple schematic symbol for the leaf cell.
#   (These are not electrical or manufacturing geometries, merely Shapes
#   that comprise the symbol on the schematic.)

def make_ha_symbol
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    label  = "HalfAdder"
    design = make_sch_sym_view( label )
    block  = design.getTopBlock

    # Create a rectangle shape to be the half-adder symbol with the lower left at
    # the origin and the upper right at {64,50}. Use the layer and purpose from
    # the globals singleton object.
    #
    #      0,50__________64,50
    #         |          |
    #         |          |
    #         |          |
    #         |          |
    #         |__________|
    #       0,0          64,0
    #
    box = [ 0, 0, 64, 50 ]
    OaRect.create( block, $GlobalData.num_layer_dev, $GlobalData.num_purpose_dev, box )

    # Use the make_label function to put the cell's name at the location {2,25}
    pt = [ 2, 25 ]
    make_label( block, pt )

    save_close_design( design )
end

def make_and_symbol
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    label = "And"
    design = make_sch_sym_view( label )
    block = design.getTopBlock

    # make a crude AND symbol out of two open Shapes
    #
    #    0,24              16,24
    #       .---------------+
    #       |                    +
    #       |                       +
    #       |                         +
    #       |                          +
    #       |                           +
    #       |                           o <--- midpoint is 28,12
    #       |                           +
    #       |                          +
    #       |                         +
    #       |                       +
    #       |                    +
    #       .---------------+
    #     0,0              16,0

    # Create a PointArray representing the 3-sided, open box part on the left.
    #
    pt0 = [ 16,  0 ]
    pt1 = [  0,  0 ]
    pt2 = [  0, 24 ]
    pt3 = [ 16, 24 ]
    pts = [ pt0, pt1, pt2, pt3 ]

    # Create in the owner Block the appropriate, non-closed oaShape using the array.
    OaLine.create( block, $GlobalData.num_layer_dev, $GlobalData.num_purpose_dev, pts )

    # Create an arc representing the part on the right.
    # To make it easy, assume the curve is simply the right half of
    # an ellipse centered in a bounding Box 12 to the right and 12 left
    #
    #    0,24   4,24       16,24
    #       |   |           |
    #            ---------- + ----------
    #           |                +      |
    #           |                   +   |
    #           |                     + |
    #           |                      +|
    #           |                       +
    #           |                       o <--- midpoint is 28,12
    #           |                       +
    #           |                      +|
    #           |                     + |
    #           |                   +   |
    #           |                +      |
    #            ---------- + ----------
    #       |   |           |
    #     0,0   4,0         16,0

    # Define the BBox for the right arc of the "And" symbol.
    bbox = [ 4, 0, 28, 24 ]

    # Create an appropriate open Shape for this right-hand part of the "And" symbol.
    # Assume the angle sweeps from -90 to 90 degrees to make it easy)
    radiansStart = -1.571
    radiansEnd =    1.571
    OaArc.create( block, $GlobalData.num_layer_dev, $GlobalData.num_purpose_dev,
                     bbox, radiansStart, radiansEnd )

    # Use the make_label function to put the cell's name at the location {2,2}
    pt = [ 2, 2 ]
    make_label( block, pt )

    save_close_design( design )
end


def make_or_symbol
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    label = "Or"
    design = make_sch_sym_view( label )
    block = design.getTopBlock

    # make a crude OR symbol out of two identical arcs, left and right
    # plus two lines joining them top and bottom
    #
    #    0,24               16,24
    #       .---------------+
    #            +               +
    #               +               +
    #                 +               +
    #                  +               +
    #                   +               +
    #         12,12 --> o               o <--- 28,12
    #                   +               +
    #                  +               +
    #                 +               +
    #               +               +
    #            +               +
    #       .---------------+
    #     0,0               16,0

    # Copy the same code that made the oaArc for the AND symbol and assign it to arc_right.
    bbox = [ 4, 0, 28, 24 ]
    arc_right = OaArc.create( block, $GlobalData.num_layer_dev,  $GlobalData.num_purpose_dev,
                              bbox, -1.571,  1.571 )

    # Reuse this arc by making a copy of it in the Block and moving it left by (16,0)
    trans = OaTransform.new( -16, 0 )
    arc_right.copy( trans )

    # Create the top Line from PointArray pa
    pa = [ [ 0, 24 ], [ 16, 24 ] ]
    OaLine.create( block,  $GlobalData.num_layer_dev,  $GlobalData.num_purpose_dev, pa )

    # Rethe = Y values of pa1 to match those of the bottom line of the symbol
    pa = [ [ 0, 0 ], [ 16, 0 ] ]

    # Create another Line reusing pa1
    OaLine.create( block, $GlobalData.num_layer_dev, $GlobalData.num_purpose_dev, pa )

    # Use the make_label function to put the cell's name at the location {2,2}
    pt = [ 2, 2 ]
    make_label( block, pt )

    save_close_design( design )
end


def make_xor_symbol
    # Define a char array with the name to be displayed on the schematic for this symbol.
    #
    label = "Xor"
    design = make_sch_sym_view( label )

    # make a crude XOR symbol that looks just like the OR, moved to the right 6
    # with an extra copy of the arc on the left at the Y-axis position.
    #
    #    0,24                     22,24
    #       +  .  +---------------+ .
    #            +     +               +
    #               +     +               +
    #                 +     +               +
    #                  +     +               +
    #                   +     +               +
    #         12,12 --> o     o <--18,12      o <--- 34,12
    #                   +     +               +
    #                  +     +               +
    #                 +     +               +
    #               +     +               +
    #           .+    .+              .+
    #       +     +---------------+
    #     0,0                     22,0


    # Open the symbol Design of the Or just create above (in read mode).
    scname_lib  = $GlobalData.scname_lib_symbol
    scname_cell = OaScalarName.new( $GlobalData.ns, "Or" )
    scname_view = $GlobalData.scname_view_symbol
    mode =        $GlobalData.mode_read
    view_type =   OaViewType.get( OaReservedViewType.new( "netlist") )
    design_or =   OaDesign.open( scname_lib, scname_cell, scname_view, view_type, mode )

    # Create an iterator for all the Shapes in the Or's Block
    block = design_or.getTopBlock

    saved_arc  = nil
    saved_text = nil

    # Create a loop that retrieves the next Shape.
    block.getShapes.each do |shape|
        # Copy each shape to this Xor Design, moving it to the right by 6,
        # saving the handle of the newly copied Shape in copied_shape.
        trans = OaTransform.new( 6, 0 )
        copied_shape = shape.copy( trans, design )

        # If that shape is an oaArc, then save a pointer to it in saved_arc.
        saved_arc = copied_shape if copied_shape.getType.getName == "Arc"

        # If that shape is the oaText (label) save a pointer to it in saved_text
        saved_text = copied_shape if copied_shape.getType.getName == "Text"
    end

    # At this point the new Xor Design Block has a copy of the symbol
    # shapes created for the Or, but moved to the right by 6, and
    #    - saved_arc is one of the arcs
    #    - saved_text is the Text label

    new_arc_box = [ -12, 0, 12, 24 ]

    # make one more arc in the Xor Design Block by copying that saved_arc, assigning
    # it to the new_arc variable (the position of it doesn't matter, it will be re). =
    trans = OaTransform.new( 0, 0 )
    new_arc = saved_arc.copy( trans )

    # Rethe = Box of new_arc to new_arc_box and the start/stop angles to the same angles
    # obtained from that original saved_arc.
    saved_arc.set( new_arc_box, saved_arc.getStartAngle, saved_arc.getStopAngle )

    # An oaText label was just copied (with the other Shapes) from the Or Cell,
    # so instead of using make_label to add another one, just change the value
    # of that existing oaText (saved from the loop above) from the lib/cell/view
    # name of the Or to that of the Xor.
    saved_text.setText( get_cell_name( design ) )

    save_close_design( design )
end
