#!/usr/bin/env ruby
################################################################################
#
#  Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 (the "License"] you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#   Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   08/27/10  10.0     Si2              Tutorial 10th Edition - Ruby version
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

#  Each PathSeg represented by a - line representing a different Route
#  with the function creating them listed underneath.
#  _________________________________________________________________________
#  |Top                                                                      |
#  |    ___________________    ___________________    ___________________    |
#  |   |Gate               |  |Gate               |  |Gate               |   |
#  |   |             -     |  |             -     |  |             -     |   |
#  |   |             -     |  |             -     |  |             -     |   |
#  |   |  -          -     |  |  -          -     |  |  -          -     |   |
#  |   |  -     NetShapeRoute |  -     NetShapeRoute |  -     NetShapeRoute  |
#  |   |  --               |  |  --               |  |  --               |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  --    OrInst     |  |  --    OrInst     |  |  --    OrInst     |   |
#  |   |  ---   NandInst1  |  |  ---   NandInst1  |  |  ---   NandInst1  |   |
#  |   |  ---   NandInst2  |  |  ---   NandInst2  |  |  ---   NandInst2  |   |
#  |   |  -     InvInst    |  |  -     InvInst    |  |  -     InvInst    |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   | ConnectRouteForNet|  | ConnectRouteForNet|  | ConnectRouteForNet|   |
#  |   |___________________|  |___________________|  |___________________|   |
#  |                                                                         |
#  |        -                                                                |
#  |        -                                                                |
#  |        -                   -                                            |
#  |        -                RouteForNet                                     |
#  |        -                                                                |
#  |    EmptyNets                                                            |
#  |_________________________________________________________________________|

require 'oa'
include Oa


# ****************************** Gratuitous globals ******************************

$mode_read  = 'r'
$mode_write = 'w'

def assert ( condition, linenum )
  puts "***ASSERT error at line num: #{linenum}" unless condition
end

# ****************************** Functions ******************************

def get_layer_purpose (design, layer_name, purp_name )

  tech_file = design.getTech

  unless tech_file
  if layer_name == "pin"
      layer_num = 229;
    elsif layer_name == "device"
      layer_num = 231
    end
    layer_purp = 252
    return true, layer_num, layer_purp
  end

  layer = OaLayer.find( tech_file, layer_name )
  purpose = OaPurpose.find( tech_file, purp_name )

  return false unless layer && purpose

  layer_num = layer.getNumber
  layer_purp = purpose.getNumber

  return true, layer_num, layer_purp
end

# *****************************************************************************
# Create Route with the ObjectArray, attach to the specified net, set end Conns
# *****************************************************************************

def connect_route_for_net (block, net, route_object_array, startpt, endpt)
  route = OaRoute.create( block )
  route.setObjects( route_object_array )
  route.addToNet( net )
  route.setBeginConn( startpt ) if startpt
  route.setEndConn( endpt ) if endpt
end

# *****************************************************************************
# Create oaInstTerm object with the given parameters, which may not be bound.
# *****************************************************************************

def create_instTerm (net, inst, term_name)
  ns = OaNativeNS.new

  # In Ruby it seems that we can't use any subclass of oaName for creating InstTerms.
  #return OaInstTerm.create( net, inst, OaSimpleName.new( ns, term_name) )
  return OaInstTerm.create( net, inst, OaName.new( ns, term_name) )
end

# *****************************************************************************
# Create an InstTerm object for the given terminal with 'term_name' in 'inst'
# *****************************************************************************

def get_instTerm (inst, net, term_name)
  instTerm = create_instTerm( net, inst, term_name )
  return instTerm if instTerm.getTerm

  puts "Inst term for $term_name unbound. Unable to create InstTerm"
  return nil
end

# *****************************************************************************
# Create a Pin with the given name, Points and direction, on 'term'
# *****************************************************************************

def create_pin (block, term, points, name, dir)
  success, layer, purpose = get_layer_purpose( block.getDesign, "pin", "drawing" )
  fig = OaPolygon.create( block, layer, purpose,  points )
  pin = OaPin.create( term, name, dir )
  fig.addToPin( pin )
  return pin
end


# *****************************************************************************
# Create a Net with the given name in the given Block.
# *****************************************************************************

def create_net (block, str)
  ns = OaNativeNS.new
  name = OaScalarName.new( ns, str )
  return OaScalarNet.create( block, name )
end


# *****************************************************************************
# Create a Term with the given name on the given Net.
# *****************************************************************************

def create_term (net, str, type)
  ns = OaNativeNS.new
  name = OaScalarName.new( ns, str )
  return OaScalarTerm.create( net, name, type )
end


# ***************************************************************************
# create_net_shape_route()
# ***************************************************************************

def create_net_shape_route (design, block, refx, refy)
  # Create path shape, route, terminal and net
  net = create_net( block, "ShapeRouteNet" )
  create_term( net, "TermOnShapeRouteNet", OacInputTermType )

  begin_ext = 1
  end_ext   = 1
  width    = 2
  success, lnum, pnum = get_layer_purpose( design, "device", "drawing1")

  end_style = OaEndStyle.new( "variable" )
  seg_style = OaSegStyle.new( width, end_style, end_style, begin_ext, end_ext )

  point1 = OaPoint.new( refx, refy )
  point2 = OaPoint.new( refx+100, refy )
  seg1 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array1 = [ seg1 ]

  route1 = OaRoute.create( block )
  route1.setObjects( route_object_array1 )

  puts "Unconnected route is created in Gate view"

  # Creating the shape: Ellipse

  centerx = refx+100
  centery = refy

  ell_rect = [ centerx-10, centery-10, centerx+10, centery+10 ]

  fig = OaEllipse.create( block, lnum, pnum, ell_rect )
  fig.addToNet( net )

  puts "An Ellipse is created in the Gate view"

  route1.addToNet( net )
  route1.setEndConn( fig )

  startx = centerx
  starty = centery

  point1 = OaPoint.new( startx, starty )
  point2 = OaPoint.new( startx+50, starty )
  seg2 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array2 = [ seg2 ]

  route2 = OaRoute.create( block )
  route2.setObjects( route_object_array2 )
  route2.addToNet( net )
  route2.setBeginConn( fig )

  p1 = OaPoint.new( startx+55, starty )
  p2 = OaPoint.new( startx+65, starty )
  ptarray = [p1, p2]

  netPath = OaPath.create( block, lnum, pnum, 1, ptarray )
  netPath.addToNet( net )

  puts "Net with Shape Route is created in Gate view"

  point1 = OaPoint.new( startx+70, starty+5 )
  point2 = OaPoint.new( startx+70, starty+20 )

  seg3 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array3 = [seg3]

  route3 = OaRoute.create( block )
  route3.setObjects( route_object_array3 )

  p1 = OaPoint.new( startx+70, starty-5 )
  p2 = OaPoint.new( startx+70, starty-20 )
  ptarray2 = [p1, p2]

  OaPath.create( block, lnum, pnum, 1, ptarray2 )
end


# *****************************************************************************
# Create a ScalarInst in a Block with the given cellname. The View is
# assumed to be 'schematic for this purpose. Open the Design if it is not
# already open in the current session.
# *****************************************************************************

def create_instance (block, trans, cellname, inst_str, scname_lib)
  ns = OaNativeNS.new
  gate = OaScalarName.new( ns, cellname )
  view = OaScalarName.new( ns, "schematic" )
  master = OaDesign.find( scname_lib, gate, view )
  master = OaDesign.open( scname_lib, gate, view, $mode_read ) unless master

  return nil unless master

  instname = OaScalarName.new( ns, inst_str )
  return OaScalarInst.create( block, master, instname, trans )
end


# *****************************************************************************
# Create a gate schematic which contains 4 gates, (Or, 2 Nand, and an Inv) and
# connect the gates with physical and logical elements. Routes are used for
# physical connections. The created Design is represented by 'Lib' 'Gate'
# 'schematic' (lib, cell and view names), respectively
# *****************************************************************************

def create_gate_schematic (scname_lib)
  ns = OaNativeNS.new
  cell = OaScalarName.new( ns, "Gate" )
  view = OaScalarName.new( ns, "schematic" )

  puts "Creating Gate schematic with 0r, 2 nand, Inv instances"

  vt_sch = OaViewType.get( OaReservedViewType.new('schematic') )

  design = OaDesign.open( scname_lib, cell, view, vt_sch, $mode_write )
  design.setCellType( OaCellType.new( "softMacro" ) )
  block = OaBlock.create( design )

  trans = OaTransform.new( 0, 0 )
  or_inst = create_instance( block, trans, "Or", "OrInst", scname_lib )

  or_inst.getBBox( orBox=OaBox.new )

  height = orBox.getHeight
  width  = orBox.getWidth

  x_mid = orBox.left
  y_mid = orBox.bottom + height / 2

  trans = OaTransform.new( 0, (0-2 * height) )
  nand1 = create_instance( block, trans, "Nand", "NandInst1", scname_lib )

  nand1.getBBox( nand1Box=OaBox.new )
  y_mid2 = nand1Box.bottom+height / 2

  net1 = create_net( block, "N1" )
  bbox = [0, 0, 0, 0]
  stein1 = OaSteiner.create( block, bbox )

  begin_ext = 0
  end_ext   = 0
  width_rt  = 2

  success, lnum, pnum = get_layer_purpose( design, "device", "drawing1")

  end_style = OaEndStyle.new( "truncate" )
  seg_style = OaSegStyle.new( width_rt, end_style, end_style, begin_ext, end_ext )

  point1 = OaPoint.new( x_mid-50, y_mid+10 )
  point2 = OaPoint.new( x_mid-15, y_mid+10 )
  seg1 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array1 = [seg1]

  ptarray = [ OaPoint.new(x_mid-70, y_mid+15), \
              OaPoint.new(x_mid-55, y_mid+15), \
              OaPoint.new(x_mid-50, y_mid+10), \
              OaPoint.new(x_mid-55, y_mid+5), \
              OaPoint.new(x_mid-70, y_mid+5) ]

  term1 = create_term( net1, "a", OacInputTermType )

  pin1 = create_pin( block, term1, ptarray, "P1-In", OacLeft )
  connect_route_for_net( block, net1, route_object_array1, pin1, stein1 )

  point1 = OaPoint.new( x_mid-15, y_mid+10 )
  point2 = OaPoint.new( x_mid,    y_mid+10 )
  seg2 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array2 =  [seg2]

  instTerm = get_instTerm( or_inst, net1, "T1" )

  connect_route_for_net( block, net1, route_object_array2, stein1, instTerm )

  point1 = OaPoint.new( x_mid-15, y_mid+10 )
  point2 = OaPoint.new( x_mid-15, y_mid2+10 )
  seg3 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  point1 = OaPoint.new( x_mid-15, y_mid2+10 )
  point2 = OaPoint.new( x_mid ,   y_mid2+10 )
  seg4 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array3 =  [seg3, seg4]

  instTerm = get_instTerm( nand1, net1, "T1" )
  connect_route_for_net( block, net1, route_object_array3, stein1, instTerm )

  net2 = create_net( block, "N2" )
  bbox =  OaBox.new( 0, 0, 0, 0 )
  stein2 = OaSteiner.create block, bbox

  point1 = OaPoint.new( x_mid-50, y_mid-10 )
  point2 = OaPoint.new( x_mid-25, y_mid-10 )
  seg5 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array4 =  [seg5]

  term2 = create_term( net2, "b", OacInputTermType )

  ptarray2 = [ OaPoint.new( x_mid-70, y_mid-5 ),  \
               OaPoint.new( x_mid-55, y_mid-5 ),  \
               OaPoint.new( x_mid-50, y_mid-10 ), \
               OaPoint.new( x_mid-55, y_mid-15 ), \
               OaPoint.new( x_mid-70, y_mid-15 )]

  pin2 = create_pin( block, term2, ptarray2, "P2-In", OacLeft )

  connect_route_for_net( block, net2, route_object_array4, pin2, stein2 )

  point1 = OaPoint.new( x_mid-25, y_mid-10 )
  point2 = OaPoint.new( x_mid,    y_mid-10 )
  seg6 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array5 =  [seg6]

  instTerm = get_instTerm( or_inst, net2, "T2" )

  connect_route_for_net( block, net2, route_object_array5, stein2, instTerm )

  point1 = OaPoint.new( x_mid-25, y_mid-10 )
  point2 = OaPoint.new( x_mid-25, y_mid2-10 )
  seg7 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  point1 = point2
  point2 = OaPoint.new( x_mid, y_mid2-10 )
  seg8 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array6 =  [seg7, seg8]

  instTerm = get_instTerm( nand1, net2, "T2" )
  connect_route_for_net( block, net2, route_object_array6, stein2, instTerm )

  x_pt  = x_mid+width
  y_pt  = y_mid
  y_pt2 = y_mid2
  y_pt3 = (y_pt+y_pt2)/2

  y_nand2 =  (y_pt+y_pt2)/2-height/2
  # 0, 0 of nand gate is 65 units inside in x from lower left corner of its bBox.
  x_nand2 =  x_pt+40+65

  trans = OaTransform.new( x_nand2, y_nand2 )
  nand2 = create_instance( block, trans, "Nand", "NandInst2", scname_lib )

  net3 = create_net( block, "N3" )

  instTerm1 = get_instTerm( or_inst, net3, "T3" )
  instTerm2 = get_instTerm( nand2, net3, "T1"  )

  point1 =  OaPoint.new( x_pt,    y_pt )
  point2 =  OaPoint.new( x_pt+20, y_pt )
  seg9 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  point1 = point2
  point2 = OaPoint.new( x_pt+20, y_pt3+10 )
  seg10 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  point1 = point2
  point2 = OaPoint.new( x_pt+40, y_pt3+10 )
  seg11 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array7 =  [ seg9, seg10, seg11 ]

  connect_route_for_net( block, net3, route_object_array7, instTerm1, instTerm2 )

  net4 = create_net( block, "N4" )

  instTerm1 = get_instTerm( nand1, net4, "T3" )
  instTerm2 = get_instTerm( nand2, net4, "T2" )


  point1 = OaPoint.new( x_pt,    y_pt2 )
  point2 = OaPoint.new( x_pt+20, y_pt2 )
  seg12 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  point1 = point2
  point2 = OaPoint.new( x_pt+20, y_pt3-10 )
  seg13 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  point1 = point2
  point2 = OaPoint.new( x_pt+40, y_pt3-10 )
  seg14 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array8 = [ seg12, seg13, seg14 ]

  connect_route_for_net( block, net4, route_object_array8, instTerm1, instTerm2 )

  xfto9 = x_pt+40+width
  yfto9 = (y_pt+y_pt2) / 2
  net5 = create_net( block, "N5" )

  point1 = OaPoint.new( xfto9,    yfto9 )
  point2 = OaPoint.new( xfto9+30, yfto9 )
  seg15 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array9 =  [seg15]

  y_inv = yfto9-25
  # (0, 0} of Inv gate is 35 units inside in x from lower left corner of its bBox.
  x_inv =  xfto9+30+35

  trans = OaTransform.new( x_inv, y_inv )
  inv = create_instance( block, trans, "Inv", "InvInst", scname_lib )
  # create_instTerm(net5, inv, "T1")

  instTerm1 = get_instTerm( nand2, net5, "T3" )
  instTerm2 = get_instTerm( inv, net5, "T1" )
  connect_route_for_net( block, net5, route_object_array9, instTerm1, instTerm2 )

  point1 = OaPoint.new( xfto9+15, yfto9 )
  point2 = OaPoint.new( xfto9+15, yfto9-90 )
  seg16 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array10 =  [seg16]

  dangling_term = create_term( net5, "Term-open", OacOutputTermType )
  connect_route_for_net( block, net5, route_object_array10, nil, nil )

  xpt = xfto9+15
  ypt = yfto9-90

  ptarray3 =  [ OaPoint.new(xpt+5, ypt-20), \
                OaPoint.new(xpt+5, ypt-5),  \
                OaPoint.new(xpt,   ypt),    \
                OaPoint.new(xpt-5, ypt-5),  \
                OaPoint.new(xpt-5, ypt-20)]

  create_pin( block, dangling_term, ptarray3, "P-Out", OacBottom  )

  xfto10 = xfto9+30+160
  yfto10 = yfto9

  net6 = create_net( block, "N6" )

  x_pin_pt =  xfto10+30
  ptarray4 = [ OaPoint.new(x_pin_pt+20, yfto10+5), \
               OaPoint.new(x_pin_pt+ 5, yfto10+5), \
               OaPoint.new(x_pin_pt,    yfto10),   \
               OaPoint.new(x_pin_pt+ 5, yfto10-5), \
               OaPoint.new(x_pin_pt+20, yfto10-5)]

  term3 = create_term( net6, "SUM", OacOutputTermType )
  pin4 =  create_pin( block, term3, ptarray4, "P3-Out", OacRight )

  instTerm = get_instTerm( inv, net6, "T2" )

  point1 =  OaPoint.new( xfto10,    yfto10 )
  point2 =  OaPoint.new( xfto10+30, yfto10 )
  seg17 = OaPathSeg.create( block, lnum, pnum, point1, point2, seg_style )

  route_object_array11 =  [seg17]

  connect_route_for_net( block, net6, route_object_array11, instTerm, pin4 )

  create_net_shape_route( design, block, x_mid, yfto9 )

  design.save
  design.close
end


# *****************************************************************************
# Create an oaPath element between 'start' and 'end'. Also create a
# Rect Fig and oaPin object with the Rect if 'addpin' is true
# *****************************************************************************

def create_fig_for_net (design, block, term, net, startpt, endpt, pinName, addpin=true)

  success, lnum, pnum = get_layer_purpose( design, "device", "drawing1")

  ptarray = [startpt]

  if (endpt.x != startpt.x) && (endpt.x > startpt.x)
    ptarray << OaPoint.new( startpt.x, endpt.y ) if endpt.y != startpt.y
  elsif (endpt.x != startpt.x) && (endpt.x < startpt.x)
    ptarray << OaPoint.new( endpt.x, startpt.y ) if endpt.y != startpt.y
  end

  ptarray << endpt
  path = OaPath.create( block, lnum, pnum, 2, ptarray )
  path.addToNet( net )

  if addpin
    dir = ""
    pt1 = endpt
    pt2 = endpt

    if term.getTermType.getName == "input"
      pt1 += OaPoint.new( -2, -2 )
      dir = OacLeft
    elsif term.getTermType.getName == "output"
      pt2 += OaPoint.new( 2, 2 )
      dir = OacRight
    end
    box = OaBox.new( pt1, pt2 )
    rect = OaRect.create( block, lnum, pnum, box )
    pin  = OaPin.create(  term, pinName, dir )
    rect.addToPin( pin )
  end
  return path
end


# ***************************************************************************
# Create a few Nets in the top Block with some Routes associated.
# ***************************************************************************

def create_empty_nets (top, top_block, box)
  refx = box.right
  refy = box.top

  # A Net with one terminal but without connection to top
  net1 = create_net( top_block, "EmptyNet1" )
  create_term( net1, "TermEmptyNet", OacInputTermType )

  puts "An empty net with one terminal is created"

  # Creating Net that has route->shape->route connection

  rec_box = OaBox.new( refx+50, refy,  refx+70,  refy+ 20 )

  begin_ext = 0
  end_ext   = 0
  width    = 2
  success, lnum, pnum = get_layer_purpose( top, "device", "drawing1" )

  end_style = OaEndStyle.new( "truncate" )
  seg_style = OaSegStyle.new( width, end_style, end_style, begin_ext, end_ext )

  rect = OaRect.create( top_block, lnum, pnum, rec_box )

  point1 = OaPoint.new( refx,    refy+10 )
  point2 = OaPoint.new( refx+50, refy+10 )
  seg1 = OaPathSeg.create( top_block, lnum, pnum, point1, point2, seg_style )

  route_object_array1 =  [seg1]
  route1 = OaRoute.create( top_block )
  route1.setObjects( route_object_array1 )

  point1 = OaPoint.new( refx+60, refy+10 )
  point2 = OaPoint.new( refx+90, refy+10 )
  seg2 = OaPathSeg.create( top_block, lnum, pnum, point1, point2, seg_style )

  route_object_array2 =  [seg2]
  route2 = OaRoute.create( top_block )
  route2.setObjects( route_object_array2 )

  route1.setEndConn(   rect )
  route2.setBeginConn( rect )

  route_net1 = create_net( top_block, "RouteNet1" )
  rect.addToNet(   route_net1 )
  route1.addToNet( route_net1 )
  route2.addToNet(   route_net1 )

  puts "A net with 2 routes and a shape is created"

  # A net with route->route

  point1 = OaPoint.new( refx+100, refy )
  point2 = OaPoint.new( refx+120, refy )
  seg3 = OaPathSeg.create( top_block, lnum, pnum, point1, point2, seg_style )

  route_object_array3 =  [seg3]
  route3 = OaRoute.create( top_block )
  route3.setObjects( route_object_array3 )

  point1 = OaPoint.new( refx+120, refy-20 )
  point2 = OaPoint.new( refx+120, refy+20 )
  seg4 = OaPathSeg.create( top_block, lnum, pnum, point1, point2, seg_style )

  route_object_array4 =  [seg4]
  route4 = OaRoute.create( top_block )
  route4.setObjects( route_object_array4 )

  point1 = OaPoint.new( refx+120, refy-10 )
  point2 = OaPoint.new( refx+130, refy-10 )
  seg5 = OaPathSeg.create( top_block, lnum, pnum, point1, point2, seg_style )

  route_object_array5 =  [seg5]
  route5 = OaRoute.create( top_block )
  route5.setObjects( route_object_array5 )

  route_net2 = create_net( top_block, "RouteNet2" )
  route3.addToNet( route_net2 )
  route4.addToNet( route_net2 )
  route5.addToNet( route_net2 )

  puts "A net with 2 routes connecting to each other is created"
end


# *****************************************************************************
# Creates a hierarchy in the top level Design with cellname "Sample".
# It contains three Inst of the Design with the cellname "Gate" and
# connects the input and output Terms of the gates. Logical and physical
# connections are also created. The Design has 4 input and an 1 output Terms.
# *****************************************************************************

def create_hierarchy (scname_lib)

  ns = OaNativeNS.new
  cell  = OaScalarName.new( ns, "Sample" )
  cell2 = OaScalarName.new( ns, "Gate" )
  view  = OaScalarName.new( ns, "schematic" )

  type = OaViewType.get( OaReservedViewType.new('schematic') )

  puts "\nCreating Sample schematic with 3 Gate instances"
  top = OaDesign.open( scname_lib, cell, view, type, $mode_write )
  top_block = OaBlock.create( top )

  top.setCellType( OaCellType.new( "softMacro") )
  master = OaDesign.open( scname_lib, cell2, view, type, $mode_read )
  master_block = master.getTopBlock

  master_block.getBBox( bbox=OaBox.new )

  width  = bbox.getWidth
  height = bbox.getHeight

  trans = OaTransform.new(  -height-5, 0, OacR90 )
  inst1 = create_instance( top_block, trans, "Gate", "Inst1", scname_lib )

  trans = OaTransform.new( height+5, 0, OacR90 )
  inst2 = create_instance( top_block, trans, "Gate", "Inst2", scname_lib )

  trans = OaTransform.new( 0, width+20 )
  inst3 = create_instance( top_block, trans, "Gate", "Inst3", scname_lib )

  net1  = create_net( top_block, "N1" )
  term1 = create_term( net1, "T1", OacInputTermType )

  inst1.getBBox( box=OaBox.new )

  xEnd = box.left
  yEnd = box.bottom

  point1 = OaPoint.new( xEnd+20, yEnd )
  point2 = OaPoint.new( xEnd+20, yEnd-50 )
  create_fig_for_net( top, top_block, term1, net1, point1, point2, "P-1" )

  pin = OaPin.find( term1, "P-1" )
  instTerm = get_instTerm( inst1, net1, 'a' )

  begin_ext = 0
  end_ext   = 0
  width_rt  = 2
  success, lnum, pnum = get_layer_purpose( top, "device", "drawing1")

  end_style = OaEndStyle.new( "truncate" )
  seg_style = OaSegStyle.new( width_rt, end_style, end_style, begin_ext, end_ext )

  point1 = OaPoint.new( xEnd+20, yEnd )
  point2 = OaPoint.new( xEnd+20, yEnd-50 )
  seg1 = OaPathSeg.create( top_block, lnum, pnum, point1, point2, seg_style )

  route_object_array_elements =  [seg1]

  connect_route_for_net( top_block, net1, route_object_array_elements, instTerm, pin )

  net2 = create_net( top_block, "N2" )
  term2 = create_term( net2, "T2", OacInputTermType )
  create_instTerm( net2, inst1, 'b' )

  point1 = OaPoint.new( xEnd+40, yEnd )
  point2 = OaPoint.new( xEnd+40, yEnd-50 )
  create_fig_for_net( top, top_block, term2, net2, point1, point2, "P-2" )

  net3 = create_net( top_block, "N3" )
  create_instTerm( net3, inst1, "SUM" )

  box.getCenter( center=OaPoint.new )

  inst3.getBBox( box=OaBox.new )

  xEnd3 = box.left
  yEnd3 = box.bottom

  box.getCenter( center3=OaPoint.new )

  point1 = center + OaPoint.new( 0,  width/2 )
  point2 = OaPoint.new( xEnd3, yEnd3+height-20 )
  create_fig_for_net( top, top_block, nil, net3, point1, point2, "", false )

  net4 = create_net( top_block, "N4" )
  term3 = create_term( net4, "T3",  OacInputTermType )
  create_instTerm( net4, inst2, "a" )

  inst2.getBBox( box )
  xEnd2 = box.left
  yEnd2 = box.bottom

  box.getCenter( center2=OaPoint.new )

  point1 = OaPoint.new( xEnd2+20, yEnd2 )
  point2 = OaPoint.new( xEnd2+20, yEnd2-50 )
  create_fig_for_net( top, top_block, term3, net4, point1, point2, "P-3" )

  net5 = create_net( top_block, "N5" )
  term4 = create_term( net5, "T4", OacInputTermType )
  create_instTerm( net5, inst2, 'b' )

  point1 = OaPoint.new( xEnd2+40, yEnd2 )
  point2 = OaPoint.new( xEnd2+40, yEnd2-50 )
  create_fig_for_net( top, top_block, term4, net5, point1, point2, "P-4" )

  net6 = create_net( top_block, "N6" )
  create_instTerm( net6, inst2, "SUM" )

  point1 = center2 + OaPoint.new( 0, width/2 )
  point2 = OaPoint.new( xEnd3, yEnd3+height-40 )
  create_fig_for_net( top, top_block, nil, net6, point1, point2, "", false )

  create_instTerm( net3, inst3, 'a' )
  create_instTerm( net6, inst3, 'b' )

  net7 = create_net( top_block, "N7" )
  term5 = create_term( net7, "T5", OacOutputTermType )
  create_instTerm( net7, inst3, "SUM" )

  point1 = center3 + OaPoint.new( width/2,    0 )
  point2 = center3 + OaPoint.new( width/2+50, 0 )
  create_fig_for_net( top, top_block, term5, net7, point1, point2, "P-5" )
  create_empty_nets( top, top_block, box )

  top.save
  top.close
end


# *****************************************************************************
# Creates the OR schematic Design.
# *****************************************************************************

def create_or_schematic (scname_lib)

  ns = OaNativeNS.new
  cell = OaScalarName.new( ns, "Or" )
  view_str = OaScalarName.new( ns, "schematic" )

  vt_sch = OaViewType.get( OaReservedViewType.new('schematic') )

  view = OaDesign.open( scname_lib, cell, view_str, vt_sch, $mode_write )

  block = OaBlock.create( view )

  layer = "pin"
  purpose = "drawing"

  pin_layer = 0
  pin_purp  = 0

  success, pin_layer, pin_purp = get_layer_purpose( view, layer, purpose)
  assert( success, __LINE__ )

  pointArray1 = [ OaPoint.new(-50,50), OaPoint.new(15,50) ]

  path = OaPath.create( block, pin_layer, pin_purp, 2, pointArray1 )
  assert( path.isValid, __LINE__ )

  name = OaScalarName.new( ns, "TopTail" )
  net = OaScalarNet.create( block, name )
  assert( net.isValid, __LINE__ )
  path.addToNet( net )

  term_name = OaScalarName.new( ns, "T1" )
  term = OaScalarTerm.create( net, term_name, OacInputTermType )
  assert( term.isValid, __LINE__ )

  box =  OaBox.new( -65, 46, -50, 54 )
  rect = OaRect.create( block, pin_layer, pin_purp, box )
  assert( rect.isValid, __LINE__ )

  pin = OaPin.create( term, "IN1", OacLeft )
  rect.addToPin( pin )
  assert( pin.isValid, __LINE__ )

  pointArray2 = [ OaPoint.new(-50,20), OaPoint.new(15,20) ]

  path = OaPath.create( block, pin_layer, pin_purp, 2, pointArray2 )
  assert( path.isValid, __LINE__ )

  name = OaScalarName.new( ns, "BotTail" )
  net = OaScalarNet.create( block, name )
  assert( net.isValid, __LINE__ )
  path.addToNet( net )

  term_name = OaScalarName.new( ns, "T2" )
  term = OaScalarTerm.create( net, term_name, OacInputTermType )
  assert( term.isValid, __LINE__ )

  box =  OaBox.new( -65, 16, -50, 24 )
  rect = OaRect.create( block, pin_layer, pin_purp, box )
  assert( rect.isValid, __LINE__ )

  pin = OaPin.create( term, "IN2", OacLeft )
  rect.addToPin( pin )
  assert( pin.isValid, __LINE__ )

  success, dr_layer, dr_purp = get_layer_purpose( view, "device", "drawing2" )
  assert( success, __LINE__ )

  ptarray3 = [ OaPoint.new(0,0),    \
               OaPoint.new(50,0),   \
               OaPoint.new(80,10),  \
               OaPoint.new(100,35), \
               OaPoint.new(80,60),  \
               OaPoint.new(50,70),  \
               OaPoint.new(0,70),   \
               OaPoint.new(14,55),  \
               OaPoint.new(20,35),  \
               OaPoint.new(14,15) ]

  gate = OaPolygon.create( block, dr_layer, dr_purp, ptarray3 )
  assert( gate.isValid, __LINE__ )

  ptarray5 =  [ OaPoint.new(100,35), OaPoint.new(140,35) ]

  path2 = OaPath.create( block, pin_layer, pin_purp, 2, ptarray5 )
  assert( path2.isValid, __LINE__ )

  name = OaScalarName.new( ns, "OutTail" )
  net2 = OaScalarNet.create( block, name )
  assert( net2.isValid, __LINE__ )

  path2.addToNet( net2 )
  term_name = OaScalarName.new( ns, "T3" )
  term2 = OaScalarTerm.create( net2, term_name, OacOutputTermType )

  box =  OaBox.new( 140, 31, 155, 39 )
  rect2 = OaRect.create( block, pin_layer, pin_purp, box )
  assert( rect2.isValid, __LINE__ )

  pin2 = OaPin.create( term2, "OUT", OacRight )
  rect2.addToPin( pin2 )
  assert( pin2.isValid, __LINE__ )

  view.save
  view.close
end


# *****************************************************************************
# Creates the NANE schematic Design
# *****************************************************************************

def create_nand_schematic (scname_lib)
  ns = OaNativeNS.new
  cell = OaScalarName.new( ns, "Nand" )
  view_str = OaScalarName.new( ns, "schematic" )

  vt_sch = OaViewType.get( OaReservedViewType.new('schematic') )

  view = OaDesign.open( scname_lib, cell, view_str, vt_sch, $mode_write )
  block = OaBlock.create( view )

  assert( view.isValid, __LINE__ )

  layer = "pin"
  purpose = "drawing"

  success, pin_layer, pin_purp = get_layer_purpose( view, layer, purpose )
  assert( success, __LINE__ )

  success, dr_layer, dr_purp = get_layer_purpose( view, "device", "drawing" )
  assert( success, __LINE__ )

  ptarray1 =  [ OaPoint.new(-50,50), OaPoint.new(0,50) ]

  line = OaLine.create( block, dr_layer, dr_purp, ptarray1 )
  assert( line.isValid, __LINE__ )

  netName1 = OaScalarName.new( ns, "TopTail" )
  net = OaScalarNet.create( block, netName1 )
  assert( net.isValid, __LINE__ )
  line.addToNet( net )

  term_name1 = OaScalarName.new( ns, "T1" )
  term = OaScalarTerm.create( net, term_name1, OacInputTermType )
  assert( term.isValid, __LINE__ )

  box =  OaBox.new( -65, 46, -50, 54 )
  rect = OaRect.create( block, pin_layer, pin_purp, box )
  assert( rect.isValid, __LINE__ )

  pin = OaPin.create( term, "IN1", OacLeft )
  rect.addToPin( pin )
  assert( pin.isValid, __LINE__ )

  ptarray2 = [ OaPoint.new(-50,20), OaPoint.new(0,20) ]

  line = OaLine.create( block, dr_layer, dr_purp, ptarray2 )
  assert( line.isValid, __LINE__ )

  netName2 = OaScalarName.new( ns, "BotTail" )
  net = OaScalarNet.create( block, netName2 )
  assert( net.isValid, __LINE__ )
  line.addToNet( net )

  term_name2 = OaScalarName.new( ns, "T2" )
  term = OaScalarTerm.create( net, term_name2, OacInputTermType )
  assert( term.isValid, __LINE__ )

  box =  OaBox.new( -65, 16, -50, 24 )
  rect = OaRect.create( block, pin_layer, pin_purp, box )
  assert( rect.isValid, __LINE__ )

  pin = OaPin.create( term, "IN2", OacLeft )
  rect.addToPin( pin )
  assert( pin.isValid, __LINE__ )

  ptarray3 = [ OaPoint.new(0,0),    \
               OaPoint.new(80,0),   \
               OaPoint.new(100,20), \
               OaPoint.new(100,50), \
               OaPoint.new(80,70),  \
               OaPoint.new(0,70) ]

  gate = OaPolygon.create( block, dr_layer, dr_purp, ptarray3 )
  assert( gate.isValid, __LINE__ )

  ptarray4 = [ OaPoint.new(100,40),  \
               OaPoint.new(100,30),  \
               OaPoint.new(110,26),  \
               OaPoint.new(118,35),  \
               OaPoint.new(110,44) ]

  bubble = OaPolygon.create( block, dr_layer, dr_purp, ptarray4 )
  assert( bubble.isValid, __LINE__ )

  ptarray5 =  [ OaPoint.new(118,35), OaPoint.new(140,35) ]

  line2 = OaLine.create( block, dr_layer, dr_purp, ptarray5 )
  assert( line2.isValid, __LINE__ )

  netName3 = OaScalarName.new( ns, "OutTail" )
  net2 = OaScalarNet.create( block,  netName3 )
  assert( net2.isValid, __LINE__ )

  line2.addToNet( net2 )
  term_name3 = OaScalarName.new( ns, "T3" )
  term2 = OaScalarTerm.create( net2, term_name3, OacOutputTermType )

  box =  OaBox.new( 140, 31, 155, 39 )
  rect2 = OaRect.create( block, pin_layer, pin_purp, box )
  assert( rect2.isValid, __LINE__ )

  pin2 = OaPin.create( term2, "OUT", OacRight )
  rect2.addToPin( pin2 )
  assert( pin2.isValid, __LINE__ )

  view.save
  view.close
end


# *****************************************************************************
# Create the INV schematic Design
# *****************************************************************************

def create_inv_schematic (scname_lib)

  ns = OaNativeNS.new
  cell = OaScalarName.new( ns, "Inv" )
  view_str = OaScalarName.new( ns, "schematic" )

  type = OaViewType.get( OaReservedViewType.new('schematic') )

  view = OaDesign.open( scname_lib, cell, view_str, type, $mode_write )
  block = OaBlock.create( view )

  assert( view.isValid, __LINE__ )

  layer = "pin"
  purpose = "drawing"

  pin_layer = 0
  pin_purp =  0

  success, pin_layer, pin_purp = get_layer_purpose( view, layer, purpose )
  assert( success, __LINE__ )

  ptarray1 = [ OaPoint.new(-20,25), OaPoint.new(0,25) ]

  path = OaPath.create( block, pin_layer, pin_purp, 2, ptarray1 )
  assert( path.isValid, __LINE__ )

  name = OaScalarName.new( ns, "TopTail" )
  net = OaScalarNet.create( block, name )
  assert( net.isValid, __LINE__ )
  path.addToNet( net )

  term_name = OaScalarName.new( ns, "T1" )
  term = OaScalarTerm.create( net, term_name, OacInputTermType )
  assert( term.isValid, __LINE__ )

  box = OaBox.new( -35, 21, -20, 29 )
  rect = OaRect.create( block, pin_layer, pin_purp, box )
  assert( rect.isValid, __LINE__ )

  pin = OaPin.create( term, "IN1", OacLeft )
  rect.addToPin( pin )
  assert( pin.isValid, __LINE__ )

  success, dr_layer, dr_purp = get_layer_purpose( view, "device", "drawing2" )
  assert( success, __LINE__ )

  ptarray3 = [ OaPoint.new(0,0), OaPoint.new(0,50), OaPoint.new(70,25) ]

  gate = OaPolygon.create( block, dr_layer, dr_purp, ptarray3 )
  assert( gate.isValid, __LINE__ )

  ptarray4 = [ OaPoint.new(70,30), \
               OaPoint.new(70,20), \
               OaPoint.new(80,16), \
               OaPoint.new(88,25), \
               OaPoint.new(80,34) ]

  bubble = OaPolygon.create( block, dr_layer, dr_purp, ptarray4 )
  assert( bubble.isValid, __LINE__ )

  ptarray5 = [ OaPoint.new(88,25), OaPoint.new(110,25) ]

  path2 = OaPath.create( block, pin_layer, pin_purp, 2, ptarray5 )
  assert( path2.isValid, __LINE__ )

  name = OaScalarName.new( ns, "OutTail" )
  net2 = OaScalarNet.create( block, name )
  assert( net2.isValid, __LINE__ )

  path2.addToNet( net2 )
  term_name = OaScalarName.new( ns, "T2" )
  term2 = OaScalarTerm.create( net2, term_name, OacOutputTermType )

  box =  OaBox.new( 110, 21, 125, 29 )
  rect2 = OaRect.create( block, pin_layer, pin_purp, box )
  assert( rect2.isValid, __LINE__ )

  pin2 = OaPin.create( term2, "OUT", OacRight )
  rect2.addToPin( pin2 )
  assert( pin2.isValid, __LINE__ )

  view.save
  view.close
end



def open_library
  ns = OaUnixNS.new
  lib = nil

  scname_lib = OaScalarName.new( ns, ARGV[0] )
  str_path_lib = ARGV[1]
  scname_lib.get( ns, strname_lib='' )

  lib = OaLib.find( scname_lib )
  puts "***ERROR error lib is already open" if lib


  # The following commented out code illustrates how the API locates a lib.defs file.
  # Unfortunately, the results are unpredictable because the current user may or may
  # not have a lib.defs file either in the current directory or in $HOME.
  # It has been commented out so that automated run/check tests can predict the output.
  # If you are a user is interested in this, please feel free to uncomment this section.
  #OaLibDefList.getDefaultPath( libdefs_file='' )

  #if libdefs_file == ''
  #  puts "No libdefs file"
  #else
  #  puts "Existing lib.defs file"
  #end

  ldl = OaLibDefList.get( "lib.defs", 'w' )

  if OaLib.exists( str_path_lib )
    lib = OaLib.open( scname_lib, str_path_lib )
    puts "Opened existing #{strname_lib} in #{str_path_lib}"
  else
    lib = OaLib.create( scname_lib, str_path_lib, OaLibMode.new( OacSharedLibMode ), "oaDMFileSys" )
    puts "Created #{strname_lib} in #{str_path_lib}"
  end

  OaLibDef.create( ldl, scname_lib, str_path_lib )
  ldl.save

  puts "  Adding def for #{strname_lib} to #{str_path_lib} to newly created libdefs file\n"

  puts "***ERROR lib is not valid" unless lib.isValid
  puts "***ERROR lib does not exist" unless OaLib.exists( str_path_lib )

  return scname_lib
end

# ****************************** MAIN ******************************

begin

  if ARGV.length != 2
    puts "\nCommand line arguments required: LibName  LibDirectoryPath\n\n"
    exit 1
  end

  oaDesignInit

  scname_lib = open_library

  create_inv_schematic( scname_lib )
  create_or_schematic( scname_lib )
  create_nand_schematic( scname_lib )
  create_gate_schematic( scname_lib )
  create_hierarchy( scname_lib )

  puts "......Normal completion......"

  rescue OaException => excp
    puts "Caught exception: #{excp}"
    excp.backtrace.each do | stack_item |
      puts "  #{stack_item.to_s}"
    end

end
