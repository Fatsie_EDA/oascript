###############################################################################
#
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   07/28/2010: Initial copy
#
###############################################################################

module Oa

  module OaBaseCollectionMixin
    # Mix-in additional methods from the Enumerable module (requires an #each
    # method)      
    include Enumerable

    # Easily loop through all objects in an iterator.  Returns the expired
    # iterator (call OaBaseIter#reset to reuse if desired).
#    def each
#      iter = self.to_iter
#      while obj = iter.getNext do
#        yield obj
#      end
#      iter
#    end
  end

end
