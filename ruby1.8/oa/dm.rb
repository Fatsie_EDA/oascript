###############################################################################
#
# Copyright 2011 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   02/26/2011: Initial copy
#
###############################################################################

require 'oa/dm.so'

module Oa

  class OaLibDefList
    
    # Yield each library definition object (DEFINE).  Operates recursively by
    # following INCLUDE statements.
    def each_def(&block)
      getMembers.each do |mem|
        case mem.getType.to_i
        when OacLibDefType
          yield mem
        when OacLibDefListRefType
          mem.getRefListPath(path='')
          sub_list = OaLibDefList.get(path, 'r');
          sub_list.each_def(&block)
        end
      end
    end

    # Walk through each library definition of the lib.def file and yield
    # name/path of the library definition.  Operates recursively by following
    # INCLUDE statements.
    def each_def_pair(&block)
      self.each_def do |mem|
        name = OaScalarName.new
        mem.getLibName(name)
        path = mem.getLibPath
        yield [name, path]
      end
    end

  end
  
end
