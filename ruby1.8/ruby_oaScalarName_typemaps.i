/* ****************************************************************************

   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     05/18/2010: Initial typemaps to accept a string in place of an input
                 const oaScalarName.  The default namespace is used
		 (oaNativeNS).

 *************************************************************************** */

%header %{

int string_to_oaScalarName (VALUE vstr, oaScalarName& sn)
{
  VALUE vdefns = rb_iv_get(((swig_class *) SWIGTYPE_p_OpenAccess_4__oaNameSpace->clientdata)->klass, "@default");
  void *pdefns;
  if (SWIG_IsOK(SWIG_ConvertPtr(vdefns, &pdefns, SWIGTYPE_p_OpenAccess_4__oaNameSpace, 0)) && pdefns != NULL) {
    oaNameSpace& defns = *(reinterpret_cast<oaNameSpace *>(pdefns));
    OAR_TRY(sn.init(defns, (const oaChar*)StringValueCStr(vstr)));
    return SWIG_OK;
  }
  return SWIG_ERROR;
}
  
%}

%typemap(in) const OpenAccess_4::oaScalarName& (OpenAccess_4::oaScalarName tmpSN)
{
  int res;
  void *argp;
  
  if (TYPE($input) == T_STRING) {
    if (SWIG_IsOK((res = string_to_oaScalarName($input, tmpSN)))) {
      $1 = &tmpSN;
    } else {
      SWIG_exception_fail(SWIG_ArgError(res), "String used in place of oaScalarName without setting default NS");
    }
  } else if (SWIG_IsOK(SWIG_ConvertPtr($input, &argp, $1_descriptor,  0)) && argp != NULL) {
    $1 = reinterpret_cast<$1_basetype *>(argp);
  } else {
    SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
  }
}

%typecheck(0) const OpenAccess_4::oaScalarName& {
  void *tmpVoidPtr;
  $1 = (TYPE($input) == T_STRING) || SWIG_IsOK(SWIG_ConvertPtr($input, &tmpVoidPtr, $1_descriptor, 0));
 }
