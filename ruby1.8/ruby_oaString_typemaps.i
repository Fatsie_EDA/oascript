/* ****************************************************************************

   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     02/16/2010: Copied from Python bindings (authored by AMD) and modified by
                 Intel Corporation for Ruby.

 *************************************************************************** */

%ignore OpenAccess_4::oaString;

%typemap(in) OpenAccess_4::oaString& (OpenAccess_4::oaString tmpString)
{
  $1 = &tmpString;
}

%typemap(in) const OpenAccess_4::oaString& (OpenAccess_4::oaString tmpString)
{
  tmpString = StringValueCStr($input);
  $1 = &tmpString;
}

// TODO: use rb_obj_is_kind_of or STRING_P or respond_to(to_str)?
%typecheck(0) OpenAccess_4::oaString&
{
  $1 = (TYPE($input) == T_STRING);
}

// TODO: can we eventually use this private function (could be faster than
// rb_funcall)
//    rb_str_replace($input, rb_str_new((const oaChar*) *$1, (const unsigned int)$1->getLength()));
%typemap(argout) OpenAccess_4::oaString&
{
  rb_funcall($input, oarStrReplaceID, 1, rb_str_new((const oaChar*) *$1, (const unsigned int)$1->getLength()));
}

%typemap(argout) const OpenAccess_4::oaString&
{
}

// Return strings as native Ruby strings
%typemap(out) OpenAccess_4::oaString&
{
  $result = rb_str_new((const oaChar*) *$1, (const unsigned int)$1->getLength());
}

%typemap(out) OpenAccess_4::oaString
{
  $result = rb_str_new((const oaChar*) $1, (const unsigned int)$1.getLength());
}

