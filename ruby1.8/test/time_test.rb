#! /usr/bin/env ruby
###############################################################################
#
# Copyright 2011 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   01/11/2011: Initial copy
#
###############################################################################

require 'oa'
require 'getoptlong'

opts = GetoptLong.new(['--libdefs', GetoptLong::REQUIRED_ARGUMENT],
                      ['--lib', GetoptLong::REQUIRED_ARGUMENT],
                      ['--cell', GetoptLong::REQUIRED_ARGUMENT],
                      ['--view', GetoptLong::REQUIRED_ARGUMENT])

libname = nil
cellname = nil
viewname = nil
libdefs = nil

opts.each do |opt, arg|
  case opt
  when '--libdefs'
    libdefs = arg
  when '--lib'
    libname = arg
  when '--cell'
    cellname = arg
  when '--view'
    viewname = arg
  end
end

abort "Missing mandatory --lib argument" unless libname
abort "Missing mandatory --cell argument" unless cellname
abort "Missing mandatory --view argument" unless viewname                                     

Oa::oaDesignInit
Oa::OaLibDefList.openLibs

design = Oa::OaDesign.open(libname, cellname, viewname, 'r')

puts "Read-in time #{design.getReadInTime}"
puts "Create time #{design.getCreateTime}"
puts "Last saved time #{design.getLastSavedTime}"
puts "Design time stamp: #{design.getTimeStamp(:designDataType)}"

design.close
