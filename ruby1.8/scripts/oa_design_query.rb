#! /usr/bin/env/ruby
###############################################################################
#
# Copyright 2011 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################

require 'oa'
require 'getoptlong'

include Oa


##############################################################################
## USAGE ##
###########

usage = "
oa_design_query.rb --lib <lib> --cell <cell> --view <view> --eval <str>
                   [OPTIONS]

--lib           Library name
--cell          Cell name
--view          View name
--libdefs, -L   Alternate lib.defs file (default is ./lib.defs)
--help, -h      Shows this usage

"

opts = GetoptLong.new(['--libdefs', '-L', GetoptLong::REQUIRED_ARGUMENT],
                      ['--lib', '-l', GetoptLong::REQUIRED_ARGUMENT],
                      ['--cell', '-c', GetoptLong::REQUIRED_ARGUMENT],
                      ['--view', '-v', GetoptLong::REQUIRED_ARGUMENT],
                      ['--eval', '-e', GetoptLong::REQUIRED_ARGUMENT],
                      ['--help', '-h', GetoptLong::OPTIONAL_ARGUMENT])

libname = nil
cellname = nil
viewname = nil
libdefs = nil
evalstr = nil

opts.each do |opt, arg|
  case opt
  when '--libdefs'
    libdefs = arg
  when '--lib'
    libname = arg
  when '--cell'
    cellname = arg
  when '--view'
    viewname = arg
  when '--eval'
    evalstr = arg
  when '--help'
    abort usage
  end
end

unless libname and cellname and viewname
  abort usage+"\nMissing --lib, --cell, and/or --view\n"
end

unless evalstr
  abort usage+"\nMissing --eval\n"
end


##############################################################################
## MAIN ##
##########

libdefs ? OaLibDefList.openLibs(libdefs) : OaLibDefList.openLibs

d = OaDesign.open(libname, cellname, viewname, 'r')
b = d.getTopBlock

abort "Design #{d} has no top block!" unless b

eval(evalstr)

d.close
