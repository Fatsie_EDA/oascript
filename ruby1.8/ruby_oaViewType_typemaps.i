/* ****************************************************************************

   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     08/13/2010: Copied from Python bindings (authored by AMD) and modified by
                 Intel Corporation for Ruby.

 *************************************************************************** */

%typemap(in) const OpenAccess_4::oaViewType* (OpenAccess_4::oaViewType* tmpViewType)
{
  int res;
  void *argp;
    
  if (LANG_SYMBOLP($input)) {
    VALUE rbstr = LANG_SYM_TO_STR($input);
    $1 = oaViewType::find((const oaString&) oaString(RSTRING(rbstr)->ptr));
  }
  else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))) && argp != NULL) {
    $1 = reinterpret_cast<$1_basetype *>(argp);
  }
  else {
    SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
  }
}

%typemap(in) OpenAccess_4::oaViewType
{
  int res;
  void *argp;
    
  if (LANG_SYMBOLP($input)) {
    VALUE rbstr = LANG_SYM_TO_STR($input);
    $1 = &(oaViewType::find((const oaString&) oaString(RSTRING(rbstr)->ptr)));
  }
  else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))) && argp != NULL) {
    $1 = *(reinterpret_cast<$1_basetype *>(argp));
  }
  else {
    SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
  }
}

%typecheck(0) const OpenAccess_4::oaViewType*
{
  void *tmpVoidPtr;
  $1 = (LANG_SYMBOLP($input) || SWIG_IsOK(SWIG_ConvertPtr($input, &tmpVoidPtr, $1_descriptor, 0)));
}

%typecheck(0) OpenAccess_4::oaViewType = (const OpenAccess_4::oaViewType*);

