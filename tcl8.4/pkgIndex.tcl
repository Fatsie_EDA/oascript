#
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

package ifneeded oa 0.0 "

package provide oa 0.0

load [file join $dir oa [exec uname -i] common[info sharedlibextension]]
load [file join $dir oa [exec uname -i] base[info sharedlibextension]]
load [file join $dir oa [exec uname -i] plugin[info sharedlibextension]]
load [file join $dir oa [exec uname -i] dm[info sharedlibextension]]
load [file join $dir oa [exec uname -i] tech[info sharedlibextension]]
load [file join $dir oa [exec uname -i] design[info sharedlibextension]]
load [file join $dir oa [exec uname -i] wafer[info sharedlibextension]]
load [file join $dir oa [exec uname -i] util[info sharedlibextension]]

source [file join $dir oa etc oaAppDef.tcl]
source [file join $dir oa etc namespace.tcl]
"

package ifneeded oaTclCompat 0.0 "source [file join $dir oa etc oaTclCompat.tcl]"

