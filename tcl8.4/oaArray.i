/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#undef INTARRAY
%define INTARRAY(safetype,paramtype)
%ignore OpenAccess_4::oaArrayBase<paramtype>;
%ignore OpenAccess_4::oaArray<paramtype>;

%typemap(in, numinputs=0) OpenAccess_4::oaArray<paramtype>& (paramtype tmpArr)
{
    /// oaArray_int_0: %typemap(in, numinputs=0) OpenAccess_4::oaArray<paramtype>& (paramtype tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const OpenAccess_4::oaArray<paramtype>& (oaArray<paramtype> tmpArr)
{
    /// oaArray_int_1: %typemap(in) const OpenAccess_4::oaArray<paramtype>& (oaArray<paramtype> tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        long intVal;
        if (Tcl_GetLongFromObj(interp, tcl_obj, &intVal) != TCL_OK) {
            %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
        }
        tmpArr.append((paramtype) intVal));
    
    }
    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<paramtype>&, const OpenAccess_4::oaArray<paramtype>&
{
    /// oaArray_int_2: %typecheck(0) OpenAccess_4::oaArray<paramtype>&, const OpenAccess_4::oaArray<paramtype>&
    $1 = 0;
    int len;
    if (Tcl_ListObjLength(interp, $input, &len) == TCL_OK && len > 0) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, 0, &tcl_obj);
        long intVal;
        if (Tcl_GetLongFromObj(interp, tcl_obj, &intVal) == TCL_OK) {
            $1 = 1;
        }
    }
}

%typemap(argout) OpenAccess_4::oaArray<paramtype>&
{
    /// oaArray_int_3: %typemap(argout) OpenAccess_4::oaArray<paramtype>&
    oaUInt4 len;
    len = $1->getNumElements();
    Tcl_Obj* tcl_list = Tcl_NewListObj(0,0);
    for (oaUInt4 i = 0; i < len; ++i) {
        paramtype val = $1->get(i);
        Tcl_Obj* tcl_obj = Tcl_NewLongObj((long) val);
        if (Tcl_ListObjAppendElement(interp, tcl_list, tcl_obj) != TCL_OK) {
            SWIG_exception_fail(SWIG_ValueError, "in method '" "$symname" "', could not populate list with " #paramtype " results.");
        }
    }
    char* str = Tcl_GetString(tcl_list);
    Tcl_AppendResult(interp, str, NULL);
}

%typemap(argout) const OpenAccess_4::oaArray<paramtype>& {}

%apply OpenAccess_4::oaArray<paramtype>& { OpenAccess_4::oaArray<paramtype>* };
%apply const OpenAccess_4::oaArray<paramtype>& { const OpenAccess_4::oaArray<paramtype>* };

%enddef

#undef INTARRAYEXT
%define INTARRAYEXT(ns,class,safetype,paramtype)

%ignore ns::class;

%typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
{
    /// oaArray_intext_0: %typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const ns::class& (ns::class tmpArr)
{
    /// oaArray_intext_1: %typemap(in) const ns::class& (ns::class tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        long intVal;
        if (Tcl_GetLongFromObj(interp, tcl_obj, &intVal) != TCL_OK) {
            %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
        }
        tmpArr.append((paramtype) intVal);
    }
    $1 = &tmpArr;
}

%typecheck(0) ns::class&
{
    /// oaArray_inext_2: %typecheck(0) ns::class&
    $1 = 0;
    int len;
    if (Tcl_ListObjLength(interp, $input, &len) == TCL_OK && len > 0) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, 0, &tcl_obj);
        long intVal;
        if (Tcl_GetLongFromObj(interp, tcl_obj, &intVal) == TCL_OK) {
            $1 = 1;
        }
    }
}

%typemap(argout) ns::class&
{
    /// oaArray_intext_3: %typemap(argout) ns::class&
    oaUInt4 len;
    len = $1->getNumElements();
    Tcl_Obj* tcl_list = Tcl_NewListObj(0,0);
    for (oaUInt4 i = 0; i < len; ++i) {
        paramtype val = $1->get(i);
        Tcl_Obj* tcl_obj = Tcl_NewLongObj((long) val);
        if (Tcl_ListObjAppendElement(interp, tcl_list, tcl_obj) != TCL_OK) {
            SWIG_exception_fail(SWIG_ValueError, "in method '" "$symname" "', could not populate list with " #ns "::" #class " results.");
        }
    }
    char* str = Tcl_GetString(tcl_list);
    Tcl_AppendResult(interp, str, NULL);
}

%typemap(argout) const ns::class& {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

#undef STRINGARRAY
%define STRINGARRAY

%ignore OpenAccess_4::oaArrayBase<OpenAccess_4::oaString>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaString>;

%typemap(in, numinputs=0) OpenAccess_4::oaArray<OpenAccess_4::oaString>& (oaArray<oaString> tmpArr)
{
    /// oaArray_oaString_0: %typemap(in, numinputs=0) OpenAccess_4::oaArray<OpenAccess_4::oaString>& (oaArray<oaString> tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const OpenAccess_4::oaArray<OpenAccess_4::oaString>& (oaArray<oaString> tmpArr)
{
    /// oaArray_oaString_1: %typemap(in) const OpenAccess_4::oaArray<OpenAccess_4::oaString>& (oaArray<oaString> tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        char* str = Tcl_GetStringFromObj(tcl_obj, NULL);
        tmpArr.append(oaString(str));
    }
    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<OpenAccess_4::oaString>&, const OpenAccess_4::oaArray<OpenAccess_4::oaString>&
{
    /// oaArray_oaString_2: %typecheck(0) OpenAccess_4::oaArray<OpenAccess_4::oaString>&, const OpenAccess_4::oaArray<OpenAccess_4::oaString>&
    int len;
    $1 = (Tcl_ListObjLength(interp, $input, &len) == TCL_OK);
}

%typemap(argout) OpenAccess_4::oaArray<OpenAccess_4::oaString>&
{
    /// oaArray_oaString_3: %typemap(argout) OpenAccess_4::oaArray<OpenAccess_4::oaString>&
    oaUInt4 len;
    len = $1->getNumElements();
    Tcl_Obj* tcl_list = Tcl_NewListObj(0,0);
    for (oaUInt4 i = 0; i < len; ++i) {
        oaString val = $1->get(i);
        Tcl_Obj* tcl_obj = Tcl_NewStringObj((const oaChar*)val, -1);
        if (Tcl_ListObjAppendElement(interp, tcl_list, tcl_obj) != TCL_OK) {
            SWIG_exception_fail(SWIG_ValueError, "in method '" "$symname" "', could not populate list with oaString results.");
        }
    }
    char* str = Tcl_GetString(tcl_list);
    Tcl_AppendResult(interp, str, NULL);
}

%typemap(argout) const OpenAccess_4::oaArray<OpenAccess_4::oaString>& {}

%apply (OpenAccess_4::oaArray<OpenAccess_4::oaString>&) { OpenAccess_4::oaArray<OpenAccess_4::oaString>* };
%apply (const OpenAccess_4::oaArray<OpenAccess_4::oaString>&) { const OpenAccess_4::oaArray<OpenAccess_4::oaString>* };

%enddef

#undef STRINGARRAYEXT
%define STRINGARRAYEXT(ns,class)

%ignore ns::class;

%typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
{
    /// oaArray_oaStringext_0: %typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const ns::class& (ns::class tmpArr)
{
    /// oaArray_oaStringext_1: %typemap(in) const ns::class& (ns::class tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        char* str = Tcl_GetStringFromObj(tcl_obj, NULL);
        tmpArr.append(oaString(str));
    }
    $1 = &tmpArr;
}

%typecheck(0) ns::class& = OpenAccess_4::oaArray<OpenAccess_4::oaString>&;
%typemap(argout) ns::class& = OpenAccess_4::oaArray<OpenAccess_4::oaString>&;
%typemap(argout) const ns::class& {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

#undef POINTARRAY
%define POINTARRAY

%ignore OpenAccess_4::oaArrayBase<OpenAccess_4::oaPoint>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaPoint>;

%typemap(in, numinputs=0) OpenAccess_4::oaArray<OpenAccess_4::oaPoint>& (oaArray<oaPoint> tmpArr)
{
    /// oaArray_oaPoint_0: %typemap(in, numinputs=0) OpenAccess_4::oaArray<OpenAccess_4::oaPoint>& (oaArray<oaPoint> tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>& (oaArray<oaPoint> tmpArr)
{
    /// oaArray_oaPoint_1: %typemap(in) const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>& (oaArray<oaPoint> tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        oaPoint pt;
        void* argp;
        if (SWIG_IsOK(TclObj_to_oaPoint(interp, tcl_obj, pt))) {
            tmpArr.append(pt);
        } else {
            %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
        }
    }
    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&, const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&
{
    /// oaArray_oaPoint_2: %typecheck(0) OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&, const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&
    $1 = 0;
    int len;
    if (Tcl_ListObjLength(interp, $input, &len) == TCL_OK && len > 0) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, 0, &tcl_obj);
        oaPoint pt;
        void* argp;
        if (SWIG_IsOK(TclObj_to_oaPoint(interp, tcl_obj, pt))) {
            $1 = 1;
        }
    }
}

%typemap(argout) OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&
{
    /// oaArray_oaPoint_3: %typemap(argout) OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&
    oaUInt4 len;
    len = $1->getNumElements();
    Tcl_Obj* tcl_list = Tcl_NewListObj(0,0);
    for (oaUInt4 i = 0; i < len; ++i) {
        oaPoint* val = new OpenAccess_4::oaPoint($1->get(i));
        Tcl_Obj* tcl_obj = SWIG_NewInstanceObj(SWIG_as_voidptr(val), SWIG_TypeDynamicCast($descriptor(OpenAccess_4::oaPoint*), (void**) &val), SWIG_POINTER_OWN);
        if (Tcl_ListObjAppendElement(interp, tcl_list, tcl_obj) != TCL_OK) {
            SWIG_exception_fail(SWIG_ValueError, "in method '" "$symname" "', could not populate list with oaPoint results.");
        }
    }
    char* str = Tcl_GetString(tcl_list);
    Tcl_AppendResult(interp, str, NULL);
}

%typemap(argout) const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>& {}

%apply (OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) { OpenAccess_4::oaArray<OpenAccess_4::oaPoint>* };
%apply (const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&) { const OpenAccess_4::oaArray<OpenAccess_4::oaPoint>* };

%enddef

#undef POINTARRAYEXT
%define POINTARRAYEXT(ns,class)

%ignore ns::class;

%typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
{
    /// oaArray_oaPointext_0: %typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const ns::class& (ns::class tmpArr)
{
    /// oaArray_oaPointext_1: %typemap(in) const ns::class& (ns::class tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        oaPoint pt;
        if (SWIG_IsOK(TclObj_to_oaPoint(interp, tcl_obj, pt))) {
            tmpArr.append(pt);
        } else {
            %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
        }
    }
    $1 = &tmpArr;
}

%typecheck(0) ns::class& = OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&;
%typemap(argout) ns::class& = OpenAccess_4::oaArray<OpenAccess_4::oaPoint>&;
%typemap(argout) const ns::class& {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

#undef BOXARRAY
%define BOXARRAY

%ignore OpenAccess_4::oaArrayBase<OpenAccess_4::oaBox>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaBox>;

%typemap(in, numinputs=0) OpenAccess_4::oaArray<OpenAccess_4::oaBox>& (oaArray<oaBox> tmpArr)
{
    /// oaArray_oaBox_0: %typemap(in, numinputs=0) OpenAccess_4::oaArray<OpenAccess_4::oaBox>& (oaArray<oaBox> tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const OpenAccess_4::oaArray<OpenAccess_4::oaBox>& (oaArray<oaBox> tmpArr)
{
    /// oaArray_oaBox_1: %typemap(in) const OpenAccess_4::oaArray<OpenAccess_4::oaBox>& (oaArray<oaBox> tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        oaBox box;
        if (SWIG_IsOK(TclObj_to_oaBox(interp, tcl_obj, box))) {
            tmpArr.append(box);
        } else {
            %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
        }
    }
    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<OpenAccess_4::oaBox>&, const OpenAccess_4::oaArray::oaArray<OpenAccess_4::oaBox>&
{
    /// oaArray_oaBox_2: %typecheck(0) OpenAccess_4::oaArray<OpenAccess_4::oaBox>&, const OpenAccess_4::oaArray::oaArray<OpenAccess_4::oaBox>&
    $1 = 0;
    int len;
    if (Tcl_ListObjLength(interp, $input, &len) == TCL_OK && len > 0) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, 0, &tcl_obj);
        oaBox box;
        if (SWIG_IsOK(TclObj_to_oaBox(interp, tcl_obj, box))) {
            $1 = 1;
        }
    }
}

%typemap(argout) OpenAccess_4::oaArray<OpenAccess_4::oaBox>&
{
    /// oaArray_oaBox_3: %typemap(argout) OpenAccess_4::oaArray<OpenAccess_4::oaBox>&
    oaUInt4 len;
    len = $1->getNumElements();
    Tcl_Obj* tcl_list = Tcl_NewListObj(0,0);
    for (oaUInt4 i = 0; i < len; ++i) {
        oaBox* val = new OpenAccess_4::oaBox($1->get(i));
        Tcl_Obj* tcl_obj = SWIG_NewInstanceObj(SWIG_as_voidptr(val), SWIG_TypeDynamicCast($descriptor(OpenAccess_4::oaBox*), (void**) &val), SWIG_POINTER_OWN);
        if (Tcl_ListObjAppendElement(interp, tcl_list, tcl_obj) != TCL_OK) {
            SWIG_exception_fail(SWIG_ValueError, "in method '" "$symname" "', could not populate list with oaBox results.");
        }
    }
    char* str = Tcl_GetString(tcl_list);
    Tcl_AppendResult(interp, str, NULL);
}

%typemap(argout) const OpenAccess_4::oaArray<OpenAccess_4::oaBox>& {}

%apply (OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) { OpenAccess_4::oaArray<OpenAccess_4::oaBox>* };
%apply (const OpenAccess_4::oaArray<OpenAccess_4::oaBox>&) { const OpenAccess_4::oaArray<OpenAccess_4::oaBox>* };

%enddef

#undef BOXARRAYEXT
%define BOXARRAYEXT(ns,class)

%ignore ns::class;

%typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
{
    /// oaArray_oaBoxext_0: %typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const ns::class& (ns::class tmpArr)
{
    /// oaArray_oaBoxext_1: %typemap(in) const ns::class& (ns::class tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        oaBox box;
        if (SWIG_IsOK(TclObj_to_oaBox(interp, tcl_obj, box))) {
            tmpArr.append(box);
        } else {
            %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
        }
    }
    $1 = &tmpArr;
}

%typecheck(0) ns::class& = OpenAccess_4::oaArray<OpenAccess_4::oaBox>&;
%typemap(argout) ns::class& = OpenAccess_4::oaArray<OpenAccess_4::oaBox>&;
%typemap(argout) const ns::class& {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

#undef ARRAYCLASS
%define ARRAYCLASS(ns,paramtype)

%ignore OpenAccess_4::oaArray<ns::paramtype>;
%ignore OpenAccess_4::oaArrayBase<ns::paramtype>;

%typemap(in, numinputs=0) OpenAccess_4::oaArray<ns::paramtype>& (oaArray<ns::paramtype> tmpArr)
{
    /// oaArray_class_0: %typemap(in, numinputs=0) OpenAccess_4::oaArray<ns::paramtype>& (oaArray<ns::paramtype> tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const OpenAccess_4::oaArray<ns::paramtype>& (oaArray<ns::paramtype> tmpArr)
{
    /// oaArray_class_1: %typemap(in) const OpenAccess_4::oaArray<ns::paramtype>& (oaArray<ns::paramtype> tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        void* argp;
        if (SWIG_IsOK(SWIG_ConvertPtr(tcl_obj, &argp, $descriptor(ns::paramtype*), 0)) && argp != 0) {
            tmpArr.append(*(reinterpret_cast<ns::paramtype*>(argp)));
        } else {
            %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
        }
    }
    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<ns::paramtype>&, const OpenAccess_4::oaArray<ns::paramtype>&
{
    /// oaArray_class_2: %typecheck(0) OpenAccess_4::oaArray<ns::paramtype>&, const OpenAccess_4::oaArray<ns::paramtype>&
    $1 = 0;
    int len;
    if (Tcl_ListObjLength(interp, $input, &len) == TCL_OK && len > 0) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, 0, &tcl_obj);
        void* argp;
        if (SWIG_IsOK(SWIG_ConvertPtr(tcl_obj, &argp, $descriptor(ns::paramtype*), 0))) {
            $1 = 1;
        }
    }
}

%typemap(argout) OpenAccess_4::oaArray<ns::paramtype>&
{
    /// oaArray_class_3: %typemap(argout) OpenAccess_4::oaArray<ns::paramtype>&
    oaUInt4 len;
    len = $1->getNumElements();
    Tcl_Obj* tcl_list = Tcl_NewListObj(0,0);
    for (oaUInt4 i = 0; i < len; ++i) {
        ns::paramtype *oaobj = new ns::paramtype($1->get(i));
        Tcl_Obj* tcl_obj = SWIG_NewInstanceObj(SWIG_as_voidptr(oaobj), SWIG_TypeDynamicCast($descriptor(ns::paramtype*), (void**) &oaobj), SWIG_POINTER_OWN);
        if (Tcl_ListObjAppendElement(interp, tcl_list, tcl_obj) != TCL_OK) {
            SWIG_exception_fail(SWIG_ValueError, "in method '" "$symname" "', could not populate list with " #ns "::" #paramtype " results.");
        }
    }
    char* str = Tcl_GetString(tcl_list);
    Tcl_AppendResult(interp, str, NULL);
}

%typemap(argout) const OpenAccess_4::oaArray<ns::paramtype>& {}

%apply (OpenAccess_4::oaArray<ns::paramtype>&) { OpenAccess_4::oaArray<ns::paramtype>* };
%apply (const OpenAccess_4::oaArray<ns::paramtype>&) { const OpenAccess_4::oaArray<ns::paramtype>* };

%enddef

#undef ARRAYEXT
%define ARRAYEXT(ns,class,paramtype)

%ignore ns::class;

%typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
{
    /// oaArray_classext_0: %typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const ns::class& (ns::class tmpArr)
{
    /// oaArray_classext_1: %typemap(in) const ns::class& (ns::class tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        void* argp;
        if (SWIG_IsOK(SWIG_ConvertPtr(tcl_obj, &argp, $descriptor(paramtype*), 0)) && argp != 0) {
            tmpArr.append(*(reinterpret_cast<paramtype*>(argp)));
        } else {
            %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
        }
    }
    $1 = &tmpArr;
}

%typecheck(0) ns::class& = OpenAccess_4::oaArray<paramtype>&;
%typemap(argout) ns::class& = OpenAccess_4::oaArray<paramtype>&;
%typemap(argout) const ns::class& {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

#undef PTRARRAYCLASS
%define PTRARRAYCLASS(ns,paramtype)

%ignore OpenAccess_4::oaArray<ns::paramtype *>;
%ignore OpenAccess_4::oaArrayBase<ns::paramtype *>;

%typemap(in, numinputs=0) (OpenAccess_4::oaArray<ns::paramtype *>&) (oaArray<ns::paramtype *> tmpArr)
{
    /// oaArray_ptr_0: %typemap(in, numinputs=0) (OpenAccess_4::oaArray<ns::paramtype *>&) (oaArray<ns::paramtype *> tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const OpenAccess_4::oaArray<ns::paramtype *>& (oaArray<ns::paramtype *> tmpArr)
{
    /// oaArray_ptr_1: %typemap(in) const OpenAccess_4::oaArray<ns::paramtype *>& (oaArray<ns::paramtype *> tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        void* argp;
        if (SWIG_IsOK(SWIG_ConvertPtr(tcl_obj, &argp, $descriptor(ns::paramtype*), 0)) && argp != 0) {
            tmpArr.append(reinterpret_cast<ns::paramtype*>(argp));
        } else {
            %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
        }
    }
    $1 = &tmpArr;
}

%typecheck(0) OpenAccess_4::oaArray<ns::paramtype *>&, const OpenAccess_4::oaArray<ns::paramtype *>&
{
    /// oaArray_ptr_2: %typecheck(0) OpenAccess_4::oaArray<ns::paramtype *>&, const OpenAccess_4::oaArray<ns::paramtype *>&
    $1 = 0;
    int len;
    if (Tcl_ListObjLength(interp, $input, &len) == TCL_OK && len > 0) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, 0, &tcl_obj);
        void* argp;
        if (SWIG_IsOK(SWIG_ConvertPtr(tcl_obj, &argp, $descriptor(ns::paramtype*), 0))) {
            $1 = 1;
        }
    }
}

%typemap(argout) OpenAccess_4::oaArray<ns::paramtype *>&
{
    /// oaArray_ptr_3: %typemap(argout) OpenAccess_4::oaArray<ns::paramtype *>&
    oaUInt4 len;
    len = $1->getNumElements();
    Tcl_Obj* tcl_list = Tcl_NewListObj(0,0);
    for (oaUInt4 i = 0; i < len; ++i) {
        ns::paramtype *oaobj = $1->get(i);
        Tcl_Obj* tcl_obj = SWIG_NewInstanceObj(SWIG_as_voidptr(oaobj), SWIG_TypeDynamicCast($descriptor(ns::paramtype*), (void**) &oaobj), SWIG_POINTER_OWN);
        if (Tcl_ListObjAppendElement(interp, tcl_list, tcl_obj) != TCL_OK) {
            SWIG_exception_fail(SWIG_ValueError, "in method '" "$symname" "', could not populate list with " #ns "::" #paramtype " results.");
        }
    }
    char* str = Tcl_GetString(tcl_list);
    Tcl_AppendResult(interp, str, NULL);
}

%typemap(argout) const OpenAccess_4::oaArray<ns::paramtype *>& {}

%apply (OpenAccess_4::oaArray<ns::paramtype *>&) { OpenAccess_4::oaArray<ns::paramtype *>* };
%apply (const OpenAccess_4::oaArray<ns::paramtype *>&) { const OpenAccess_4::oaArray<ns::paramtype *>* };

%enddef

#undef PTRARRAYEXT
%define PTRARRAYEXT(ns,class,basetype)

%ignore ns::class;

%typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
{
    /// oaArray_ptrext_0: %typemap(in, numinputs=0) ns::class& (ns::class tmpArr)
    $1 = &tmpArr;
}

%typemap(in) const ns::class& (ns::class tmpArr)
{
    /// oaArray_ptrext_1: %typemap(in) const ns::class& (ns::class tmpArr)
    int arrSize;
    if (Tcl_ListObjLength(interp, $input, &arrSize) != TCL_OK) {
        %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
    }
    for (int i=0; i < arrSize; ++i) {
        Tcl_Obj* tcl_obj;
        Tcl_ListObjIndex(interp, $input, i, &tcl_obj);
        void* argp;
        if (SWIG_IsOK(SWIG_ConvertPtr(tcl_obj, &argp, $descriptor(basetype*), 0)) && argp != 0) {
            tmpArr.append(reinterpret_cast<basetype*>(argp));
        } else {
            %argument_fail(SWIG_ERROR, "$type", $symname, $argnum);
        }
    }
    $1 = &tmpArr;
}

%typecheck(0) ns::class& = OpenAccess_4::oaArray<basetype *>&;
%typemap(argout) ns::class& = OpenAccess_4::oaArray<basetype *>&;
%typemap(argout) const ns::class& {}

%apply (ns::class&) { ns::class* };
%apply (const ns::class&) { const ns::class* };

%enddef

#undef INTSUBSET
%define INTSUBSET(safetype,paramtype)
%enddef

#undef INTSUBSETEXT
%define INTSUBSETEXT(ns,class,safetype,paramtype)
%enddef

#undef STRINGSUBSET
%define STRINGSUBSET
%enddef

#undef STRINGSUBSETEXT
%define STRINGSUBSETEXT(ns,class)
%enddef

#undef POINTSUBSET
%define POINTSUBSET
%enddef

#undef POINTSUBSETEXT
%define POINTSUBSETEXT(ns,class)
%enddef

#undef BOXSUBSET
%define BOXSUBSET
%enddef

#undef BOXSUBSETEXT
%define BOXSUBSETEXT(ns,class)
%enddef

#undef SUBSETCLASS
%define SUBSETCLASS(ns,paramtype)
%enddef

#undef SUBSETEXT
%define SUBSETEXT(ns,class,paramtype)
%enddef

#undef PTRSUBSETCLASS
%define PTRSUBSETCLASS(ns,paramtype)
%enddef

#undef PTRSUBSETEXT
%define PTRSUBSETEXT(ns,class,basetype)
%enddef


%include <macros/oa_arrays.i>

