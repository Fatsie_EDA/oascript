###############################################################################
# Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   09/07/10  1.0      scarver, Si2     OA Scripting Language Labs
#
###############################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
##############################################################################
proc unexpectedException {} {
    puts $errorInfo
    puts "Quitting."
    exit 2
}

proc assert {condition msg} {
    if {!$condition} {
        puts "***ASSERT error $msg"
    }
}

proc setLayerPurpose {scNameLib layerName layNumIn purpName purpNumIn closeTech} {
    upvar 1 $layNumIn layNum
    upvar 1 $purpNumIn  purpNum
    # First see if a Tech database can be found already open for the lib,
    # i.e., from a prior call to setLayerPurpose (or any other module
    # if this were part of a large application).
    #
    set tech [oa::oaTech_find $scNameLib]
    if {$tech == "NULL"} {
        #
        # If one's not already open in memory, try opening it from disk.
        #
        if {[catch { set tech [ oa::oaTech_open $scNameLib "a"]} err]} {
            #
            # No Tech open in memory, none on disk by that name. So create one.
            #
            if { [string first "OpenAccessException" $err] != -1 &&
                 [string first $oa::oacCannotFindLibraryTech $err] != -1 } {
                set tech [oa::oaTech_create $scNameLib]
                #
                # Set the DBU to 1/3 nanometer for all Netlist type CellViews.
                #
                $tech setUserUnits [oa::oaViewType_get [oa::oaReservedViewType $oa::oacNetlist]] $oa::oacMicron
                $tech setDBUPerUU  [oa::oaViewType_get [oa::oaReservedViewType $oa::oacNetlist]] 3000
            } else {
                unexpectedException
            }
        }
    }
    # Try locating a Purpose of the given name. If so, overwrite
    # the purpNum defaulted above with the value found in the Tech.
    # Else, create a new Purpose with this name and num in the Tech.
    #
    set purpose [oa::oaPurpose_find $tech $purpName]
    if {$purpose != "NULL"} {
        set purpNum [$purpose getNumber]
        puts "Found Purpose name=$purpName  num=[format %x $purpNum]"
    } else {
        set purpose [oa::oaPurpose_create $tech $purpName $purpNumIn]
        set purpNum [$purpose getNumber]
        puts "Created Purpose name=$purpName num=[format %x $purpNum]"
    }
    #
    # If there is a Layer by the given name in the Tech already, get its num.
    # Else create one.
    #
    set layer [oa::oaLayer_find $tech $layerName]
    if {$layer != "NULL"} {
        set layNum [$layer getNumber]
        puts  "Found Layer name=$layerName num=[format %s $layNum]"
    } else {
        set layer [oa::oaPhysicalLayer_create $tech $layerName $layNumIn]
        set layNum [$layer getNumber]
        puts  "Created Layer name=$layerName num=[format %s $layNum]"
    }
    if {$closeTech} {
        $tech save
        $tech close
    }
    return
}


proc mapLibs {chpNameLibDefsFile libPrevIn libCurrIn scNameLibPrev scNameLibCurr chpPathLibCurr} {
    upvar 1 $libPrevIn libPrev
    upvar 1 $libCurrIn libCurr

    set ns [oa::oaUnixNS]

    # Perform logical lib name to physical path name mappings for the source library
    # created in the netlist/ lab, then the symbol library to be created in this lab.
    #
    # Make sure the lib.defs is LOCAL to avoid clobbering a real one.

    set strLibDefListDef [oa::oaLibDefList_getDefaultPath]

    if { [file normalize $strLibDefListDef] != [file normalize $chpNameLibDefsFile] } {
        puts "***Missing local $chpNameLibDefsFile file."
        puts "   Please create one with the one line shown below: "
        puts "       INCLUDE ../netlist/lib.defs"
        exit 4
    }
    oa::oaLibDefList_openLibs

    set libPrev [oa::oaLib_find $scNameLibPrev]
    if { $libPrev == "NULL" } {
        puts  "***Either lib.defs file missing or netlist lab was not completed."
        exit 1
    }

    set strNameLibCurr [$scNameLibCurr get $ns]

    set libCurr [oa::oaLib_find $scNameLibCurr]
    if { $libCurr != "NULL" } {
        puts  "$strNameLibCurr Lib definition found in $chpNameLibDefsFile and opened."
    } else {
        # If this program was run once already, a Lib directory will exist.
        # If the lib.defs file was deleted, the Lib for this current lab won't
        # have been opened by oaLibDefFile::load(), so it must be opened now.
        # If the updated lib.defs (with the def for this lab's Lib) remains but
        # this lab's Lib directory was deleted, then the oaLib::find() above would
        # have failed and the exists() test below will also fail;
        # hence, the subsequent create() will be invoked.
        #
        if {[oa::oaLib_exists $strNameLibCurr]} {
            puts  "Opened from disk existing Lib=$strNameLibCurr"
            set libCurr [oa::oaLib_open $scNameLibCurr $chpPathLibCurr]
      } else {
        # If there is no directory for the Lib, then it must be created now.
        #
        set libCurr [oa::oaLib_create $scNameLibCurr $chpPathLibCurr $oa::oacSharedLibMode "oaDMFileSys"]
        puts  "Created new Lib=$strNameLibCurr"
        #
        # Update the lib.defs file for future labs that reference this Design.
        #
        set ldl [oa::oaLibDefList_get $strLibDefListDef "a"]
        oa::oaLibDef_create $ldl $scNameLibCurr $chpPathLibCurr
        $ldl save
        }
    }
}
#
# Many of these values are reused by subsequent labs.
#
namespace eval globalData {
    variable ns                 [oa::oaNativeNS]
    variable chpNameLibNetlist  "Lib"
    variable chpNameLibSymbol   "LibSymbol"
    variable chpNameViewNetlist "netlist"
    variable chpNameViewSymbol  "symbol"
    variable chpNameLibDefsFile "lib.defs"
    variable chpPathLibSymbol   "./LibSymbol"
    variable scNameLibNetlist   [oa::oaScalarName $ns $chpNameLibNetlist]
    variable scNameLibSymbol    [oa::oaScalarName $ns $chpNameLibSymbol]
    variable scNameViewNetlist  [oa::oaScalarName $ns $chpNameViewNetlist]
    variable scNameViewSymbol   [oa::oaScalarName $ns $chpNameViewSymbol]

    variable mode_write  "w"
    variable mode_read   "r"
    variable mode_append "a"

    # Layer, purpose settings are tool/methodology-specific.
    # These are rather arbitrary

    variable strLayerDev    "device"
    variable strLayerText   "text"
    variable strPurposeDev  "drawing"
    variable strPurposeText "labels"

    variable numLayerDev  230
    variable numLayerText 229

    variable numPurposeDev  $oa::oavPurposeNumberDrawing
    variable numPurposeText 1013

    variable libNetlist "NULL"
    variable libSymbol  "NULL"

    mapLibs $chpNameLibDefsFile $libNetlist $libSymbol $scNameLibNetlist \
            $scNameLibSymbol $chpPathLibSymbol

    # Set up Layer and Purpose nums for the layers in the Tech database.
    # to be used for the device shapes.
    #
    setLayerPurpose $scNameLibSymbol $strLayerDev $numLayerDev $strPurposeDev $numPurposeDev  false
    setLayerPurpose $scNameLibSymbol $strLayerText  $numLayerText $strPurposeText $numPurposeText true
}
