################################################################################
#  Copyright {c} 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 {the "License" you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   04/15/11  10.0     scarver, Si2     Tutorial 10th Edition - Tcl version
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction {CAI} projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library {UNIX man pages}
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation {www.swig.org}
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

namespace eval emhGlobals {
    variable indent_inc  2

    # Global indent value for prettier log printing
    variable indent      0

    # Global flags for clients to set based on how much info they want to see.
    #
    # Default to NOT expanding the Occ hierarchy into other Designs.
    variable expandOccDesignInstMasters  false
    variable dumpBlockDomain             true
    variable dumpModDomain               true
    variable dumpOccDomain               true
    variable skipIfZero                  true
    variable dontSkipIfZero              false
}

proc assertq {statement} {
    if { ![uplevel 1 expr $statement] } {
        puts "\n***ASSERT\[FAIL\] $statement"
    }
}


proc add_indent {} {
    incr emhGlobals::indent $emhGlobals::indent_inc
}

proc sub_indent {} {
    incr emhGlobals::indent -$emhGlobals::indent_inc
}

proc doIndent {} {
    for { set ix 0 } { $ix < $emhGlobals::indent } { incr ix } {
        puts -nonewline " "
    }
}

proc log { mesg } {
    doIndent
    puts -nonewline $mesg
}


proc printObjType { obj } {
    puts -nonewline " [[$obj  getType] getName]"
}


proc newObjBaseID { obj } {
    puts ""
    doIndent
    printObjType $obj
    puts -nonewline " "
}


proc boundStatus { ref } {
    if { [$ref isBound] } {
        puts -nonewline " BOUND "
    } else {
        puts -nonewline " UNBOUND "
    }
}

proc printObjName_scalar { obj {prepend ""} } {
    # This is used for those classes that don't have a simple getName func.
    # Now try the more complex way
    set ns [oa::oaNativeNS]
    set scalarName [oa::oaScalarName]
    set simpleStr ""

    $obj getName $scalarName
    $scalarName get $ns simpleStr
    puts -nonewline "$prepend\"$simpleStr\""
}

proc printObjName { obj {prepend ""} } {
    if { [$obj isOccObject] } {
        if { [$obj isOccNet] || [$obj isOccInst] } {
            puts -nonewline "$prepend\"[$obj  getPathName]\""
        } else {
            puts -nonewline "$prepend\"[$obj  getName]\"" 
        }
    } else {
        puts -nonewline "$prepend\"[$obj  getName]\""
    }
}

proc printTransform  { inst } {
    set tranny [$inst getTransform]

    puts -nonewline " [[$tranny orient] getName]"
    puts -nonewline " ([[$tranny offset] x],[[$tranny offset] y])"
}


proc printOnlyLCV { container } {
    puts -nonewline "\"[$container getLibName]"
    puts -nonewline "|[$container getCellName]"
    puts -nonewline "|[$container getViewName]\""
}

proc getObjName { obj } {
    return [$obj getName $emhGlobals::ns]
}


proc printOrigLCV { mod } {

    set scNameOrig [oa::oaScalarName]
    set scNameLib  [oa::oaScalarName]
    set scNameCell [oa::oaScalarName]
    set scNameView [oa::oaScalarName]

    $mod getOrig $scNameOrig $scNameLib $scNameCell $scNameView

    doIndent
    puts "ORIG=$scNameOrig\[$scNameLib|$scNameCell|scNameView\]"
}

proc printHeaderLine { count category {skipIfZero true}  } {
    if { $count == 0 && $skipIfZero } {
        return false
    }
    # force a linefeed
    puts ""
    doIndent
    puts -nonewline "\[$count\]$category"
    if { $count != 1 } {
        puts -nonewline "s"
    }
    return true
}


proc dumpTermDefs { container descrip } {
    if { [printHeaderLine [[$container getBusTermDefs] getCount] descrip] } {
        return
    }
    oa::foreach def [$container getBusTermDefs] {
        newObjBaseID $def
        puts -nonewline ": BITS [$def getNumBits]"
        puts -nonewline " MINIX=[$def getMinIndex]"
        puts -nonewline " MAXIX=[$def getMaxIndex]"
        puts -nonewline "  BASENAME=[$def getName]"
        puts -nonewline "  BITS:"
        oa::foreach bit [$def getBusTermBits] {
            puts -nonewline " [$bit getBitIndex]"
        }
    }
}


proc printSSS { bus } {
    puts -nonewline "\[[$bus getStart]"
    puts -nonewline  ":[$bus getStop]"
    puts -nonewline  ":[$bus getStep]\]"
}


proc dumpBits { obj } {
    newObjBaseID $obj
    printObjName $obj
    if { [$obj isImplicit] } {
        puts -nonewline " IM"
    } else {
        puts -nonewline " EX"
    }
    puts -nonewline "BITS=[$obj getNumBits]"

}


proc printNetSummary { net } {
    dumpBits $net
    if { [string equal [[$net getType] getName] "BusNet"] } {
        printSSS $net
    }
    if { [$net isBitNet] } {
        # The following is programmed to more closely follow the C++ lab,
        # in which you have to downcast the Net
        set bitNet $net
        if { [$bitNet isPreferredEquivalent] } {
            puts -nonewline " ISPREF"
        }
        if { ! [[$bitNet getEquivalentNets] isEmpty] } {
            puts -nonewline " EQUIVALENTS:"
            oa::foreach eqNet [$bitNet getEquivalentNets] {
              puts -nonewline " [$eqNet getName]"
            }
        }
    }
    if { ! [[$net getTerms] isEmpty] } {
        puts ""
        add_indent
        log "CONNECTS Terms:"
        oa::foreach term [$net getTerms] {
            printObjName $term " "
        }
        sub_indent
    }
    if { ! [[$net getInstTerms] isEmpty] } {
        puts ""
        add_indent
        log "CONNECTS InstTerms:"
        oa::foreach instTerm [$net getInstTerms] {
            $instTerm getTerm
            if { [$instTerm isBound] } {
                puts -nonewline " "
            } else {
                puts -nonewline " CANTBIND"
            }
            puts -nonewline "\"[[$instTerm getInst] getName]"
            puts -nonewline "|[$instTerm getTermName]\""
        }
        sub_indent
    }
}


proc printDomainRef { ref } {
  if { ![string equal $ref "NULL"] && [$ref isValid] } {
    puts -nonewline "  ->"
    printObjType $ref
    puts -nonewline " "
    return true
  }
  return false
}


proc printOccNetSummary { net } {
    dumpBits $net
    if { [string equal [[$net getType] getName] "OccBusNet"] } {
        printSSS $net
    }
    if { [$net getNumBits] == 1 } {
        set obNet $net
        if { ! [[$obNet getEquivalentNets] isEmpty] } {
            puts -nonewline " EQUIVALENTS:"
            oa::foreach eqNet [$obNet getEquivalentNets] {
                puts -nonewline " "
                printObjName $eqNet " "
            }
        }
    }
    if { ! [[$net getTerms] isEmpty] } {
        puts ""
        add_indent
        log "CONNECTS OccTerms:"
        oa::foreach term [$net getTerms] {
            printObjName $term " "
        }
        sub_indent
    }
    if { ! [[$net getInstTerms] isEmpty] } {
        puts ""
        add_indent
        log "CONNECTS OccInstTerms:"
        oa::foreach instTerm [$net getInstTerms] {
            puts -nonewline "\"[[$instTerm getInst] getName]"
            puts -nonewline "|[$instTerm getTermName]\""
        }
        sub_indent
    }
    # Cross-domain references
    #
    puts ""
    doIndent
    if { [printDomainRef [$net getNet]] } {
        printObjName [$net getNet]
    }
    if { [printDomainRef [$net getModNet]] } {
        printObjName [$net getModNet]
    }
}

proc printModNetSummary { net } {
    dumpBits $net
    if { [string equal [[$net getType] getName] "ModBusNet"] } {
        printSSS $net
    }
    if { [$net getNumBits] == 1 } {
        set mbNet $net
        if { ! [[$mbNet getEquivalentNets] isEmpty] } {
            puts -nonewline " EQUIVALENTS:"
            oa::foreach eqNet [$mbNet getEquivalentNets] {
                puts -nonewline " [$nbNet getName]"
            }
        }
    }
    if { ! [[$net getTerms] isEmpty] } {
        puts ""
        add_indent
        log "CONNECTS ModTerms:"
        oa::foreach term [$net getTerms] {
            printObjName $term " "
        }
        sub_indent
    }
    if { ! [[$net getInstTerms] isEmpty] } {
        puts ""
        add_indent
        log  "CONNECTS ModInstTerms:"
        oa::foreach instTerm [$net getInstTerms] {
            puts -nonewline "\"[[$instTerm getInst] getName]"
            puts -nonewline "|[$instTerm getTermName]\""
        }
        sub_indent
    }
}


proc dumpNetMembers { netCollection } {
    oa::foreach net $netCollection {
        printNetSummary $net
        switch [[$net getType] getName]  {
            "ScalarNet" {
            }
            "BusNetBit" {
            }
            "BusNet" {
                add_indent
                dumpNetMembers [$net getSingleBitMembers]
                sub_indent
            }
            "BundleNet" {
                add_indent
                dumpNetMembers [$net getMembers]
                sub_indent
            }
            default  {
                log "Unrecognized net type: [[$net getType] getName]\n"
            }
        }
    }
}

proc dumpModNetMembers { netCollection } {
    oa::foreach net $netCollection {
        printModNetSummary $net
        switch [[$net getType] getName]  {
            "ModScalarNet" {
            }
            "ModBusNetBit" {
            }
            "ModBusNet" {
                add_indent
                dumpModNetMembers [$net getSingleBitMembers]
                sub_indent
            }
            "ModBundleNet" {
                add_indent
                dumpNetModMembers [$net getMembers]
                sub_indent
            }
            default  {
                log "Unrecognized net type: [[$net getType] getName]\n"
            }
        }
    }
}

proc dumpOccNetMembers { netCollection } {
    oa::foreach net $netCollection {
        printOccNetSummary $net
        switch [[$net getType] getName] {
            "OccScalarNet" -
            "OccBusNetBit" {
            }
            "OccBusNet" {
                add_indent
                dumpOccNetMembers [$net getSingleBitMembers]
                sub_indent
            }
            "OccBundleNet" {
                add_indent
                dumpOccNetMembers [$net getMembers]
                sub_indent
            }
            default  {
                log "Unrecognized net type: [[$net getType] getName]\n"
            }
        }
    }
}

proc printMasterInfo { ref nameType } {
    if { [$ref isBound] } {
        set master [$ref getMaster]
        puts -nonewline " BOUND=>"
        printObjType $master
        printOnlyLCV $master
    } else {
        puts -nonewline " UNBOUND=> $nameType"
    }
}

# Specialization: Occ[Mod]Module masters have just a name, no LCV.
# Plus the hier traversal name for oaModModuleInstHeader
# is getMasterModule, not getMaster!
#
proc printOccMasterInfo { ref } {
    if { [$ref isBound] } {
        set modMaster [$ref getMaster]
        puts -nonewline " BOUND=>"
        printObjType $modMaster
        printObjName $modMaster
    } else {
        puts -nonewline " UNBOUND=>MODULE"
    }
}

proc printModMasterInfo { ref } {
    if { [$ref isBound] } {
        set modMaster [$ref getMasterModule]
        puts -nonewline " BOUND=>"
        printObjType $modMaster
        printObjName $modMaster
    } else {
        puts -nonewline " UNBOUND=>MODULE"
    }
}

proc dumpMasterOcc { inst oiHeaderListIn } {

    set wasBound [$inst isBound]

    puts ""
    add_indent
    if { $wasBound } {
        log "MASTEROCC=BOUND2="
    } else {
        log "MASTEROCC=UNBOUND"
    }

    switch [[$inst getType] getName] {
        "OccVectorInst"    -
        "OccVectorInstBit" {
          puts -nonewline "NOMASTER]"
        }
        "OccVectorInstBit" {
        }
        default {
            set masterOcc [$inst getMasterOccurrence]
            if { [$masterOcc isValid] } {
                set masterType [$masterOcc getType]
                assertq { [$masterType typeEnum] == $oa::oacOccurrenceType }
                #
                # Always expand OccModuleInsts, since they are part of this Design.
                # But only expand beyond the frontier into other Designs if flag is set.
                #
                if { [$inst isOccModuleInst] ||
                    $emhGlobals::expandOccDesignInstMasters } {
                    if { !$wasBound } {
                        puts -nonewline " BINDING2="
                    }
                    dumpOcc $masterOcc oiHeaderList
                }
            } else {
                puts -nonewline "CANTBIND"
            }
        }
    }
    sub_indent
}


proc maybeAddNewOccInstHeader { inst oiHeaderListIn } {
    upvar $oiHeaderListIn oiHeaderList

    set ih [$inst getHeader]

    # Add the Header to the list if it's not already there.

    if { [lsearch $oiHeaderList $ih] == -1 } {
        lappend oiHeaderList $ih
    }
}


proc dumpOccDesignInsts { occ oiHeaderListIn } {
    upvar $oiHeaderListIn oiHeaderList

    set odiList ""

    # Organize only the OccDesignInsts into a list
    #
    oa::foreach inst [$occ getInsts] {
        if { [$inst isOccDesignInst] } {
            lappend odiList $inst
        }
    }

    set nInsts [llength $odiList]
    if { ! [printHeaderLine $nInsts "OCCDESIGNINST"] } {
        return
    }

    # Note: this is a tcl foreach, NOT an oa::foreach
    foreach inst $odiList {
        newObjBaseID $inst
        printObjName $inst

        # If we were NOT called [indirectly] from DumpOccDesignInstsFromHeader
        #
        if { [info exists oiHeaderList] } {
            maybeAddNewOccInstHeader $inst oiHeaderList
        }

        # Cross-domain references
        #
        puts ""
        doIndent
        if { [printDomainRef  [$inst getInst]] } {
            printObjName [$inst getInst]
        }
        if { [printDomainRef  [$inst getModInst]] } {
            printObjName [$inst getModInst]
        }

        # No XFORMS in Occ Domain!!
    }
}


proc dumpOccDesignInstsFromHeader { ih } {
    if { ! [printHeaderLine [[$ih getInsts] getCount] "OCCDESIGNINST" ] } {
        return
    }

    oa::foreach inst [$ih getInsts] {
        newObjBaseID $inst
        printObjName $inst

        # Cross-domain references
        #
        puts ""
        doIndent
        if { [printDomainRef [$inst getInst]] } {
            printObjName [$inst getInst]
        }
        if { [printDomainRef [$inst getModInst]] } {
            printObjName [$inst getModInst]
        }

        # No XFORMS in Occ Domain!!

        add_indent
        dumpOneInst $inst
        sub_indent

        dumpMasterOcc $inst ""
    }
}


proc dumpOccInstHeaders { oiHeaderListIn } {
    upvar $oiHeaderListIn oiHeaderList
    # Need this cheesy code because there's no getInstHeaders in oaOccurrence
    # and OccInstHeaders are scoped to the Design.

    if { ! [printHeaderLine [llength $oiHeaderList] "OCCINSTHEADER"] }  {
        return
    }

    foreach ih $oiHeaderList {
        # Pcell processing is taken out of this tcl version.
        #
        if { [$ih isSubHeader] || [$ih isSuperHeader] } {
            continue
        }
    
        newObjBaseID $ih
        printOnlyLCV $ih
        printMasterInfo $ih "DESIGN"
        puts ""
        doIndent
        printDomainRef [$ih getInstHeader]
        printDomainRef [$ih getModInstHeader]
        add_indent
        dumpOccDesignInstsFromHeader $ih
        sub_indent
    }
}


proc dumpOccModuleInsts { ih oiHeaderListIn } {
    upvar $oiHeaderListIn oiHeaderList

    if { ! [printHeaderLine [[$ih getInsts] getCount] "OCCMODULEINST"] }  return

    oa::foreach inst [$ih getInsts] {
        newObjBaseID $inst
        printObjName $inst
        add_indent
        dumpOneInst $inst
        sub_indent
        dumpMasterOcc $inst oiHeadList
    }
}


proc dumpOccModuleInstHeaders { occ oiHeaderListIn } {
    upvar $oiHeaderListIn oiHeaderList

    set iHeadColl [$occ getModuleInstHeaders]
    if { ! [printHeaderLine [$iHeadColl getCount] "OCCMODULEINSTHEADER"] } {
        return
    }

    oa::foreach ih $iHeadColl {
        newObjBaseID $ih
        printObjName $ih
        printOccMasterInfo $ih

        # Cross-domain reference
        #
        set mmIH [$ih getModModuleInstHeader]
        puts ""
        doIndent
        printDomainRef      $mmIH
        printObjName_scalar $mmIH

        add_indent
        dumpOccModuleInsts $ih oiHeaderList
        sub_indent
    }
}


proc dumpOccNets { occ } {
    set netColl [$occ getNets $oa::oacNetIterAll]
    if { ! [printHeaderLine [$netColl getCount] "OCCNET"] } {
        return
    }
    dumpOccNetMembers $netColl
}


proc printOccTermSummary { term } {
    dumpBits $term
    if { [string equal [[$term getType] getName] "OccBusTerm"] } {
        printSSS $term
    }

    puts -nonewline [format " %-6s" [[$term getTermType] getName]]

    set conNet [$term getNet]
    if { [$conNet isValid] } {
        printObjName $conNet " CONN2"
    }

    # Cross-domain references
    #
    puts ""
    doIndent
    if { [printDomainRef [$term getTerm]] } {
        printObjName [$term getTerm]
    }
    if { [printDomainRef [$term getModTerm]] } {
        printObjName [$term getModTerm]
    }
}


proc dumpOccTermMembers { termColl } {
    oa::foreach term $termColl {
        printOccTermSummary $term
        switch [[$term getType] getName] {
            "OccScalarTerm" -
            "OccBusTermBit" {
            }
            "OccBusTerm" {
                set occ   [$term getOccurrence]
                set start [$term getStart]
                set stop  [$term getStop]
                set step  [$term getStep]
                  
                set baseName [[$term getDef] getName]
                add_indent
                for {set ix $start}  {$ix <= $stop}  {incr ix $step} {
                    set termBit [oa::oaOccBusTermBit_find $occ $baseName $ix]
                    printOccTermSummary $termBit
                }
                sub_indent
            }
            "OccBundleTerm" {
                add_indent
                oa::foreach memTerm [$term getMembers] {
                    printOccTermSummary $memTerm
                }
                sub_indent
              }
            default {
                log "Unrecognized OccTerm type: [[$term getType] getName]\n"
            }
        }
    }
}


proc dumpOccTerms { occ } {
    if { ! [printHeaderLine [[$occ getTerms] getCount] "OCCTERM"] }  return
    dumpOccTermMembers [$occ getTerms]
}


proc dumpOcc { occ oiHeaderListIn } {
    upvar $oiHeaderListIn oiHeaderList

    printObjType $occ

    # Cross-domain references
    #
    puts ""
    doIndent
    set block [$occ getBlock]
    if { [printDomainRef $block] } {
        printOnlyLCV [$block getDesign]
    }
    set mod [$occ getModule]
    if { [printDomainRef $mod] } {
        printObjName $mod
    }

    add_indent
    dumpOccModuleInstHeaders $occ oiHeaderList
    dumpOccDesignInsts $occ oiHeaderList
    dumpOccNets $occ
    dumpOccTerms $occ
    sub_indent
}


proc dumpNets { block } {
    set netColl [$block getNets $oa::oacNetIterAll]

    if { ! [printHeaderLine [$netColl getCount] "NET" ] } return
    dumpNetMembers $netColl
}


proc dumpModNets { mod } {
    set netColl [$mod getNets $oa::oacNetIterAll]

    if { ! [printHeaderLine [$netColl getCount] "MODNET" ] } return
    dumpModNetMembers $netColl
}


proc printTermSummary { term } {
    dumpBits $term
    if { [string equal [[$term getType] getName] "BusTerm"] } {
        printSSS $term
    }
    puts -nonewline [format " %-6s" [[$term getTermType] getName]]
    set conNet [$term getNet]
    if { [$conNet isValid] } {
        printObjName $conNet " CONN2"
    } 
    if { [$term isBitTerm] } {
        puts -nonewline " PINCON=[[$term getPinConnectMethod] getName]"
    }
}

proc printModTermSummary { term } {
    dumpBits $term
    if { [string equal [[$term getType] getName] "ModBusTerm"] } {
        printSSS $term
    }
    puts -nonewline [format " %-6s" [[$term getTermType] getName]]
    set conNet [$term getNet]
    if { [$conNet isValid] } {
        printObjName $conNet " CONN2"
    } 
}

proc dumpModTermMembers { termColl } {
    oa::foreach term $termColl {
        printModTermSummary $term
        switch [[$term getType] getName] {
            "ModScalarTerm" -
            "ModBusTermBit" {
            }
            "ModBusTerm" {
                set mod   [$term getModule]
                set start [$term getStart]
                set stop  [$term getStop]
                set step  [$term getStep]
                  
                set baseName [[$term getDef] getName]
                add_indent
                for {set ix $start}  {$ix <= $stop}  {incr ix $step} {
                    set termBit [oa::oaModBusTermBit_find $mod $baseName $ix]
                    printModTermSummary $termBit
                }
                sub_indent
            }
            "ModBundleTerm" {
                add_indent
                oa::foreach memTerm [$term getMembers] {
                    printModTermSummary $memTerm
                }
                sub_indent
            }
            default {
                log "Unrecognized ModTerm type: [[$term getType] getName]\n"
            }
        }
    }
}


proc dumpTermMembers { termColl } {
    oa::foreach term $termColl {
        printTermSummary $term
        switch [[$term getType] getName] {
            "ScalarTerm" -
            "BusTermBit" {
            }
            "BusTerm" {
                set block [$term getBlock]
                set start [$term getStart]
                set stop  [$term getStop]
                set step  [$term getStep]
                  
                set baseName [[$term getDef] getName]
                add_indent
                for {set ix $start}  {$ix <= $stop}  {incr ix $step} {
                    set termBit [oa::oaBusTermBit_find $block $baseName $ix]
                    printTermSummary $termBit
                }
                sub_indent
            }
            "BundleTerm" {
                add_indent
                oa::foreach memTerm [$term getMembers] {
                    printTermSummary $memTerm
                }
                sub_indent
            }
            default {
                log "Unrecognized Term type: [[$term getType] getName]\n"
            }
        }
    }
}

proc dumpTerms { block } {
    if { ! [printHeaderLine [[$block getTerms] getCount] "TERM"] }  return
    dumpTermMembers [$block getTerms]
}



proc dumpModTerms { mod } {
    if { ! [printHeaderLine [[$mod getTerms] getCount] "MODTERM"] }  return
    dumpModTermMembers [$mod getTerms]
}


proc printInstTerm { instTerm } {
    newObjBaseID $instTerm

    puts -nonewline "\" [$instTerm getTermName]\""
    if { [$instTerm isImplicit] } {
        puts -nonewline " IM"
    } else {
        puts -nonewline " EX"
    }

    puts -nonewline " BITS=[$instTerm getNumBits]"

    if {  [$instTerm isBound] } {
        puts -nonewline " BOUND"
    } else {
        puts -nonewline " UNBOUND"
    }
    if  { [$instTerm usesTermPosition] } {
        puts -nonewline " POS"
    }
    set conNet [$instTerm getNet]
    if { [$conNet isValid] } {
        printObjName $conNet " CONN2"
    }
}


proc dumpInstTerms { container } {
    if { ! [printHeaderLine [[$container getInstTerms] getCount] "INSTTERM"] } {
        return
    }
    oa::foreach instTerm [$container getInstTerms] {
        printInstTerm $instTerm
    }
}

proc dumpOneInst { inst } {
  dumpInstTerms $inst
}


proc dumpProps { obj } {
    if { ! [printHeaderLine [[$obj getProps] getCount] "PROP" ] } {
        return
    }
    add_indent
    oa::foreach prop [$obj getProps] {
        puts ""
        log [[$prop getType] getName]
        puts -nonewline " [$prop getName]=[$prop getValue]"
    }
    sub_indent
}


proc dumpInsts { container } {
    if  {! [printHeaderLine [[$container getInsts] getCount] "INST" ] } return
    oa::foreach inst [$container getInsts] {
        newObjBaseID $inst
        printObjName $inst
        printTransform $inst
        add_indent
        dumpOneInst $inst
        sub_indent
    }
}


proc dumpModDesignInsts { container } {
    #
    # If container Type is Module the Iter has both kinds of ModInsts; so to get an
    # accurate count of only the ModDesignInsts, do an initial cycle through the iterator.
    #
    set count 0
    oa::foreach inst [$container getInsts] {
        if  { [$inst isModDesignInst] } {
            incr count
        }
    }

    if { ! [printHeaderLine $count "MODDESIGNINST" ] } return
    #
    # Go through the iterater again, this time printing out ModInst information
    #
    oa::foreach inst [$container getInsts] {
        set instDb     [$inst getDatabase] 
        set instDbType [$instDb getType]
        set containerDb [$container getDatabase]
        assertq { [$instDbType typeEnum] == $oa::oacDesignType }
        assertq { [string equal $instDb  $containerDb] }
        #
        # If the container Type is Module the Iter has both kinds of ModInsts.
        # Skip the ModModuleInsts since they are dumped from the ModModuleInstHeader
        #
        if  { [$inst isModModuleInst] } continue
        newObjBaseID $inst
        if { [[$container getType] getName] != "Module" } {
            printObjName [$inst getModule]
            puts -nonewline "|" 
        }
        printObjName $inst
        #
        # No XFORMS in Mod Domain.
        # Don't need to repeat the guts in the ModInstHeader.
        # But DO need to in each Module, since the InstTerms could be connected
        # to different Nets, have different annotations, etc.
        #
        if { [string equal [[$container getType] getName] "Module"] } {
            add_indent
            dumpOneInst $inst
            sub_indent
        } else {
            boundStatus $inst
            if { [$inst isBound] } {
                set mod [$inst getMasterModule]
                if { [$mod isValid] } {
                    printObjName $mod
                    if { [[$mod getDesign] getTopModule] == $mod } {
                        puts -nonewline "\[TOP\]" 
                    }
                } else {
                    puts -nonewline "***NULL MASTER MODULE!" 
                }
            }
        }
    }
}


proc dumpModModuleInsts { container } {
    add_indent
    if { ![printHeaderLine [[$container getInsts] getCount] "MODMODULEINST"] } {
        return
    }
    oa::foreach inst [$container getInsts] {
        newObjBaseID $inst
        printObjName $inst
        # No XFORMS in Mod Domain!!
        add_indent
        dumpOneInst $inst
        sub_indent
    }
    sub_indent
}


proc dumpNetDefs { container descrip } {
    printHeaderLine [[$container getBusNetDefs] getCount] descrip

    oa::foreach def [$container getBusNetDefs] {
        newObjBaseID $def
        puts -nonewline " BITS=[$def getNumBits]"
        puts -nonewline " MINIX=[$def getMinIndex]"
        puts -nonewline " MAXIX=[$def getMaxIndex]"
        puts -nonewline "  BASENAME=[$def getName]"
        puts -nonewline "  BITS:" 
        oa::foreach bit [$def getBusNetBits] {
            puts -nonewline " [$bit getBitIndex]"
        }
    }
}


proc dumpModInstHeaders { design } {
    set iHeadColl [$design getModInstHeaders]
    if { ![printHeaderLine [$iHeadColl getCount] "MODINSTHEADER" \
       $emhGlobals::dontSkipIfZero] } return

    oa::foreach ih $iHeadColl {
        # Pcell processing is taken out of this tcl version.
        #
        if { [$ih isSubHeader] || [$ih isSuperHeader] } {
            continue
        }
  
        newObjBaseID $ih
        printOnlyLCV $ih
        printMasterInfo $ih "DESIGN"
        add_indent
        dumpModDesignInsts $ih
        sub_indent
    }
}


proc dumpModModuleInstHeaders { mod } {
    set iHeadColl [$mod getModuleInstHeaders]
    if { ![printHeaderLine [$iHeadColl getCount] "MODMODULEINSTHEADER"] } return

    oa::foreach ih $iHeadColl {
        #
        # ModModules can't have parameters, so no Super/SubHeaders
        #
        newObjBaseID $ih
        printObjName_scalar $ih
        printModMasterInfo  $ih
        dumpModModuleInsts  $ih
    }
}


proc dumpInstHeaders { block } {
    set iHeadColl [$block getInstHeaders]
    if { ![printHeaderLine [$iHeadColl getCount] "INSTHEADER"] } return

    oa::foreach ih $iHeadColl {
        # Pcell processing is taken out of this tcl version.
        #
        if { [$ih isSubHeader] || [$ih isSuperHeader] } {
            continue
        }
  
        newObjBaseID $ih
        printMasterInfo $ih "DESIGN"
        add_indent
        dumpInsts $ih
        sub_indent
    }
}


proc dumpModules { design } {
    set modTop [$design getTopModule]

    # First dump the ModInstHeaders, which are scoped to the Design,
    # common across all Modules that use them.
    #
    dumpModInstHeaders $design

    # Next take each Module in turn, start with its unique ModModuleInstHeaders.
    #
    set iModColl [$design getModules]
    puts ""
    printHeaderLine [$iModColl getCount] "MODULE" !$emhGlobals::skipIfZero]
    oa::foreach mod $iModColl {
        newObjBaseID $mod
        printObjName $mod
        if { $mod == $modTop } {
            puts -nonewline " TOP" 
        } elseif { [$mod isEmbedded] } {
            puts -nonewline " EMBEDDED" 
        }
        if { [$mod isDerived] } {
            printOrigLCV $mod
        }
        add_indent
        dumpModModuleInstHeaders $mod
        dumpModDesignInsts       $mod
        dumpNetDefs  $mod "MODBUSNETDEF"
        dumpTermDefs $mod "MODBUSTERMDEF"
        dumpModNets  $mod
        dumpModTerms $mod
        sub_indent
    }
}


proc separator { mesg } {
    puts "\n"
    log "==================== $mesg ====================\n" 
}

proc dumpBlock { block } {
    if { [$block isValid] } {
        printHeaderLine 1  "BLOCK"
        newObjBaseID $block
        add_indent
        dumpInstHeaders $block
        dumpNetDefs $block "BUSNETDEF"
        dumpTermDefs $block "BUSTERMDEF"
        dumpNets $block
        dumpTerms $block
        sub_indent
    } else {
        puts ""
        log "NO BLOCK"
    }
}


proc dumpDesign { design } {
    puts "\n"
    log "______________________________ "
    printObjType $design
    printOnlyLCV $design
    add_indent
    if { [$design isSubMaster]} {
        puts ""
        log "SUBMASTER" 
    }
    sub_indent
    dumpProps $design
    if { $emhGlobals::dumpBlockDomain } {
        separator "BLOCK DOMAIN"
        dumpBlock [$design getTopBlock]
    }
    if { $emhGlobals::dumpModDomain } {
        separator "MODULE DOMAIN" 
        dumpModules $design
    }
    if { $emhGlobals::dumpOccDomain } {
        separator "OCCURRENCE DOMAIN"
        puts ""
        doIndent
        set occTop [$design getTopOccurrence]
        if { [$occTop isValid] } {
            puts -nonewline "\[TOP\]" 
            set oiHeaderList ""
            dumpOcc $occTop oiHeaderList
            #
            # Dump out any OccInstHeaders we saved while iterating over
            # OccDesignInsts in the Occurrence.
            #
            dumpOccInstHeaders oiHeaderList
        } else {
            cout << "NO OCCURRENCE" 
        }
    }
    puts ""
}


proc dumpOpenDesigns {} {
    puts ""
    log "Currently open Designs:"
    oa::foreach design [oa::oaDesign_getOpenDesigns] {
        dumpDesign $design
    }
    puts ""
}
