#!/usr/bin/env tcl
################################################################################
#  Copyright {c} 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 {the "License" you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   04/15/11  10.0     scarver, Si2     Tutorial 10th Edition - Tcl version
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction {CAI} projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library {UNIX man pages}
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation {www.swig.org}
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

# This is a driver for the utility functions in dumpUtils.tcl
# It can be called for any lib/cell/view whose Lib is defined in the
# local lib.defs file.
#
# Mostly, the dumpUtils.tcl module is sourced by other labs to print out
# the contents of OA Objects.

package require oa

oa::oaDesignInit

source dumpUtils.tcl


proc main {} {
    global argv

    if { $argv < 3 } {
      puts "***Call with LIB CELL VIEW name of lib to list."
      exit 1
    }

    set libName  [lindex $argv 0]
    set cellName [lindex $argv 1]
    set viewName [lindex $argv 2]

		# NULL until an OpenLibs is done.
		assertq { [string equal [oa::oaLibDefList_getTopList] "NULL"] }

    if { [catch {oa::oaLibDefList_openLibs} err] } {
        puts "Error opening libs: $err"
        exit 1
    }

    oa::oaDesign_open $libName $cellName $viewName "r"
    dumpOpenDesigns
    puts "\n\n.............. Normal Termination ........... \n\n"  ;
}

main
