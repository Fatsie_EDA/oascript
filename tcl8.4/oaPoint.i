/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%header %{

int TclObj_to_oaPoint(Tcl_Interp *interp, Tcl_Obj *tclobj, oaPoint& point)
{
    int len;
    if (Tcl_ListObjLength(interp, tclobj, &len) == TCL_OK && len == 2) {
        Tcl_Obj* tempObj;
        oaInt4 x, y;
        if (Tcl_ListObjIndex(interp, tclobj, 0, &tempObj) == TCL_OK &&
            Tcl_GetIntFromObj(interp, tempObj, &x) == TCL_OK &&
            Tcl_ListObjIndex(interp, tclobj, 1, &tempObj) == TCL_OK &&
            Tcl_GetIntFromObj(interp, tempObj, &y) == TCL_OK) {
            point.set(x, y);
            return SWIG_OK;
        }
    }
    void* argp;
    if (SWIG_IsOK(SWIG_ConvertPtr(tclobj, &argp, SWIGTYPE_p_OpenAccess_4__oaPoint, 0)) && argp != 0) {
        point = *(reinterpret_cast<oaPoint*>(argp));
        return SWIG_OK;
    }
    return SWIG_ERROR;
}

%}

/// Ignore non-const versions of these functions
%ignore OpenAccess_4::oaPoint::x();
%ignore OpenAccess_4::oaPoint::y();

%typemap(in) const OpenAccess_4::oaPoint& (OpenAccess_4::oaPoint temp)
{
    /// oaPoint_2: %typemap(in) const OpenAccess_4::oaPoint& (OpenAccess_4::oaPoint tmpPt)
    int res;
    if (SWIG_IsOK(res = TclObj_to_oaPoint(interp, $input, temp))) {
        $1 = &temp;
    } else {
        SWIG_exception_fail(SWIG_ArgError(res), "in method '$symname', argument $argnum of type '$1_type'");
    }
}

%typecheck(0) const OpenAccess_4::oaPoint&
{
    /// oaPoint_3: %typecheck(0) const OpenAccess_4::oaPoint&
    void* tmpVoidPtr;
    int len;
    $1 = 0;
    if (Tcl_ListObjLength(interp, $input, &len) == TCL_OK && len == 2) {
        $1 = 1;
    } else if (SWIG_IsOK(SWIG_ConvertPtr($input, &tmpVoidPtr, $1_descriptor, 0))) {
        $1 = 1;
    } else {
        $1 = 0;
    }
}

