# This is file auto-generated.

package provide oaTclCompat 0.0

namespace eval oa {

    # Constructor for 'oa1DLookupTbl_float_float'
    proc 1DLookupTbl_float_float { args } {
        ::set result [eval oa1DLookupTbl_float_float $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oa1DLookupTbl_int_float'
    proc 1DLookupTbl_int_float { args } {
        ::set result [eval oa1DLookupTbl_int_float $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oa1DLookupTbl_int_int'
    proc 1DLookupTbl_int_int { args } {
        ::set result [eval oa1DLookupTbl_int_int $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oa1DLookupTbl_int_oaDualIntArray'
    proc 1DLookupTbl_int_oaDualIntArray { args } {
        ::set result [eval oa1DLookupTbl_int_oaDualIntArray $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oa1DLookupTbl_int_oaIntRangeArray'
    proc 1DLookupTbl_int_oaIntRangeArray { args } {
        ::set result [eval oa1DLookupTbl_int_oaIntRangeArray $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc 1DLookupTbl_long_long_float { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oa2DLookupTbl_float_float_float'
    proc 2DLookupTbl_float_float_float { args } {
        ::set result [eval oa2DLookupTbl_float_float_float $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oa2DLookupTbl_float_int_float'
    proc 2DLookupTbl_float_int_float { args } {
        ::set result [eval oa2DLookupTbl_float_int_float $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oa2DLookupTbl_int_int_int'
    proc 2DLookupTbl_int_int_int { args } {
        ::set result [eval oa2DLookupTbl_int_int_int $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oa2DLookupTbl_int_int_oaIntRangeArray'
    proc 2DLookupTbl_int_int_oaIntRangeArray { args } {
        ::set result [eval oa2DLookupTbl_int_int_oaIntRangeArray $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oa2DLookupTbl_int_int_oaViaDefArrayValue'
    proc 2DLookupTbl_int_int_oaViaDefArrayValue { args } {
        ::set result [eval oa2DLookupTbl_int_int_oaViaDefArrayValue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oa2DLookupTbl_int_int_oaViaTopologyArrayValue'
    proc 2DLookupTbl_int_int_oaViaTopologyArrayValue { args } {
        ::set result [eval oa2DLookupTbl_int_int_oaViaTopologyArrayValue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaAbstractType'
    proc AbstractType { args } {
        ::set result [eval oaAbstractType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for AbstractType
    proc AbstractTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaAlignmentType'
    proc AlignmentType { args } {
        ::set result [eval oaAlignmentType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for AlignmentType
    proc AlignmentTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AnalysisLibCreate { args } {
        ::set result [eval oaAnalysisLib_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AnalysisLibFind { args } {
        ::set result [eval oaAnalysisLib_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AnalysisOpPointCreate { args } {
        ::set result [eval oaAnalysisOpPoint_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc AnalysisPointArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc AnalysisPointCreate { args } {
        ::set result [eval oaAnalysisPoint_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AnalysisPointFind { args } {
        ::set result [eval oaAnalysisPoint_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaAntennaArea'
    proc AntennaArea { args } {
        ::set result [eval oaAntennaArea $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc AntennaAreaArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaAntennaData'
    proc AntennaData { args } {
        ::set result [eval oaAntennaData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaAntennaModel'
    proc AntennaModel { args } {
        ::set result [eval oaAntennaModel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for AntennaModel
    proc AntennaModelEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AntennaRatioArrayValueCreate { args } {
        ::set result [eval oaAntennaRatioArrayValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AntennaRatioValueCreate { args } {
        ::set result [eval oaAntennaRatioValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaAppObjectCollection'
    proc AppObjectCollection { args } {
        ::set result [eval oaAppObjectCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AppObjectCreate { args } {
        ::set result [eval oaAppObject_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaAppObjectDefCollection'
    proc AppObjectDefCollection { args } {
        ::set result [eval oaAppObjectDefCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AppObjectDefFind { args } {
        ::set result [eval oaAppObjectDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AppObjectDefGet { args } {
        ::set result [eval oaAppObjectDef_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AppPropCreate { args } {
        ::set result [eval oaAppProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ArcCalcArc { args } {
        ::set result [eval oaArc_calcArc $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ArcCalcBBox { args } {
        ::set result [eval oaArc_calcBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Static member function
    proc ArcCreate { args } {
        ::set result [eval oaArc_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ArcGenPoints { args } {
        ::set result [eval oaArc_genPoints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Static member function
    proc AreaBlockageCreate { args } {
        ::set result [eval oaAreaBlockage_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AreaBoundaryCreate { args } {
        ::set result [eval oaAreaBoundary_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AreaBoundaryFind { args } {
        ::set result [eval oaAreaBoundary_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AreaHaloCreate { args } {
        ::set result [eval oaAreaHalo_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AreaHaloFind { args } {
        ::set result [eval oaAreaHalo_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ArrayBase_UserBox { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_UserPoint { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_float { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaAnalysisPoint { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaAntennaArea { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaBox { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaBuildInfo { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaCMAttr { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaCMProtocol { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaConstraintParam { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaDBType { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaDMAttr { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaDerivedLayerParam { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaDesignObject { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaDualInt { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaFeature { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaFig { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaGroupDef { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaIntRange { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaLayerHeader { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaLib { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaManagedType { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaModTerm { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaObject { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaOccTerm { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaParam { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaPcellDefData { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaPoint { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaSiteRef { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaString { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaTech { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaTechHeader { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaTerm { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaType { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaValue { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaViaDef { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_oaViaTopology { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_unsigned_char { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc ArrayBase_unsigned_int { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc ArrayInstCreate { args } {
        ::set result [eval oaArrayInst_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ArrayInstFind { args } {
        ::set result [eval oaArrayInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ArrayInstIsValidName { args } {
        ::set result [eval oaArrayInst_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ArraySiteDefCreate { args } {
        ::set result [eval oaArraySiteDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc Array_UserBox { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_UserPoint { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_float { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaAnalysisPoint { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaAntennaArea { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaBox { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaBuildInfo { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaCMAttr { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaCMProtocol { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaConstraintParam { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaDBType { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaDMAttr { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaDerivedLayerParam { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaDesignObject { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaDualInt { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaFeature { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaFig { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaGroupDef { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaIntRange { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaLayerHeader { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaLib { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaManagedType { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaModTerm { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaObject { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaOccTerm { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaParam { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaPcellDefData { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaPoint { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaSiteRef { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaString { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaTech { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaTechHeader { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaTerm { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaType { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaValue { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaViaDef { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_oaViaTopology { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_unsigned_char { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Array_unsigned_int { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc AssignAssignmentCreate { args } {
        ::set result [eval oaAssignAssignment_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AssignValueCreate { args } {
        ::set result [eval oaAssignValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc AssignedNetSpec { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaAssignmentAttrType'
    proc AssignmentAttrType { args } {
        ::set result [eval oaAssignmentAttrType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaAssignmentDef'
    proc AssignmentDef { args } {
        ::set result [eval oaAssignmentDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AssignmentFind { args } {
        ::set result [eval oaAssignment_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc AttrDisplayCreate { args } {
        ::set result [eval oaAttrDisplay_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaAttrType'
    proc AttrType { args } {
        ::set result [eval oaAttrType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaBaseCollection'
    proc BaseCollection { args } {
        ::set result [eval oaBaseCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Init function for module oaBase
    proc BaseInit { args } {
        ::set result [eval oaBaseInit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaBaseIter'
    proc BaseIter { args } {
        ::set result [eval oaBaseIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaBaseMemNetCollection'
    proc BaseMemNetCollection { args } {
        ::set result [eval oaBaseMemNetCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaBaseMemNetIter'
    proc BaseMemNetIter { args } {
        ::set result [eval oaBaseMemNetIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaBitOrder'
    proc BitOrder { args } {
        ::set result [eval oaBitOrder $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for BitOrder
    proc BitOrderEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BlockCreate { args } {
        ::set result [eval oaBlock_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaBlockDomainVisibility'
    proc BlockDomainVisibility { args } {
        ::set result [eval oaBlockDomainVisibility $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for BlockDomainVisibility
    proc BlockDomainVisibilityEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaBlockObjectIter'
    proc BlockObjectIter { args } {
        ::set result [eval oaBlockObjectIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaBlockageType'
    proc BlockageType { args } {
        ::set result [eval oaBlockageType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for BlockageType
    proc BlockageTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BooleanPropCreate { args } {
        ::set result [eval oaBooleanProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BooleanValueCreate { args } {
        ::set result [eval oaBooleanValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaBox'
    proc Box { args } {
        ::set result [eval oaBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Unsupported function
    proc BoxArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc BoxArrayValueCreate { args } {
        ::set result [eval oaBoxArrayValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Constructor for 'oaBuildInfo'
    proc BuildInfo { args } {
        ::set result [eval oaBuildInfo $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc BuildInfoArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc BuildInfoFind { args } {
        ::set result [eval oaBuildInfo_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BuildInfoGetAppBuildName { args } {
        ::set result [eval oaBuildInfo_getAppBuildName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BuildInfoGetNumPackages { args } {
        ::set result [eval oaBuildInfo_getNumPackages $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BuildInfoGetPackages { args } {
        ::set result [eval oaBuildInfo_getPackages $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BuildInfoGetPlatformName { args } {
        ::set result [eval oaBuildInfo_getPlatformName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaBundleName'
    proc BundleName { args } {
        ::set result [eval oaBundleName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BundleNetCreate { args } {
        ::set result [eval oaBundleNet_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BundleNetFind { args } {
        ::set result [eval oaBundleNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BundleNetIsValidName { args } {
        ::set result [eval oaBundleNet_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BundleTermCreate { args } {
        ::set result [eval oaBundleTerm_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BundleTermFind { args } {
        ::set result [eval oaBundleTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BundleTermIsValidName { args } {
        ::set result [eval oaBundleTerm_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusNetBitCreate { args } {
        ::set result [eval oaBusNetBit_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusNetBitFind { args } {
        ::set result [eval oaBusNetBit_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusNetBitIsValidName { args } {
        ::set result [eval oaBusNetBit_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusNetCreate { args } {
        ::set result [eval oaBusNet_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusNetDefCreate { args } {
        ::set result [eval oaBusNetDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusNetDefFind { args } {
        ::set result [eval oaBusNetDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusNetFind { args } {
        ::set result [eval oaBusNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusNetIsValidName { args } {
        ::set result [eval oaBusNet_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusTermBitCreate { args } {
        ::set result [eval oaBusTermBit_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusTermBitFind { args } {
        ::set result [eval oaBusTermBit_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusTermBitIsValidName { args } {
        ::set result [eval oaBusTermBit_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusTermCreate { args } {
        ::set result [eval oaBusTerm_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusTermDefCreate { args } {
        ::set result [eval oaBusTermDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusTermDefFind { args } {
        ::set result [eval oaBusTermDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusTermFind { args } {
        ::set result [eval oaBusTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc BusTermIsValidName { args } {
        ::set result [eval oaBusTerm_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ByteArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc CMAttr { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc CMAttrArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Init function for module oaCM
    proc CMInit { args } {
        ::set result [eval oaCMInit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc CMProtocol { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc CMProtocolArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc CMapGetCMap { args } {
        ::set result [eval oaCMap_getCMap $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaCategory'
    proc Category { args } {
        ::set result [eval oaCategory $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for Category
    proc CategoryEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaCdbaNS'
    proc CdbaNS { args } {
        ::set result [eval oaCdbaNS $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellDMDataDestroy { args } {
        ::set result [eval oaCellDMData_destroy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellDMDataExists { args } {
        ::set result [eval oaCellDMData_exists $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellDMDataFind { args } {
        ::set result [eval oaCellDMData_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellDMDataOpen { args } {
        ::set result [eval oaCellDMData_open $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellDMDataRecover { args } {
        ::set result [eval oaCellDMData_recover $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellFind { args } {
        ::set result [eval oaCell_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellGet { args } {
        ::set result [eval oaCell_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaCellType'
    proc CellType { args } {
        ::set result [eval oaCellType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for CellType
    proc CellTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellViewDMDataDestroy { args } {
        ::set result [eval oaCellViewDMData_destroy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellViewDMDataExists { args } {
        ::set result [eval oaCellViewDMData_exists $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellViewDMDataFind { args } {
        ::set result [eval oaCellViewDMData_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellViewDMDataOpen { args } {
        ::set result [eval oaCellViewDMData_open $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellViewDMDataRecover { args } {
        ::set result [eval oaCellViewDMData_recover $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellViewFind { args } {
        ::set result [eval oaCellView_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CellViewGet { args } {
        ::set result [eval oaCellView_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ChangeMgrGet { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaClearanceMeasure'
    proc ClearanceMeasure { args } {
        ::set result [eval oaClearanceMeasure $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ClearanceMeasure
    proc ClearanceMeasureEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ClusterBoundaryCreate { args } {
        ::set result [eval oaClusterBoundary_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ClusterBoundaryFind { args } {
        ::set result [eval oaClusterBoundary_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ClusterCreate { args } {
        ::set result [eval oaCluster_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ClusterFind { args } {
        ::set result [eval oaCluster_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaClusterType'
    proc ClusterType { args } {
        ::set result [eval oaClusterType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ClusterType
    proc ClusterTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaCollection_oaAnalysisLib_oaTech'
    proc Collection_oaAnalysisLib_oaTech { args } {
        ::set result [eval oaCollection_oaAnalysisLib_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAnalysisOpPoint_oaAnalysisPoint'
    proc Collection_oaAnalysisOpPoint_oaAnalysisPoint { args } {
        ::set result [eval oaCollection_oaAnalysisOpPoint_oaAnalysisPoint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAnalysisOpPoint_oaOpPointHeader'
    proc Collection_oaAnalysisOpPoint_oaOpPointHeader { args } {
        ::set result [eval oaCollection_oaAnalysisOpPoint_oaOpPointHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAnalysisPoint_oaDesign'
    proc Collection_oaAnalysisPoint_oaDesign { args } {
        ::set result [eval oaCollection_oaAnalysisPoint_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAnalysisPoint_oaParasiticNetwork'
    proc Collection_oaAnalysisPoint_oaParasiticNetwork { args } {
        ::set result [eval oaCollection_oaAnalysisPoint_oaParasiticNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppDef_oaDMData'
    proc Collection_oaAppDef_oaDMData { args } {
        ::set result [eval oaCollection_oaAppDef_oaDMData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppDef_oaDesign'
    proc Collection_oaAppDef_oaDesign { args } {
        ::set result [eval oaCollection_oaAppDef_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppDef_oaObject'
    proc Collection_oaAppDef_oaObject { args } {
        ::set result [eval oaCollection_oaAppDef_oaObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppDef_oaSession'
    proc Collection_oaAppDef_oaSession { args } {
        ::set result [eval oaCollection_oaAppDef_oaSession $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppDef_oaTech'
    proc Collection_oaAppDef_oaTech { args } {
        ::set result [eval oaCollection_oaAppDef_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppObjectDef_oaDMData'
    proc Collection_oaAppObjectDef_oaDMData { args } {
        ::set result [eval oaCollection_oaAppObjectDef_oaDMData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppObjectDef_oaDesign'
    proc Collection_oaAppObjectDef_oaDesign { args } {
        ::set result [eval oaCollection_oaAppObjectDef_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppObjectDef_oaSession'
    proc Collection_oaAppObjectDef_oaSession { args } {
        ::set result [eval oaCollection_oaAppObjectDef_oaSession $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppObjectDef_oaTech'
    proc Collection_oaAppObjectDef_oaTech { args } {
        ::set result [eval oaCollection_oaAppObjectDef_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppObject_oaDMData'
    proc Collection_oaAppObject_oaDMData { args } {
        ::set result [eval oaCollection_oaAppObject_oaDMData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppObject_oaDesign'
    proc Collection_oaAppObject_oaDesign { args } {
        ::set result [eval oaCollection_oaAppObject_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAppObject_oaTech'
    proc Collection_oaAppObject_oaTech { args } {
        ::set result [eval oaCollection_oaAppObject_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAreaBoundary_oaBlock'
    proc Collection_oaAreaBoundary_oaBlock { args } {
        ::set result [eval oaCollection_oaAreaBoundary_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAssignment_oaBlock'
    proc Collection_oaAssignment_oaBlock { args } {
        ::set result [eval oaCollection_oaAssignment_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaAssignment_oaInst'
    proc Collection_oaAssignment_oaInst { args } {
        ::set result [eval oaCollection_oaAssignment_oaInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBitNet_oaBitNet'
    proc Collection_oaBitNet_oaBitNet { args } {
        ::set result [eval oaCollection_oaBitNet_oaBitNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBitNet_oaNet'
    proc Collection_oaBitNet_oaNet { args } {
        ::set result [eval oaCollection_oaBitNet_oaNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBitTerm_oaBitTerm'
    proc Collection_oaBitTerm_oaBitTerm { args } {
        ::set result [eval oaCollection_oaBitTerm_oaBitTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBlockObject_oaMarker'
    proc Collection_oaBlockObject_oaMarker { args } {
        ::set result [eval oaCollection_oaBlockObject_oaMarker $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBlockage_oaBlock'
    proc Collection_oaBlockage_oaBlock { args } {
        ::set result [eval oaCollection_oaBlockage_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBlockage_oaCluster'
    proc Collection_oaBlockage_oaCluster { args } {
        ::set result [eval oaCollection_oaBlockage_oaCluster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBlockage_oaInst'
    proc Collection_oaBlockage_oaInst { args } {
        ::set result [eval oaCollection_oaBlockage_oaInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBlockage_oaLayerHeader'
    proc Collection_oaBlockage_oaLayerHeader { args } {
        ::set result [eval oaCollection_oaBlockage_oaLayerHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBlockage_oaPRBoundary'
    proc Collection_oaBlockage_oaPRBoundary { args } {
        ::set result [eval oaCollection_oaBlockage_oaPRBoundary $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBusNetBit_oaBusNetDef'
    proc Collection_oaBusNetBit_oaBusNetDef { args } {
        ::set result [eval oaCollection_oaBusNetBit_oaBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBusNetDef_oaBlock'
    proc Collection_oaBusNetDef_oaBlock { args } {
        ::set result [eval oaCollection_oaBusNetDef_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBusNet_oaBusNetDef'
    proc Collection_oaBusNet_oaBusNetDef { args } {
        ::set result [eval oaCollection_oaBusNet_oaBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBusTermBit_oaBusTermDef'
    proc Collection_oaBusTermBit_oaBusTermDef { args } {
        ::set result [eval oaCollection_oaBusTermBit_oaBusTermDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBusTermDef_oaBlock'
    proc Collection_oaBusTermDef_oaBlock { args } {
        ::set result [eval oaCollection_oaBusTermDef_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaBusTerm_oaBusTermDef'
    proc Collection_oaBusTerm_oaBusTermDef { args } {
        ::set result [eval oaCollection_oaBusTerm_oaBusTermDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaCellView_oaCell'
    proc Collection_oaCellView_oaCell { args } {
        ::set result [eval oaCollection_oaCellView_oaCell $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaCellView_oaLib'
    proc Collection_oaCellView_oaLib { args } {
        ::set result [eval oaCollection_oaCellView_oaLib $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaCellView_oaView'
    proc Collection_oaCellView_oaView { args } {
        ::set result [eval oaCollection_oaCellView_oaView $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaCell_oaLib'
    proc Collection_oaCell_oaLib { args } {
        ::set result [eval oaCollection_oaCell_oaLib $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaClusterBoundary_oaCluster'
    proc Collection_oaClusterBoundary_oaCluster { args } {
        ::set result [eval oaCollection_oaClusterBoundary_oaCluster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaCluster_oaBlock'
    proc Collection_oaCluster_oaBlock { args } {
        ::set result [eval oaCollection_oaCluster_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaCluster_oaCluster'
    proc Collection_oaCluster_oaCluster { args } {
        ::set result [eval oaCollection_oaCluster_oaCluster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConnectDef_oaBlock'
    proc Collection_oaConnectDef_oaBlock { args } {
        ::set result [eval oaCollection_oaConnectDef_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConstraintGroupHeader_oaDesign'
    proc Collection_oaConstraintGroupHeader_oaDesign { args } {
        ::set result [eval oaCollection_oaConstraintGroupHeader_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConstraintGroupHeader_oaTech'
    proc Collection_oaConstraintGroupHeader_oaTech { args } {
        ::set result [eval oaCollection_oaConstraintGroupHeader_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConstraintGroupMem_oaConstraintGroup'
    proc Collection_oaConstraintGroupMem_oaConstraintGroup { args } {
        ::set result [eval oaCollection_oaConstraintGroupMem_oaConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConstraintGroupMem_oaConstraintGroupHeader'
    proc Collection_oaConstraintGroupMem_oaConstraintGroupHeader { args } {
        ::set result [eval oaCollection_oaConstraintGroupMem_oaConstraintGroupHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConstraintGroup_oaDesign'
    proc Collection_oaConstraintGroup_oaDesign { args } {
        ::set result [eval oaCollection_oaConstraintGroup_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConstraintGroup_oaTech'
    proc Collection_oaConstraintGroup_oaTech { args } {
        ::set result [eval oaCollection_oaConstraintGroup_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConstraintParam_oaDesign'
    proc Collection_oaConstraintParam_oaDesign { args } {
        ::set result [eval oaCollection_oaConstraintParam_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConstraintParam_oaTech'
    proc Collection_oaConstraintParam_oaTech { args } {
        ::set result [eval oaCollection_oaConstraintParam_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConstraint_oaConstraintGroup'
    proc Collection_oaConstraint_oaConstraintGroup { args } {
        ::set result [eval oaCollection_oaConstraint_oaConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConstraint_oaDesign'
    proc Collection_oaConstraint_oaDesign { args } {
        ::set result [eval oaCollection_oaConstraint_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaConstraint_oaTech'
    proc Collection_oaConstraint_oaTech { args } {
        ::set result [eval oaCollection_oaConstraint_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaDMData_oaDMData'
    proc Collection_oaDMData_oaDMData { args } {
        ::set result [eval oaCollection_oaDMData_oaDMData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaDMFile_oaDMContainer'
    proc Collection_oaDMFile_oaDMContainer { args } {
        ::set result [eval oaCollection_oaDMFile_oaDMContainer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaDMFile_oaDMFile'
    proc Collection_oaDMFile_oaDMFile { args } {
        ::set result [eval oaCollection_oaDMFile_oaDMFile $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaDerivedLayerParam_oaTech'
    proc Collection_oaDerivedLayerParam_oaTech { args } {
        ::set result [eval oaCollection_oaDerivedLayerParam_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaDesignObject_oaNode'
    proc Collection_oaDesignObject_oaNode { args } {
        ::set result [eval oaCollection_oaDesignObject_oaNode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaDesignObject_oaParasiticNetwork'
    proc Collection_oaDesignObject_oaParasiticNetwork { args } {
        ::set result [eval oaCollection_oaDesignObject_oaParasiticNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaDesign_oaDesign'
    proc Collection_oaDesign_oaDesign { args } {
        ::set result [eval oaCollection_oaDesign_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaDevice_oaNode'
    proc Collection_oaDevice_oaNode { args } {
        ::set result [eval oaCollection_oaDevice_oaNode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaDevice_oaParasiticNetwork'
    proc Collection_oaDevice_oaParasiticNetwork { args } {
        ::set result [eval oaCollection_oaDevice_oaParasiticNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaDevice_oaSubNetwork'
    proc Collection_oaDevice_oaSubNetwork { args } {
        ::set result [eval oaCollection_oaDevice_oaSubNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaElmore_oaLumpedElmore'
    proc Collection_oaElmore_oaLumpedElmore { args } {
        ::set result [eval oaCollection_oaElmore_oaLumpedElmore $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaElmore_oaPiElmore'
    proc Collection_oaElmore_oaPiElmore { args } {
        ::set result [eval oaCollection_oaElmore_oaPiElmore $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaFigGroupMem_oaFigGroup'
    proc Collection_oaFigGroupMem_oaFigGroup { args } {
        ::set result [eval oaCollection_oaFigGroupMem_oaFigGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaFigGroup_oaBlock'
    proc Collection_oaFigGroup_oaBlock { args } {
        ::set result [eval oaCollection_oaFigGroup_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaGCellPattern_oaBlock'
    proc Collection_oaGCellPattern_oaBlock { args } {
        ::set result [eval oaCollection_oaGCellPattern_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaGroupMember_oaObject'
    proc Collection_oaGroupMember_oaObject { args } {
        ::set result [eval oaCollection_oaGroupMember_oaObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaGroup_oaDMData'
    proc Collection_oaGroup_oaDMData { args } {
        ::set result [eval oaCollection_oaGroup_oaDMData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaGroup_oaDesign'
    proc Collection_oaGroup_oaDesign { args } {
        ::set result [eval oaCollection_oaGroup_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaGroup_oaLib'
    proc Collection_oaGroup_oaLib { args } {
        ::set result [eval oaCollection_oaGroup_oaLib $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaGroup_oaObject'
    proc Collection_oaGroup_oaObject { args } {
        ::set result [eval oaCollection_oaGroup_oaObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaGroup_oaTech'
    proc Collection_oaGroup_oaTech { args } {
        ::set result [eval oaCollection_oaGroup_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaGuide_oaBlock'
    proc Collection_oaGuide_oaBlock { args } {
        ::set result [eval oaCollection_oaGuide_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaGuide_oaLayerHeader'
    proc Collection_oaGuide_oaLayerHeader { args } {
        ::set result [eval oaCollection_oaGuide_oaLayerHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaGuide_oaNet'
    proc Collection_oaGuide_oaNet { args } {
        ::set result [eval oaCollection_oaGuide_oaNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaInstHeader_oaBlock'
    proc Collection_oaInstHeader_oaBlock { args } {
        ::set result [eval oaCollection_oaInstHeader_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaInstHeader_oaDesign'
    proc Collection_oaInstHeader_oaDesign { args } {
        ::set result [eval oaCollection_oaInstHeader_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaInstHeader_oaInstHeader'
    proc Collection_oaInstHeader_oaInstHeader { args } {
        ::set result [eval oaCollection_oaInstHeader_oaInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaInstTerm_oaBlock'
    proc Collection_oaInstTerm_oaBlock { args } {
        ::set result [eval oaCollection_oaInstTerm_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaInstTerm_oaInst'
    proc Collection_oaInstTerm_oaInst { args } {
        ::set result [eval oaCollection_oaInstTerm_oaInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaInstTerm_oaNet'
    proc Collection_oaInstTerm_oaNet { args } {
        ::set result [eval oaCollection_oaInstTerm_oaNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaInst_oaBlock'
    proc Collection_oaInst_oaBlock { args } {
        ::set result [eval oaCollection_oaInst_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaInst_oaCluster'
    proc Collection_oaInst_oaCluster { args } {
        ::set result [eval oaCollection_oaInst_oaCluster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaInst_oaInstHeader'
    proc Collection_oaInst_oaInstHeader { args } {
        ::set result [eval oaCollection_oaInst_oaInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaLPPHeader_oaBlock'
    proc Collection_oaLPPHeader_oaBlock { args } {
        ::set result [eval oaCollection_oaLPPHeader_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaLPPHeader_oaLayerHeader'
    proc Collection_oaLPPHeader_oaLayerHeader { args } {
        ::set result [eval oaCollection_oaLPPHeader_oaLayerHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaLayerArrayConstraint_oaConstraintGroup'
    proc Collection_oaLayerArrayConstraint_oaConstraintGroup { args } {
        ::set result [eval oaCollection_oaLayerArrayConstraint_oaConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaLayerConstraint_oaConstraintGroup'
    proc Collection_oaLayerConstraint_oaConstraintGroup { args } {
        ::set result [eval oaCollection_oaLayerConstraint_oaConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaLayerHeader_oaBlock'
    proc Collection_oaLayerHeader_oaBlock { args } {
        ::set result [eval oaCollection_oaLayerHeader_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaLayerPairConstraint_oaConstraintGroup'
    proc Collection_oaLayerPairConstraint_oaConstraintGroup { args } {
        ::set result [eval oaCollection_oaLayerPairConstraint_oaConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaLayer_oaTech'
    proc Collection_oaLayer_oaTech { args } {
        ::set result [eval oaCollection_oaLayer_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaLibDefListMem_oaLibDefList'
    proc Collection_oaLibDefListMem_oaLibDefList { args } {
        ::set result [eval oaCollection_oaLibDefListMem_oaLibDefList $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaLibDefList_oaLibDefList'
    proc Collection_oaLibDefList_oaLibDefList { args } {
        ::set result [eval oaCollection_oaLibDefList_oaLibDefList $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaLib_oaLib'
    proc Collection_oaLib_oaLib { args } {
        ::set result [eval oaCollection_oaLib_oaLib $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaMarker_oaBlock'
    proc Collection_oaMarker_oaBlock { args } {
        ::set result [eval oaCollection_oaMarker_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaMarker_oaBlockObject'
    proc Collection_oaMarker_oaBlockObject { args } {
        ::set result [eval oaCollection_oaMarker_oaBlockObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModAssignment_oaModInst'
    proc Collection_oaModAssignment_oaModInst { args } {
        ::set result [eval oaCollection_oaModAssignment_oaModInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModAssignment_oaModule'
    proc Collection_oaModAssignment_oaModule { args } {
        ::set result [eval oaCollection_oaModAssignment_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModBitNet_oaModBitNet'
    proc Collection_oaModBitNet_oaModBitNet { args } {
        ::set result [eval oaCollection_oaModBitNet_oaModBitNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModBitNet_oaModNet'
    proc Collection_oaModBitNet_oaModNet { args } {
        ::set result [eval oaCollection_oaModBitNet_oaModNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModBusNetBit_oaModBusNetDef'
    proc Collection_oaModBusNetBit_oaModBusNetDef { args } {
        ::set result [eval oaCollection_oaModBusNetBit_oaModBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModBusNetDef_oaModule'
    proc Collection_oaModBusNetDef_oaModule { args } {
        ::set result [eval oaCollection_oaModBusNetDef_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModBusNet_oaModBusNetDef'
    proc Collection_oaModBusNet_oaModBusNetDef { args } {
        ::set result [eval oaCollection_oaModBusNet_oaModBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModBusTermBit_oaModBusTermDef'
    proc Collection_oaModBusTermBit_oaModBusTermDef { args } {
        ::set result [eval oaCollection_oaModBusTermBit_oaModBusTermDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModBusTermDef_oaModule'
    proc Collection_oaModBusTermDef_oaModule { args } {
        ::set result [eval oaCollection_oaModBusTermDef_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModBusTerm_oaModBusTermDef'
    proc Collection_oaModBusTerm_oaModBusTermDef { args } {
        ::set result [eval oaCollection_oaModBusTerm_oaModBusTermDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModConnectDef_oaModule'
    proc Collection_oaModConnectDef_oaModule { args } {
        ::set result [eval oaCollection_oaModConnectDef_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModDesignInst_oaModInstHeader'
    proc Collection_oaModDesignInst_oaModInstHeader { args } {
        ::set result [eval oaCollection_oaModDesignInst_oaModInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModInstHeader_oaDesign'
    proc Collection_oaModInstHeader_oaDesign { args } {
        ::set result [eval oaCollection_oaModInstHeader_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModInstHeader_oaModInstHeader'
    proc Collection_oaModInstHeader_oaModInstHeader { args } {
        ::set result [eval oaCollection_oaModInstHeader_oaModInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModInstTerm_oaModInst'
    proc Collection_oaModInstTerm_oaModInst { args } {
        ::set result [eval oaCollection_oaModInstTerm_oaModInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModInstTerm_oaModNet'
    proc Collection_oaModInstTerm_oaModNet { args } {
        ::set result [eval oaCollection_oaModInstTerm_oaModNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModInstTerm_oaModule'
    proc Collection_oaModInstTerm_oaModule { args } {
        ::set result [eval oaCollection_oaModInstTerm_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModInst_oaModVectorInstDef'
    proc Collection_oaModInst_oaModVectorInstDef { args } {
        ::set result [eval oaCollection_oaModInst_oaModVectorInstDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModInst_oaModule'
    proc Collection_oaModInst_oaModule { args } {
        ::set result [eval oaCollection_oaModInst_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModModuleInstHeader_oaModule'
    proc Collection_oaModModuleInstHeader_oaModule { args } {
        ::set result [eval oaCollection_oaModModuleInstHeader_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModModuleInst_oaModModuleInstHeader'
    proc Collection_oaModModuleInst_oaModModuleInstHeader { args } {
        ::set result [eval oaCollection_oaModModuleInst_oaModModuleInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModNet_oaModBundleNet'
    proc Collection_oaModNet_oaModBundleNet { args } {
        ::set result [eval oaCollection_oaModNet_oaModBundleNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModNet_oaModule'
    proc Collection_oaModNet_oaModule { args } {
        ::set result [eval oaCollection_oaModNet_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModTerm_oaModBundleTerm'
    proc Collection_oaModTerm_oaModBundleTerm { args } {
        ::set result [eval oaCollection_oaModTerm_oaModBundleTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModTerm_oaModNet'
    proc Collection_oaModTerm_oaModNet { args } {
        ::set result [eval oaCollection_oaModTerm_oaModNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModTerm_oaModule'
    proc Collection_oaModTerm_oaModule { args } {
        ::set result [eval oaCollection_oaModTerm_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModVectorInstDef_oaModule'
    proc Collection_oaModVectorInstDef_oaModule { args } {
        ::set result [eval oaCollection_oaModVectorInstDef_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaModule_oaDesign'
    proc Collection_oaModule_oaDesign { args } {
        ::set result [eval oaCollection_oaModule_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaMutualInductor_oaInductor'
    proc Collection_oaMutualInductor_oaInductor { args } {
        ::set result [eval oaCollection_oaMutualInductor_oaInductor $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaNet_oaBlock'
    proc Collection_oaNet_oaBlock { args } {
        ::set result [eval oaCollection_oaNet_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaNet_oaBundleNet'
    proc Collection_oaNet_oaBundleNet { args } {
        ::set result [eval oaCollection_oaNet_oaBundleNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaNode_oaParasiticNetwork'
    proc Collection_oaNode_oaParasiticNetwork { args } {
        ::set result [eval oaCollection_oaNode_oaParasiticNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaNode_oaStdDevice'
    proc Collection_oaNode_oaStdDevice { args } {
        ::set result [eval oaCollection_oaNode_oaStdDevice $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaNode_oaSubNetwork'
    proc Collection_oaNode_oaSubNetwork { args } {
        ::set result [eval oaCollection_oaNode_oaSubNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccAssignment_oaOccInst'
    proc Collection_oaOccAssignment_oaOccInst { args } {
        ::set result [eval oaCollection_oaOccAssignment_oaOccInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccAssignment_oaOccurrence'
    proc Collection_oaOccAssignment_oaOccurrence { args } {
        ::set result [eval oaCollection_oaOccAssignment_oaOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccBitNet_oaOccBitNet'
    proc Collection_oaOccBitNet_oaOccBitNet { args } {
        ::set result [eval oaCollection_oaOccBitNet_oaOccBitNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccBitNet_oaOccNet'
    proc Collection_oaOccBitNet_oaOccNet { args } {
        ::set result [eval oaCollection_oaOccBitNet_oaOccNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccBusNetBit_oaBusNetDef'
    proc Collection_oaOccBusNetBit_oaBusNetDef { args } {
        ::set result [eval oaCollection_oaOccBusNetBit_oaBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccBusNetBit_oaOccBusNetDef'
    proc Collection_oaOccBusNetBit_oaOccBusNetDef { args } {
        ::set result [eval oaCollection_oaOccBusNetBit_oaOccBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccBusNetDef_oaOccurrence'
    proc Collection_oaOccBusNetDef_oaOccurrence { args } {
        ::set result [eval oaCollection_oaOccBusNetDef_oaOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccBusNet_oaBusNetDef'
    proc Collection_oaOccBusNet_oaBusNetDef { args } {
        ::set result [eval oaCollection_oaOccBusNet_oaBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccBusNet_oaOccBusNetDef'
    proc Collection_oaOccBusNet_oaOccBusNetDef { args } {
        ::set result [eval oaCollection_oaOccBusNet_oaOccBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccBusTermBit_oaOccBusTermDef'
    proc Collection_oaOccBusTermBit_oaOccBusTermDef { args } {
        ::set result [eval oaCollection_oaOccBusTermBit_oaOccBusTermDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccBusTermDef_oaOccurrence'
    proc Collection_oaOccBusTermDef_oaOccurrence { args } {
        ::set result [eval oaCollection_oaOccBusTermDef_oaOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccBusTerm_oaOccBusTermDef'
    proc Collection_oaOccBusTerm_oaOccBusTermDef { args } {
        ::set result [eval oaCollection_oaOccBusTerm_oaOccBusTermDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccConnectDef_oaOccurrence'
    proc Collection_oaOccConnectDef_oaOccurrence { args } {
        ::set result [eval oaCollection_oaOccConnectDef_oaOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccDesignInst_oaOccInstHeader'
    proc Collection_oaOccDesignInst_oaOccInstHeader { args } {
        ::set result [eval oaCollection_oaOccDesignInst_oaOccInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccInstHeader_oaOccInstHeader'
    proc Collection_oaOccInstHeader_oaOccInstHeader { args } {
        ::set result [eval oaCollection_oaOccInstHeader_oaOccInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccInstTerm_oaInstTerm'
    proc Collection_oaOccInstTerm_oaInstTerm { args } {
        ::set result [eval oaCollection_oaOccInstTerm_oaInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccInstTerm_oaModInstTerm'
    proc Collection_oaOccInstTerm_oaModInstTerm { args } {
        ::set result [eval oaCollection_oaOccInstTerm_oaModInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccInstTerm_oaOccInst'
    proc Collection_oaOccInstTerm_oaOccInst { args } {
        ::set result [eval oaCollection_oaOccInstTerm_oaOccInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccInstTerm_oaOccNet'
    proc Collection_oaOccInstTerm_oaOccNet { args } {
        ::set result [eval oaCollection_oaOccInstTerm_oaOccNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccInstTerm_oaOccurrence'
    proc Collection_oaOccInstTerm_oaOccurrence { args } {
        ::set result [eval oaCollection_oaOccInstTerm_oaOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccInst_oaInst'
    proc Collection_oaOccInst_oaInst { args } {
        ::set result [eval oaCollection_oaOccInst_oaInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccInst_oaModInst'
    proc Collection_oaOccInst_oaModInst { args } {
        ::set result [eval oaCollection_oaOccInst_oaModInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccInst_oaOccVectorInstDef'
    proc Collection_oaOccInst_oaOccVectorInstDef { args } {
        ::set result [eval oaCollection_oaOccInst_oaOccVectorInstDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccInst_oaOccurrence'
    proc Collection_oaOccInst_oaOccurrence { args } {
        ::set result [eval oaCollection_oaOccInst_oaOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccModuleInstHeader_oaOccurrence'
    proc Collection_oaOccModuleInstHeader_oaOccurrence { args } {
        ::set result [eval oaCollection_oaOccModuleInstHeader_oaOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccModuleInst_oaOccModuleInstHeader'
    proc Collection_oaOccModuleInst_oaOccModuleInstHeader { args } {
        ::set result [eval oaCollection_oaOccModuleInst_oaOccModuleInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccNet_oaBundleNet'
    proc Collection_oaOccNet_oaBundleNet { args } {
        ::set result [eval oaCollection_oaOccNet_oaBundleNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccNet_oaModNet'
    proc Collection_oaOccNet_oaModNet { args } {
        ::set result [eval oaCollection_oaOccNet_oaModNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccNet_oaNet'
    proc Collection_oaOccNet_oaNet { args } {
        ::set result [eval oaCollection_oaOccNet_oaNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccNet_oaOccBundleNet'
    proc Collection_oaOccNet_oaOccBundleNet { args } {
        ::set result [eval oaCollection_oaOccNet_oaOccBundleNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccNet_oaOccNet'
    proc Collection_oaOccNet_oaOccNet { args } {
        ::set result [eval oaCollection_oaOccNet_oaOccNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccNet_oaOccurrence'
    proc Collection_oaOccNet_oaOccurrence { args } {
        ::set result [eval oaCollection_oaOccNet_oaOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccTerm_oaModTerm'
    proc Collection_oaOccTerm_oaModTerm { args } {
        ::set result [eval oaCollection_oaOccTerm_oaModTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccTerm_oaOccBundleTerm'
    proc Collection_oaOccTerm_oaOccBundleTerm { args } {
        ::set result [eval oaCollection_oaOccTerm_oaOccBundleTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccTerm_oaOccNet'
    proc Collection_oaOccTerm_oaOccNet { args } {
        ::set result [eval oaCollection_oaOccTerm_oaOccNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccTerm_oaOccurrence'
    proc Collection_oaOccTerm_oaOccurrence { args } {
        ::set result [eval oaCollection_oaOccTerm_oaOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccTerm_oaTerm'
    proc Collection_oaOccTerm_oaTerm { args } {
        ::set result [eval oaCollection_oaOccTerm_oaTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccVectorInstDef_oaOccurrence'
    proc Collection_oaOccVectorInstDef_oaOccurrence { args } {
        ::set result [eval oaCollection_oaOccVectorInstDef_oaOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOccurrence_oaModule'
    proc Collection_oaOccurrence_oaModule { args } {
        ::set result [eval oaCollection_oaOccurrence_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOpPointHeader_oaOpPoint'
    proc Collection_oaOpPointHeader_oaOpPoint { args } {
        ::set result [eval oaCollection_oaOpPointHeader_oaOpPoint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOpPoint_oaAnalysisLib'
    proc Collection_oaOpPoint_oaAnalysisLib { args } {
        ::set result [eval oaCollection_oaOpPoint_oaAnalysisLib $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaOpPoint_oaTech'
    proc Collection_oaOpPoint_oaTech { args } {
        ::set result [eval oaCollection_oaOpPoint_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaParasiticNetwork_oaParasiticNetwork'
    proc Collection_oaParasiticNetwork_oaParasiticNetwork { args } {
        ::set result [eval oaCollection_oaParasiticNetwork_oaParasiticNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaPinFig_oaPin'
    proc Collection_oaPinFig_oaPin { args } {
        ::set result [eval oaCollection_oaPinFig_oaPin $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaPin_oaBlock'
    proc Collection_oaPin_oaBlock { args } {
        ::set result [eval oaCollection_oaPin_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaPin_oaNet'
    proc Collection_oaPin_oaNet { args } {
        ::set result [eval oaCollection_oaPin_oaNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaPin_oaTerm'
    proc Collection_oaPin_oaTerm { args } {
        ::set result [eval oaCollection_oaPin_oaTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaPoleResidue_oaPiPoleResidue'
    proc Collection_oaPoleResidue_oaPiPoleResidue { args } {
        ::set result [eval oaCollection_oaPoleResidue_oaPiPoleResidue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaProp_oaObject'
    proc Collection_oaProp_oaObject { args } {
        ::set result [eval oaCollection_oaProp_oaObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaPurpose_oaTech'
    proc Collection_oaPurpose_oaTech { args } {
        ::set result [eval oaCollection_oaPurpose_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRoute_oaBlock'
    proc Collection_oaRoute_oaBlock { args } {
        ::set result [eval oaCollection_oaRoute_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRoute_oaInstTerm'
    proc Collection_oaRoute_oaInstTerm { args } {
        ::set result [eval oaCollection_oaRoute_oaInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRoute_oaNet'
    proc Collection_oaRoute_oaNet { args } {
        ::set result [eval oaCollection_oaRoute_oaNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRoute_oaPin'
    proc Collection_oaRoute_oaPin { args } {
        ::set result [eval oaCollection_oaRoute_oaPin $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRoute_oaShape'
    proc Collection_oaRoute_oaShape { args } {
        ::set result [eval oaCollection_oaRoute_oaShape $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRoute_oaSteiner'
    proc Collection_oaRoute_oaSteiner { args } {
        ::set result [eval oaCollection_oaRoute_oaSteiner $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRoute_oaTerm'
    proc Collection_oaRoute_oaTerm { args } {
        ::set result [eval oaCollection_oaRoute_oaTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRoute_oaVia'
    proc Collection_oaRoute_oaVia { args } {
        ::set result [eval oaCollection_oaRoute_oaVia $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRowHeader_oaBlock'
    proc Collection_oaRowHeader_oaBlock { args } {
        ::set result [eval oaCollection_oaRowHeader_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRowHeader_oaSiteDef'
    proc Collection_oaRowHeader_oaSiteDef { args } {
        ::set result [eval oaCollection_oaRowHeader_oaSiteDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRow_oaBlock'
    proc Collection_oaRow_oaBlock { args } {
        ::set result [eval oaCollection_oaRow_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaRow_oaRowHeader'
    proc Collection_oaRow_oaRowHeader { args } {
        ::set result [eval oaCollection_oaRow_oaRowHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaScanChainInst_oaScanChainSet'
    proc Collection_oaScanChainInst_oaScanChainSet { args } {
        ::set result [eval oaCollection_oaScanChainInst_oaScanChainSet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaScanChainSet_oaScanChain'
    proc Collection_oaScanChainSet_oaScanChain { args } {
        ::set result [eval oaCollection_oaScanChainSet_oaScanChain $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaScanChain_oaBlock'
    proc Collection_oaScanChain_oaBlock { args } {
        ::set result [eval oaCollection_oaScanChain_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaShape_oaBlock'
    proc Collection_oaShape_oaBlock { args } {
        ::set result [eval oaCollection_oaShape_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaShape_oaLPPHeader'
    proc Collection_oaShape_oaLPPHeader { args } {
        ::set result [eval oaCollection_oaShape_oaLPPHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaShape_oaNet'
    proc Collection_oaShape_oaNet { args } {
        ::set result [eval oaCollection_oaShape_oaNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaSimpleConstraint_oaConstraintGroup'
    proc Collection_oaSimpleConstraint_oaConstraintGroup { args } {
        ::set result [eval oaCollection_oaSimpleConstraint_oaConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaSiteDef_oaTech'
    proc Collection_oaSiteDef_oaTech { args } {
        ::set result [eval oaCollection_oaSiteDef_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaSteiner_oaBlock'
    proc Collection_oaSteiner_oaBlock { args } {
        ::set result [eval oaCollection_oaSteiner_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaSteiner_oaLayerHeader'
    proc Collection_oaSteiner_oaLayerHeader { args } {
        ::set result [eval oaCollection_oaSteiner_oaLayerHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaSteiner_oaNet'
    proc Collection_oaSteiner_oaNet { args } {
        ::set result [eval oaCollection_oaSteiner_oaNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaSubNetwork_oaDevice'
    proc Collection_oaSubNetwork_oaDevice { args } {
        ::set result [eval oaCollection_oaSubNetwork_oaDevice $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaSubNetwork_oaNode'
    proc Collection_oaSubNetwork_oaNode { args } {
        ::set result [eval oaCollection_oaSubNetwork_oaNode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaSubNetwork_oaParasiticNetwork'
    proc Collection_oaSubNetwork_oaParasiticNetwork { args } {
        ::set result [eval oaCollection_oaSubNetwork_oaParasiticNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaTechHeader_oaTech'
    proc Collection_oaTechHeader_oaTech { args } {
        ::set result [eval oaCollection_oaTechHeader_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaTechLayerHeader_oaTech'
    proc Collection_oaTechLayerHeader_oaTech { args } {
        ::set result [eval oaCollection_oaTechLayerHeader_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaTechViaDefHeader_oaTech'
    proc Collection_oaTechViaDefHeader_oaTech { args } {
        ::set result [eval oaCollection_oaTechViaDefHeader_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaTech_oaTech'
    proc Collection_oaTech_oaTech { args } {
        ::set result [eval oaCollection_oaTech_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaTerm_oaBlock'
    proc Collection_oaTerm_oaBlock { args } {
        ::set result [eval oaCollection_oaTerm_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaTerm_oaBundleTerm'
    proc Collection_oaTerm_oaBundleTerm { args } {
        ::set result [eval oaCollection_oaTerm_oaBundleTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaTerm_oaNet'
    proc Collection_oaTerm_oaNet { args } {
        ::set result [eval oaCollection_oaTerm_oaNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaTrackPattern_oaBlock'
    proc Collection_oaTrackPattern_oaBlock { args } {
        ::set result [eval oaCollection_oaTrackPattern_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaValue_oaDesign'
    proc Collection_oaValue_oaDesign { args } {
        ::set result [eval oaCollection_oaValue_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaValue_oaTech'
    proc Collection_oaValue_oaTech { args } {
        ::set result [eval oaCollection_oaValue_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaVectorInstBit_oaVectorInstDef'
    proc Collection_oaVectorInstBit_oaVectorInstDef { args } {
        ::set result [eval oaCollection_oaVectorInstBit_oaVectorInstDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaVectorInstDef_oaBlock'
    proc Collection_oaVectorInstDef_oaBlock { args } {
        ::set result [eval oaCollection_oaVectorInstDef_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaVectorInst_oaVectorInstDef'
    proc Collection_oaVectorInst_oaVectorInstDef { args } {
        ::set result [eval oaCollection_oaVectorInst_oaVectorInstDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaViaDef_oaTech'
    proc Collection_oaViaDef_oaTech { args } {
        ::set result [eval oaCollection_oaViaDef_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaViaHeader_oaBlock'
    proc Collection_oaViaHeader_oaBlock { args } {
        ::set result [eval oaCollection_oaViaHeader_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaViaHeader_oaDesign'
    proc Collection_oaViaHeader_oaDesign { args } {
        ::set result [eval oaCollection_oaViaHeader_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaViaHeader_oaViaDef'
    proc Collection_oaViaHeader_oaViaDef { args } {
        ::set result [eval oaCollection_oaViaHeader_oaViaDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaViaHeader_oaViaHeader'
    proc Collection_oaViaHeader_oaViaHeader { args } {
        ::set result [eval oaCollection_oaViaHeader_oaViaHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaViaSpec_oaTech'
    proc Collection_oaViaSpec_oaTech { args } {
        ::set result [eval oaCollection_oaViaSpec_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaViaVariantHeader_oaDesign'
    proc Collection_oaViaVariantHeader_oaDesign { args } {
        ::set result [eval oaCollection_oaViaVariantHeader_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaViaVariantHeader_oaTech'
    proc Collection_oaViaVariantHeader_oaTech { args } {
        ::set result [eval oaCollection_oaViaVariantHeader_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaViaVariant_oaDesign'
    proc Collection_oaViaVariant_oaDesign { args } {
        ::set result [eval oaCollection_oaViaVariant_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaViaVariant_oaTech'
    proc Collection_oaViaVariant_oaTech { args } {
        ::set result [eval oaCollection_oaViaVariant_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaVia_oaBlock'
    proc Collection_oaVia_oaBlock { args } {
        ::set result [eval oaCollection_oaVia_oaBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaVia_oaNet'
    proc Collection_oaVia_oaNet { args } {
        ::set result [eval oaCollection_oaVia_oaNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaVia_oaViaHeader'
    proc Collection_oaVia_oaViaHeader { args } {
        ::set result [eval oaCollection_oaVia_oaViaHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaViewType_oaSession'
    proc Collection_oaViewType_oaSession { args } {
        ::set result [eval oaCollection_oaViewType_oaSession $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaCollection_oaView_oaLib'
    proc Collection_oaView_oaLib { args } {
        ::set result [eval oaCollection_oaView_oaLib $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaConnStatus'
    proc ConnStatus { args } {
        ::set result [eval oaConnStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ConnStatus
    proc ConnStatusEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaConnectivityType'
    proc ConnectivityType { args } {
        ::set result [eval oaConnectivityType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ConnectivityType
    proc ConnectivityTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaConstraintCollection'
    proc ConstraintCollection { args } {
        ::set result [eval oaConstraintCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ConstraintDefFind { args } {
        ::set result [eval oaConstraintDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ConstraintGroupCreate { args } {
        ::set result [eval oaConstraintGroup_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ConstraintGroupDefCreate { args } {
        ::set result [eval oaConstraintGroupDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ConstraintGroupDefFind { args } {
        ::set result [eval oaConstraintGroupDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ConstraintGroupDefGet { args } {
        ::set result [eval oaConstraintGroupDef_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ConstraintGroupFind { args } {
        ::set result [eval oaConstraintGroup_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ConstraintGroupHeaderFind { args } {
        ::set result [eval oaConstraintGroupHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ConstraintGroupMemCreate { args } {
        ::set result [eval oaConstraintGroupMem_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaConstraintGroupOperator'
    proc ConstraintGroupOperator { args } {
        ::set result [eval oaConstraintGroupOperator $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ConstraintGroupOperator
    proc ConstraintGroupOperatorEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaConstraintGroupType'
    proc ConstraintGroupType { args } {
        ::set result [eval oaConstraintGroupType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ConstraintGroupType
    proc ConstraintGroupTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaConstraintInGroupCollection'
    proc ConstraintInGroupCollection { args } {
        ::set result [eval oaConstraintInGroupCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ConstraintParamArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc ConstraintParamCreate { args } {
        ::set result [eval oaConstraintParam_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ConstraintParamDefCreate { args } {
        ::set result [eval oaConstraintParamDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ConstraintParamDefFind { args } {
        ::set result [eval oaConstraintParamDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ConstraintParamDefGet { args } {
        ::set result [eval oaConstraintParamDef_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaConstraintParamType'
    proc ConstraintParamType { args } {
        ::set result [eval oaConstraintParamType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ConstraintParamType
    proc ConstraintParamTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaCoreBoxSpec'
    proc CoreBoxSpec { args } {
        ::set result [eval oaCoreBoxSpec $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CouplingCapCreate { args } {
        ::set result [eval oaCouplingCap_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CustomViaCreate { args } {
        ::set result [eval oaCustomVia_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CustomViaDefCreate { args } {
        ::set result [eval oaCustomViaDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CustomViaVariantCreate { args } {
        ::set result [eval oaCustomViaVariant_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc CustomViaVariantFind { args } {
        ::set result [eval oaCustomViaVariant_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDBType'
    proc DBType { args } {
        ::set result [eval oaDBType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for DBType
    proc DBTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDMAttr'
    proc DMAttr { args } {
        ::set result [eval oaDMAttr $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc DMAttrArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc DMDataDestroy { args } {
        ::set result [eval oaDMData_destroy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DMDataExists { args } {
        ::set result [eval oaDMData_exists $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DMDataFind { args } {
        ::set result [eval oaDMData_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DMDataGetOpenDMDataObjs { args } {
        ::set result [eval oaDMData_getOpenDMDataObjs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Static member function
    proc DMDataGetRevNumber { args } {
        ::set result [eval oaDMData_getRevNumber $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DMDataOpen { args } {
        ::set result [eval oaDMData_open $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DMDataRecover { args } {
        ::set result [eval oaDMData_recover $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDMDataType'
    proc DMDataType { args } {
        ::set result [eval oaDMDataType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for DMDataType
    proc DMDataTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DMFileCreate { args } {
        ::set result [eval oaDMFile_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DMFileFind { args } {
        ::set result [eval oaDMFile_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Init function for module oaDM
    proc DMInit { args } {
        ::set result [eval oaDMInit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDMLockStatus'
    proc DMLockStatus { args } {
        ::set result [eval oaDMLockStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for DMLockStatus
    proc DMLockStatusEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDMObjectIter'
    proc DMObjectIter { args } {
        ::set result [eval oaDMObjectIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDMObjectStatusRef'
    proc DMObjectStatusRef { args } {
        ::set result [eval oaDMObjectStatusRef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDMObjectStatusRefIter'
    proc DMObjectStatusRefIter { args } {
        ::set result [eval oaDMObjectStatusRefIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDMObjectVersionRef'
    proc DMObjectVersionRef { args } {
        ::set result [eval oaDMObjectVersionRef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDMObjectVersionRefIter'
    proc DMObjectVersionRefIter { args } {
        ::set result [eval oaDMObjectVersionRefIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDataModelModType'
    proc DataModelModType { args } {
        ::set result [eval oaDataModelModType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for DataModelModType
    proc DataModelModTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDatabaseCollection'
    proc DatabaseCollection { args } {
        ::set result [eval oaDatabaseCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDatabaseIter'
    proc DatabaseIter { args } {
        ::set result [eval oaDatabaseIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDefNS'
    proc DefNS { args } {
        ::set result [eval oaDefNS $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDerivedLayerConnectivityType'
    proc DerivedLayerConnectivityType { args } {
        ::set result [eval oaDerivedLayerConnectivityType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for DerivedLayerConnectivityType
    proc DerivedLayerConnectivityTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DerivedLayerCreate { args } {
        ::set result [eval oaDerivedLayer_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DerivedLayerDefCreate { args } {
        ::set result [eval oaDerivedLayerDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DerivedLayerDefFind { args } {
        ::set result [eval oaDerivedLayerDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DerivedLayerDefGet { args } {
        ::set result [eval oaDerivedLayerDef_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DerivedLayerFind { args } {
        ::set result [eval oaDerivedLayer_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc DerivedLayerParamArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc DerivedLayerParamCreate { args } {
        ::set result [eval oaDerivedLayerParam_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DerivedLayerParamDefCreate { args } {
        ::set result [eval oaDerivedLayerParamDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DerivedLayerParamDefFind { args } {
        ::set result [eval oaDerivedLayerParamDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DerivedLayerParamDefGet { args } {
        ::set result [eval oaDerivedLayerParamDef_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDerivedLayerParamType'
    proc DerivedLayerParamType { args } {
        ::set result [eval oaDerivedLayerParamType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for DerivedLayerParamType
    proc DerivedLayerParamTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDesignAttrType'
    proc DesignAttrType { args } {
        ::set result [eval oaDesignAttrType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDesignCollection'
    proc DesignCollection { args } {
        ::set result [eval oaDesignCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDesignDataType'
    proc DesignDataType { args } {
        ::set result [eval oaDesignDataType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for DesignDataType
    proc DesignDataTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DesignDestroy { args } {
        ::set result [eval oaDesign_destroy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DesignExists { args } {
        ::set result [eval oaDesign_exists $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DesignFind { args } {
        ::set result [eval oaDesign_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DesignGetOpenDesigns { args } {
        ::set result [eval oaDesign_getOpenDesigns $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Static member function
    proc DesignGetRevNumber { args } {
        ::set result [eval oaDesign_getRevNumber $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Init function for module oaDesign
    proc DesignInit { args } {
        ::set result [eval oaDesignInit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDesignIter'
    proc DesignIter { args } {
        ::set result [eval oaDesignIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDesignObjectIter'
    proc DesignObjectIter { args } {
        ::set result [eval oaDesignObjectIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DesignOpen { args } {
        ::set result [eval oaDesign_open $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DesignRecover { args } {
        ::set result [eval oaDesign_recover $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc DesignViaVariantHeaderFind { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc DeviceFind { args } {
        ::set result [eval oaDevice_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DeviceGetUniqueId { args } {
        ::set result [eval oaDevice_getUniqueId $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DeviceIsValidName { args } {
        ::set result [eval oaDevice_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DiodeCreate { args } {
        ::set result [eval oaDiode_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDir'
    proc Dir { args } {
        ::set result [eval oaDir $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDirIter'
    proc DirIter { args } {
        ::set result [eval oaDirIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDistanceMeasureType'
    proc DistanceMeasureType { args } {
        ::set result [eval oaDistanceMeasureType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for DistanceMeasureType
    proc DistanceMeasureTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDomain'
    proc Domain { args } {
        ::set result [eval oaDomain $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for Domain
    proc DomainEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DonutCreate { args } {
        ::set result [eval oaDonut_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DonutGenBoundary { args } {
        ::set result [eval oaDonut_genBoundary $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Static member function
    proc DotCreate { args } {
        ::set result [eval oaDot_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc DoublePoint { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc DoublePropCreate { args } {
        ::set result [eval oaDoubleProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DoubleRangePropCreate { args } {
        ::set result [eval oaDoubleRangeProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaDualInt'
    proc DualInt { args } {
        ::set result [eval oaDualInt $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc DualInt1DTblValueCreate { args } {
        ::set result [eval oaDualInt1DTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc DualIntArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc DualIntValueCreate { args } {
        ::set result [eval oaDualIntValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc EllipseCreate { args } {
        ::set result [eval oaEllipse_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc EllipseGenBoundary { args } {
        ::set result [eval oaEllipse_genBoundary $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Static member function
    proc ElmoreCreate { args } {
        ::set result [eval oaElmore_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ElmoreFind { args } {
        ::set result [eval oaElmore_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaEndStyle'
    proc EndStyle { args } {
        ::set result [eval oaEndStyle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for EndStyle
    proc EndStyleEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaEndpointType'
    proc EndpointType { args } {
        ::set result [eval oaEndpointType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for EndpointType
    proc EndpointTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for 
    proc EnumCollection { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for 
    proc EnumPropCreate { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for 
    proc EnumPropIter { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc EvalTextCreate { args } {
        ::set result [eval oaEvalText_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc EvalTextLinkCreate { args } {
        ::set result [eval oaEvalTextLink_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc EvalTextLinkFind { args } {
        ::set result [eval oaEvalTextLink_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaExtrapolateType'
    proc ExtrapolateType { args } {
        ::set result [eval oaExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ExtrapolateType
    proc ExtrapolateTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaFSComponent'
    proc FSComponent { args } {
        ::set result [eval oaFSComponent $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc FeatureArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc FigGroupCreate { args } {
        ::set result [eval oaFigGroup_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc FigGroupFind { args } {
        ::set result [eval oaFigGroup_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc FigGroupMemCreate { args } {
        ::set result [eval oaFigGroupMem_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaFigGroupStatus'
    proc FigGroupStatus { args } {
        ::set result [eval oaFigGroupStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for FigGroupStatus
    proc FigGroupStatusEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaFile'
    proc File { args } {
        ::set result [eval oaFile $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc FileCopyFile { args } {
        ::set result [eval oaFile_copyFile $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc FlatGroupDefCreate { args } {
        ::set result [eval oaFlatGroupDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc FloatPropCreate { args } {
        ::set result [eval oaFloatProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc FloatRangePropCreate { args } {
        ::set result [eval oaFloatRangeProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc Flt1DTblValueCreate { args } {
        ::set result [eval oaFlt1DTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc Flt2DTblValueCreate { args } {
        ::set result [eval oaFlt2DTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc FltIntFltTblValueCreate { args } {
        ::set result [eval oaFltIntFltTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc FltValueCreate { args } {
        ::set result [eval oaFltValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaFont'
    proc Font { args } {
        ::set result [eval oaFont $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for Font
    proc FontEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc GCellPatternCreate { args } {
        ::set result [eval oaGCellPattern_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaGateOrientationType'
    proc GateOrientationType { args } {
        ::set result [eval oaGateOrientationType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for GateOrientationType
    proc GateOrientationTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc GroundedNodeCreate { args } {
        ::set result [eval oaGroundedNode_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaGroupCollection'
    proc GroupCollection { args } {
        ::set result [eval oaGroupCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc GroupCreate { args } {
        ::set result [eval oaGroup_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaGroupDefArray'
    proc GroupDefArray { args } {
        ::set result [eval oaGroupDefArray $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc GroupDefFind { args } {
        ::set result [eval oaGroupDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc GroupDefGet { args } {
        ::set result [eval oaGroupDef_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaGroupDeleteWhen'
    proc GroupDeleteWhen { args } {
        ::set result [eval oaGroupDeleteWhen $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for GroupDeleteWhen
    proc GroupDeleteWhenEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc GroupMemberCreate { args } {
        ::set result [eval oaGroupMember_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaGroupPurposeType'
    proc GroupPurposeType { args } {
        ::set result [eval oaGroupPurposeType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for GroupPurposeType
    proc GroupPurposeTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaGroupType'
    proc GroupType { args } {
        ::set result [eval oaGroupType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for GroupType
    proc GroupTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc GuideCreate { args } {
        ::set result [eval oaGuide_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc HierGroupDefCreate { args } {
        ::set result [eval oaHierGroupDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaHierPath'
    proc HierPath { args } {
        ::set result [eval oaHierPath $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc HierPropCreate { args } {
        ::set result [eval oaHierProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc InductorCreate { args } {
        ::set result [eval oaInductor_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc InstAttrDisplayCreate { args } {
        ::set result [eval oaInstAttrDisplay_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaInstAttrType'
    proc InstAttrType { args } {
        ::set result [eval oaInstAttrType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc InstFind { args } {
        ::set result [eval oaInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc InstHeaderFind { args } {
        ::set result [eval oaInstHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc InstHeaderSetMaster { args } {
        ::set result [eval oaInstHeader_setMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc InstPropDisplayCreate { args } {
        ::set result [eval oaInstPropDisplay_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaInstTermAttrType'
    proc InstTermAttrType { args } {
        ::set result [eval oaInstTermAttrType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc InstTermCreate { args } {
        ::set result [eval oaInstTerm_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc InstTermFind { args } {
        ::set result [eval oaInstTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc Int1DTblValueCreate { args } {
        ::set result [eval oaInt1DTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc Int2DTblValueCreate { args } {
        ::set result [eval oaInt2DTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc IntDualIntArrayTblValueCreate { args } {
        ::set result [eval oaIntDualIntArrayTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc IntFltTblValueCreate { args } {
        ::set result [eval oaIntFltTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc IntPropCreate { args } {
        ::set result [eval oaIntProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIntRange'
    proc IntRange { args } {
        ::set result [eval oaIntRange $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc IntRangeArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc IntRangeArray1DTblValueCreate { args } {
        ::set result [eval oaIntRangeArray1DTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc IntRangeArray2DTblValueCreate { args } {
        ::set result [eval oaIntRangeArray2DTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc IntRangeArrayValueCreate { args } {
        ::set result [eval oaIntRangeArrayValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc IntRangePropCreate { args } {
        ::set result [eval oaIntRangeProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc IntRangeValueCreate { args } {
        ::set result [eval oaIntRangeValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc IntValueCreate { args } {
        ::set result [eval oaIntValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaInterpolateType'
    proc InterpolateType { args } {
        ::set result [eval oaInterpolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for InterpolateType
    proc InterpolateTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc IsProcessActive { args } {
        ::set result [eval oaIsProcessActive $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaAnalysisLib'
    proc Iter_oaAnalysisLib { args } {
        ::set result [eval oaIter_oaAnalysisLib $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaAnalysisOpPoint'
    proc Iter_oaAnalysisOpPoint { args } {
        ::set result [eval oaIter_oaAnalysisOpPoint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaAnalysisPoint'
    proc Iter_oaAnalysisPoint { args } {
        ::set result [eval oaIter_oaAnalysisPoint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaAppDef'
    proc Iter_oaAppDef { args } {
        ::set result [eval oaIter_oaAppDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaAppObject'
    proc Iter_oaAppObject { args } {
        ::set result [eval oaIter_oaAppObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaAppObjectDef'
    proc Iter_oaAppObjectDef { args } {
        ::set result [eval oaIter_oaAppObjectDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaAreaBoundary'
    proc Iter_oaAreaBoundary { args } {
        ::set result [eval oaIter_oaAreaBoundary $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaAssignment'
    proc Iter_oaAssignment { args } {
        ::set result [eval oaIter_oaAssignment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaBitNet'
    proc Iter_oaBitNet { args } {
        ::set result [eval oaIter_oaBitNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaBitTerm'
    proc Iter_oaBitTerm { args } {
        ::set result [eval oaIter_oaBitTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaBlockObject'
    proc Iter_oaBlockObject { args } {
        ::set result [eval oaIter_oaBlockObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaBlockage'
    proc Iter_oaBlockage { args } {
        ::set result [eval oaIter_oaBlockage $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaBusNet'
    proc Iter_oaBusNet { args } {
        ::set result [eval oaIter_oaBusNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaBusNetBit'
    proc Iter_oaBusNetBit { args } {
        ::set result [eval oaIter_oaBusNetBit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaBusNetDef'
    proc Iter_oaBusNetDef { args } {
        ::set result [eval oaIter_oaBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaBusTerm'
    proc Iter_oaBusTerm { args } {
        ::set result [eval oaIter_oaBusTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaBusTermBit'
    proc Iter_oaBusTermBit { args } {
        ::set result [eval oaIter_oaBusTermBit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaBusTermDef'
    proc Iter_oaBusTermDef { args } {
        ::set result [eval oaIter_oaBusTermDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaCell'
    proc Iter_oaCell { args } {
        ::set result [eval oaIter_oaCell $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaCellView'
    proc Iter_oaCellView { args } {
        ::set result [eval oaIter_oaCellView $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaCluster'
    proc Iter_oaCluster { args } {
        ::set result [eval oaIter_oaCluster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaClusterBoundary'
    proc Iter_oaClusterBoundary { args } {
        ::set result [eval oaIter_oaClusterBoundary $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaConnectDef'
    proc Iter_oaConnectDef { args } {
        ::set result [eval oaIter_oaConnectDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaConstraint'
    proc Iter_oaConstraint { args } {
        ::set result [eval oaIter_oaConstraint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaConstraintGroup'
    proc Iter_oaConstraintGroup { args } {
        ::set result [eval oaIter_oaConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaConstraintGroupHeader'
    proc Iter_oaConstraintGroupHeader { args } {
        ::set result [eval oaIter_oaConstraintGroupHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaConstraintGroupMem'
    proc Iter_oaConstraintGroupMem { args } {
        ::set result [eval oaIter_oaConstraintGroupMem $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaConstraintParam'
    proc Iter_oaConstraintParam { args } {
        ::set result [eval oaIter_oaConstraintParam $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaDMData'
    proc Iter_oaDMData { args } {
        ::set result [eval oaIter_oaDMData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaDMFile'
    proc Iter_oaDMFile { args } {
        ::set result [eval oaIter_oaDMFile $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaDerivedLayerParam'
    proc Iter_oaDerivedLayerParam { args } {
        ::set result [eval oaIter_oaDerivedLayerParam $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaDesign'
    proc Iter_oaDesign { args } {
        ::set result [eval oaIter_oaDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaDesignObject'
    proc Iter_oaDesignObject { args } {
        ::set result [eval oaIter_oaDesignObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaDevice'
    proc Iter_oaDevice { args } {
        ::set result [eval oaIter_oaDevice $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaElmore'
    proc Iter_oaElmore { args } {
        ::set result [eval oaIter_oaElmore $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaFigGroup'
    proc Iter_oaFigGroup { args } {
        ::set result [eval oaIter_oaFigGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaFigGroupMem'
    proc Iter_oaFigGroupMem { args } {
        ::set result [eval oaIter_oaFigGroupMem $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaGCellPattern'
    proc Iter_oaGCellPattern { args } {
        ::set result [eval oaIter_oaGCellPattern $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaGroup'
    proc Iter_oaGroup { args } {
        ::set result [eval oaIter_oaGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaGroupMember'
    proc Iter_oaGroupMember { args } {
        ::set result [eval oaIter_oaGroupMember $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaGuide'
    proc Iter_oaGuide { args } {
        ::set result [eval oaIter_oaGuide $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaInst'
    proc Iter_oaInst { args } {
        ::set result [eval oaIter_oaInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaInstHeader'
    proc Iter_oaInstHeader { args } {
        ::set result [eval oaIter_oaInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaInstTerm'
    proc Iter_oaInstTerm { args } {
        ::set result [eval oaIter_oaInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaLPPHeader'
    proc Iter_oaLPPHeader { args } {
        ::set result [eval oaIter_oaLPPHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaLayer'
    proc Iter_oaLayer { args } {
        ::set result [eval oaIter_oaLayer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaLayerArrayConstraint'
    proc Iter_oaLayerArrayConstraint { args } {
        ::set result [eval oaIter_oaLayerArrayConstraint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaLayerConstraint'
    proc Iter_oaLayerConstraint { args } {
        ::set result [eval oaIter_oaLayerConstraint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaLayerHeader'
    proc Iter_oaLayerHeader { args } {
        ::set result [eval oaIter_oaLayerHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaLayerPairConstraint'
    proc Iter_oaLayerPairConstraint { args } {
        ::set result [eval oaIter_oaLayerPairConstraint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaLib'
    proc Iter_oaLib { args } {
        ::set result [eval oaIter_oaLib $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaLibDefList'
    proc Iter_oaLibDefList { args } {
        ::set result [eval oaIter_oaLibDefList $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaLibDefListMem'
    proc Iter_oaLibDefListMem { args } {
        ::set result [eval oaIter_oaLibDefListMem $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaMarker'
    proc Iter_oaMarker { args } {
        ::set result [eval oaIter_oaMarker $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModAssignment'
    proc Iter_oaModAssignment { args } {
        ::set result [eval oaIter_oaModAssignment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModBitNet'
    proc Iter_oaModBitNet { args } {
        ::set result [eval oaIter_oaModBitNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModBusNet'
    proc Iter_oaModBusNet { args } {
        ::set result [eval oaIter_oaModBusNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModBusNetBit'
    proc Iter_oaModBusNetBit { args } {
        ::set result [eval oaIter_oaModBusNetBit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModBusNetDef'
    proc Iter_oaModBusNetDef { args } {
        ::set result [eval oaIter_oaModBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModBusTerm'
    proc Iter_oaModBusTerm { args } {
        ::set result [eval oaIter_oaModBusTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModBusTermBit'
    proc Iter_oaModBusTermBit { args } {
        ::set result [eval oaIter_oaModBusTermBit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModBusTermDef'
    proc Iter_oaModBusTermDef { args } {
        ::set result [eval oaIter_oaModBusTermDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModConnectDef'
    proc Iter_oaModConnectDef { args } {
        ::set result [eval oaIter_oaModConnectDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModDesignInst'
    proc Iter_oaModDesignInst { args } {
        ::set result [eval oaIter_oaModDesignInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModInst'
    proc Iter_oaModInst { args } {
        ::set result [eval oaIter_oaModInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModInstHeader'
    proc Iter_oaModInstHeader { args } {
        ::set result [eval oaIter_oaModInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModInstTerm'
    proc Iter_oaModInstTerm { args } {
        ::set result [eval oaIter_oaModInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModModuleInst'
    proc Iter_oaModModuleInst { args } {
        ::set result [eval oaIter_oaModModuleInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModModuleInstHeader'
    proc Iter_oaModModuleInstHeader { args } {
        ::set result [eval oaIter_oaModModuleInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModNet'
    proc Iter_oaModNet { args } {
        ::set result [eval oaIter_oaModNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModTerm'
    proc Iter_oaModTerm { args } {
        ::set result [eval oaIter_oaModTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModVectorInstDef'
    proc Iter_oaModVectorInstDef { args } {
        ::set result [eval oaIter_oaModVectorInstDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaModule'
    proc Iter_oaModule { args } {
        ::set result [eval oaIter_oaModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaMutualInductor'
    proc Iter_oaMutualInductor { args } {
        ::set result [eval oaIter_oaMutualInductor $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaNet'
    proc Iter_oaNet { args } {
        ::set result [eval oaIter_oaNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaNode'
    proc Iter_oaNode { args } {
        ::set result [eval oaIter_oaNode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccAssignment'
    proc Iter_oaOccAssignment { args } {
        ::set result [eval oaIter_oaOccAssignment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccBitNet'
    proc Iter_oaOccBitNet { args } {
        ::set result [eval oaIter_oaOccBitNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccBusNet'
    proc Iter_oaOccBusNet { args } {
        ::set result [eval oaIter_oaOccBusNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccBusNetBit'
    proc Iter_oaOccBusNetBit { args } {
        ::set result [eval oaIter_oaOccBusNetBit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccBusNetDef'
    proc Iter_oaOccBusNetDef { args } {
        ::set result [eval oaIter_oaOccBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccBusTerm'
    proc Iter_oaOccBusTerm { args } {
        ::set result [eval oaIter_oaOccBusTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccBusTermBit'
    proc Iter_oaOccBusTermBit { args } {
        ::set result [eval oaIter_oaOccBusTermBit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccBusTermDef'
    proc Iter_oaOccBusTermDef { args } {
        ::set result [eval oaIter_oaOccBusTermDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccConnectDef'
    proc Iter_oaOccConnectDef { args } {
        ::set result [eval oaIter_oaOccConnectDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccDesignInst'
    proc Iter_oaOccDesignInst { args } {
        ::set result [eval oaIter_oaOccDesignInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccInst'
    proc Iter_oaOccInst { args } {
        ::set result [eval oaIter_oaOccInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccInstHeader'
    proc Iter_oaOccInstHeader { args } {
        ::set result [eval oaIter_oaOccInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccInstTerm'
    proc Iter_oaOccInstTerm { args } {
        ::set result [eval oaIter_oaOccInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccModuleInst'
    proc Iter_oaOccModuleInst { args } {
        ::set result [eval oaIter_oaOccModuleInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccModuleInstHeader'
    proc Iter_oaOccModuleInstHeader { args } {
        ::set result [eval oaIter_oaOccModuleInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccNet'
    proc Iter_oaOccNet { args } {
        ::set result [eval oaIter_oaOccNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccTerm'
    proc Iter_oaOccTerm { args } {
        ::set result [eval oaIter_oaOccTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccVectorInstDef'
    proc Iter_oaOccVectorInstDef { args } {
        ::set result [eval oaIter_oaOccVectorInstDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOccurrence'
    proc Iter_oaOccurrence { args } {
        ::set result [eval oaIter_oaOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOpPoint'
    proc Iter_oaOpPoint { args } {
        ::set result [eval oaIter_oaOpPoint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaOpPointHeader'
    proc Iter_oaOpPointHeader { args } {
        ::set result [eval oaIter_oaOpPointHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaParasiticNetwork'
    proc Iter_oaParasiticNetwork { args } {
        ::set result [eval oaIter_oaParasiticNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaPin'
    proc Iter_oaPin { args } {
        ::set result [eval oaIter_oaPin $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaPinFig'
    proc Iter_oaPinFig { args } {
        ::set result [eval oaIter_oaPinFig $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaPoleResidue'
    proc Iter_oaPoleResidue { args } {
        ::set result [eval oaIter_oaPoleResidue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaProp'
    proc Iter_oaProp { args } {
        ::set result [eval oaIter_oaProp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaPurpose'
    proc Iter_oaPurpose { args } {
        ::set result [eval oaIter_oaPurpose $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaRoute'
    proc Iter_oaRoute { args } {
        ::set result [eval oaIter_oaRoute $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaRow'
    proc Iter_oaRow { args } {
        ::set result [eval oaIter_oaRow $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaRowHeader'
    proc Iter_oaRowHeader { args } {
        ::set result [eval oaIter_oaRowHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaScanChain'
    proc Iter_oaScanChain { args } {
        ::set result [eval oaIter_oaScanChain $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaScanChainInst'
    proc Iter_oaScanChainInst { args } {
        ::set result [eval oaIter_oaScanChainInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaScanChainSet'
    proc Iter_oaScanChainSet { args } {
        ::set result [eval oaIter_oaScanChainSet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaShape'
    proc Iter_oaShape { args } {
        ::set result [eval oaIter_oaShape $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaSimpleConstraint'
    proc Iter_oaSimpleConstraint { args } {
        ::set result [eval oaIter_oaSimpleConstraint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaSiteDef'
    proc Iter_oaSiteDef { args } {
        ::set result [eval oaIter_oaSiteDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaSteiner'
    proc Iter_oaSteiner { args } {
        ::set result [eval oaIter_oaSteiner $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaSubNetwork'
    proc Iter_oaSubNetwork { args } {
        ::set result [eval oaIter_oaSubNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaTech'
    proc Iter_oaTech { args } {
        ::set result [eval oaIter_oaTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaTechHeader'
    proc Iter_oaTechHeader { args } {
        ::set result [eval oaIter_oaTechHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaTechLayerHeader'
    proc Iter_oaTechLayerHeader { args } {
        ::set result [eval oaIter_oaTechLayerHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaTechViaDefHeader'
    proc Iter_oaTechViaDefHeader { args } {
        ::set result [eval oaIter_oaTechViaDefHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaTerm'
    proc Iter_oaTerm { args } {
        ::set result [eval oaIter_oaTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaTrackPattern'
    proc Iter_oaTrackPattern { args } {
        ::set result [eval oaIter_oaTrackPattern $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaValue'
    proc Iter_oaValue { args } {
        ::set result [eval oaIter_oaValue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaVectorInst'
    proc Iter_oaVectorInst { args } {
        ::set result [eval oaIter_oaVectorInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaVectorInstBit'
    proc Iter_oaVectorInstBit { args } {
        ::set result [eval oaIter_oaVectorInstBit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaVectorInstDef'
    proc Iter_oaVectorInstDef { args } {
        ::set result [eval oaIter_oaVectorInstDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaVia'
    proc Iter_oaVia { args } {
        ::set result [eval oaIter_oaVia $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaViaDef'
    proc Iter_oaViaDef { args } {
        ::set result [eval oaIter_oaViaDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaViaHeader'
    proc Iter_oaViaHeader { args } {
        ::set result [eval oaIter_oaViaHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaViaSpec'
    proc Iter_oaViaSpec { args } {
        ::set result [eval oaIter_oaViaSpec $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaViaVariant'
    proc Iter_oaViaVariant { args } {
        ::set result [eval oaIter_oaViaVariant $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaViaVariantHeader'
    proc Iter_oaViaVariantHeader { args } {
        ::set result [eval oaIter_oaViaVariantHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaView'
    proc Iter_oaView { args } {
        ::set result [eval oaIter_oaView $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaIter_oaViewType'
    proc Iter_oaViewType { args } {
        ::set result [eval oaIter_oaViewType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LPPHeaderFind { args } {
        ::set result [eval oaLPPHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc LayerArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc LayerArrayConstraintCreate { args } {
        ::set result [eval oaLayerArrayConstraint_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerArrayConstraintDefCreate { args } {
        ::set result [eval oaLayerArrayConstraintDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerArrayConstraintDefGet { args } {
        ::set result [eval oaLayerArrayConstraintDef_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerArrayConstraintFind { args } {
        ::set result [eval oaLayerArrayConstraint_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerArrayConstraintGetConstraints { args } {
        ::set result [eval oaLayerArrayConstraint_getConstraints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaLayerArrayConstraintType'
    proc LayerArrayConstraintType { args } {
        ::set result [eval oaLayerArrayConstraintType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for LayerArrayConstraintType
    proc LayerArrayConstraintTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerArrayConstraintTypeValidate { args } {
        ::set result [eval oaLayerArrayConstraintType_validate $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerArrayValueCreate { args } {
        ::set result [eval oaLayerArrayValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerBlockageCreate { args } {
        ::set result [eval oaLayerBlockage_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerConstraintCreate { args } {
        ::set result [eval oaLayerConstraint_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerConstraintDefCreate { args } {
        ::set result [eval oaLayerConstraintDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerConstraintDefGet { args } {
        ::set result [eval oaLayerConstraintDef_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerConstraintFind { args } {
        ::set result [eval oaLayerConstraint_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerConstraintGetConstraints { args } {
        ::set result [eval oaLayerConstraint_getConstraints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaLayerConstraintType'
    proc LayerConstraintType { args } {
        ::set result [eval oaLayerConstraintType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for LayerConstraintType
    proc LayerConstraintTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerConstraintTypeValidate { args } {
        ::set result [eval oaLayerConstraintType_validate $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerFind { args } {
        ::set result [eval oaLayer_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerHaloCreate { args } {
        ::set result [eval oaLayerHalo_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc LayerHeaderArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc LayerHeaderFind { args } {
        ::set result [eval oaLayerHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc LayerNameArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaLayerOp'
    proc LayerOp { args } {
        ::set result [eval oaLayerOp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for LayerOp
    proc LayerOpEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerPairConstraintCreate { args } {
        ::set result [eval oaLayerPairConstraint_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerPairConstraintDefCreate { args } {
        ::set result [eval oaLayerPairConstraintDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerPairConstraintDefGet { args } {
        ::set result [eval oaLayerPairConstraintDef_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerPairConstraintFind { args } {
        ::set result [eval oaLayerPairConstraint_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerPairConstraintGetConstraints { args } {
        ::set result [eval oaLayerPairConstraint_getConstraints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaLayerPairConstraintType'
    proc LayerPairConstraintType { args } {
        ::set result [eval oaLayerPairConstraintType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for LayerPairConstraintType
    proc LayerPairConstraintTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerPairConstraintTypeValidate { args } {
        ::set result [eval oaLayerPairConstraintType_validate $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerRangeBlockageCreate { args } {
        ::set result [eval oaLayerRangeBlockage_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerRangeHaloCreate { args } {
        ::set result [eval oaLayerRangeHalo_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LayerValueCreate { args } {
        ::set result [eval oaLayerValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaLefNS'
    proc LefNS { args } {
        ::set result [eval oaLefNS $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaLibAccess'
    proc LibAccess { args } {
        ::set result [eval oaLibAccess $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for LibAccess
    proc LibAccessEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaLibAccessLevel'
    proc LibAccessLevel { args } {
        ::set result [eval oaLibAccessLevel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for LibAccessLevel
    proc LibAccessLevelEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibCreate { args } {
        ::set result [eval oaLib_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDMDataDestroy { args } {
        ::set result [eval oaLibDMData_destroy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDMDataExists { args } {
        ::set result [eval oaLibDMData_exists $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDMDataFind { args } {
        ::set result [eval oaLibDMData_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDMDataOpen { args } {
        ::set result [eval oaLibDMData_open $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDMDataRecover { args } {
        ::set result [eval oaLibDMData_recover $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaLibDataType'
    proc LibDataType { args } {
        ::set result [eval oaLibDataType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for LibDataType
    proc LibDataTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDefCreate { args } {
        ::set result [eval oaLibDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDefFind { args } {
        ::set result [eval oaLibDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDefListCreate { args } {
        ::set result [eval oaLibDefList_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDefListFind { args } {
        ::set result [eval oaLibDefList_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDefListGet { args } {
        ::set result [eval oaLibDefList_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDefListGetDefaultFileName { args } {
        ::set result [eval oaLibDefList_getDefaultFileName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDefListGetDefaultPath { args } {
        ::set result [eval oaLibDefList_getDefaultPath $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDefListGetLibDefLists { args } {
        ::set result [eval oaLibDefList_getLibDefLists $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Static member function
    proc LibDefListGetTopList { args } {
        ::set result [eval oaLibDefList_getTopList $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDefListOpenLibs { args } {
        ::set result [eval oaLibDefList_openLibs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDefListRefCreate { args } {
        ::set result [eval oaLibDefListRef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibDefListRefFind { args } {
        ::set result [eval oaLibDefListRef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibExists { args } {
        ::set result [eval oaLib_exists $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibFind { args } {
        ::set result [eval oaLib_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibGetOpenLibs { args } {
        ::set result [eval oaLib_getOpenLibs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaLibMode'
    proc LibMode { args } {
        ::set result [eval oaLibMode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for LibMode
    proc LibModeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LibOpen { args } {
        ::set result [eval oaLib_open $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LineCreate { args } {
        ::set result [eval oaLine_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LumpedElmoreCreate { args } {
        ::set result [eval oaLumpedElmore_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc LumpedElmoreFind { args } {
        ::set result [eval oaLumpedElmore_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaManagedType'
    proc ManagedType { args } {
        ::set result [eval oaManagedType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ManagedTypeArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc MarkerCreate { args } {
        ::set result [eval oaMarker_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaMarkerDeleteWhen'
    proc MarkerDeleteWhen { args } {
        ::set result [eval oaMarkerDeleteWhen $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for MarkerDeleteWhen
    proc MarkerDeleteWhenEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaMarkerSeverity'
    proc MarkerSeverity { args } {
        ::set result [eval oaMarkerSeverity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for MarkerSeverity
    proc MarkerSeverityEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaMaterial'
    proc Material { args } {
        ::set result [eval oaMaterial $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for Material
    proc MaterialEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaMemNetCollection'
    proc MemNetCollection { args } {
        ::set result [eval oaMemNetCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaMemNetIter'
    proc MemNetIter { args } {
        ::set result [eval oaMemNetIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModAssignAssignmentCreate { args } {
        ::set result [eval oaModAssignAssignment_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModAssignValueCreate { args } {
        ::set result [eval oaModAssignValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModAssignmentFind { args } {
        ::set result [eval oaModAssignment_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBundleNetCreate { args } {
        ::set result [eval oaModBundleNet_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBundleNetFind { args } {
        ::set result [eval oaModBundleNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBundleNetIsValidName { args } {
        ::set result [eval oaModBundleNet_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBundleTermCreate { args } {
        ::set result [eval oaModBundleTerm_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBundleTermFind { args } {
        ::set result [eval oaModBundleTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBundleTermIsValidName { args } {
        ::set result [eval oaModBundleTerm_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusNetBitCreate { args } {
        ::set result [eval oaModBusNetBit_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusNetBitFind { args } {
        ::set result [eval oaModBusNetBit_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusNetBitIsValidName { args } {
        ::set result [eval oaModBusNetBit_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusNetCreate { args } {
        ::set result [eval oaModBusNet_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusNetDefCreate { args } {
        ::set result [eval oaModBusNetDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusNetDefFind { args } {
        ::set result [eval oaModBusNetDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusNetFind { args } {
        ::set result [eval oaModBusNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusNetIsValidName { args } {
        ::set result [eval oaModBusNet_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusTermBitCreate { args } {
        ::set result [eval oaModBusTermBit_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusTermBitFind { args } {
        ::set result [eval oaModBusTermBit_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusTermBitIsValidName { args } {
        ::set result [eval oaModBusTermBit_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusTermCreate { args } {
        ::set result [eval oaModBusTerm_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusTermDefCreate { args } {
        ::set result [eval oaModBusTermDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusTermDefFind { args } {
        ::set result [eval oaModBusTermDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusTermFind { args } {
        ::set result [eval oaModBusTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModBusTermIsValidName { args } {
        ::set result [eval oaModBusTerm_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModInstFind { args } {
        ::set result [eval oaModInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModInstHeaderFind { args } {
        ::set result [eval oaModInstHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModInstTermCreate { args } {
        ::set result [eval oaModInstTerm_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModInstTermFind { args } {
        ::set result [eval oaModInstTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaModMemNetCollection'
    proc ModMemNetCollection { args } {
        ::set result [eval oaModMemNetCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaModMemNetIter'
    proc ModMemNetIter { args } {
        ::set result [eval oaModMemNetIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModModuleInstHeaderFind { args } {
        ::set result [eval oaModModuleInstHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModModuleScalarInstCreate { args } {
        ::set result [eval oaModModuleScalarInst_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModModuleScalarInstFind { args } {
        ::set result [eval oaModModuleScalarInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModModuleScalarInstIsValidName { args } {
        ::set result [eval oaModModuleScalarInst_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModModuleVectorInstBitCreate { args } {
        ::set result [eval oaModModuleVectorInstBit_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModModuleVectorInstBitFind { args } {
        ::set result [eval oaModModuleVectorInstBit_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModModuleVectorInstBitIsValidName { args } {
        ::set result [eval oaModModuleVectorInstBit_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModModuleVectorInstCreate { args } {
        ::set result [eval oaModModuleVectorInst_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModModuleVectorInstFind { args } {
        ::set result [eval oaModModuleVectorInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModModuleVectorInstIsValidName { args } {
        ::set result [eval oaModModuleVectorInst_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModNetConnectDefCreate { args } {
        ::set result [eval oaModNetConnectDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModNetCreate { args } {
        ::set result [eval oaModNet_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModNetFind { args } {
        ::set result [eval oaModNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModNetIsValidName { args } {
        ::set result [eval oaModNet_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaModNetTermArray'
    proc ModNetTermArray { args } {
        ::set result [eval oaModNetTermArray $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaModNetTermNameArray'
    proc ModNetTermNameArray { args } {
        ::set result [eval oaModNetTermNameArray $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaModNetTermPosArray'
    proc ModNetTermPosArray { args } {
        ::set result [eval oaModNetTermPosArray $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModScalarInstCreate { args } {
        ::set result [eval oaModScalarInst_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModScalarInstFind { args } {
        ::set result [eval oaModScalarInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModScalarInstIsValidName { args } {
        ::set result [eval oaModScalarInst_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModScalarNetCreate { args } {
        ::set result [eval oaModScalarNet_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModScalarNetFind { args } {
        ::set result [eval oaModScalarNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModScalarNetIsValidName { args } {
        ::set result [eval oaModScalarNet_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModScalarTermCreate { args } {
        ::set result [eval oaModScalarTerm_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModScalarTermFind { args } {
        ::set result [eval oaModScalarTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModScalarTermIsValidName { args } {
        ::set result [eval oaModScalarTerm_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ModTermArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc ModTermConnectDefCreate { args } {
        ::set result [eval oaModTermConnectDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModTermCreate { args } {
        ::set result [eval oaModTerm_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModTermFind { args } {
        ::set result [eval oaModTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModTermGetMaxPosition { args } {
        ::set result [eval oaModTerm_getMaxPosition $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModTermIsValidName { args } {
        ::set result [eval oaModTerm_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModVectorInstBitCreate { args } {
        ::set result [eval oaModVectorInstBit_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModVectorInstBitFind { args } {
        ::set result [eval oaModVectorInstBit_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModVectorInstBitIsValidName { args } {
        ::set result [eval oaModVectorInstBit_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModVectorInstCreate { args } {
        ::set result [eval oaModVectorInst_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModVectorInstDefCreate { args } {
        ::set result [eval oaModVectorInstDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModVectorInstDefFind { args } {
        ::set result [eval oaModVectorInstDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModVectorInstFind { args } {
        ::set result [eval oaModVectorInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModVectorInstIsValidName { args } {
        ::set result [eval oaModVectorInst_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModuleCreate { args } {
        ::set result [eval oaModule_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModuleEmbed { args } {
        ::set result [eval oaModule_embed $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ModuleFind { args } {
        ::set result [eval oaModule_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc MutualInductorCreate { args } {
        ::set result [eval oaMutualInductor_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaName'
    proc Name { args } {
        ::set result [eval oaName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaNameBase'
    proc NameBase { args } {
        ::set result [eval oaNameBase $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaNameMem'
    proc NameMem { args } {
        ::set result [eval oaNameMem $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaNameType'
    proc NameType { args } {
        ::set result [eval oaNameType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for NameType
    proc NameTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaNativeNS'
    proc NativeNS { args } {
        ::set result [eval oaNativeNS $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaNetAttrType'
    proc NetAttrType { args } {
        ::set result [eval oaNetAttrType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc NetConnectDefCreate { args } {
        ::set result [eval oaNetConnectDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc NetCreate { args } {
        ::set result [eval oaNet_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc NetFind { args } {
        ::set result [eval oaNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc NetIsValidName { args } {
        ::set result [eval oaNet_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaNetTermArray'
    proc NetTermArray { args } {
        ::set result [eval oaNetTermArray $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc NetTermArrayBase_oaModNet_oaModTerm { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc NetTermArrayBase_oaModNet_oaName { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc NetTermArrayBase_oaModNet_unsigned_int { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc NetTermArrayBase_oaNet_oaName { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc NetTermArrayBase_oaNet_oaTerm { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc NetTermArrayBase_oaNet_unsigned_int { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaNetTermNameArray'
    proc NetTermNameArray { args } {
        ::set result [eval oaNetTermNameArray $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaNetTermPosArray'
    proc NetTermPosArray { args } {
        ::set result [eval oaNetTermPosArray $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc NodeCreate { args } {
        ::set result [eval oaNode_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc NodeFind { args } {
        ::set result [eval oaNode_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc NodeGetUniqueId { args } {
        ::set result [eval oaNode_getUniqueId $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc NodeIsValidName { args } {
        ::set result [eval oaNode_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ObjectArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc OccArrayInstFind { args } {
        ::set result [eval oaOccArrayInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccAssignmentFind { args } {
        ::set result [eval oaOccAssignment_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccBundleNetFind { args } {
        ::set result [eval oaOccBundleNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccBundleTermFind { args } {
        ::set result [eval oaOccBundleTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccBusNetBitFind { args } {
        ::set result [eval oaOccBusNetBit_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccBusNetDefFind { args } {
        ::set result [eval oaOccBusNetDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccBusNetFind { args } {
        ::set result [eval oaOccBusNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccBusTermBitFind { args } {
        ::set result [eval oaOccBusTermBit_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccBusTermDefFind { args } {
        ::set result [eval oaOccBusTermDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccBusTermFind { args } {
        ::set result [eval oaOccBusTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccInstFind { args } {
        ::set result [eval oaOccInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccInstTermFind { args } {
        ::set result [eval oaOccInstTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaOccMemNetCollection'
    proc OccMemNetCollection { args } {
        ::set result [eval oaOccMemNetCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaOccMemNetIter'
    proc OccMemNetIter { args } {
        ::set result [eval oaOccMemNetIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccModuleScalarInstFind { args } {
        ::set result [eval oaOccModuleScalarInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccModuleVectorInstBitFind { args } {
        ::set result [eval oaOccModuleVectorInstBit_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccModuleVectorInstFind { args } {
        ::set result [eval oaOccModuleVectorInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccNetFind { args } {
        ::set result [eval oaOccNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccScalarInstFind { args } {
        ::set result [eval oaOccScalarInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccScalarNetFind { args } {
        ::set result [eval oaOccScalarNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccScalarTermFind { args } {
        ::set result [eval oaOccScalarTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccShapeGet { args } {
        ::set result [eval oaOccShape_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc OccTermArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc OccTermFind { args } {
        ::set result [eval oaOccTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccTermGetMaxPosition { args } {
        ::set result [eval oaOccTerm_getMaxPosition $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaOccTraverser'
    proc OccTraverser { args } {
        ::set result [eval oaOccTraverser $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccVectorInstBitFind { args } {
        ::set result [eval oaOccVectorInstBit_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccVectorInstDefFind { args } {
        ::set result [eval oaOccVectorInstDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OccVectorInstFind { args } {
        ::set result [eval oaOccVectorInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OpPointCreate { args } {
        ::set result [eval oaOpPoint_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OpPointFind { args } {
        ::set result [eval oaOpPoint_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc OpPointHeaderFind { args } {
        ::set result [eval oaOpPointHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaOrient'
    proc Orient { args } {
        ::set result [eval oaOrient $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for Orient
    proc OrientEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PRBoundaryCreate { args } {
        ::set result [eval oaPRBoundary_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PRBoundaryFind { args } {
        ::set result [eval oaPRBoundary_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaParam'
    proc Param { args } {
        ::set result [eval oaParam $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ParamArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaParamType'
    proc ParamType { args } {
        ::set result [eval oaParamType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ParamType
    proc ParamTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ParasiticNetworkCreate { args } {
        ::set result [eval oaParasiticNetwork_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ParasiticNetworkDestroy { args } {
        ::set result [eval oaParasiticNetwork_destroy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ParasiticNetworkExists { args } {
        ::set result [eval oaParasiticNetwork_exists $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ParasiticNetworkFind { args } {
        ::set result [eval oaParasiticNetwork_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ParasiticNetworkLoad { args } {
        ::set result [eval oaParasiticNetwork_load $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PathCreate { args } {
        ::set result [eval oaPath_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PathGenBoundary { args } {
        ::set result [eval oaPath_genBoundary $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Static member function
    proc PathSegCreate { args } {
        ::set result [eval oaPathSeg_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PathSegGenBoundary { args } {
        ::set result [eval oaPathSeg_genBoundary $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Constructor for 'oaPathStyle'
    proc PathStyle { args } {
        ::set result [eval oaPathStyle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for PathStyle
    proc PathStyleEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaPcellDef'
    proc PcellDef { args } {
        ::set result [eval oaPcellDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PcellLinkCreate { args } {
        ::set result [eval oaPcellLink_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PcellLinkFind { args } {
        ::set result [eval oaPcellLink_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PcellLinkGetPcellDef { args } {
        ::set result [eval oaPcellLink_getPcellDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PhysicalLayerCreate { args } {
        ::set result [eval oaPhysicalLayer_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PhysicalLayerFind { args } {
        ::set result [eval oaPhysicalLayer_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PiElmoreCreate { args } {
        ::set result [eval oaPiElmore_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PiElmoreFind { args } {
        ::set result [eval oaPiElmore_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PiPoleResidueCreate { args } {
        ::set result [eval oaPiPoleResidue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PiPoleResidueFind { args } {
        ::set result [eval oaPiPoleResidue_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaPinConnectMethod'
    proc PinConnectMethod { args } {
        ::set result [eval oaPinConnectMethod $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for PinConnectMethod
    proc PinConnectMethodEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PinCreate { args } {
        ::set result [eval oaPin_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PinFind { args } {
        ::set result [eval oaPin_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaPinType'
    proc PinType { args } {
        ::set result [eval oaPinType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for PinType
    proc PinTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaPlacementStatus'
    proc PlacementStatus { args } {
        ::set result [eval oaPlacementStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for PlacementStatus
    proc PlacementStatusEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaPoint'
    proc Point { args } {
        ::set result [eval oaPoint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Unsupported function
    proc PointArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc PoleResidueCreate { args } {
        ::set result [eval oaPoleResidue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PoleResidueFind { args } {
        ::set result [eval oaPoleResidue_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PolygonCreate { args } {
        ::set result [eval oaPolygon_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaPrefRoutingDir'
    proc PrefRoutingDir { args } {
        ::set result [eval oaPrefRoutingDir $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for PrefRoutingDir
    proc PrefRoutingDirEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PropDisplayCreate { args } {
        ::set result [eval oaPropDisplay_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PropFind { args } {
        ::set result [eval oaProp_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc PurposeArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc PurposeCreate { args } {
        ::set result [eval oaPurpose_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PurposeFind { args } {
        ::set result [eval oaPurpose_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PurposeGet { args } {
        ::set result [eval oaPurpose_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaPurposeType'
    proc PurposeType { args } {
        ::set result [eval oaPurposeType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for PurposeType
    proc PurposeTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PurposeTypeGet { args } {
        ::set result [eval oaPurposeType_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc PurposeValueCreate { args } {
        ::set result [eval oaPurposeValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaRangeBase'
    proc RangeBase { args } {
        ::set result [eval oaRangeBase $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaRangeType'
    proc RangeType { args } {
        ::set result [eval oaRangeType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for RangeType
    proc RangeTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc Range_int { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Range_unsigned_long_long { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc RectCreate { args } {
        ::set result [eval oaRect_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ReducedModelDestroy { args } {
        ::set result [eval oaReducedModel_destroy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaReservedViewType'
    proc ReservedViewType { args } {
        ::set result [eval oaReservedViewType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ReservedViewType
    proc ReservedViewTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ResistorCreate { args } {
        ::set result [eval oaResistor_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc RouteCreate { args } {
        ::set result [eval oaRoute_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaRouteMethod'
    proc RouteMethod { args } {
        ::set result [eval oaRouteMethod $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for RouteMethod
    proc RouteMethodEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc RouteObjectArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaRouteOptimizer'
    proc RouteOptimizer { args } {
        ::set result [eval oaRouteOptimizer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaRoutePattern'
    proc RoutePattern { args } {
        ::set result [eval oaRoutePattern $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for RoutePattern
    proc RoutePatternEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaRouteStatus'
    proc RouteStatus { args } {
        ::set result [eval oaRouteStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for RouteStatus
    proc RouteStatusEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaRouteTopology'
    proc RouteTopology { args } {
        ::set result [eval oaRouteTopology $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for RouteTopology
    proc RouteTopologyEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc RowCreate { args } {
        ::set result [eval oaRow_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc RowFind { args } {
        ::set result [eval oaRow_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaRowFlipType'
    proc RowFlipType { args } {
        ::set result [eval oaRowFlipType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for RowFlipType
    proc RowFlipTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc RowHeaderFind { args } {
        ::set result [eval oaRowHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaRowSpacingType'
    proc RowSpacingType { args } {
        ::set result [eval oaRowSpacingType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for RowSpacingType
    proc RowSpacingTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSaveRecoverType'
    proc SaveRecoverType { args } {
        ::set result [eval oaSaveRecoverType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for SaveRecoverType
    proc SaveRecoverTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScalarInstCreate { args } {
        ::set result [eval oaScalarInst_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScalarInstFind { args } {
        ::set result [eval oaScalarInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScalarInstIsValidName { args } {
        ::set result [eval oaScalarInst_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaScalarName'
    proc ScalarName { args } {
        ::set result [eval oaScalarName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScalarNetCreate { args } {
        ::set result [eval oaScalarNet_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScalarNetFind { args } {
        ::set result [eval oaScalarNet_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScalarNetIsValidName { args } {
        ::set result [eval oaScalarNet_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScalarSiteDefCreate { args } {
        ::set result [eval oaScalarSiteDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScalarTermCreate { args } {
        ::set result [eval oaScalarTerm_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScalarTermFind { args } {
        ::set result [eval oaScalarTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScalarTermIsValidName { args } {
        ::set result [eval oaScalarTerm_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScanChainCreate { args } {
        ::set result [eval oaScanChain_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScanChainFind { args } {
        ::set result [eval oaScanChain_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScanChainInstCreate { args } {
        ::set result [eval oaScanChainInst_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ScanChainSetCreate { args } {
        ::set result [eval oaScanChainSet_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaScanChainSetType'
    proc ScanChainSetType { args } {
        ::set result [eval oaScanChainSetType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ScanChainSetType
    proc ScanChainSetTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSegStyle'
    proc SegStyle { args } {
        ::set result [eval oaSegStyle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSegment'
    proc Segment { args } {
        ::set result [eval oaSegment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SeriesRLCreate { args } {
        ::set result [eval oaSeriesRL_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SessionGet { args } {
        ::set result [eval oaSession_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaShapeAngleType'
    proc ShapeAngleType { args } {
        ::set result [eval oaShapeAngleType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ShapeAngleType
    proc ShapeAngleTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSigType'
    proc SigType { args } {
        ::set result [eval oaSigType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for SigType
    proc SigTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SimpleConstraintCreate { args } {
        ::set result [eval oaSimpleConstraint_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SimpleConstraintDefCreate { args } {
        ::set result [eval oaSimpleConstraintDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SimpleConstraintDefGet { args } {
        ::set result [eval oaSimpleConstraintDef_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SimpleConstraintFind { args } {
        ::set result [eval oaSimpleConstraint_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SimpleConstraintGetConstraints { args } {
        ::set result [eval oaSimpleConstraint_getConstraints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaSimpleConstraintType'
    proc SimpleConstraintType { args } {
        ::set result [eval oaSimpleConstraintType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for SimpleConstraintType
    proc SimpleConstraintTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SimpleConstraintTypeValidate { args } {
        ::set result [eval oaSimpleConstraintType_validate $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSimpleName'
    proc SimpleName { args } {
        ::set result [eval oaSimpleName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SiteDefFind { args } {
        ::set result [eval oaSiteDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSiteDefType'
    proc SiteDefType { args } {
        ::set result [eval oaSiteDefType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for SiteDefType
    proc SiteDefTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc SitePattern { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaSiteRef'
    proc SiteRef { args } {
        ::set result [eval oaSiteRef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSizeOp'
    proc SizeOp { args } {
        ::set result [eval oaSizeOp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for SizeOp
    proc SizeOpEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SizedLayerCreate { args } {
        ::set result [eval oaSizedLayer_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SizedLayerFind { args } {
        ::set result [eval oaSizedLayer_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc Sleep { args } {
        ::set result [eval oaSleep $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SnapBoundaryCreate { args } {
        ::set result [eval oaSnapBoundary_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SnapBoundaryFind { args } {
        ::set result [eval oaSnapBoundary_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSource'
    proc Source { args } {
        ::set result [eval oaSource $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for Source
    proc SourceEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSpacingDirectionType'
    proc SpacingDirectionType { args } {
        ::set result [eval oaSpacingDirectionType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for SpacingDirectionType
    proc SpacingDirectionTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSpefNS'
    proc SpefNS { args } {
        ::set result [eval oaSpefNS $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSpfNS'
    proc SpfNS { args } {
        ::set result [eval oaSpfNS $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSpiceNS'
    proc SpiceNS { args } {
        ::set result [eval oaSpiceNS $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc StartDaemon { args } {
        ::set result [eval oaStartDaemon $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc StdViaCreate { args } {
        ::set result [eval oaStdVia_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc StdViaDefCreate { args } {
        ::set result [eval oaStdViaDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc StdViaVariantCreate { args } {
        ::set result [eval oaStdViaVariant_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc StdViaVariantFind { args } {
        ::set result [eval oaStdViaVariant_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SteinerCreate { args } {
        ::set result [eval oaSteiner_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc StringArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc StringPropCreate { args } {
        ::set result [eval oaStringProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc StringValueCreate { args } {
        ::set result [eval oaStringValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SubNetworkCreate { args } {
        ::set result [eval oaSubNetwork_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc SubNetworkFind { args } {
        ::set result [eval oaSubNetwork_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc Subset_oaDBType { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Subset_oaGroupDef { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Subset_oaManagedType { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc Subset_oaType { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaSymmetry'
    proc Symmetry { args } {
        ::set result [eval oaSymmetry $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for Symmetry
    proc SymmetryEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaSystemTime'
    proc SystemTime { args } {
        ::set result [eval oaSystemTime $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc TechArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc TechAttach { args } {
        ::set result [eval oaTech_attach $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechCreate { args } {
        ::set result [eval oaTech_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaTechDataType'
    proc TechDataType { args } {
        ::set result [eval oaTechDataType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for TechDataType
    proc TechDataTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechDestroy { args } {
        ::set result [eval oaTech_destroy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechDetach { args } {
        ::set result [eval oaTech_detach $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechExists { args } {
        ::set result [eval oaTech_exists $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechFind { args } {
        ::set result [eval oaTech_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechGetAttachment { args } {
        ::set result [eval oaTech_getAttachment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechGetDefaultClearanceMeasure { args } {
        ::set result [eval oaTech_getDefaultClearanceMeasure $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechGetDefaultDBUPerUU { args } {
        ::set result [eval oaTech_getDefaultDBUPerUU $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechGetDefaultDefaultManufacturingGrid { args } {
        ::set result [eval oaTech_getDefaultDefaultManufacturingGrid $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechGetDefaultIsGateGrounded { args } {
        ::set result [eval oaTech_getDefaultIsGateGrounded $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechGetDefaultUserUnits { args } {
        ::set result [eval oaTech_getDefaultUserUnits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechGetOpenTechs { args } {
        ::set result [eval oaTech_getOpenTechs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Static member function
    proc TechGetRevNumber { args } {
        ::set result [eval oaTech_getRevNumber $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechHasAttachment { args } {
        ::set result [eval oaTech_hasAttachment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc TechHeaderArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc TechHeaderFind { args } {
        ::set result [eval oaTechHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Init function for module oaTech
    proc TechInit { args } {
        ::set result [eval oaTechInit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechLayerHeaderFind { args } {
        ::set result [eval oaTechLayerHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechOpen { args } {
        ::set result [eval oaTech_open $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechRecover { args } {
        ::set result [eval oaTech_recover $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechViaDefHeaderFind { args } {
        ::set result [eval oaTechViaDefHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TechViaVariantHeaderFind { args } {
        ::set result [eval oaTechViaVariantHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc TermArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaTermAttrType'
    proc TermAttrType { args } {
        ::set result [eval oaTermAttrType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TermConnectDefCreate { args } {
        ::set result [eval oaTermConnectDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TermCreate { args } {
        ::set result [eval oaTerm_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TermFind { args } {
        ::set result [eval oaTerm_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TermGetMaxPosition { args } {
        ::set result [eval oaTerm_getMaxPosition $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TermIsValidName { args } {
        ::set result [eval oaTerm_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaTermType'
    proc TermType { args } {
        ::set result [eval oaTermType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for TermType
    proc TermTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaTextAlign'
    proc TextAlign { args } {
        ::set result [eval oaTextAlign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for TextAlign
    proc TextAlignEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TextCreate { args } {
        ::set result [eval oaText_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaTextDisplayCollection'
    proc TextDisplayCollection { args } {
        ::set result [eval oaTextDisplayCollection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaTextDisplayFormat'
    proc TextDisplayFormat { args } {
        ::set result [eval oaTextDisplayFormat $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for TextDisplayFormat
    proc TextDisplayFormatEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TextDisplayGetTextDisplays { args } {
        ::set result [eval oaTextDisplay_getTextDisplays $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaTextDisplayIter'
    proc TextDisplayIter { args } {
        ::set result [eval oaTextDisplayIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TextOverrideCreate { args } {
        ::set result [eval oaTextOverride_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TimePropCreate { args } {
        ::set result [eval oaTimeProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TimeRangePropCreate { args } {
        ::set result [eval oaTimeRangeProp_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaTimeStamp'
    proc TimeStamp { args } {
        ::set result [eval oaTimeStamp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaTimer'
    proc Timer { args } {
        ::set result [eval oaTimer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc TrackPatternCreate { args } {
        ::set result [eval oaTrackPattern_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaTransform'
    proc Transform { args } {
        ::set result [eval oaTransform $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaType'
    proc Type { args } {
        ::set result [eval oaType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for Type
    proc TypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc UInt4 { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaUInt8Range'
    proc UInt8Range { args } {
        ::set result [eval oaUInt8Range $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc UInt8RangeValueCreate { args } {
        ::set result [eval oaUInt8RangeValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc UInt8ValueCreate { args } {
        ::set result [eval oaUInt8Value_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaUnixNS'
    proc UnixNS { args } {
        ::set result [eval oaUnixNS $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaUserUnitsType'
    proc UserUnitsType { args } {
        ::set result [eval oaUserUnitsType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for UserUnitsType
    proc UserUnitsTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaVCCap'
    proc VCCap { args } {
        ::set result [eval oaVCCap $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for VCCap
    proc VCCapEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaVCVersion'
    proc VCVersion { args } {
        ::set result [eval oaVCVersion $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaVCVersionIter'
    proc VCVersionIter { args } {
        ::set result [eval oaVCVersionIter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ValueArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc ValueArrayValueCreate { args } {
        ::set result [eval oaValueArrayValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaVector'
    proc Vector { args } {
        ::set result [eval oaVector $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaVectorBitName'
    proc VectorBitName { args } {
        ::set result [eval oaVectorBitName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc VectorInstBitCreate { args } {
        ::set result [eval oaVectorInstBit_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc VectorInstBitFind { args } {
        ::set result [eval oaVectorInstBit_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc VectorInstBitIsValidName { args } {
        ::set result [eval oaVectorInstBit_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc VectorInstCreate { args } {
        ::set result [eval oaVectorInst_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc VectorInstDefCreate { args } {
        ::set result [eval oaVectorInstDef_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc VectorInstDefFind { args } {
        ::set result [eval oaVectorInstDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc VectorInstFind { args } {
        ::set result [eval oaVectorInst_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc VectorInstIsValidName { args } {
        ::set result [eval oaVectorInst_isValidName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaVectorName'
    proc VectorName { args } {
        ::set result [eval oaVectorName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaVerilogNS'
    proc VerilogNS { args } {
        ::set result [eval oaVerilogNS $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaVersionComp'
    proc VersionComp { args } {
        ::set result [eval oaVersionComp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for VersionComp
    proc VersionCompEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaVhdlNS'
    proc VhdlNS { args } {
        ::set result [eval oaVhdlNS $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViaDef2DTblValueCreate { args } {
        ::set result [eval oaViaDef2DTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ViaDefArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc ViaDefArrayValueCreate { args } {
        ::set result [eval oaViaDefArrayValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViaDefFind { args } {
        ::set result [eval oaViaDef_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ViaDefNameArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Constructor for 'oaViaDirection'
    proc ViaDirection { args } {
        ::set result [eval oaViaDirection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ViaDirection
    proc ViaDirectionEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViaHeaderFind { args } {
        ::set result [eval oaViaHeader_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaViaParam'
    proc ViaParam { args } {
        ::set result [eval oaViaParam $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViaSpecCreate { args } {
        ::set result [eval oaViaSpec_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViaSpecFind { args } {
        ::set result [eval oaViaSpec_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaViaTopology'
    proc ViaTopology { args } {
        ::set result [eval oaViaTopology $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViaTopology2DTblValueCreate { args } {
        ::set result [eval oaViaTopology2DTblValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc ViaTopologyArray { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Static member function
    proc ViaTopologyArrayValueCreate { args } {
        ::set result [eval oaViaTopologyArrayValue_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaViaTopologyType'
    proc ViaTopologyType { args } {
        ::set result [eval oaViaTopologyType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for ViaTopologyType
    proc ViaTopologyTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViaVariantFind { args } {
        ::set result [eval oaViaVariant_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViewDMDataDestroy { args } {
        ::set result [eval oaViewDMData_destroy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViewDMDataExists { args } {
        ::set result [eval oaViewDMData_exists $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViewDMDataFind { args } {
        ::set result [eval oaViewDMData_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViewDMDataOpen { args } {
        ::set result [eval oaViewDMData_open $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViewDMDataRecover { args } {
        ::set result [eval oaViewDMData_recover $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViewFind { args } {
        ::set result [eval oaView_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViewGet { args } {
        ::set result [eval oaView_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViewTypeCreate { args } {
        ::set result [eval oaViewType_create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViewTypeFind { args } {
        ::set result [eval oaViewType_find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViewTypeGet { args } {
        ::set result [eval oaViewType_get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Static member function
    proc ViewTypeGetViewTypes { args } {
        ::set result [eval oaViewType_getViewTypes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Constructor for 'oaWidthLengthTableType'
    proc WidthLengthTableType { args } {
        ::set result [eval oaWidthLengthTableType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Enum function for WidthLengthTableType
    proc WidthLengthTableTypeEnum { object args } {
        ::set result [eval $object typeEnum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Constructor for 'oaWinNS'
    proc WinNS { args } {
        ::set result [eval oaWinNS $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'abort'
    proc abort { object args } {
        ::set result [eval $object abort $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'add'
    proc add { object args } {
        ::set result [eval $object add $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'addConn'
    proc addConn { object args } {
        ::set result [eval $object addConn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'addData'
    proc addData { object args } {
        ::set result [eval $object addData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'addHierMem'
    proc addHierMem { object args } {
        ::set result [eval $object addHierMem $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'addHierPath'
    proc addHierPath { object args } {
        ::set result [eval $object addHierPath $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'addLibAttribute'
    proc addLibAttribute { object args } {
        ::set result [eval $object addLibAttribute $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'addObject'
    proc addObject { object args } {
        ::set result [eval $object addObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'addToCluster'
    proc addToCluster { object args } {
        ::set result [eval $object addToCluster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'addToNet'
    proc addToNet { object args } {
        ::set result [eval $object addToNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'addToPin'
    proc addToPin { object args } {
        ::set result [eval $object addToPin $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'addToPoint'
    proc addToPoint { object args } {
        ::set result [eval $object addToPoint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'addToSubNetwork'
    proc addToSubNetwork { object args } {
        ::set result [eval $object addToSubNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'allowPGNet'
    proc allowPGNet { object args } {
        ::set result [eval $object allowPGNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'append'
    proc append { object args } {
        ::set result [eval $object append $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'area'
    proc area { object args } {
        ::set result [eval $object area $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'assignmentName'
    proc assignmentName { object args } {
        ::set result [eval $object assignmentName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc beginChangeSet { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc beginTracking { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'bottom'
    proc bottom { object args } {
        ::set result [eval $object bottom $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'breakEquivalence'
    proc breakEquivalence { object args } {
        ::set result [eval $object breakEquivalence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'calcBBox'
    proc calcBBox { object args } {
        ::set result [eval $object calcBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'calcVMSize'
    proc calcVMSize { object args } {
        ::set result [eval $object calcVMSize $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'cancelEdit'
    proc cancelEdit { object args } {
        ::set result [eval $object cancelEdit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'close'
    proc close { object args } {
        ::set result [eval $object close $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'collinearContains'
    proc collinearContains { object args } {
        ::set result [eval $object collinearContains $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'commitEdits'
    proc commitEdits { object args } {
        ::set result [eval $object commitEdits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'compare'
    proc compare { object args } {
        ::set result [eval $object compare $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc compress { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'concat'
    proc concat { object args } {
        ::set result [eval $object concat $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc consistsOf { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'contains'
    proc contains { object args } {
        ::set result [eval $object contains $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'convertToLine'
    proc convertToLine { object args } {
        ::set result [eval $object convertToLine $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'convertToPolygon'
    proc convertToPolygon { object args } {
        ::set result [eval $object convertToPolygon $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'convertToRect'
    proc convertToRect { object args } {
        ::set result [eval $object convertToRect $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'copy'
    proc copy { object args } {
        ::set result [eval $object copy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'copyTo'
    proc copyTo { object args } {
        ::set result [eval $object copyTo $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'create'
    proc create { object args } {
        ::set result [eval $object create $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'dbuToUU'
    proc dbuToUU { object args } {
        ::set result [eval $object dbuToUU $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'dbuToUUArea'
    proc dbuToUUArea { object args } {
        ::set result [eval $object dbuToUUArea $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'dbuToUUDistance'
    proc dbuToUUDistance { object args } {
        ::set result [eval $object dbuToUUDistance $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'defaultName'
    proc defaultName { object args } {
        ::set result [eval $object defaultName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'defineSuperMaster'
    proc defineSuperMaster { object args } {
        ::set result [eval $object defineSuperMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'deleteSupplyDemand'
    proc deleteSupplyDemand { object args } {
        ::set result [eval $object deleteSupplyDemand $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'destroy'
    proc destroy { object args } {
        ::set result [eval $object destroy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'destroyFigs'
    proc destroyFigs { object args } {
        ::set result [eval $object destroyFigs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'destroyObjects'
    proc destroyObjects { object args } {
        ::set result [eval $object destroyObjects $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'detach'
    proc detach { object args } {
        ::set result [eval $object detach $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'diff'
    proc diff { object args } {
        ::set result [eval $object diff $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc discardAllChangeSets { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'distanceFrom2'
    proc distanceFrom2 { object args } {
        ::set result [eval $object distanceFrom2 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'enableTimeStamps'
    proc enableTimeStamps { object args } {
        ::set result [eval $object enableTimeStamps $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'enableUndo'
    proc enableUndo { object args } {
        ::set result [eval $object enableUndo $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc endChangeSet { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'endInst'
    proc endInst { object args } {
        ::set result [eval $object endInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc endTracking { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'evalSuperMaster'
    proc evalSuperMaster { object args } {
        ::set result [eval $object evalSuperMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'exists'
    proc exists { object args } {
        ::set result [eval $object exists $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'existsOnDisk'
    proc existsOnDisk { object args } {
        ::set result [eval $object existsOnDisk $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc exportFull { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc exportIncr { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'find'
    proc find { object args } {
        ::set result [eval $object find $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc findAttr { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc findByName { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc findChangeSet { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'findParam'
    proc findParam { object args } {
        ::set result [eval $object findParam $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'fix'
    proc fix { object args } {
        ::set result [eval $object fix $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'flags'
    proc flags { object args } {
        ::set result [eval $object flags $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'flush'
    proc flush { object args } {
        ::set result [eval $object flush $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'followLayers'
    proc followLayers { object args } {
        ::set result [eval $object followLayers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'gate'
    proc gate { object args } {
        ::set result [eval $object gate $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'get'
    proc get { object args } {
        ::set result [eval $object get $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'getAbstractType'
    proc getAbstractType { object args } {
        ::set result [eval $object getAbstractType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAccess'
    proc getAccess { object args } {
        ::set result [eval $object getAccess $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAccessDir'
    proc getAccessDir { object args } {
        ::set result [eval $object getAccessDir $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAccessLevel'
    proc getAccessLevel { object args } {
        ::set result [eval $object getAccessLevel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAlignment'
    proc getAlignment { object args } {
        ::set result [eval $object getAlignment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAllParams'
    proc getAllParams { object args } {
        ::set result [eval $object getAllParams $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAllowedDatabases'
    proc getAllowedDatabases { object args } {
        ::set result [eval $object getAllowedDatabases $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAllowedOwners'
    proc getAllowedOwners { object args } {
        ::set result [eval $object getAllowedOwners $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAnalysisLibName'
    proc getAnalysisLibName { object args } {
        ::set result [eval $object getAnalysisLibName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAnalysisLibs'
    proc getAnalysisLibs { object args } {
        ::set result [eval $object getAnalysisLibs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getAnalysisOpPoints'
    proc getAnalysisOpPoints { object args } {
        ::set result [eval $object getAnalysisOpPoints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getAnalysisPoint'
    proc getAnalysisPoint { object args } {
        ::set result [eval $object getAnalysisPoint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAnalysisPoints'
    proc getAnalysisPoints { object args } {
        ::set result [eval $object getAnalysisPoints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getAntennaData'
    proc getAntennaData { object args } {
        ::set result [eval $object getAntennaData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAppDefs'
    proc getAppDefs { object args } {
        ::set result [eval $object getAppDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getAppDefsByDataType'
    proc getAppDefsByDataType { object args } {
        ::set result [eval $object getAppDefsByDataType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getAppObjectDef'
    proc getAppObjectDef { object args } {
        ::set result [eval $object getAppObjectDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAppObjectDefs'
    proc getAppObjectDefs { object args } {
        ::set result [eval $object getAppObjectDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getAppObjects'
    proc getAppObjects { object args } {
        ::set result [eval $object getAppObjects $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getAppType'
    proc getAppType { object args } {
        ::set result [eval $object getAppType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAppVal'
    proc getAppVal { object args } {
        ::set result [eval $object getAppVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc getArea { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'getAssignedNet'
    proc getAssignedNet { object args } {
        ::set result [eval $object getAssignedNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAssignmentDef'
    proc getAssignmentDef { object args } {
        ::set result [eval $object getAssignmentDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAssignments'
    proc getAssignments { object args } {
        ::set result [eval $object getAssignments $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getAttribute'
    proc getAttribute { object args } {
        ::set result [eval $object getAttribute $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAttributes'
    proc getAttributes { object args } {
        ::set result [eval $object getAttributes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAvgACCurrentDensity'
    proc getAvgACCurrentDensity { object args } {
        ::set result [eval $object getAvgACCurrentDensity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getAvgDCCurrentDensity'
    proc getAvgDCCurrentDensity { object args } {
        ::set result [eval $object getAvgDCCurrentDensity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBBox'
    proc getBBox { object args } {
        ::set result [eval $object getBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'getBaseName'
    proc getBaseName { object args } {
        ::set result [eval $object getBaseName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBeginConn'
    proc getBeginConn { object args } {
        ::set result [eval $object getBeginConn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBeginExt'
    proc getBeginExt { object args } {
        ::set result [eval $object getBeginExt $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBeginLayerHeader'
    proc getBeginLayerHeader { object args } {
        ::set result [eval $object getBeginLayerHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBeginLayerNum'
    proc getBeginLayerNum { object args } {
        ::set result [eval $object getBeginLayerNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBeginStyle'
    proc getBeginStyle { object args } {
        ::set result [eval $object getBeginStyle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBit'
    proc getBit { object args } {
        ::set result [eval $object getBit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBitIndex'
    proc getBitIndex { object args } {
        ::set result [eval $object getBitIndex $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBitName'
    proc getBitName { object args } {
        ::set result [eval $object getBitName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBitOrder'
    proc getBitOrder { object args } {
        ::set result [eval $object getBitOrder $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBlock'
    proc getBlock { object args } {
        ::set result [eval $object getBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBlockageBBox'
    proc getBlockageBBox { object args } {
        ::set result [eval $object getBlockageBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'getBlockageType'
    proc getBlockageType { object args } {
        ::set result [eval $object getBlockageType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBlockages'
    proc getBlockages { object args } {
        ::set result [eval $object getBlockages $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getBlockagesOwnedBy'
    proc getBlockagesOwnedBy { object args } {
        ::set result [eval $object getBlockagesOwnedBy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getBooleanVal'
    proc getBooleanVal { object args } {
        ::set result [eval $object getBooleanVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBoundaries'
    proc getBoundaries { object args } {
        ::set result [eval $object getBoundaries $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getBoundary'
    proc getBoundary { object args } {
        ::set result [eval $object getBoundary $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'getBuildName'
    proc getBuildName { object args } {
        ::set result [eval $object getBuildName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBuildNumber'
    proc getBuildNumber { object args } {
        ::set result [eval $object getBuildNumber $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBuildTime'
    proc getBuildTime { object args } {
        ::set result [eval $object getBuildTime $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBundle'
    proc getBundle { object args } {
        ::set result [eval $object getBundle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBundleName'
    proc getBundleName { object args } {
        ::set result [eval $object getBundleName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getBusNetBits'
    proc getBusNetBits { object args } {
        ::set result [eval $object getBusNetBits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getBusNetDefs'
    proc getBusNetDefs { object args } {
        ::set result [eval $object getBusNetDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getBusNets'
    proc getBusNets { object args } {
        ::set result [eval $object getBusNets $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getBusTermBits'
    proc getBusTermBits { object args } {
        ::set result [eval $object getBusTermBits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getBusTermDefs'
    proc getBusTermDefs { object args } {
        ::set result [eval $object getBusTermDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getBusTerms'
    proc getBusTerms { object args } {
        ::set result [eval $object getBusTerms $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getCategory'
    proc getCategory { object args } {
        ::set result [eval $object getCategory $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCell'
    proc getCell { object args } {
        ::set result [eval $object getCell $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCellName'
    proc getCellName { object args } {
        ::set result [eval $object getCellName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCellType'
    proc getCellType { object args } {
        ::set result [eval $object getCellType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCellViews'
    proc getCellViews { object args } {
        ::set result [eval $object getCellViews $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getCells'
    proc getCells { object args } {
        ::set result [eval $object getCells $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getCenter'
    proc getCenter { object args } {
        ::set result [eval $object getCenter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'getClearanceMeasure'
    proc getClearanceMeasure { object args } {
        ::set result [eval $object getClearanceMeasure $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCloseBusChar'
    proc getCloseBusChar { object args } {
        ::set result [eval $object getCloseBusChar $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCluster'
    proc getCluster { object args } {
        ::set result [eval $object getCluster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getClusterType'
    proc getClusterType { object args } {
        ::set result [eval $object getClusterType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getClusters'
    proc getClusters { object args } {
        ::set result [eval $object getClusters $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getColHeader'
    proc getColHeader { object args } {
        ::set result [eval $object getColHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getColInterpolateType'
    proc getColInterpolateType { object args } {
        ::set result [eval $object getColInterpolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getColLowerExtrapolateType'
    proc getColLowerExtrapolateType { object args } {
        ::set result [eval $object getColLowerExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getColName'
    proc getColName { object args } {
        ::set result [eval $object getColName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getColUpperExtrapolateType'
    proc getColUpperExtrapolateType { object args } {
        ::set result [eval $object getColUpperExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getConcreteType'
    proc getConcreteType { object args } {
        ::set result [eval $object getConcreteType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getConnNodes'
    proc getConnNodes { object args } {
        ::set result [eval $object getConnNodes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getConnRoutes'
    proc getConnRoutes { object args } {
        ::set result [eval $object getConnRoutes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getConnStatus'
    proc getConnStatus { object args } {
        ::set result [eval $object getConnStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getConnectDef'
    proc getConnectDef { object args } {
        ::set result [eval $object getConnectDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getConnectDefs'
    proc getConnectDefs { object args } {
        ::set result [eval $object getConnectDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getConns'
    proc getConns { object args } {
        ::set result [eval $object getConns $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getConstraintDefs'
    proc getConstraintDefs { object args } {
        ::set result [eval $object getConstraintDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getConstraintGroup'
    proc getConstraintGroup { object args } {
        ::set result [eval $object getConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getConstraintGroupDefs'
    proc getConstraintGroupDefs { object args } {
        ::set result [eval $object getConstraintGroupDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getConstraintGroupHeaders'
    proc getConstraintGroupHeaders { object args } {
        ::set result [eval $object getConstraintGroupHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getConstraintGroupMems'
    proc getConstraintGroupMems { object args } {
        ::set result [eval $object getConstraintGroupMems $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getConstraintGroupType'
    proc getConstraintGroupType { object args } {
        ::set result [eval $object getConstraintGroupType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getConstraintGroups'
    proc getConstraintGroups { object args } {
        ::set result [eval $object getConstraintGroups $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getConstraintParamDefs'
    proc getConstraintParamDefs { object args } {
        ::set result [eval $object getConstraintParamDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getConstraintParams'
    proc getConstraintParams { object args } {
        ::set result [eval $object getConstraintParams $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getConstraints'
    proc getConstraints { object args } {
        ::set result [eval $object getConstraints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getControlledObjects'
    proc getControlledObjects { object args } {
        ::set result [eval $object getControlledObjects $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCoreBoxSpec'
    proc getCoreBoxSpec { object args } {
        ::set result [eval $object getCoreBoxSpec $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCount'
    proc getCount { object args } {
        ::set result [eval $object getCount $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCoupledNets'
    proc getCoupledNets { object args } {
        ::set result [eval $object getCoupledNets $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getCpID'
    proc getCpID { object args } {
        ::set result [eval $object getCpID $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCreateTime'
    proc getCreateTime { object args } {
        ::set result [eval $object getCreateTime $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCurrentTime'
    proc getCurrentTime { object args } {
        ::set result [eval $object getCurrentTime $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCutArea'
    proc getCutArea { object args } {
        ::set result [eval $object getCutArea $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCutColumns'
    proc getCutColumns { object args } {
        ::set result [eval $object getCutColumns $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCutHeight'
    proc getCutHeight { object args } {
        ::set result [eval $object getCutHeight $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCutLayer'
    proc getCutLayer { object args } {
        ::set result [eval $object getCutLayer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCutPatternVal'
    proc getCutPatternVal { object args } {
        ::set result [eval $object getCutPatternVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCutPerimeter'
    proc getCutPerimeter { object args } {
        ::set result [eval $object getCutPerimeter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCutRows'
    proc getCutRows { object args } {
        ::set result [eval $object getCutRows $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCutSpacing'
    proc getCutSpacing { object args } {
        ::set result [eval $object getCutSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getCutWidth'
    proc getCutWidth { object args } {
        ::set result [eval $object getCutWidth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDBType'
    proc getDBType { object args } {
        ::set result [eval $object getDBType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDBUPerUU'
    proc getDBUPerUU { object args } {
        ::set result [eval $object getDBUPerUU $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDMFiles'
    proc getDMFiles { object args } {
        ::set result [eval $object getDMFiles $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getDMSystemName'
    proc getDMSystemName { object args } {
        ::set result [eval $object getDMSystemName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDX'
    proc getDX { object args } {
        ::set result [eval $object getDX $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDY'
    proc getDY { object args } {
        ::set result [eval $object getDY $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDataModelModType'
    proc getDataModelModType { object args } {
        ::set result [eval $object getDataModelModType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDataModelRev'
    proc getDataModelRev { object args } {
        ::set result [eval $object getDataModelRev $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDataValue'
    proc getDataValue { object args } {
        ::set result [eval $object getDataValue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDatabase'
    proc getDatabase { object args } {
        ::set result [eval $object getDatabase $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDatabaseTypes'
    proc getDatabaseTypes { object args } {
        ::set result [eval $object getDatabaseTypes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDay'
    proc getDay { object args } {
        ::set result [eval $object getDay $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDef'
    proc getDef { object args } {
        ::set result [eval $object getDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDefIndex'
    proc getDefIndex { object args } {
        ::set result [eval $object getDefIndex $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDefaultConstraintGroup'
    proc getDefaultConstraintGroup { object args } {
        ::set result [eval $object getDefaultConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDefaultManufacturingGrid'
    proc getDefaultManufacturingGrid { object args } {
        ::set result [eval $object getDefaultManufacturingGrid $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDefaultValue'
    proc getDefaultValue { object args } {
        ::set result [eval $object getDefaultValue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDelay'
    proc getDelay { object args } {
        ::set result [eval $object getDelay $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDeleteWhen'
    proc getDeleteWhen { object args } {
        ::set result [eval $object getDeleteWhen $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDeltaX'
    proc getDeltaX { object args } {
        ::set result [eval $object getDeltaX $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDeltaY'
    proc getDeltaY { object args } {
        ::set result [eval $object getDeltaY $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDensity'
    proc getDensity { object args } {
        ::set result [eval $object getDensity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDepth'
    proc getDepth { object args } {
        ::set result [eval $object getDepth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDerivedLayerDefs'
    proc getDerivedLayerDefs { object args } {
        ::set result [eval $object getDerivedLayerDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDerivedLayerParamDefs'
    proc getDerivedLayerParamDefs { object args } {
        ::set result [eval $object getDerivedLayerParamDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDerivedLayerParams'
    proc getDerivedLayerParams { object args } {
        ::set result [eval $object getDerivedLayerParams $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getDescription'
    proc getDescription { object args } {
        ::set result [eval $object getDescription $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDesign'
    proc getDesign { object args } {
        ::set result [eval $object getDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDevices'
    proc getDevices { object args } {
        ::set result [eval $object getDevices $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getDiode'
    proc getDiode { object args } {
        ::set result [eval $object getDiode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDirection'
    proc getDirection { object args } {
        ::set result [eval $object getDirection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDisplayName'
    proc getDisplayName { object args } {
        ::set result [eval $object getDisplayName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDomain'
    proc getDomain { object args } {
        ::set result [eval $object getDomain $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDoubleVal'
    proc getDoubleVal { object args } {
        ::set result [eval $object getDoubleVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getDriver'
    proc getDriver { object args } {
        ::set result [eval $object getDriver $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getEEQMaster'
    proc getEEQMaster { object args } {
        ::set result [eval $object getEEQMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getEdgeNames'
    proc getEdgeNames { object args } {
        ::set result [eval $object getEdgeNames $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getEffectiveUndoModel'
    proc getEffectiveUndoModel { object args } {
        ::set result [eval $object getEffectiveUndoModel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getEffectiveWidth'
    proc getEffectiveWidth { object args } {
        ::set result [eval $object getEffectiveWidth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getElapsed'
    proc getElapsed { object args } {
        ::set result [eval $object getElapsed $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc getElement { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Unsupported function
    proc getElements { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'getEllipseBBox'
    proc getEllipseBBox { object args } {
        ::set result [eval $object getEllipseBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'getElmores'
    proc getElmores { object args } {
        ::set result [eval $object getElmores $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getEndConn'
    proc getEndConn { object args } {
        ::set result [eval $object getEndConn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getEndExt'
    proc getEndExt { object args } {
        ::set result [eval $object getEndExt $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getEndLayerHeader'
    proc getEndLayerHeader { object args } {
        ::set result [eval $object getEndLayerHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getEndLayerNum'
    proc getEndLayerNum { object args } {
        ::set result [eval $object getEndLayerNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getEndStyle'
    proc getEndStyle { object args } {
        ::set result [eval $object getEndStyle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getEndpoint'
    proc getEndpoint { object args } {
        ::set result [eval $object getEndpoint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getEnums'
    proc getEnums { object args } {
        ::set result [eval $object getEnums $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getEquivalentNets'
    proc getEquivalentNets { object args } {
        ::set result [eval $object getEquivalentNets $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getEvaluatorName'
    proc getEvaluatorName { object args } {
        ::set result [eval $object getEvaluatorName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getExcludedLayers'
    proc getExcludedLayers { object args } {
        ::set result [eval $object getExcludedLayers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc getExportProtocols { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'getExpression'
    proc getExpression { object args } {
        ::set result [eval $object getExpression $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFarCap'
    proc getFarCap { object args } {
        ::set result [eval $object getFarCap $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFeatures'
    proc getFeatures { object args } {
        ::set result [eval $object getFeatures $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFig'
    proc getFig { object args } {
        ::set result [eval $object getFig $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFigGroup'
    proc getFigGroup { object args } {
        ::set result [eval $object getFigGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFigGroupMem'
    proc getFigGroupMem { object args } {
        ::set result [eval $object getFigGroupMem $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFigGroupStatus'
    proc getFigGroupStatus { object args } {
        ::set result [eval $object getFigGroupStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFigGroups'
    proc getFigGroups { object args } {
        ::set result [eval $object getFigGroups $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getFigs'
    proc getFigs { object args } {
        ::set result [eval $object getFigs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getFileName'
    proc getFileName { object args } {
        ::set result [eval $object getFileName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFirst'
    proc getFirst { object args } {
        ::set result [eval $object getFirst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc getFixedAttrs { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'getFloatVal'
    proc getFloatVal { object args } {
        ::set result [eval $object getFloatVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFollowers'
    proc getFollowers { object args } {
        ::set result [eval $object getFollowers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getFont'
    proc getFont { object args } {
        ::set result [eval $object getFont $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFormat'
    proc getFormat { object args } {
        ::set result [eval $object getFormat $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFoundryRules'
    proc getFoundryRules { object args } {
        ::set result [eval $object getFoundryRules $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFromDevices'
    proc getFromDevices { object args } {
        ::set result [eval $object getFromDevices $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getFromMutualInductors'
    proc getFromMutualInductors { object args } {
        ::set result [eval $object getFromMutualInductors $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getFullName'
    proc getFullName { object args } {
        ::set result [eval $object getFullName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getFullPath'
    proc getFullPath { object args } {
        ::set result [eval $object getFullPath $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getGCell'
    proc getGCell { object args } {
        ::set result [eval $object getGCell $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getGCellPatterns'
    proc getGCellPatterns { object args } {
        ::set result [eval $object getGCellPatterns $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getGate'
    proc getGate { object args } {
        ::set result [eval $object getGate $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getGlobalNets'
    proc getGlobalNets { object args } {
        ::set result [eval $object getGlobalNets $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getGroundSensitivity'
    proc getGroundSensitivity { object args } {
        ::set result [eval $object getGroundSensitivity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getGroup'
    proc getGroup { object args } {
        ::set result [eval $object getGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getGroupDefs'
    proc getGroupDefs { object args } {
        ::set result [eval $object getGroupDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getGroupDomain'
    proc getGroupDomain { object args } {
        ::set result [eval $object getGroupDomain $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getGroupLeaders'
    proc getGroupLeaders { object args } {
        ::set result [eval $object getGroupLeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getGroupMems'
    proc getGroupMems { object args } {
        ::set result [eval $object getGroupMems $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getGroupType'
    proc getGroupType { object args } {
        ::set result [eval $object getGroupType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getGroups'
    proc getGroups { object args } {
        ::set result [eval $object getGroups $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getGroupsByName'
    proc getGroupsByName { object args } {
        ::set result [eval $object getGroupsByName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getGroupsOwnedBy'
    proc getGroupsOwnedBy { object args } {
        ::set result [eval $object getGroupsOwnedBy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getGuideBBox'
    proc getGuideBBox { object args } {
        ::set result [eval $object getGuideBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'getGuides'
    proc getGuides { object args } {
        ::set result [eval $object getGuides $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getHeader'
    proc getHeader { object args } {
        ::set result [eval $object getHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getHeight'
    proc getHeight { object args } {
        ::set result [eval $object getHeight $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getHierDelimiter'
    proc getHierDelimiter { object args } {
        ::set result [eval $object getHierDelimiter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getHierPath'
    proc getHierPath { object args } {
        ::set result [eval $object getHierPath $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getHoleBBox'
    proc getHoleBBox { object args } {
        ::set result [eval $object getHoleBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'getHoleRadius'
    proc getHoleRadius { object args } {
        ::set result [eval $object getHoleRadius $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getHorizontalDemand'
    proc getHorizontalDemand { object args } {
        ::set result [eval $object getHorizontalDemand $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getHorizontalSupply'
    proc getHorizontalSupply { object args } {
        ::set result [eval $object getHorizontalSupply $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getHour'
    proc getHour { object args } {
        ::set result [eval $object getHour $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getID'
    proc getID { object args } {
        ::set result [eval $object getID $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getIEvalText'
    proc getIEvalText { object args } {
        ::set result [eval $object getIEvalText $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getIOBox'
    proc getIOBox { object args } {
        ::set result [eval $object getIOBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'getIPcell'
    proc getIPcell { object args } {
        ::set result [eval $object getIPcell $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getId'
    proc getId { object args } {
        ::set result [eval $object getId $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getImplant1'
    proc getImplant1 { object args } {
        ::set result [eval $object getImplant1 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getImplant1Enc'
    proc getImplant1Enc { object args } {
        ::set result [eval $object getImplant1Enc $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getImplant1Num'
    proc getImplant1Num { object args } {
        ::set result [eval $object getImplant1Num $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getImplant2'
    proc getImplant2 { object args } {
        ::set result [eval $object getImplant2 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getImplant2Enc'
    proc getImplant2Enc { object args } {
        ::set result [eval $object getImplant2Enc $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getImplant2Num'
    proc getImplant2Num { object args } {
        ::set result [eval $object getImplant2Num $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInInstTerm'
    proc getInInstTerm { object args } {
        ::set result [eval $object getInInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getIndex'
    proc getIndex { object args } {
        ::set result [eval $object getIndex $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInductor'
    proc getInductor { object args } {
        ::set result [eval $object getInductor $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInst'
    proc getInst { object args } {
        ::set result [eval $object getInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInstHeader'
    proc getInstHeader { object args } {
        ::set result [eval $object getInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInstHeaders'
    proc getInstHeaders { object args } {
        ::set result [eval $object getInstHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getInstTerm'
    proc getInstTerm { object args } {
        ::set result [eval $object getInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInstTerms'
    proc getInstTerms { object args } {
        ::set result [eval $object getInstTerms $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getInstance'
    proc getInstance { object args } {
        ::set result [eval $object getInstance $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInstances'
    proc getInstances { object args } {
        ::set result [eval $object getInstances $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getInsts'
    proc getInsts { object args } {
        ::set result [eval $object getInsts $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getIntVal'
    proc getIntVal { object args } {
        ::set result [eval $object getIntVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInternalBundle'
    proc getInternalBundle { object args } {
        ::set result [eval $object getInternalBundle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInternalMember'
    proc getInternalMember { object args } {
        ::set result [eval $object getInternalMember $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInternalScalar'
    proc getInternalScalar { object args } {
        ::set result [eval $object getInternalScalar $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInternalVBit'
    proc getInternalVBit { object args } {
        ::set result [eval $object getInternalVBit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInternalVector'
    proc getInternalVector { object args } {
        ::set result [eval $object getInternalVector $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getInterpolateType'
    proc getInterpolateType { object args } {
        ::set result [eval $object getInterpolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getIterAll'
    proc getIterAll { object args } {
        ::set result [eval $object getIterAll $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLPPHeaders'
    proc getLPPHeaders { object args } {
        ::set result [eval $object getLPPHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getLastSavedTime'
    proc getLastSavedTime { object args } {
        ::set result [eval $object getLastSavedTime $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLastWriteTime'
    proc getLastWriteTime { object args } {
        ::set result [eval $object getLastWriteTime $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayer'
    proc getLayer { object args } {
        ::set result [eval $object getLayer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayer1'
    proc getLayer1 { object args } {
        ::set result [eval $object getLayer1 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayer1Enc'
    proc getLayer1Enc { object args } {
        ::set result [eval $object getLayer1Enc $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayer1Num'
    proc getLayer1Num { object args } {
        ::set result [eval $object getLayer1Num $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayer1Offset'
    proc getLayer1Offset { object args } {
        ::set result [eval $object getLayer1Offset $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayer2'
    proc getLayer2 { object args } {
        ::set result [eval $object getLayer2 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayer2Enc'
    proc getLayer2Enc { object args } {
        ::set result [eval $object getLayer2Enc $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayer2Num'
    proc getLayer2Num { object args } {
        ::set result [eval $object getLayer2Num $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayer2Offset'
    proc getLayer2Offset { object args } {
        ::set result [eval $object getLayer2Offset $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayerAbove'
    proc getLayerAbove { object args } {
        ::set result [eval $object getLayerAbove $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getLayerBelow'
    proc getLayerBelow { object args } {
        ::set result [eval $object getLayerBelow $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getLayerHeader'
    proc getLayerHeader { object args } {
        ::set result [eval $object getLayerHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayerHeaders'
    proc getLayerHeaders { object args } {
        ::set result [eval $object getLayerHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getLayerNum'
    proc getLayerNum { object args } {
        ::set result [eval $object getLayerNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLayers'
    proc getLayers { object args } {
        ::set result [eval $object getLayers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getLeader'
    proc getLeader { object args } {
        ::set result [eval $object getLeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLength'
    proc getLength { object args } {
        ::set result [eval $object getLength $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLevel'
    proc getLevel { object args } {
        ::set result [eval $object getLevel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLib'
    proc getLib { object args } {
        ::set result [eval $object getLib $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLibAttributes'
    proc getLibAttributes { object args } {
        ::set result [eval $object getLibAttributes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLibMode'
    proc getLibMode { object args } {
        ::set result [eval $object getLibMode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLibName'
    proc getLibName { object args } {
        ::set result [eval $object getLibName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLibPath'
    proc getLibPath { object args } {
        ::set result [eval $object getLibPath $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLibWritePath'
    proc getLibWritePath { object args } {
        ::set result [eval $object getLibWritePath $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLink'
    proc getLink { object args } {
        ::set result [eval $object getLink $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getList'
    proc getList { object args } {
        ::set result [eval $object getList $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLocation'
    proc getLocation { object args } {
        ::set result [eval $object getLocation $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'getLock'
    proc getLock { object args } {
        ::set result [eval $object getLock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLockStatus'
    proc getLockStatus { object args } {
        ::set result [eval $object getLockStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLowerBound'
    proc getLowerBound { object args } {
        ::set result [eval $object getLowerBound $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLowerExtrapolateType'
    proc getLowerExtrapolateType { object args } {
        ::set result [eval $object getLowerExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getLowerRight'
    proc getLowerRight { object args } {
        ::set result [eval $object getLowerRight $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'getMajorReleaseNum'
    proc getMajorReleaseNum { object args } {
        ::set result [eval $object getMajorReleaseNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getManufacturingGrid'
    proc getManufacturingGrid { object args } {
        ::set result [eval $object getManufacturingGrid $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMarkers'
    proc getMarkers { object args } {
        ::set result [eval $object getMarkers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getMarkersOwnedBy'
    proc getMarkersOwnedBy { object args } {
        ::set result [eval $object getMarkersOwnedBy $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getMaskNumber'
    proc getMaskNumber { object args } {
        ::set result [eval $object getMaskNumber $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMaskRange'
    proc getMaskRange { object args } {
        ::set result [eval $object getMaskRange $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMaster'
    proc getMaster { object args } {
        ::set result [eval $object getMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMasterAttribute'
    proc getMasterAttribute { object args } {
        ::set result [eval $object getMasterAttribute $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMasterBBox'
    proc getMasterBBox { object args } {
        ::set result [eval $object getMasterBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'getMasterModule'
    proc getMasterModule { object args } {
        ::set result [eval $object getMasterModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMasterOccurrence'
    proc getMasterOccurrence { object args } {
        ::set result [eval $object getMasterOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMasterProp'
    proc getMasterProp { object args } {
        ::set result [eval $object getMasterProp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMasterText'
    proc getMasterText { object args } {
        ::set result [eval $object getMasterText $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMasterTextText'
    proc getMasterTextText { object args } {
        ::set result [eval $object getMasterTextText $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMaterial'
    proc getMaterial { object args } {
        ::set result [eval $object getMaterial $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMaxBits'
    proc getMaxBits { object args } {
        ::set result [eval $object getMaxBits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMaxIndex'
    proc getMaxIndex { object args } {
        ::set result [eval $object getMaxIndex $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMember'
    proc getMember { object args } {
        ::set result [eval $object getMember $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMemberArray'
    proc getMemberArray { object args } {
        ::set result [eval $object getMemberArray $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMemberNets'
    proc getMemberNets { object args } {
        ::set result [eval $object getMemberNets $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMembers'
    proc getMembers { object args } {
        ::set result [eval $object getMembers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getMinIndex'
    proc getMinIndex { object args } {
        ::set result [eval $object getMinIndex $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMinorReleaseNum'
    proc getMinorReleaseNum { object args } {
        ::set result [eval $object getMinorReleaseNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMinute'
    proc getMinute { object args } {
        ::set result [eval $object getMinute $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc getModAttrs { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'getModInst'
    proc getModInst { object args } {
        ::set result [eval $object getModInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getModInstHeader'
    proc getModInstHeader { object args } {
        ::set result [eval $object getModInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getModInstHeaders'
    proc getModInstHeaders { object args } {
        ::set result [eval $object getModInstHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getModInstTerm'
    proc getModInstTerm { object args } {
        ::set result [eval $object getModInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getModModuleInstHeader'
    proc getModModuleInstHeader { object args } {
        ::set result [eval $object getModModuleInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getModNet'
    proc getModNet { object args } {
        ::set result [eval $object getModNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getModTerm'
    proc getModTerm { object args } {
        ::set result [eval $object getModTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMode'
    proc getMode { object args } {
        ::set result [eval $object getMode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getModel'
    proc getModel { object args } {
        ::set result [eval $object getModel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getModule'
    proc getModule { object args } {
        ::set result [eval $object getModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getModuleInstHeaders'
    proc getModuleInstHeaders { object args } {
        ::set result [eval $object getModuleInstHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getModules'
    proc getModules { object args } {
        ::set result [eval $object getModules $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getMonth'
    proc getMonth { object args } {
        ::set result [eval $object getMonth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMsg'
    proc getMsg { object args } {
        ::set result [eval $object getMsg $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getMustJoinTerms'
    proc getMustJoinTerms { object args } {
        ::set result [eval $object getMustJoinTerms $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getName'
    proc getName { object args } {
        ::set result [eval $object getName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Special command 'getNameSpace'
    proc getNameSpace { args } {
        ::set result [eval oaNameSpace_getDefault $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNames'
    proc getNames { object args } {
        ::set result [eval $object getNames $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNearCap'
    proc getNearCap { object args } {
        ::set result [eval $object getNearCap $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNet'
    proc getNet { object args } {
        ::set result [eval $object getNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNets'
    proc getNets { object args } {
        ::set result [eval $object getNets $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getNext'
    proc getNext { object args } {
        ::set result [eval $object getNext $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNode'
    proc getNode { object args } {
        ::set result [eval $object getNode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNodes'
    proc getNodes { object args } {
        ::set result [eval $object getNodes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getNumBits'
    proc getNumBits { object args } {
        ::set result [eval $object getNumBits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumBytes'
    proc getNumBytes { object args } {
        ::set result [eval $object getNumBytes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumCols'
    proc getNumCols { object args } {
        ::set result [eval $object getNumCols $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumCuts'
    proc getNumCuts { object args } {
        ::set result [eval $object getNumCuts $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumEdges'
    proc getNumEdges { object args } {
        ::set result [eval $object getNumEdges $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumElements'
    proc getNumElements { object args } {
        ::set result [eval $object getNumElements $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumItems'
    proc getNumItems { object args } {
        ::set result [eval $object getNumItems $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumLayers'
    proc getNumLayers { object args } {
        ::set result [eval $object getNumLayers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumMembers'
    proc getNumMembers { object args } {
        ::set result [eval $object getNumMembers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc getNumMultiBit { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'getNumObjects'
    proc getNumObjects { object args } {
        ::set result [eval $object getNumObjects $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumPoints'
    proc getNumPoints { object args } {
        ::set result [eval $object getNumPoints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumRows'
    proc getNumRows { object args } {
        ::set result [eval $object getNumRows $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumSites'
    proc getNumSites { object args } {
        ::set result [eval $object getNumSites $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumTracks'
    proc getNumTracks { object args } {
        ::set result [eval $object getNumTracks $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumUndoCpRecs'
    proc getNumUndoCpRecs { object args } {
        ::set result [eval $object getNumUndoCpRecs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumUndoRecsOnCp'
    proc getNumUndoRecsOnCp { object args } {
        ::set result [eval $object getNumUndoRecsOnCp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumXGCell'
    proc getNumXGCell { object args } {
        ::set result [eval $object getNumXGCell $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumYGCell'
    proc getNumYGCell { object args } {
        ::set result [eval $object getNumYGCell $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getNumber'
    proc getNumber { object args } {
        ::set result [eval $object getNumber $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getObject'
    proc getObject { object args } {
        ::set result [eval $object getObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getObjectType'
    proc getObjectType { object args } {
        ::set result [eval $object getObjectType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getObjectTypes'
    proc getObjectTypes { object args } {
        ::set result [eval $object getObjectTypes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getObjects'
    proc getObjects { object args } {
        ::set result [eval $object getObjects $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getOccBusNetBits'
    proc getOccBusNetBits { object args } {
        ::set result [eval $object getOccBusNetBits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getOccBusNets'
    proc getOccBusNets { object args } {
        ::set result [eval $object getOccBusNets $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getOccInst'
    proc getOccInst { object args } {
        ::set result [eval $object getOccInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOccInstTerm'
    proc getOccInstTerm { object args } {
        ::set result [eval $object getOccInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOccInstTerms'
    proc getOccInstTerms { object args } {
        ::set result [eval $object getOccInstTerms $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getOccInsts'
    proc getOccInsts { object args } {
        ::set result [eval $object getOccInsts $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getOccMembers'
    proc getOccMembers { object args } {
        ::set result [eval $object getOccMembers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getOccNet'
    proc getOccNet { object args } {
        ::set result [eval $object getOccNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOccNets'
    proc getOccNets { object args } {
        ::set result [eval $object getOccNets $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getOccTerm'
    proc getOccTerm { object args } {
        ::set result [eval $object getOccTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOccTerms'
    proc getOccTerms { object args } {
        ::set result [eval $object getOccTerms $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getOccurrence'
    proc getOccurrence { object args } {
        ::set result [eval $object getOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOccurrences'
    proc getOccurrences { object args } {
        ::set result [eval $object getOccurrences $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getOffsets'
    proc getOffsets { object args } {
        ::set result [eval $object getOffsets $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOnDiskSize'
    proc getOnDiskSize { object args } {
        ::set result [eval $object getOnDiskSize $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOpPoint'
    proc getOpPoint { object args } {
        ::set result [eval $object getOpPoint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOpPointName'
    proc getOpPointName { object args } {
        ::set result [eval $object getOpPointName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOpPoints'
    proc getOpPoints { object args } {
        ::set result [eval $object getOpPoints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getOpenBusChar'
    proc getOpenBusChar { object args } {
        ::set result [eval $object getOpenBusChar $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOperation'
    proc getOperation { object args } {
        ::set result [eval $object getOperation $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOperator'
    proc getOperator { object args } {
        ::set result [eval $object getOperator $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOrient'
    proc getOrient { object args } {
        ::set result [eval $object getOrient $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOrig'
    proc getOrig { object args } {
        ::set result [eval $object getOrig $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOrigin'
    proc getOrigin { object args } {
        ::set result [eval $object getOrigin $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'getOriginOffset'
    proc getOriginOffset { object args } {
        ::set result [eval $object getOriginOffset $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOriginal'
    proc getOriginal { object args } {
        ::set result [eval $object getOriginal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOtherConn'
    proc getOtherConn { object args } {
        ::set result [eval $object getOtherConn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOtherInductor'
    proc getOtherInductor { object args } {
        ::set result [eval $object getOtherInductor $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOtherNode'
    proc getOtherNode { object args } {
        ::set result [eval $object getOtherNode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOutInstTerm'
    proc getOutInstTerm { object args } {
        ::set result [eval $object getOutInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOverlap'
    proc getOverlap { object args } {
        ::set result [eval $object getOverlap $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getOwner'
    proc getOwner { object args } {
        ::set result [eval $object getOwner $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPackageName'
    proc getPackageName { object args } {
        ::set result [eval $object getPackageName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getParams'
    proc getParams { object args } {
        ::set result [eval $object getParams $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getParasiticConfidence'
    proc getParasiticConfidence { object args } {
        ::set result [eval $object getParasiticConfidence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getParasiticNetwork'
    proc getParasiticNetwork { object args } {
        ::set result [eval $object getParasiticNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getParent'
    proc getParent { object args } {
        ::set result [eval $object getParent $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPartitionName'
    proc getPartitionName { object args } {
        ::set result [eval $object getPartitionName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPartitions'
    proc getPartitions { object args } {
        ::set result [eval $object getPartitions $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getPath'
    proc getPath { object args } {
        ::set result [eval $object getPath $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPathName'
    proc getPathName { object args } {
        ::set result [eval $object getPathName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPcellDef'
    proc getPcellDef { object args } {
        ::set result [eval $object getPcellDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPcellEvaluatorName'
    proc getPcellEvaluatorName { object args } {
        ::set result [eval $object getPcellEvaluatorName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPeakACCurrentDensity'
    proc getPeakACCurrentDensity { object args } {
        ::set result [eval $object getPeakACCurrentDensity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPiModel'
    proc getPiModel { object args } {
        ::set result [eval $object getPiModel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPiPoleResidue'
    proc getPiPoleResidue { object args } {
        ::set result [eval $object getPiPoleResidue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPin'
    proc getPin { object args } {
        ::set result [eval $object getPin $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPinConnectMethod'
    proc getPinConnectMethod { object args } {
        ::set result [eval $object getPinConnectMethod $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPinDelimiter'
    proc getPinDelimiter { object args } {
        ::set result [eval $object getPinDelimiter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPinType'
    proc getPinType { object args } {
        ::set result [eval $object getPinType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPins'
    proc getPins { object args } {
        ::set result [eval $object getPins $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getPlacementStatus'
    proc getPlacementStatus { object args } {
        ::set result [eval $object getPlacementStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPlugInName'
    proc getPlugInName { object args } {
        ::set result [eval $object getPlugInName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPoints'
    proc getPoints { object args } {
        ::set result [eval $object getPoints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'getPoleResidues'
    proc getPoleResidues { object args } {
        ::set result [eval $object getPoleResidues $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getPoles'
    proc getPoles { object args } {
        ::set result [eval $object getPoles $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPosition'
    proc getPosition { object args } {
        ::set result [eval $object getPosition $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPrefRoutingDir'
    proc getPrefRoutingDir { object args } {
        ::set result [eval $object getPrefRoutingDir $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPreferredEquivalent'
    proc getPreferredEquivalent { object args } {
        ::set result [eval $object getPreferredEquivalent $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPrimary'
    proc getPrimary { object args } {
        ::set result [eval $object getPrimary $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPriority'
    proc getPriority { object args } {
        ::set result [eval $object getPriority $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getProcess'
    proc getProcess { object args } {
        ::set result [eval $object getProcess $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getProcessFamily'
    proc getProcessFamily { object args } {
        ::set result [eval $object getProcessFamily $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getProp'
    proc getProp { object args } {
        ::set result [eval $object getProp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getProps'
    proc getProps { object args } {
        ::set result [eval $object getProps $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getPurpose'
    proc getPurpose { object args } {
        ::set result [eval $object getPurpose $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPurpose1'
    proc getPurpose1 { object args } {
        ::set result [eval $object getPurpose1 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPurpose2'
    proc getPurpose2 { object args } {
        ::set result [eval $object getPurpose2 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPurposeNum'
    proc getPurposeNum { object args } {
        ::set result [eval $object getPurposeNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPurposeType'
    proc getPurposeType { object args } {
        ::set result [eval $object getPurposeType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getPurposes'
    proc getPurposes { object args } {
        ::set result [eval $object getPurposes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getRadius'
    proc getRadius { object args } {
        ::set result [eval $object getRadius $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRangeType'
    proc getRangeType { object args } {
        ::set result [eval $object getRangeType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRaw'
    proc getRaw { object args } {
        ::set result [eval $object getRaw $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRawValue'
    proc getRawValue { object args } {
        ::set result [eval $object getRawValue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getReadInTime'
    proc getReadInTime { object args } {
        ::set result [eval $object getReadInTime $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getReceiver'
    proc getReceiver { object args } {
        ::set result [eval $object getReceiver $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getReducedModel'
    proc getReducedModel { object args } {
        ::set result [eval $object getReducedModel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRefCount'
    proc getRefCount { object args } {
        ::set result [eval $object getRefCount $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRefLibName'
    proc getRefLibName { object args } {
        ::set result [eval $object getRefLibName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRefListPath'
    proc getRefListPath { object args } {
        ::set result [eval $object getRefListPath $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRefTech'
    proc getRefTech { object args } {
        ::set result [eval $object getRefTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getReflection'
    proc getReflection { object args } {
        ::set result [eval $object getReflection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRelativeOrient'
    proc getRelativeOrient { object args } {
        ::set result [eval $object getRelativeOrient $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRepeat'
    proc getRepeat { object args } {
        ::set result [eval $object getRepeat $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRes'
    proc getRes { object args } {
        ::set result [eval $object getRes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getResidues'
    proc getResidues { object args } {
        ::set result [eval $object getResidues $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getResistancePerCut'
    proc getResistancePerCut { object args } {
        ::set result [eval $object getResistancePerCut $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRmsACCurrentDensity'
    proc getRmsACCurrentDensity { object args } {
        ::set result [eval $object getRmsACCurrentDensity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRoute'
    proc getRoute { object args } {
        ::set result [eval $object getRoute $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRouteMethod'
    proc getRouteMethod { object args } {
        ::set result [eval $object getRouteMethod $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRoutePattern'
    proc getRoutePattern { object args } {
        ::set result [eval $object getRoutePattern $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRouteStatus'
    proc getRouteStatus { object args } {
        ::set result [eval $object getRouteStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRouteTopology'
    proc getRouteTopology { object args } {
        ::set result [eval $object getRouteTopology $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRoutes'
    proc getRoutes { object args } {
        ::set result [eval $object getRoutes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getRoutingLayer'
    proc getRoutingLayer { object args } {
        ::set result [eval $object getRoutingLayer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRowBBox'
    proc getRowBBox { object args } {
        ::set result [eval $object getRowBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'getRowFlipType'
    proc getRowFlipType { object args } {
        ::set result [eval $object getRowFlipType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRowHeader'
    proc getRowHeader { object args } {
        ::set result [eval $object getRowHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRowHeaders'
    proc getRowHeaders { object args } {
        ::set result [eval $object getRowHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getRowInterpolateType'
    proc getRowInterpolateType { object args } {
        ::set result [eval $object getRowInterpolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRowLowerExtrapolateType'
    proc getRowLowerExtrapolateType { object args } {
        ::set result [eval $object getRowLowerExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRowName'
    proc getRowName { object args } {
        ::set result [eval $object getRowName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRowSpacing'
    proc getRowSpacing { object args } {
        ::set result [eval $object getRowSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRowSpacingType'
    proc getRowSpacingType { object args } {
        ::set result [eval $object getRowSpacingType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRowUpperExtrapolateType'
    proc getRowUpperExtrapolateType { object args } {
        ::set result [eval $object getRowUpperExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getRows'
    proc getRows { object args } {
        ::set result [eval $object getRows $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getScalar'
    proc getScalar { object args } {
        ::set result [eval $object getScalar $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getScalarName'
    proc getScalarName { object args } {
        ::set result [eval $object getScalarName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getScanChainSets'
    proc getScanChainSets { object args } {
        ::set result [eval $object getScanChainSets $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getScanChains'
    proc getScanChains { object args } {
        ::set result [eval $object getScanChains $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getSecond'
    proc getSecond { object args } {
        ::set result [eval $object getSecond $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSeqBitLength'
    proc getSeqBitLength { object args } {
        ::set result [eval $object getSeqBitLength $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSession'
    proc getSession { object args } {
        ::set result [eval $object getSession $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSeverity'
    proc getSeverity { object args } {
        ::set result [eval $object getSeverity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getShape'
    proc getShape { object args } {
        ::set result [eval $object getShape $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getShapeBBox'
    proc getShapeBBox { object args } {
        ::set result [eval $object getShapeBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'getShapes'
    proc getShapes { object args } {
        ::set result [eval $object getShapes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getShieldNet1'
    proc getShieldNet1 { object args } {
        ::set result [eval $object getShieldNet1 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getShieldNet2'
    proc getShieldNet2 { object args } {
        ::set result [eval $object getShieldNet2 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getShieldedNet1'
    proc getShieldedNet1 { object args } {
        ::set result [eval $object getShieldedNet1 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getShieldedNet2'
    proc getShieldedNet2 { object args } {
        ::set result [eval $object getShieldedNet2 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getShortMsg'
    proc getShortMsg { object args } {
        ::set result [eval $object getShortMsg $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSigType'
    proc getSigType { object args } {
        ::set result [eval $object getSigType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSimpleName'
    proc getSimpleName { object args } {
        ::set result [eval $object getSimpleName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSingleBitMembers'
    proc getSingleBitMembers { object args } {
        ::set result [eval $object getSingleBitMembers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getSiteDef'
    proc getSiteDef { object args } {
        ::set result [eval $object getSiteDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSiteDefHeight'
    proc getSiteDefHeight { object args } {
        ::set result [eval $object getSiteDefHeight $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSiteDefName'
    proc getSiteDefName { object args } {
        ::set result [eval $object getSiteDefName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSiteDefType'
    proc getSiteDefType { object args } {
        ::set result [eval $object getSiteDefType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSiteDefWidth'
    proc getSiteDefWidth { object args } {
        ::set result [eval $object getSiteDefWidth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSiteDefs'
    proc getSiteDefs { object args } {
        ::set result [eval $object getSiteDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getSiteOrient'
    proc getSiteOrient { object args } {
        ::set result [eval $object getSiteOrient $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSitePattern'
    proc getSitePattern { object args } {
        ::set result [eval $object getSitePattern $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSiteSpacing'
    proc getSiteSpacing { object args } {
        ::set result [eval $object getSiteSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSize'
    proc getSize { object args } {
        ::set result [eval $object getSize $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSource'
    proc getSource { object args } {
        ::set result [eval $object getSource $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSpacing'
    proc getSpacing { object args } {
        ::set result [eval $object getSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSpan'
    proc getSpan { object args } {
        ::set result [eval $object getSpan $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getStart'
    proc getStart { object args } {
        ::set result [eval $object getStart $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getStartAngle'
    proc getStartAngle { object args } {
        ::set result [eval $object getStartAngle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getStartCoord'
    proc getStartCoord { object args } {
        ::set result [eval $object getStartCoord $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getStartObject'
    proc getStartObject { object args } {
        ::set result [eval $object getStartObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getStatus'
    proc getStatus { object args } {
        ::set result [eval $object getStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSteinerBBox'
    proc getSteinerBBox { object args } {
        ::set result [eval $object getSteinerBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'getSteiners'
    proc getSteiners { object args } {
        ::set result [eval $object getSteiners $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getStep'
    proc getStep { object args } {
        ::set result [eval $object getStep $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getStop'
    proc getStop { object args } {
        ::set result [eval $object getStop $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getStopAngle'
    proc getStopAngle { object args } {
        ::set result [eval $object getStopAngle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getStopObject'
    proc getStopObject { object args } {
        ::set result [eval $object getStopObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getStringVal'
    proc getStringVal { object args } {
        ::set result [eval $object getStringVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getStyle'
    proc getStyle { object args } {
        ::set result [eval $object getStyle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSubHeaders'
    proc getSubHeaders { object args } {
        ::set result [eval $object getSubHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getSubMasters'
    proc getSubMasters { object args } {
        ::set result [eval $object getSubMasters $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getSubNetworks'
    proc getSubNetworks { object args } {
        ::set result [eval $object getSubNetworks $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getSuperHeader'
    proc getSuperHeader { object args } {
        ::set result [eval $object getSuperHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSuperMaster'
    proc getSuperMaster { object args } {
        ::set result [eval $object getSuperMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSupplySensitivity'
    proc getSupplySensitivity { object args } {
        ::set result [eval $object getSupplySensitivity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getSymmetry'
    proc getSymmetry { object args } {
        ::set result [eval $object getSymmetry $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTbl'
    proc getTbl { object args } {
        ::set result [eval $object getTbl $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTech'
    proc getTech { object args } {
        ::set result [eval $object getTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTechHeaders'
    proc getTechHeaders { object args } {
        ::set result [eval $object getTechHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTempFile'
    proc getTempFile { object args } {
        ::set result [eval $object getTempFile $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTemperature'
    proc getTemperature { object args } {
        ::set result [eval $object getTemperature $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTerm'
    proc getTerm { object args } {
        ::set result [eval $object getTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTermName'
    proc getTermName { object args } {
        ::set result [eval $object getTermName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTermPosition'
    proc getTermPosition { object args } {
        ::set result [eval $object getTermPosition $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTermType'
    proc getTermType { object args } {
        ::set result [eval $object getTermType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTerms'
    proc getTerms { object args } {
        ::set result [eval $object getTerms $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getTermsByPosition'
    proc getTermsByPosition { object args } {
        ::set result [eval $object getTermsByPosition $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getText'
    proc getText { object args } {
        ::set result [eval $object getText $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTimeStamp'
    proc getTimeStamp { object args } {
        ::set result [eval $object getTimeStamp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTimeVal'
    proc getTimeVal { object args } {
        ::set result [eval $object getTimeVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getToDevices'
    proc getToDevices { object args } {
        ::set result [eval $object getToDevices $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getToMutualInductors'
    proc getToMutualInductors { object args } {
        ::set result [eval $object getToMutualInductors $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getTool'
    proc getTool { object args } {
        ::set result [eval $object getTool $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTopBlock'
    proc getTopBlock { object args } {
        ::set result [eval $object getTopBlock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTopModule'
    proc getTopModule { object args } {
        ::set result [eval $object getTopModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTopOccurrence'
    proc getTopOccurrence { object args } {
        ::set result [eval $object getTopOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTotalCap'
    proc getTotalCap { object args } {
        ::set result [eval $object getTotalCap $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getTrackPatterns'
    proc getTrackPatterns { object args } {
        ::set result [eval $object getTrackPatterns $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getTrackSpacing'
    proc getTrackSpacing { object args } {
        ::set result [eval $object getTrackSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc getTrackingProtocols { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'getTransform'
    proc getTransform { object args } {
        ::set result [eval $object getTransform $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getType'
    proc getType { object args } {
        ::set result [eval $object getType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getUndoModel'
    proc getUndoModel { object args } {
        ::set result [eval $object getUndoModel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getUpDemand'
    proc getUpDemand { object args } {
        ::set result [eval $object getUpDemand $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getUpSupply'
    proc getUpSupply { object args } {
        ::set result [eval $object getUpSupply $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getUpperBound'
    proc getUpperBound { object args } {
        ::set result [eval $object getUpperBound $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getUpperExtrapolateType'
    proc getUpperExtrapolateType { object args } {
        ::set result [eval $object getUpperExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getUpperLeft'
    proc getUpperLeft { object args } {
        ::set result [eval $object getUpperLeft $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'getUsage'
    proc getUsage { object args } {
        ::set result [eval $object getUsage $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getUsedIn'
    proc getUsedIn { object args } {
        ::set result [eval $object getUsedIn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getUserUnits'
    proc getUserUnits { object args } {
        ::set result [eval $object getUserUnits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getVCStatus'
    proc getVCStatus { object args } {
        ::set result [eval $object getVCStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getVCSystemName'
    proc getVCSystemName { object args } {
        ::set result [eval $object getVCSystemName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getValidDatabases'
    proc getValidDatabases { object args } {
        ::set result [eval $object getValidDatabases $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getValidGroupTypes'
    proc getValidGroupTypes { object args } {
        ::set result [eval $object getValidGroupTypes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getValidTypes'
    proc getValidTypes { object args } {
        ::set result [eval $object getValidTypes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getValue'
    proc getValue { object args } {
        ::set result [eval $object getValue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getValueType'
    proc getValueType { object args } {
        ::set result [eval $object getValueType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getValueTypes'
    proc getValueTypes { object args } {
        ::set result [eval $object getValueTypes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getValues'
    proc getValues { object args } {
        ::set result [eval $object getValues $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getVector'
    proc getVector { object args } {
        ::set result [eval $object getVector $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getVectorBit'
    proc getVectorBit { object args } {
        ::set result [eval $object getVectorBit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getVectorBitName'
    proc getVectorBitName { object args } {
        ::set result [eval $object getVectorBitName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getVectorInstBits'
    proc getVectorInstBits { object args } {
        ::set result [eval $object getVectorInstBits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getVectorInstDefs'
    proc getVectorInstDefs { object args } {
        ::set result [eval $object getVectorInstDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getVectorInsts'
    proc getVectorInsts { object args } {
        ::set result [eval $object getVectorInsts $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getVectorName'
    proc getVectorName { object args } {
        ::set result [eval $object getVectorName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getVersion'
    proc getVersion { object args } {
        ::set result [eval $object getVersion $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getVersions'
    proc getVersions { object args } {
        ::set result [eval $object getVersions $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getVerticalDemand'
    proc getVerticalDemand { object args } {
        ::set result [eval $object getVerticalDemand $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getVerticalSupply'
    proc getVerticalSupply { object args } {
        ::set result [eval $object getVerticalSupply $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getViaDef'
    proc getViaDef { object args } {
        ::set result [eval $object getViaDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getViaDefHeaders'
    proc getViaDefHeaders { object args } {
        ::set result [eval $object getViaDefHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getViaDefName'
    proc getViaDefName { object args } {
        ::set result [eval $object getViaDefName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getViaDefs'
    proc getViaDefs { object args } {
        ::set result [eval $object getViaDefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getViaHeaderUsedIn'
    proc getViaHeaderUsedIn { object args } {
        ::set result [eval $object getViaHeaderUsedIn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getViaHeaders'
    proc getViaHeaders { object args } {
        ::set result [eval $object getViaHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getViaParams'
    proc getViaParams { object args } {
        ::set result [eval $object getViaParams $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getViaSpecs'
    proc getViaSpecs { object args } {
        ::set result [eval $object getViaSpecs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getViaVariant'
    proc getViaVariant { object args } {
        ::set result [eval $object getViaVariant $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getViaVariantHeaders'
    proc getViaVariantHeaders { object args } {
        ::set result [eval $object getViaVariantHeaders $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getViaVariantName'
    proc getViaVariantName { object args } {
        ::set result [eval $object getViaVariantName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getViaVariants'
    proc getViaVariants { object args } {
        ::set result [eval $object getViaVariants $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getVias'
    proc getVias { object args } {
        ::set result [eval $object getVias $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getView'
    proc getView { object args } {
        ::set result [eval $object getView $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getViewName'
    proc getViewName { object args } {
        ::set result [eval $object getViewName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getViewType'
    proc getViewType { object args } {
        ::set result [eval $object getViewType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getViews'
    proc getViews { object args } {
        ::set result [eval $object getViews $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getViewsByName'
    proc getViewsByName { object args } {
        ::set result [eval $object getViewsByName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result createIter]
        }
    }

    # Class member function 'getVoltage'
    proc getVoltage { object args } {
        ::set result [eval $object getVoltage $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getWidth'
    proc getWidth { object args } {
        ::set result [eval $object getWidth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getWorkingVersions'
    proc getWorkingVersions { object args } {
        ::set result [eval $object getWorkingVersions $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getWritePath'
    proc getWritePath { object args } {
        ::set result [eval $object getWritePath $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getXYIndex'
    proc getXYIndex { object args } {
        ::set result [eval $object getXYIndex $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'getYear'
    proc getYear { object args } {
        ::set result [eval $object getYear $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasAppDef'
    proc hasAppDef { object args } {
        ::set result [eval $object hasAppDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasAssociate'
    proc hasAssociate { object args } {
        ::set result [eval $object hasAssociate $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasConnectivity'
    proc hasConnectivity { object args } {
        ::set result [eval $object hasConnectivity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasConstraintGroup'
    proc hasConstraintGroup { object args } {
        ::set result [eval $object hasConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasCoreBoxSpec'
    proc hasCoreBoxSpec { object args } {
        ::set result [eval $object hasCoreBoxSpec $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasCutArea'
    proc hasCutArea { object args } {
        ::set result [eval $object hasCutArea $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasCutPerimeter'
    proc hasCutPerimeter { object args } {
        ::set result [eval $object hasCutPerimeter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasDefaultConstraintGroup'
    proc hasDefaultConstraintGroup { object args } {
        ::set result [eval $object hasDefaultConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasDefaultCutPattern'
    proc hasDefaultCutPattern { object args } {
        ::set result [eval $object hasDefaultCutPattern $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasEffectiveWidth'
    proc hasEffectiveWidth { object args } {
        ::set result [eval $object hasEffectiveWidth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasExcludedLayers'
    proc hasExcludedLayers { object args } {
        ::set result [eval $object hasExcludedLayers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc hasExtraPoints { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'hasGlobalNet'
    proc hasGlobalNet { object args } {
        ::set result [eval $object hasGlobalNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasGuide'
    proc hasGuide { object args } {
        ::set result [eval $object hasGuide $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasHeight'
    proc hasHeight { object args } {
        ::set result [eval $object hasHeight $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasIOBox'
    proc hasIOBox { object args } {
        ::set result [eval $object hasIOBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasId'
    proc hasId { object args } {
        ::set result [eval $object hasId $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasImplant1'
    proc hasImplant1 { object args } {
        ::set result [eval $object hasImplant1 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasImplant2'
    proc hasImplant2 { object args } {
        ::set result [eval $object hasImplant2 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasIndex'
    proc hasIndex { object args } {
        ::set result [eval $object hasIndex $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasLayer'
    proc hasLayer { object args } {
        ::set result [eval $object hasLayer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasLayerNum'
    proc hasLayerNum { object args } {
        ::set result [eval $object hasLayerNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasLength'
    proc hasLength { object args } {
        ::set result [eval $object hasLength $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasLocation'
    proc hasLocation { object args } {
        ::set result [eval $object hasLocation $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasLowerBound'
    proc hasLowerBound { object args } {
        ::set result [eval $object hasLowerBound $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasMultiBitNet'
    proc hasMultiBitNet { object args } {
        ::set result [eval $object hasMultiBitNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasMultiBitTerm'
    proc hasMultiBitTerm { object args } {
        ::set result [eval $object hasMultiBitTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasMultiTermNet'
    proc hasMultiTermNet { object args } {
        ::set result [eval $object hasMultiTermNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasNet'
    proc hasNet { object args } {
        ::set result [eval $object hasNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasNoArea'
    proc hasNoArea { object args } {
        ::set result [eval $object hasNoArea $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasNumCuts'
    proc hasNumCuts { object args } {
        ::set result [eval $object hasNumCuts $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasOverbar'
    proc hasOverbar { object args } {
        ::set result [eval $object hasOverbar $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasParams'
    proc hasParams { object args } {
        ::set result [eval $object hasParams $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasPin'
    proc hasPin { object args } {
        ::set result [eval $object hasPin $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasProcessFamily'
    proc hasProcessFamily { object args } {
        ::set result [eval $object hasProcessFamily $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasProp'
    proc hasProp { object args } {
        ::set result [eval $object hasProp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasRoute'
    proc hasRoute { object args } {
        ::set result [eval $object hasRoute $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasRoutingLayer'
    proc hasRoutingLayer { object args } {
        ::set result [eval $object hasRoutingLayer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasSpacing'
    proc hasSpacing { object args } {
        ::set result [eval $object hasSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasSymmetricConnectivity'
    proc hasSymmetricConnectivity { object args } {
        ::set result [eval $object hasSymmetricConnectivity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasUnboundRefs'
    proc hasUnboundRefs { object args } {
        ::set result [eval $object hasUnboundRefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc hasUnsetAttrs { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'hasUpperBound'
    proc hasUpperBound { object args } {
        ::set result [eval $object hasUpperBound $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasVC'
    proc hasVC { object args } {
        ::set result [eval $object hasVC $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'hasWidth'
    proc hasWidth { object args } {
        ::set result [eval $object hasWidth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'head'
    proc head { object args } {
        ::set result [eval $object head $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Unsupported function
    proc help { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'hide'
    proc hide { object args } {
        ::set result [eval $object hide $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'inGroup'
    proc inGroup { object args } {
        ::set result [eval $object inGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'includes'
    proc includes { object args } {
        ::set result [eval $object includes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'init'
    proc init { object args } {
        ::set result [eval $object init $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc initExport { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'initForRegionQuery'
    proc initForRegionQuery { object args } {
        ::set result [eval $object initForRegionQuery $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc initTracking { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'intersection'
    proc intersection { object args } {
        ::set result [eval $object intersection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'intersects'
    proc intersects { object args } {
        ::set result [eval $object intersects $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'invert'
    proc invert { object args } {
        ::set result [eval $object invert $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isAFSFileSystem'
    proc isAFSFileSystem { object args } {
        ::set result [eval $object isAFSFileSystem $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isAntennaDataSet'
    proc isAntennaDataSet { object args } {
        ::set result [eval $object isAntennaDataSet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isAppObject'
    proc isAppObject { object args } {
        ::set result [eval $object isAppObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isAssignment'
    proc isAssignment { object args } {
        ::set result [eval $object isAssignment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isBitInst'
    proc isBitInst { object args } {
        ::set result [eval $object isBitInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isBitNet'
    proc isBitNet { object args } {
        ::set result [eval $object isBitNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isBitTerm'
    proc isBitTerm { object args } {
        ::set result [eval $object isBitTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isBlockObject'
    proc isBlockObject { object args } {
        ::set result [eval $object isBlockObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isBlockage'
    proc isBlockage { object args } {
        ::set result [eval $object isBlockage $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isBound'
    proc isBound { object args } {
        ::set result [eval $object isBound $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isBoundary'
    proc isBoundary { object args } {
        ::set result [eval $object isBoundary $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isBuiltIn'
    proc isBuiltIn { object args } {
        ::set result [eval $object isBuiltIn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isCWD'
    proc isCWD { object args } {
        ::set result [eval $object isCWD $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isCaseSensitive'
    proc isCaseSensitive { object args } {
        ::set result [eval $object isCaseSensitive $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isClearanceMeasureSet'
    proc isClearanceMeasureSet { object args } {
        ::set result [eval $object isClearanceMeasureSet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isClone'
    proc isClone { object args } {
        ::set result [eval $object isClone $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isClosed'
    proc isClosed { object args } {
        ::set result [eval $object isClosed $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isConcrete'
    proc isConcrete { object args } {
        ::set result [eval $object isConcrete $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isConnFig'
    proc isConnFig { object args } {
        ::set result [eval $object isConnFig $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isConnectDef'
    proc isConnectDef { object args } {
        ::set result [eval $object isConnectDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isConstraint'
    proc isConstraint { object args } {
        ::set result [eval $object isConstraint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isConstraintGroup'
    proc isConstraintGroup { object args } {
        ::set result [eval $object isConstraintGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isConstraintGroupHeader'
    proc isConstraintGroupHeader { object args } {
        ::set result [eval $object isConstraintGroupHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isConstraintGroupMem'
    proc isConstraintGroupMem { object args } {
        ::set result [eval $object isConstraintGroupMem $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isConstraintParam'
    proc isConstraintParam { object args } {
        ::set result [eval $object isConstraintParam $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isContainer'
    proc isContainer { object args } {
        ::set result [eval $object isContainer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isContiguous'
    proc isContiguous { object args } {
        ::set result [eval $object isContiguous $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isDBUPerUUSet'
    proc isDBUPerUUSet { object args } {
        ::set result [eval $object isDBUPerUUSet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isDM'
    proc isDM { object args } {
        ::set result [eval $object isDM $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isDMData'
    proc isDMData { object args } {
        ::set result [eval $object isDMData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isDefaultManufacturingGridSet'
    proc isDefaultManufacturingGridSet { object args } {
        ::set result [eval $object isDefaultManufacturingGridSet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isDerived'
    proc isDerived { object args } {
        ::set result [eval $object isDerived $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isDesign'
    proc isDesign { object args } {
        ::set result [eval $object isDesign $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isDevice'
    proc isDevice { object args } {
        ::set result [eval $object isDevice $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isDir'
    proc isDir { object args } {
        ::set result [eval $object isDir $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isDrafting'
    proc isDrafting { object args } {
        ::set result [eval $object isDrafting $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isEmbedded'
    proc isEmbedded { object args } {
        ::set result [eval $object isEmbedded $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isEmpty'
    proc isEmpty { object args } {
        ::set result [eval $object isEmpty $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isEqual'
    proc isEqual { object args } {
        ::set result [eval $object isEqual $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isFig'
    proc isFig { object args } {
        ::set result [eval $object isFig $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isFile'
    proc isFile { object args } {
        ::set result [eval $object isFile $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isGateGrounded'
    proc isGateGrounded { object args } {
        ::set result [eval $object isGateGrounded $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isGateGroundedSet'
    proc isGateGroundedSet { object args } {
        ::set result [eval $object isGateGroundedSet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isGlobal'
    proc isGlobal { object args } {
        ::set result [eval $object isGlobal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isGroup'
    proc isGroup { object args } {
        ::set result [eval $object isGroup $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isGroupMember'
    proc isGroupMember { object args } {
        ::set result [eval $object isGroupMember $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isHard'
    proc isHard { object args } {
        ::set result [eval $object isHard $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isHier'
    proc isHier { object args } {
        ::set result [eval $object isHier $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isHorizontal'
    proc isHorizontal { object args } {
        ::set result [eval $object isHorizontal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isIdentity'
    proc isIdentity { object args } {
        ::set result [eval $object isIdentity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isImplicit'
    proc isImplicit { object args } {
        ::set result [eval $object isImplicit $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isInCWD'
    proc isInCWD { object args } {
        ::set result [eval $object isInCWD $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc isInRange { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'isInUndo'
    proc isInUndo { object args } {
        ::set result [eval $object isInUndo $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isInst'
    proc isInst { object args } {
        ::set result [eval $object isInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isInterface'
    proc isInterface { object args } {
        ::set result [eval $object isInterface $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isInverted'
    proc isInverted { object args } {
        ::set result [eval $object isInverted $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isLayer'
    proc isLayer { object args } {
        ::set result [eval $object isLayer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isLeader'
    proc isLeader { object args } {
        ::set result [eval $object isLeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isLoaded'
    proc isLoaded { object args } {
        ::set result [eval $object isLoaded $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isLocal'
    proc isLocal { object args } {
        ::set result [eval $object isLocal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isMarkedToBeClosed'
    proc isMarkedToBeClosed { object args } {
        ::set result [eval $object isMarkedToBeClosed $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc isMatch { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'isModAssignment'
    proc isModAssignment { object args } {
        ::set result [eval $object isModAssignment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isModBitInst'
    proc isModBitInst { object args } {
        ::set result [eval $object isModBitInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isModBitNet'
    proc isModBitNet { object args } {
        ::set result [eval $object isModBitNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isModBitTerm'
    proc isModBitTerm { object args } {
        ::set result [eval $object isModBitTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isModConnectDef'
    proc isModConnectDef { object args } {
        ::set result [eval $object isModConnectDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isModDesignInst'
    proc isModDesignInst { object args } {
        ::set result [eval $object isModDesignInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isModInst'
    proc isModInst { object args } {
        ::set result [eval $object isModInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isModModuleInst'
    proc isModModuleInst { object args } {
        ::set result [eval $object isModModuleInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isModNet'
    proc isModNet { object args } {
        ::set result [eval $object isModNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isModObject'
    proc isModObject { object args } {
        ::set result [eval $object isModObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isModTerm'
    proc isModTerm { object args } {
        ::set result [eval $object isModTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isModified'
    proc isModified { object args } {
        ::set result [eval $object isModified $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isNFSFileSystem'
    proc isNFSFileSystem { object args } {
        ::set result [eval $object isNFSFileSystem $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isNet'
    proc isNet { object args } {
        ::set result [eval $object isNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isNode'
    proc isNode { object args } {
        ::set result [eval $object isNode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isNonEmpty'
    proc isNonEmpty { object args } {
        ::set result [eval $object isNonEmpty $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccAssignment'
    proc isOccAssignment { object args } {
        ::set result [eval $object isOccAssignment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccBitInst'
    proc isOccBitInst { object args } {
        ::set result [eval $object isOccBitInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccBitNet'
    proc isOccBitNet { object args } {
        ::set result [eval $object isOccBitNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccBitTerm'
    proc isOccBitTerm { object args } {
        ::set result [eval $object isOccBitTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccConnectDef'
    proc isOccConnectDef { object args } {
        ::set result [eval $object isOccConnectDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccDesignInst'
    proc isOccDesignInst { object args } {
        ::set result [eval $object isOccDesignInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccInst'
    proc isOccInst { object args } {
        ::set result [eval $object isOccInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccModuleInst'
    proc isOccModuleInst { object args } {
        ::set result [eval $object isOccModuleInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccNet'
    proc isOccNet { object args } {
        ::set result [eval $object isOccNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccObject'
    proc isOccObject { object args } {
        ::set result [eval $object isOccObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccShape'
    proc isOccShape { object args } {
        ::set result [eval $object isOccShape $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccTerm'
    proc isOccTerm { object args } {
        ::set result [eval $object isOccTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOccTextDisplay'
    proc isOccTextDisplay { object args } {
        ::set result [eval $object isOccTextDisplay $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOpen'
    proc isOpen { object args } {
        ::set result [eval $object isOpen $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOrdered'
    proc isOrdered { object args } {
        ::set result [eval $object isOrdered $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOrigModified'
    proc isOrigModified { object args } {
        ::set result [eval $object isOrigModified $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOriginal'
    proc isOriginal { object args } {
        ::set result [eval $object isOriginal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOrthogonal'
    proc isOrthogonal { object args } {
        ::set result [eval $object isOrthogonal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOtherConn'
    proc isOtherConn { object args } {
        ::set result [eval $object isOtherConn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isOwned'
    proc isOwned { object args } {
        ::set result [eval $object isOwned $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isParent'
    proc isParent { object args } {
        ::set result [eval $object isParent $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isPinFig'
    proc isPinFig { object args } {
        ::set result [eval $object isPinFig $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isPreferredEquivalent'
    proc isPreferredEquivalent { object args } {
        ::set result [eval $object isPreferredEquivalent $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isPrimary'
    proc isPrimary { object args } {
        ::set result [eval $object isPrimary $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isProp'
    proc isProp { object args } {
        ::set result [eval $object isProp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isPushedDown'
    proc isPushedDown { object args } {
        ::set result [eval $object isPushedDown $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isReadable'
    proc isReadable { object args } {
        ::set result [eval $object isReadable $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isRecovered'
    proc isRecovered { object args } {
        ::set result [eval $object isRecovered $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc isRectangle { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'isRecursive'
    proc isRecursive { object args } {
        ::set result [eval $object isRecursive $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isReducedModel'
    proc isReducedModel { object args } {
        ::set result [eval $object isReducedModel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isRef'
    proc isRef { object args } {
        ::set result [eval $object isRef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isReserved'
    proc isReserved { object args } {
        ::set result [eval $object isReserved $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isRowHorizontal'
    proc isRowHorizontal { object args } {
        ::set result [eval $object isRowHorizontal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSameDiskFile'
    proc isSameDiskFile { object args } {
        ::set result [eval $object isSameDiskFile $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isScalarized'
    proc isScalarized { object args } {
        ::set result [eval $object isScalarized $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc isSelfIntersecting { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'isSession'
    proc isSession { object args } {
        ::set result [eval $object isSession $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isShape'
    proc isShape { object args } {
        ::set result [eval $object isShape $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSide'
    proc isSide { object args } {
        ::set result [eval $object isSide $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSiteDef'
    proc isSiteDef { object args } {
        ::set result [eval $object isSiteDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSoft'
    proc isSoft { object args } {
        ::set result [eval $object isSoft $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isStdDevice'
    proc isStdDevice { object args } {
        ::set result [eval $object isStdDevice $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isStdViaMaster'
    proc isStdViaMaster { object args } {
        ::set result [eval $object isStdViaMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSubHeader'
    proc isSubHeader { object args } {
        ::set result [eval $object isSubHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSubMaster'
    proc isSubMaster { object args } {
        ::set result [eval $object isSubMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSuperHeader'
    proc isSuperHeader { object args } {
        ::set result [eval $object isSuperHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSuperMaster'
    proc isSuperMaster { object args } {
        ::set result [eval $object isSuperMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSupplyDemandValid'
    proc isSupplyDemandValid { object args } {
        ::set result [eval $object isSupplyDemandValid $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSymbolicLink'
    proc isSymbolicLink { object args } {
        ::set result [eval $object isSymbolicLink $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSymmetric'
    proc isSymmetric { object args } {
        ::set result [eval $object isSymmetric $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSymmetricInRot'
    proc isSymmetricInRot { object args } {
        ::set result [eval $object isSymmetricInRot $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSymmetricInX'
    proc isSymmetricInX { object args } {
        ::set result [eval $object isSymmetricInX $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isSymmetricInY'
    proc isSymmetricInY { object args } {
        ::set result [eval $object isSymmetricInY $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isTech'
    proc isTech { object args } {
        ::set result [eval $object isTech $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isTerm'
    proc isTerm { object args } {
        ::set result [eval $object isTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isTextDisplay'
    proc isTextDisplay { object args } {
        ::set result [eval $object isTextDisplay $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isUnShielded'
    proc isUnShielded { object args } {
        ::set result [eval $object isUnShielded $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isUndoEnabled'
    proc isUndoEnabled { object args } {
        ::set result [eval $object isUndoEnabled $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isUniqueInDB'
    proc isUniqueInDB { object args } {
        ::set result [eval $object isUniqueInDB $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isUniqueName'
    proc isUniqueName { object args } {
        ::set result [eval $object isUniqueName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc isUnset { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'isUsedIn'
    proc isUsedIn { object args } {
        ::set result [eval $object isUsedIn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isUserUnitsSet'
    proc isUserUnitsSet { object args } {
        ::set result [eval $object isUserUnitsSet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isValid'
    proc isValid { object args } {
        ::set result [eval $object isValid $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isValue'
    proc isValue { object args } {
        ::set result [eval $object isValue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isVariant'
    proc isVariant { object args } {
        ::set result [eval $object isVariant $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isVertical'
    proc isVertical { object args } {
        ::set result [eval $object isVertical $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isVia'
    proc isVia { object args } {
        ::set result [eval $object isVia $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isViaDef'
    proc isViaDef { object args } {
        ::set result [eval $object isViaDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isViaHeader'
    proc isViaHeader { object args } {
        ::set result [eval $object isViaHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isViaVariant'
    proc isViaVariant { object args } {
        ::set result [eval $object isViaVariant $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isViaVariantHeader'
    proc isViaVariantHeader { object args } {
        ::set result [eval $object isViaVariantHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isVisible'
    proc isVisible { object args } {
        ::set result [eval $object isVisible $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isVisibleToModule'
    proc isVisibleToModule { object args } {
        ::set result [eval $object isVisibleToModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isWafer'
    proc isWafer { object args } {
        ::set result [eval $object isWafer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'isWritable'
    proc isWritable { object args } {
        ::set result [eval $object isWritable $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'layerNum'
    proc layerNum { object args } {
        ::set result [eval $object layerNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'left'
    proc left { object args } {
        ::set result [eval $object left $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'leftOf'
    proc leftOf { object args } {
        ::set result [eval $object leftOf $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'load'
    proc load { object args } {
        ::set result [eval $object load $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'lowerLeft'
    proc lowerLeft { object args } {
        ::set result [eval $object lowerLeft $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'makeEditable'
    proc makeEditable { object args } {
        ::set result [eval $object makeEditable $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'makeEquivalent'
    proc makeEquivalent { object args } {
        ::set result [eval $object makeEquivalent $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'makeInvertedZero'
    proc makeInvertedZero { object args } {
        ::set result [eval $object makeInvertedZero $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'makeZero'
    proc makeZero { object args } {
        ::set result [eval $object makeZero $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc match { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'maxCAR'
    proc maxCAR { object args } {
        ::set result [eval $object maxCAR $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'maxCutCAR'
    proc maxCutCAR { object args } {
        ::set result [eval $object maxCutCAR $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'maxSideCAR'
    proc maxSideCAR { object args } {
        ::set result [eval $object maxSideCAR $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'merge'
    proc merge { object args } {
        ::set result [eval $object merge $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'minimizeVM'
    proc minimizeVM { object args } {
        ::set result [eval $object minimizeVM $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'move'
    proc move { object args } {
        ::set result [eval $object move $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'moveAfter'
    proc moveAfter { object args } {
        ::set result [eval $object moveAfter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'moveTo'
    proc moveTo { object args } {
        ::set result [eval $object moveTo $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'moveToFirst'
    proc moveToFirst { object args } {
        ::set result [eval $object moveToFirst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'moveToLast'
    proc moveToLast { object args } {
        ::set result [eval $object moveToLast $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'moveToNet'
    proc moveToNet { object args } {
        ::set result [eval $object moveToNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'offset'
    proc offset { object args } {
        ::set result [eval $object offset $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Unsupported function
    proc onEdge { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'open'
    proc open { object args } {
        ::set result [eval $object open $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'openHier'
    proc openHier { object args } {
        ::set result [eval $object openHier $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'optimize'
    proc optimize { object args } {
        ::set result [eval $object optimize $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'orient'
    proc orient { object args } {
        ::set result [eval $object orient $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'overlaps'
    proc overlaps { object args } {
        ::set result [eval $object overlaps $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'override'
    proc override { object args } {
        ::set result [eval $object override $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'partialCut'
    proc partialCut { object args } {
        ::set result [eval $object partialCut $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'partialMetal'
    proc partialMetal { object args } {
        ::set result [eval $object partialMetal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'partialMetalSide'
    proc partialMetalSide { object args } {
        ::set result [eval $object partialMetalSide $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'popLevel'
    proc popLevel { object args } {
        ::set result [eval $object popLevel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Special command 'popNameSpace'
    proc popNameSpace { args } {
        ::set result [eval oaNameSpace_pop $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'preAllocate'
    proc preAllocate { object args } {
        ::set result [eval $object preAllocate $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processAssignment'
    proc processAssignment { object args } {
        ::set result [eval $object processAssignment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processBusNetDef'
    proc processBusNetDef { object args } {
        ::set result [eval $object processBusNetDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processBusTermDef'
    proc processBusTermDef { object args } {
        ::set result [eval $object processBusTermDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processConnectDef'
    proc processConnectDef { object args } {
        ::set result [eval $object processConnectDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processInst'
    proc processInst { object args } {
        ::set result [eval $object processInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processInstHeader'
    proc processInstHeader { object args } {
        ::set result [eval $object processInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processInstTerm'
    proc processInstTerm { object args } {
        ::set result [eval $object processInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processModuleInstHeader'
    proc processModuleInstHeader { object args } {
        ::set result [eval $object processModuleInstHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processNet'
    proc processNet { object args } {
        ::set result [eval $object processNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processOccurrence'
    proc processOccurrence { object args } {
        ::set result [eval $object processOccurrence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processTerm'
    proc processTerm { object args } {
        ::set result [eval $object processTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'processVectorInstDef'
    proc processVectorInstDef { object args } {
        ::set result [eval $object processVectorInstDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'purge'
    proc purge { object args } {
        ::set result [eval $object purge $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'pushLevel'
    proc pushLevel { object args } {
        ::set result [eval $object pushLevel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Special command 'pushNameSpace'
    proc pushNameSpace { args } {
        ::set result [eval oaNameSpace_push $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'redo'
    proc redo { object args } {
        ::set result [eval $object redo $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'release'
    proc release { object args } {
        ::set result [eval $object release $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'releaseAccess'
    proc releaseAccess { object args } {
        ::set result [eval $object releaseAccess $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'releaseLock'
    proc releaseLock { object args } {
        ::set result [eval $object releaseLock $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'remove'
    proc remove { object args } {
        ::set result [eval $object remove $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'removeConn'
    proc removeConn { object args } {
        ::set result [eval $object removeConn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'removeCoreBoxSpec'
    proc removeCoreBoxSpec { object args } {
        ::set result [eval $object removeCoreBoxSpec $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'removeData'
    proc removeData { object args } {
        ::set result [eval $object removeData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'removeFromCluster'
    proc removeFromCluster { object args } {
        ::set result [eval $object removeFromCluster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'removeFromNet'
    proc removeFromNet { object args } {
        ::set result [eval $object removeFromNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'removeFromPin'
    proc removeFromPin { object args } {
        ::set result [eval $object removeFromPin $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'removeFromSubNetwork'
    proc removeFromSubNetwork { object args } {
        ::set result [eval $object removeFromSubNetwork $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'removeIOBox'
    proc removeIOBox { object args } {
        ::set result [eval $object removeIOBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'removeLibAttribute'
    proc removeLibAttribute { object args } {
        ::set result [eval $object removeLibAttribute $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'removeObject'
    proc removeObject { object args } {
        ::set result [eval $object removeObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'removeRoutingLayer'
    proc removeRoutingLayer { object args } {
        ::set result [eval $object removeRoutingLayer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'rename'
    proc rename { object args } {
        ::set result [eval $object rename $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'reopen'
    proc reopen { object args } {
        ::set result [eval $object reopen $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'reset'
    proc reset { object args } {
        ::set result [eval $object reset $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'resetManufacturingGrid'
    proc resetManufacturingGrid { object args } {
        ::set result [eval $object resetManufacturingGrid $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'resize'
    proc resize { object args } {
        ::set result [eval $object resize $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'revert'
    proc revert { object args } {
        ::set result [eval $object revert $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'right'
    proc right { object args } {
        ::set result [eval $object right $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'rightOf'
    proc rightOf { object args } {
        ::set result [eval $object rightOf $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'rotate180'
    proc rotate180 { object args } {
        ::set result [eval $object rotate180 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'rotate270'
    proc rotate270 { object args } {
        ::set result [eval $object rotate270 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'rotate90'
    proc rotate90 { object args } {
        ::set result [eval $object rotate90 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'save'
    proc save { object args } {
        ::set result [eval $object save $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'saveAs'
    proc saveAs { object args } {
        ::set result [eval $object saveAs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'scalarize'
    proc scalarize { object args } {
        ::set result [eval $object scalarize $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'scale'
    proc scale { object args } {
        ::set result [eval $object scale $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'set'
    proc set { object args } {
        ::set result [eval $object set $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setAccessDir'
    proc setAccessDir { object args } {
        ::set result [eval $object setAccessDir $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setAlignment'
    proc setAlignment { object args } {
        ::set result [eval $object setAlignment $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setAllowPGNet'
    proc setAllowPGNet { object args } {
        ::set result [eval $object setAllowPGNet $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setAntennaData'
    proc setAntennaData { object args } {
        ::set result [eval $object setAntennaData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setAppType'
    proc setAppType { object args } {
        ::set result [eval $object setAppType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setAppVal'
    proc setAppVal { object args } {
        ::set result [eval $object setAppVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setAssignmentDef'
    proc setAssignmentDef { object args } {
        ::set result [eval $object setAssignmentDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setAssignmentName'
    proc setAssignmentName { object args } {
        ::set result [eval $object setAssignmentName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setAttributes'
    proc setAttributes { object args } {
        ::set result [eval $object setAttributes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setAvgACCurrentDensity'
    proc setAvgACCurrentDensity { object args } {
        ::set result [eval $object setAvgACCurrentDensity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setAvgDCCurrentDensity'
    proc setAvgDCCurrentDensity { object args } {
        ::set result [eval $object setAvgDCCurrentDensity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setBBox'
    proc setBBox { object args } {
        ::set result [eval $object setBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setBaseName'
    proc setBaseName { object args } {
        ::set result [eval $object setBaseName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setBeginConn'
    proc setBeginConn { object args } {
        ::set result [eval $object setBeginConn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setBeginExt'
    proc setBeginExt { object args } {
        ::set result [eval $object setBeginExt $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setBeginLayerNum'
    proc setBeginLayerNum { object args } {
        ::set result [eval $object setBeginLayerNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setBeginStyle'
    proc setBeginStyle { object args } {
        ::set result [eval $object setBeginStyle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setBitOrder'
    proc setBitOrder { object args } {
        ::set result [eval $object setBitOrder $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setBooleanVal'
    proc setBooleanVal { object args } {
        ::set result [eval $object setBooleanVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc setBounds { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'setCellName'
    proc setCellName { object args } {
        ::set result [eval $object setCellName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCellType'
    proc setCellType { object args } {
        ::set result [eval $object setCellType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCenter'
    proc setCenter { object args } {
        ::set result [eval $object setCenter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setClearanceMeasure'
    proc setClearanceMeasure { object args } {
        ::set result [eval $object setClearanceMeasure $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCloseBusChar'
    proc setCloseBusChar { object args } {
        ::set result [eval $object setCloseBusChar $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setClusterType'
    proc setClusterType { object args } {
        ::set result [eval $object setClusterType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setColHeader'
    proc setColHeader { object args } {
        ::set result [eval $object setColHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setColInterpolateType'
    proc setColInterpolateType { object args } {
        ::set result [eval $object setColInterpolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setColLowerExtrapolateType'
    proc setColLowerExtrapolateType { object args } {
        ::set result [eval $object setColLowerExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setColName'
    proc setColName { object args } {
        ::set result [eval $object setColName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setColUpperExtrapolateType'
    proc setColUpperExtrapolateType { object args } {
        ::set result [eval $object setColUpperExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setConnStatus'
    proc setConnStatus { object args } {
        ::set result [eval $object setConnStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setControlled'
    proc setControlled { object args } {
        ::set result [eval $object setControlled $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCoreBoxSpec'
    proc setCoreBoxSpec { object args } {
        ::set result [eval $object setCoreBoxSpec $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCp'
    proc setCp { object args } {
        ::set result [eval $object setCp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCutArea'
    proc setCutArea { object args } {
        ::set result [eval $object setCutArea $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCutColumns'
    proc setCutColumns { object args } {
        ::set result [eval $object setCutColumns $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCutHeight'
    proc setCutHeight { object args } {
        ::set result [eval $object setCutHeight $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCutLayer'
    proc setCutLayer { object args } {
        ::set result [eval $object setCutLayer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCutPatternVal'
    proc setCutPatternVal { object args } {
        ::set result [eval $object setCutPatternVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCutPerimeter'
    proc setCutPerimeter { object args } {
        ::set result [eval $object setCutPerimeter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCutRows'
    proc setCutRows { object args } {
        ::set result [eval $object setCutRows $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCutSpacing'
    proc setCutSpacing { object args } {
        ::set result [eval $object setCutSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setCutWidth'
    proc setCutWidth { object args } {
        ::set result [eval $object setCutWidth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDBUPerUU'
    proc setDBUPerUU { object args } {
        ::set result [eval $object setDBUPerUU $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDX'
    proc setDX { object args } {
        ::set result [eval $object setDX $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDY'
    proc setDY { object args } {
        ::set result [eval $object setDY $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDataValue'
    proc setDataValue { object args } {
        ::set result [eval $object setDataValue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDefaultCutPattern'
    proc setDefaultCutPattern { object args } {
        ::set result [eval $object setDefaultCutPattern $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDefaultManufacturingGrid'
    proc setDefaultManufacturingGrid { object args } {
        ::set result [eval $object setDefaultManufacturingGrid $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDefaultName'
    proc setDefaultName { object args } {
        ::set result [eval $object setDefaultName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDefaultValue'
    proc setDefaultValue { object args } {
        ::set result [eval $object setDefaultValue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDelay'
    proc setDelay { object args } {
        ::set result [eval $object setDelay $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDeleteWhen'
    proc setDeleteWhen { object args } {
        ::set result [eval $object setDeleteWhen $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDensity'
    proc setDensity { object args } {
        ::set result [eval $object setDensity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDescription'
    proc setDescription { object args } {
        ::set result [eval $object setDescription $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDiff'
    proc setDiff { object args } {
        ::set result [eval $object setDiff $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDiode'
    proc setDiode { object args } {
        ::set result [eval $object setDiode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDirection'
    proc setDirection { object args } {
        ::set result [eval $object setDirection $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDisplayName'
    proc setDisplayName { object args } {
        ::set result [eval $object setDisplayName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDoubleVal'
    proc setDoubleVal { object args } {
        ::set result [eval $object setDoubleVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setDrafting'
    proc setDrafting { object args } {
        ::set result [eval $object setDrafting $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setEEQMaster'
    proc setEEQMaster { object args } {
        ::set result [eval $object setEEQMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setEdges'
    proc setEdges { object args } {
        ::set result [eval $object setEdges $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setEffectiveWidth'
    proc setEffectiveWidth { object args } {
        ::set result [eval $object setEffectiveWidth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setEndConn'
    proc setEndConn { object args } {
        ::set result [eval $object setEndConn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setEndExt'
    proc setEndExt { object args } {
        ::set result [eval $object setEndExt $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setEndLayerNum'
    proc setEndLayerNum { object args } {
        ::set result [eval $object setEndLayerNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setEndStyle'
    proc setEndStyle { object args } {
        ::set result [eval $object setEndStyle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setExcludedLayers'
    proc setExcludedLayers { object args } {
        ::set result [eval $object setExcludedLayers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc setExportProtocol { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'setFarCap'
    proc setFarCap { object args } {
        ::set result [eval $object setFarCap $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setFig'
    proc setFig { object args } {
        ::set result [eval $object setFig $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setFigGroupStatus'
    proc setFigGroupStatus { object args } {
        ::set result [eval $object setFigGroupStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setFileName'
    proc setFileName { object args } {
        ::set result [eval $object setFileName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setFirst'
    proc setFirst { object args } {
        ::set result [eval $object setFirst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc setFixedAttrs { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'setFloatVal'
    proc setFloatVal { object args } {
        ::set result [eval $object setFloatVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setFollowLayers'
    proc setFollowLayers { object args } {
        ::set result [eval $object setFollowLayers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setFont'
    proc setFont { object args } {
        ::set result [eval $object setFont $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setFormat'
    proc setFormat { object args } {
        ::set result [eval $object setFormat $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setGCell'
    proc setGCell { object args } {
        ::set result [eval $object setGCell $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setGate'
    proc setGate { object args } {
        ::set result [eval $object setGate $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setGateGrounded'
    proc setGateGrounded { object args } {
        ::set result [eval $object setGateGrounded $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setGlobal'
    proc setGlobal { object args } {
        ::set result [eval $object setGlobal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setGroundSensitivity'
    proc setGroundSensitivity { object args } {
        ::set result [eval $object setGroundSensitivity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setGroupType'
    proc setGroupType { object args } {
        ::set result [eval $object setGroupType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setHard'
    proc setHard { object args } {
        ::set result [eval $object setHard $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setHeader'
    proc setHeader { object args } {
        ::set result [eval $object setHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setHeight'
    proc setHeight { object args } {
        ::set result [eval $object setHeight $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setHierDelimiter'
    proc setHierDelimiter { object args } {
        ::set result [eval $object setHierDelimiter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setHoleRadius'
    proc setHoleRadius { object args } {
        ::set result [eval $object setHoleRadius $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setHorizontal'
    proc setHorizontal { object args } {
        ::set result [eval $object setHorizontal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setHorizontalDemand'
    proc setHorizontalDemand { object args } {
        ::set result [eval $object setHorizontalDemand $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setHorizontalSupply'
    proc setHorizontalSupply { object args } {
        ::set result [eval $object setHorizontalSupply $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setID'
    proc setID { object args } {
        ::set result [eval $object setID $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setIOBox'
    proc setIOBox { object args } {
        ::set result [eval $object setIOBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setImplant1Enc'
    proc setImplant1Enc { object args } {
        ::set result [eval $object setImplant1Enc $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setImplant2Enc'
    proc setImplant2Enc { object args } {
        ::set result [eval $object setImplant2Enc $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setInInstTerm'
    proc setInInstTerm { object args } {
        ::set result [eval $object setInInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setIndex'
    proc setIndex { object args } {
        ::set result [eval $object setIndex $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setInst'
    proc setInst { object args } {
        ::set result [eval $object setInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setInstTerms'
    proc setInstTerms { object args } {
        ::set result [eval $object setInstTerms $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setIntVal'
    proc setIntVal { object args } {
        ::set result [eval $object setIntVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setInterpolateType'
    proc setInterpolateType { object args } {
        ::set result [eval $object setInterpolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setIsClosed'
    proc setIsClosed { object args } {
        ::set result [eval $object setIsClosed $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setIsInterface'
    proc setIsInterface { object args } {
        ::set result [eval $object setIsInterface $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setIsVisible'
    proc setIsVisible { object args } {
        ::set result [eval $object setIsVisible $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLPP'
    proc setLPP { object args } {
        ::set result [eval $object setLPP $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLayer1Enc'
    proc setLayer1Enc { object args } {
        ::set result [eval $object setLayer1Enc $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLayer1Offset'
    proc setLayer1Offset { object args } {
        ::set result [eval $object setLayer1Offset $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLayer2Enc'
    proc setLayer2Enc { object args } {
        ::set result [eval $object setLayer2Enc $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLayer2Offset'
    proc setLayer2Offset { object args } {
        ::set result [eval $object setLayer2Offset $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLayerNum'
    proc setLayerNum { object args } {
        ::set result [eval $object setLayerNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLeader'
    proc setLeader { object args } {
        ::set result [eval $object setLeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLength'
    proc setLength { object args } {
        ::set result [eval $object setLength $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLevel'
    proc setLevel { object args } {
        ::set result [eval $object setLevel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc setLibDefListErrorLogging { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'setLibMode'
    proc setLibMode { object args } {
        ::set result [eval $object setLibMode $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLibWritePath'
    proc setLibWritePath { object args } {
        ::set result [eval $object setLibWritePath $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLink'
    proc setLink { object args } {
        ::set result [eval $object setLink $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setLocation'
    proc setLocation { object args } {
        ::set result [eval $object setLocation $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc setLowerBound { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'setLowerExtrapolateType'
    proc setLowerExtrapolateType { object args } {
        ::set result [eval $object setLowerExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setManufacturingGrid'
    proc setManufacturingGrid { object args } {
        ::set result [eval $object setManufacturingGrid $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setMaskNumber'
    proc setMaskNumber { object args } {
        ::set result [eval $object setMaskNumber $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setMaskRange'
    proc setMaskRange { object args } {
        ::set result [eval $object setMaskRange $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setMaster'
    proc setMaster { object args } {
        ::set result [eval $object setMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setMasterBBox'
    proc setMasterBBox { object args } {
        ::set result [eval $object setMasterBBox $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setMaterial'
    proc setMaterial { object args } {
        ::set result [eval $object setMaterial $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setMaxBits'
    proc setMaxBits { object args } {
        ::set result [eval $object setMaxBits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setMaxCAR'
    proc setMaxCAR { object args } {
        ::set result [eval $object setMaxCAR $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setMaxCutCAR'
    proc setMaxCutCAR { object args } {
        ::set result [eval $object setMaxCutCAR $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setMaxSideCAR'
    proc setMaxSideCAR { object args } {
        ::set result [eval $object setMaxSideCAR $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc setModAttrs { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'setModel'
    proc setModel { object args } {
        ::set result [eval $object setModel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setMsg'
    proc setMsg { object args } {
        ::set result [eval $object setMsg $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setMustJoin'
    proc setMustJoin { object args } {
        ::set result [eval $object setMustJoin $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setName'
    proc setName { object args } {
        ::set result [eval $object setName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setNearCap'
    proc setNearCap { object args } {
        ::set result [eval $object setNearCap $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setNumCols'
    proc setNumCols { object args } {
        ::set result [eval $object setNumCols $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setNumCuts'
    proc setNumCuts { object args } {
        ::set result [eval $object setNumCuts $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc setNumElements { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'setNumItems'
    proc setNumItems { object args } {
        ::set result [eval $object setNumItems $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setNumRows'
    proc setNumRows { object args } {
        ::set result [eval $object setNumRows $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setNumSites'
    proc setNumSites { object args } {
        ::set result [eval $object setNumSites $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setNumTracks'
    proc setNumTracks { object args } {
        ::set result [eval $object setNumTracks $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setNumber'
    proc setNumber { object args } {
        ::set result [eval $object setNumber $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setObjects'
    proc setObjects { object args } {
        ::set result [eval $object setObjects $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setOffsets'
    proc setOffsets { object args } {
        ::set result [eval $object setOffsets $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setOpenBusChar'
    proc setOpenBusChar { object args } {
        ::set result [eval $object setOpenBusChar $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setOrdered'
    proc setOrdered { object args } {
        ::set result [eval $object setOrdered $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setOrient'
    proc setOrient { object args } {
        ::set result [eval $object setOrient $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setOrigin'
    proc setOrigin { object args } {
        ::set result [eval $object setOrigin $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setOriginOffset'
    proc setOriginOffset { object args } {
        ::set result [eval $object setOriginOffset $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setOriginal'
    proc setOriginal { object args } {
        ::set result [eval $object setOriginal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setOutInstTerm'
    proc setOutInstTerm { object args } {
        ::set result [eval $object setOutInstTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setOverbar'
    proc setOverbar { object args } {
        ::set result [eval $object setOverbar $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setOwner'
    proc setOwner { object args } {
        ::set result [eval $object setOwner $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setParams'
    proc setParams { object args } {
        ::set result [eval $object setParams $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setParasiticConfidence'
    proc setParasiticConfidence { object args } {
        ::set result [eval $object setParasiticConfidence $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPartialCut'
    proc setPartialCut { object args } {
        ::set result [eval $object setPartialCut $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPartialMetal'
    proc setPartialMetal { object args } {
        ::set result [eval $object setPartialMetal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPartialMetalSide'
    proc setPartialMetalSide { object args } {
        ::set result [eval $object setPartialMetalSide $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPartitionName'
    proc setPartitionName { object args } {
        ::set result [eval $object setPartitionName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc setPcellErrorLogging { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'setPeakACCurrentDensity'
    proc setPeakACCurrentDensity { object args } {
        ::set result [eval $object setPeakACCurrentDensity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPiModel'
    proc setPiModel { object args } {
        ::set result [eval $object setPiModel $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPinConnectMethod'
    proc setPinConnectMethod { object args } {
        ::set result [eval $object setPinConnectMethod $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPinDelimiter'
    proc setPinDelimiter { object args } {
        ::set result [eval $object setPinDelimiter $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPinType'
    proc setPinType { object args } {
        ::set result [eval $object setPinType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPlacementStatus'
    proc setPlacementStatus { object args } {
        ::set result [eval $object setPlacementStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPoints'
    proc setPoints { object args } {
        ::set result [eval $object setPoints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPoles'
    proc setPoles { object args } {
        ::set result [eval $object setPoles $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPosition'
    proc setPosition { object args } {
        ::set result [eval $object setPosition $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPrefRoutingDir'
    proc setPrefRoutingDir { object args } {
        ::set result [eval $object setPrefRoutingDir $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPreferredEquivalent'
    proc setPreferredEquivalent { object args } {
        ::set result [eval $object setPreferredEquivalent $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPriority'
    proc setPriority { object args } {
        ::set result [eval $object setPriority $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setProcess'
    proc setProcess { object args } {
        ::set result [eval $object setProcess $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setProcessFamily'
    proc setProcessFamily { object args } {
        ::set result [eval $object setProcessFamily $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPurposeNum'
    proc setPurposeNum { object args } {
        ::set result [eval $object setPurposeNum $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setPushedDown'
    proc setPushedDown { object args } {
        ::set result [eval $object setPushedDown $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRadii'
    proc setRadii { object args } {
        ::set result [eval $object setRadii $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRadius'
    proc setRadius { object args } {
        ::set result [eval $object setRadius $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRange'
    proc setRange { object args } {
        ::set result [eval $object setRange $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRefs'
    proc setRefs { object args } {
        ::set result [eval $object setRefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRes'
    proc setRes { object args } {
        ::set result [eval $object setRes $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setResidues'
    proc setResidues { object args } {
        ::set result [eval $object setResidues $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setResistancePerCut'
    proc setResistancePerCut { object args } {
        ::set result [eval $object setResistancePerCut $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRmsACCurrentDensity'
    proc setRmsACCurrentDensity { object args } {
        ::set result [eval $object setRmsACCurrentDensity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRouteMethod'
    proc setRouteMethod { object args } {
        ::set result [eval $object setRouteMethod $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRoutePattern'
    proc setRoutePattern { object args } {
        ::set result [eval $object setRoutePattern $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRouteStatus'
    proc setRouteStatus { object args } {
        ::set result [eval $object setRouteStatus $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRouteTopology'
    proc setRouteTopology { object args } {
        ::set result [eval $object setRouteTopology $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRoutingLayer'
    proc setRoutingLayer { object args } {
        ::set result [eval $object setRoutingLayer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRowFlipType'
    proc setRowFlipType { object args } {
        ::set result [eval $object setRowFlipType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRowHeader'
    proc setRowHeader { object args } {
        ::set result [eval $object setRowHeader $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRowHorizontal'
    proc setRowHorizontal { object args } {
        ::set result [eval $object setRowHorizontal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRowInterpolateType'
    proc setRowInterpolateType { object args } {
        ::set result [eval $object setRowInterpolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRowLowerExtrapolateType'
    proc setRowLowerExtrapolateType { object args } {
        ::set result [eval $object setRowLowerExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRowName'
    proc setRowName { object args } {
        ::set result [eval $object setRowName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRowSpacing'
    proc setRowSpacing { object args } {
        ::set result [eval $object setRowSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRowSpacingType'
    proc setRowSpacingType { object args } {
        ::set result [eval $object setRowSpacingType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setRowUpperExtrapolateType'
    proc setRowUpperExtrapolateType { object args } {
        ::set result [eval $object setRowUpperExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSecond'
    proc setSecond { object args } {
        ::set result [eval $object setSecond $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSeqBitLength'
    proc setSeqBitLength { object args } {
        ::set result [eval $object setSeqBitLength $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSeverity'
    proc setSeverity { object args } {
        ::set result [eval $object setSeverity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setShieldNet1'
    proc setShieldNet1 { object args } {
        ::set result [eval $object setShieldNet1 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setShieldNet2'
    proc setShieldNet2 { object args } {
        ::set result [eval $object setShieldNet2 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setShieldedNet1'
    proc setShieldedNet1 { object args } {
        ::set result [eval $object setShieldedNet1 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setShieldedNet2'
    proc setShieldedNet2 { object args } {
        ::set result [eval $object setShieldedNet2 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setShortMsg'
    proc setShortMsg { object args } {
        ::set result [eval $object setShortMsg $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSide'
    proc setSide { object args } {
        ::set result [eval $object setSide $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSigType'
    proc setSigType { object args } {
        ::set result [eval $object setSigType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSiteDef'
    proc setSiteDef { object args } {
        ::set result [eval $object setSiteDef $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSiteOrient'
    proc setSiteOrient { object args } {
        ::set result [eval $object setSiteOrient $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSitePattern'
    proc setSitePattern { object args } {
        ::set result [eval $object setSitePattern $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSiteSpacing'
    proc setSiteSpacing { object args } {
        ::set result [eval $object setSiteSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSize'
    proc setSize { object args } {
        ::set result [eval $object setSize $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSoft'
    proc setSoft { object args } {
        ::set result [eval $object setSoft $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSource'
    proc setSource { object args } {
        ::set result [eval $object setSource $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSpacing'
    proc setSpacing { object args } {
        ::set result [eval $object setSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setStart'
    proc setStart { object args } {
        ::set result [eval $object setStart $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setStartCoord'
    proc setStartCoord { object args } {
        ::set result [eval $object setStartCoord $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setStartObject'
    proc setStartObject { object args } {
        ::set result [eval $object setStartObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setStep'
    proc setStep { object args } {
        ::set result [eval $object setStep $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setStop'
    proc setStop { object args } {
        ::set result [eval $object setStop $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setStopObject'
    proc setStopObject { object args } {
        ::set result [eval $object setStopObject $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setStringVal'
    proc setStringVal { object args } {
        ::set result [eval $object setStringVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setStyle'
    proc setStyle { object args } {
        ::set result [eval $object setStyle $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSupplySensitivity'
    proc setSupplySensitivity { object args } {
        ::set result [eval $object setSupplySensitivity $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSymmetricInRot'
    proc setSymmetricInRot { object args } {
        ::set result [eval $object setSymmetricInRot $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSymmetricInX'
    proc setSymmetricInX { object args } {
        ::set result [eval $object setSymmetricInX $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSymmetricInY'
    proc setSymmetricInY { object args } {
        ::set result [eval $object setSymmetricInY $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setSymmetry'
    proc setSymmetry { object args } {
        ::set result [eval $object setSymmetry $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setTemperature'
    proc setTemperature { object args } {
        ::set result [eval $object setTemperature $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setTerm'
    proc setTerm { object args } {
        ::set result [eval $object setTerm $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setTermType'
    proc setTermType { object args } {
        ::set result [eval $object setTermType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setText'
    proc setText { object args } {
        ::set result [eval $object setText $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setTimeVal'
    proc setTimeVal { object args } {
        ::set result [eval $object setTimeVal $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setTool'
    proc setTool { object args } {
        ::set result [eval $object setTool $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setTopModule'
    proc setTopModule { object args } {
        ::set result [eval $object setTopModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setTotalCap'
    proc setTotalCap { object args } {
        ::set result [eval $object setTotalCap $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setTrackSpacing'
    proc setTrackSpacing { object args } {
        ::set result [eval $object setTrackSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc setTrackingProtocol { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'setTransform'
    proc setTransform { object args } {
        ::set result [eval $object setTransform $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setType'
    proc setType { object args } {
        ::set result [eval $object setType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setUnShielded'
    proc setUnShielded { object args } {
        ::set result [eval $object setUnShielded $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setUniqueName'
    proc setUniqueName { object args } {
        ::set result [eval $object setUniqueName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setUpDemand'
    proc setUpDemand { object args } {
        ::set result [eval $object setUpDemand $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setUpSupply'
    proc setUpSupply { object args } {
        ::set result [eval $object setUpSupply $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc setUpperBound { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'setUpperExtrapolateType'
    proc setUpperExtrapolateType { object args } {
        ::set result [eval $object setUpperExtrapolateType $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setUsage'
    proc setUsage { object args } {
        ::set result [eval $object setUsage $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setUserUnits'
    proc setUserUnits { object args } {
        ::set result [eval $object setUserUnits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setValue'
    proc setValue { object args } {
        ::set result [eval $object setValue $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setVerticalDemand'
    proc setVerticalDemand { object args } {
        ::set result [eval $object setVerticalDemand $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setVerticalSupply'
    proc setVerticalSupply { object args } {
        ::set result [eval $object setVerticalSupply $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setViaParams'
    proc setViaParams { object args } {
        ::set result [eval $object setViaParams $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setView'
    proc setView { object args } {
        ::set result [eval $object setView $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setViewName'
    proc setViewName { object args } {
        ::set result [eval $object setViewName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setVisible'
    proc setVisible { object args } {
        ::set result [eval $object setVisible $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setVisibleToModule'
    proc setVisibleToModule { object args } {
        ::set result [eval $object setVisibleToModule $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setVoltage'
    proc setVoltage { object args } {
        ::set result [eval $object setVoltage $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'setWidth'
    proc setWidth { object args } {
        ::set result [eval $object setWidth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'siteName'
    proc siteName { object args } {
        ::set result [eval $object siteName $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Unsupported function
    proc sort { args } {
        error "This call is currently unsupported in the oaTcl compatibilty API."
    }

    # Class member function 'startInst'
    proc startInst { object args } {
        ::set result [eval $object startInst $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'subFromPoint'
    proc subFromPoint { object args } {
        ::set result [eval $object subFromPoint $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'tail'
    proc tail { object args } {
        ::set result [eval $object tail $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'toPoints'
    proc toPoints { object args } {
        ::set result [eval $object toPoints $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'top'
    proc top { object args } {
        ::set result [eval $object top $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'transfer'
    proc transfer { object args } {
        ::set result [eval $object transfer $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'transform'
    proc transform { object args } {
        ::set result [eval $object transform $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toCoords]
        }
    }

    # Class member function 'traverse'
    proc traverse { object args } {
        ::set result [eval $object traverse $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'undefineSuperMaster'
    proc undefineSuperMaster { object args } {
        ::set result [eval $object undefineSuperMaster $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'undo'
    proc undo { object args } {
        ::set result [eval $object undo $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'uniquify'
    proc uniquify { object args } {
        ::set result [eval $object uniquify $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unload'
    proc unload { object args } {
        ::set result [eval $object unload $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetAntennaData'
    proc unsetAntennaData { object args } {
        ::set result [eval $object unsetAntennaData $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetBeginConn'
    proc unsetBeginConn { object args } {
        ::set result [eval $object unsetBeginConn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetClearanceMeasure'
    proc unsetClearanceMeasure { object args } {
        ::set result [eval $object unsetClearanceMeasure $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetControlled'
    proc unsetControlled { object args } {
        ::set result [eval $object unsetControlled $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetCp'
    proc unsetCp { object args } {
        ::set result [eval $object unsetCp $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetDBUPerUU'
    proc unsetDBUPerUU { object args } {
        ::set result [eval $object unsetDBUPerUU $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetDefaultManufacturingGrid'
    proc unsetDefaultManufacturingGrid { object args } {
        ::set result [eval $object unsetDefaultManufacturingGrid $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetEffectiveWidth'
    proc unsetEffectiveWidth { object args } {
        ::set result [eval $object unsetEffectiveWidth $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetEndConn'
    proc unsetEndConn { object args } {
        ::set result [eval $object unsetEndConn $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetExcludedLayers'
    proc unsetExcludedLayers { object args } {
        ::set result [eval $object unsetExcludedLayers $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetGateGrounded'
    proc unsetGateGrounded { object args } {
        ::set result [eval $object unsetGateGrounded $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetMustJoin'
    proc unsetMustJoin { object args } {
        ::set result [eval $object unsetMustJoin $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetObjects'
    proc unsetObjects { object args } {
        ::set result [eval $object unsetObjects $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetPosition'
    proc unsetPosition { object args } {
        ::set result [eval $object unsetPosition $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetProcessFamily'
    proc unsetProcessFamily { object args } {
        ::set result [eval $object unsetProcessFamily $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetRefs'
    proc unsetRefs { object args } {
        ::set result [eval $object unsetRefs $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetSpacing'
    proc unsetSpacing { object args } {
        ::set result [eval $object unsetSpacing $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'unsetUserUnits'
    proc unsetUserUnits { object args } {
        ::set result [eval $object unsetUserUnits $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'update'
    proc update { object args } {
        ::set result [eval $object update $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'upperRight'
    proc upperRight { object args } {
        ::set result [eval $object upperRight $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return [$result toStr]
        }
    }

    # Class member function 'usesTermPosition'
    proc usesTermPosition { object args } {
        ::set result [eval $object usesTermPosition $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'usesTermPositions'
    proc usesTermPositions { object args } {
        ::set result [eval $object usesTermPositions $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'uuToDBU'
    proc uuToDBU { object args } {
        ::set result [eval $object uuToDBU $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'uuToDBUArea'
    proc uuToDBUArea { object args } {
        ::set result [eval $object uuToDBUArea $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'uuToDBUDistance'
    proc uuToDBUDistance { object args } {
        ::set result [eval $object uuToDBUDistance $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'validate'
    proc validate { object args } {
        ::set result [eval $object validate $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'x'
    proc x { object args } {
        ::set result [eval $object x $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'x1'
    proc x1 { object args } {
        ::set result [eval $object x1 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'x2'
    proc x2 { object args } {
        ::set result [eval $object x2 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'xOffset'
    proc xOffset { object args } {
        ::set result [eval $object xOffset $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'y'
    proc y { object args } {
        ::set result [eval $object y $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'y1'
    proc y1 { object args } {
        ::set result [eval $object y1 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'y2'
    proc y2 { object args } {
        ::set result [eval $object y2 $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }

    # Class member function 'yOffset'
    proc yOffset { object args } {
        ::set result [eval $object yOffset $args]
        if {$result == "NULL"} {
            return {}
        } else {
            return $result
        }
    }
}
