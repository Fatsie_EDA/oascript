#!/usr/bin/env tcl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

set path [file join $env(OA_ROOT) lib linux_rhel30_gcc411_64 opt]
lappend auto_path $path

package require oa

proc main {} {
    oa::BaseInit
    set timer [oa::Timer]
    puts [format "%-15s %-15s" "package" "build name"]
    puts [string repeat - 30]
    set bi_arr [oa::BuildInfoGetPackages]
    for {set i 0} {$i < [oa::getNumElements $bi_arr]} {incr i} {
        set bi [oa::get $bi_arr $i]
        puts [format "%-15s %-15s" [oa::getPackageName $bi] [oa::getBuildName $bi]]
    }
    puts [array names bi_arr]
    set dir [oa::Dir "."]
    set path [oa::getFullName $dir]
    puts "\nOA thinks the cwd is $path\n"
    after 1000
    puts "That took [oa::getElapsed $timer] seconds.\n"
}

main

