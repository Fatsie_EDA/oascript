#!/usr/bin/env tcl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

set path [file join $env(OA_ROOT) lib linux_rhel30_gcc411_64 opt]
lappend auto_path $path

package require oa

proc main {} {
    global argv
    array set opt $argv
    if {![info exists opt(--lib)]} {
        error "Missing --lib"
    }

    oa::DMInit
    oa::TechInit

    if {[info exists opt(--libdefs)]} {
        oa::LibDefListOpenLibs $opt(--libdefs)
    } else {
        oa::LibDefListOpenLibs
    }

    set ns [oa::NativeNS]

    set techlib_scname [oa::ScalarName $ns $opt(--lib)]
    set techlib [oa::LibFind $techlib_scname]
    if {$techlib == ""} {
        error "Couldn't find $opt(--libs)."
    }

    if {![oa::getAccess $techlib [oa::LibAccess $oa::oacReadLibAccess]]} {
        error "lib $opt(--lib): No Access!"
    }

    set tech [oa::TechOpen $techlib]
    if {$tech == ""} {
        error "No tech info!"
    }

    set layers [oa::getLayers $tech]
    while {[set layer [oa::getNext $layers]] != ""} {
        set layer_name [oa::getName $layer]
        puts "layer [oa::getNumber $layer] = $layer_name"
    }
}

main

