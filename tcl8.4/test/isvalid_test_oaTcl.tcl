#!/usr/bin/env tcl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

set path [file join $env(OA_ROOT) lib linux_rhel30_gcc411_64 opt]
lappend auto_path $path

package require oa

proc time_isvalid {block num_iter} {
    set timer [oa::Timer]
    for {set i 0} {$i < $num_iter} {incr i} {
        oa::isValid $block
    }
    return [oa::getElapsed $timer]
}

proc main {} {

    set warmup_num 1000
    set nums {1000 10000 100000 1000000 10000000 100000000 1000000000}
    set verbose 0

    global argv
    array set opt $argv
    if {![info exists opt(--lib)]} {
        error "Missing --lib"
    } elseif {![info exists opt(--cell)]} {
        error "Missing --cell"
    } elseif {![info exists opt(--view)]} {
        error "Missing --view"
    }

    oa::DesignInit

    if {[info exists opt(--libdefs)]} {
        oa::LibDefListOpenLibs $opt(--libdefs)
    } else {
        oa::LibDefListOpenLibs
    }
    
    set lib [oa::LibFind $opt(--lib)]
    if {$lib == ""} {
        error "Couldn't find lib $opt(--lib)"
    }

    if {![oa::getAccess $lib [oa::LibAccess $oa::oacReadLibAccess]]} {
        error "lib $opt(--lib): No Access!"
    }

    set design [oa::DesignOpen $opt(--lib) $opt(--cell) $opt(--view) "r"]
    if {$design == ""} {
        error "No design!"
    }

    set topBlock [oa::getTopBlock $design]
    if {$topBlock == ""} {
        error "No top block!"
    }

    if {$verbose == 1} {
        puts "Warmup call to isValid ($warmup_num times)..."
    }
    time_isvalid $topBlock $warmup_num

    puts [format "%16s %16s" "# Iter" "Elapsed (s)"]
    puts [format "%16s %16s" "------" "-----------"]
    foreach num $nums {
        puts [format "%16d %16.2f" $num [time_isvalid $topBlock $num]]
    }

}

main
