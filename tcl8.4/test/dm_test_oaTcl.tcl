#!/usr/bin/env tcl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

set path [file join $env(OA_ROOT) lib linux_rhel30_gcc411_64 opt]
lappend auto_path $path

package require oa

proc main {} {
    global argv
    if {[set i [lsearch $argv "--quiet"]] != -1} {
        set quiet 1
        set argv [lreplace $argv $i $i]
    } else {
        set quiet 0
    }
    array set opt $argv

    oa::DMInit
    if {[info exists opt(--libdefs)]} {
        oa::LibDefListOpenLibs $opt(--libdefs)
    } else {
        oa::LibDefListOpenLibs
    }

    set ns [oa::NativeNS]

    set libs [oa::LibGetOpenLibs]
    while {[set lib [oa::getNext $libs]] != ""} {
        set libname [oa::getName $lib]
        if {![oa::getAccess $lib [oa::LibAccess $oa::oacReadLibAccess]]} {
            puts "lib $libname: No Access!"
            continue
        }
        set libpath [oa::getPath $lib]
        puts "lib $libname path=$libpath"
        set cells [oa::getCells $lib]
        while {[set cell [oa::getNext $cells]] != ""} {
            set cellname [oa::getName $cell]
            if {!$quiet} {
                puts "  $cellname"
            }
            set cellviews [oa::getCellViews $cell]
            while {[set cv [oa::getNext $cellviews]] != ""} {
                set view [oa::getView $cv]
                set viewname [oa::getName $view]
                if {!$quiet} {
                    puts "    $viewname"
                }
                set files [oa::getDMFiles $cv]
                while {[set file [oa::getNext $files]] != ""} {
                    set filename [oa::getPath $file]
                    set size [oa::getOnDiskSize $file]
                    if {!$quiet} {
                        puts "      $filename ($size bytes)"
                    }
                }
            }
        }
    }
}

main

