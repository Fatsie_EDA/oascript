#!/usr/bin/env tcl
##############################################################################
# Copyright 2011 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

package forget oa

set path [file dirname [info script]]
set libpath [file normalize [file join $path .. ]]
lappend auto_path $libpath

package require oa

set path [file join [file dirname [info script]] oaUUTestLib]
exec rm -rf $path

proc check { testpoint expected actual } {
    if {[expr {$expected != $actual}]} {
        puts "'$testpoint' : expected $expected, got $actual"
        incr ::numFails 1
    }
}

set numFails 0

set lib [oa::oaLib_create "testLib" $path]
set tech [oa::oaTech_create $lib]
set viewType [oa::oaViewType_find "schematic"]
set dbuPerUU [$tech getDBUPerUU $viewType]

set uuPoint [oa::oaUUPoint {100 400} $tech $viewType]
check "Constructor from oaPoint x" [$tech dbuToUU $viewType 100] [$uuPoint x]
check "Constructor from oaPoint y" [$tech dbuToUU $viewType 400] [$uuPoint y]

set transform [oa::oaTransform {100 200}]
set uuPoint [oa::oaUUPoint {100 400} $tech $viewType]
set uuPoint2 [oa::oaUUPoint $uuPoint $transform]
check "Transform Constructor x" [$tech dbuToUU $viewType 200] [$uuPoint2 x]
check "Transform Constructor y" [$tech dbuToUU $viewType 600] [$uuPoint2 y]

set uuPoint [oa::oaUUPoint 1.125 2.25 $tech $viewType]
check "Constructor from x,y x" 1.125 [$uuPoint x]
check "Constructor from x,y y" 2.25 [$uuPoint y]

set uuPoint [oa::oaUUPoint $tech $viewType]
$uuPoint set 6.125 8.250
check "set x" 6.125 [$uuPoint x]
check "set y" 8.250 [$uuPoint y]

set uuPoint [oa::oaUUPoint {200 500} $tech $viewType]
set uuPoint2 [oa::oaUUPoint $tech $viewType]
set transform [oa::oaTransform {400 300}]
$uuPoint transform $transform $uuPoint2
# make sure this operation only changed uuPoint2 and not uuPoint
check "transform other (this x)" [$tech dbuToUU $viewType 200] [$uuPoint x]
check "transform other (this y)" [$tech dbuToUU $viewType 500] [$uuPoint y]
check "transform other x" [$tech dbuToUU $viewType 600] [$uuPoint2 x]
check "transform other y" [$tech dbuToUU $viewType 800] [$uuPoint2 y]

set uuPoint [oa::oaUUPoint {800 900} $tech $viewType]
set transform [oa::oaTransform {-100 -300}]
$uuPoint transform $transform
check "transform this x" [$tech dbuToUU $viewType 700] [$uuPoint x]
check "transform this y" [$tech dbuToUU $viewType 600] [$uuPoint y]

set uuPoint [oa::oaUUPoint {-100 -200} $tech $viewType]
set uuPoint2 [oa::oaUUPoint $tech $viewType]
$uuPoint transform -1.0 180 $uuPoint2
# make sure this operation only changed uuPoint2 and not uuPoint
check "transform scale angle other (this x)" [$tech dbuToUU $viewType -100] [$uuPoint x]
check "transform scale angle other (this y)" [$tech dbuToUU $viewType -200] [$uuPoint y]
check "transform scale angle other x" [$tech dbuToUU $viewType 100] [$uuPoint2 x]
check "transform scale angle other y" [$tech dbuToUU $viewType -200] [$uuPoint2 y]

set uuPoint [oa::oaUUPoint {-100 -200} $tech $viewType]
$uuPoint transform -1.0 180
check "transform scale angle this x" [$tech dbuToUU $viewType 100] [$uuPoint x]
check "transform scale angle this y" [$tech dbuToUU $viewType -200] [$uuPoint y]

set uuPoint [oa::oaUUPoint {300 -200} $tech $viewType]
set uuPoint2 [oa::oaUUPoint {100 500} $tech $viewType]
set distanceFrom2 [$uuPoint distanceFrom2 $uuPoint2]
set expectedDbu [expr {int(pow(300 - 100,2) + pow(-200 - 500, 2))}]
check "distanceFrom2" [$tech dbuToUUDistance $viewType $expectedDbu] $distanceFrom2

set uuPoint [oa::oaUUPoint {300 -300} $tech $viewType]
set uuPoint2 [$uuPoint -]
# make sure input is not changed
check "operator unary - x" [$tech dbuToUU $viewType 300] [$uuPoint x]
check "operator unary - y" [$tech dbuToUU $viewType -300] [$uuPoint y]
check "operator unary - x" [$tech dbuToUU $viewType -300] [$uuPoint2 x]
check "operator unary - y" [$tech dbuToUU $viewType 300] [$uuPoint2 y]

set uuPoint [oa::oaUUPoint {300 600} $tech $viewType]
set uuPoint2 [oa::oaUUPoint {400 500} $tech $viewType]
set uuPoint3 [$uuPoint - $uuPoint2]
# make sure inputs are not changed
check "operator - arg1 x" [$tech dbuToUU $viewType 300] [$uuPoint x]
check "operator - arg1 y" [$tech dbuToUU $viewType 600] [$uuPoint y]
check "operator - arg2 x" [$tech dbuToUU $viewType 400] [$uuPoint2 x]
check "operator - arg2 y" [$tech dbuToUU $viewType 500] [$uuPoint2 y]
check "operator - result x" [$tech dbuToUU $viewType -100] [$uuPoint3 x]
check "operator - result y" [$tech dbuToUU $viewType 100] [$uuPoint3 y]

set uuPoint [oa::oaUUPoint {-200 -300} $tech $viewType]
set uuPoint2 [oa::oaUUPoint {400 800} $tech $viewType]
set uuPoint3 [$uuPoint + $uuPoint2]
# make sure inputs are not changed
check "operator + arg1 x" [$tech dbuToUU $viewType -200] [$uuPoint x]
check "operator + arg1 y" [$tech dbuToUU $viewType -300] [$uuPoint y]
check "operator + arg2 x" [$tech dbuToUU $viewType 400] [$uuPoint2 x]
check "operator + arg2 y" [$tech dbuToUU $viewType 800] [$uuPoint2 y]
check "operator + result x" [$tech dbuToUU $viewType 200] [$uuPoint3 x]
check "operator + result y" [$tech dbuToUU $viewType 500] [$uuPoint3 y]

set uuPoint [oa::oaUUPoint {300 400} $tech $viewType]
set point [$uuPoint getPoint]
check "getPoint x" 300 [$point x]
check "getPoint y" 400 [$point y]

set uuPoint [oa::oaUUPoint $tech $viewType]
check "getTech" $tech [$uuPoint getTech]
check "getViewType" $viewType [$uuPoint getViewType]

if {$numFails == 0} {
    puts "All tests passed."
} else {
    error "$numFails tests failed."
}

exec rm -rf $path

