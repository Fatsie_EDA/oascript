/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%init %{/* --- OAOBJECT DOWNCASTING (BEGIN) --- */%}

#undef OATYPENUM
%define OATYPENUM(typename, swigtype, typenum)
%init %{ typeinfo_map[#swigtype] = typenum; %}
%enddef

%include <macros/oa_typenums.i>

%init %{

    memset(typeinfo_lookup, 0, oavNumObjectTypes * sizeof(swig_type_info*));
    std::queue<swig_type_info*> typeQ;
    typeQ.push(SWIGTYPE_p_OpenAccess_4__oaObject);
    while (!typeQ.empty()) {
	swig_type_info *typeinfo = typeQ.front();
	typeQ.pop();

	swig_type_info *master_typeinfo = 0;
	if (swig_module.next != &swig_module)
	    master_typeinfo = SWIG_MangledTypeQueryModule(swig_module.next, &swig_module, typeinfo->name);
	if (!master_typeinfo)
	    master_typeinfo = typeinfo;

	TypeInfoMap::iterator tiMapIter = typeinfo_map.find(typeinfo->name);
	if (tiMapIter != typeinfo_map.end()) {
	    typeinfo_lookup[tiMapIter->second] = master_typeinfo;
	} else {
            typeinfo->dcast = &oaObject_dcast;
        }

	for (swig_cast_info *cast = typeinfo->cast; cast; cast = cast->next) {
	    if (cast->type != typeinfo) {
	        typeQ.push(cast->type);
            }
        }
    }

%}

%init %{/* --- OAOBJECT DOWNCASTING (END) --- */%}

