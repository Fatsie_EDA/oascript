/* ****************************************************************************

   Copyright 2011 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     03/19/2011: Initial copy.

 ******************************************************************************

 SUMMARY
 
 This interface file will intercept calls to oaLibDefList::open() and
 oaLibDefList::openLibs() to register any errors encountered when processing
 the lib.defs file.  It makes use of oaObserver<oaLibDefList> to capture the
 error messages and then throws a runtime error to the target language if any
 errors are encountered.

 *************************************************************************** */


%{

#include <exception>

#if defined(OA_WINDOWS)
#define OASCRIPT_DLL_API __declspec(dllexport)
#else 
#define OASCRIPT_DLL_API 
#endif

// Class to store lib.defs error messages
class OASCRIPT_DLL_API oasLibDefListErrors
{
  
public:

  // Constructor and destructor
  oasLibDefListErrors() : errors() {}
  ~oasLibDefListErrors() {}

  // Get an array of strings containing error messages registered on the last
  // access of lib.defs
  oaStringArray* getArray()
  {
    return(&errors);
  }

  // Get a string message joined with a newline of the error messages registered
  // on the last access of lib.defs
  void getString(oaString &out)
  {
    if (errors.getNumElements()) {
      for (int i = 0; i < errors.getNumElements(); i++)
      {
        out += errors.get(i) + "\n";
      }
    }
    else
    {
      out = oaString();
    }
  }

  // Clear the list of registered errors.
  void clear()
  {
    errors.setNumElements(0);
  }

  // Append to the list of registered errors.
  void append(const oaString &in)
  {
    errors.append(in);
  }

  // Returns true if there are any errors registered; false if no errors are registered.
  oaBoolean hasErrors()
  {
    return(errors.getNumElements() != 0);
  }

 private:
  oaStringArray errors;
  
};

// Object to store lib.def error messages
static oasLibDefListErrors LibDefListErrors;
  
// Create a class for an oaLibDefList observer to capture load warnings
class oasLibDefListObserver : public oaObserver<oaLibDefList> {
public:
  
  oasLibDefListObserver(oaUInt4   priorityIn, 
                        oaBoolean enabledIn=true);
  
  oaBoolean onLoadWarnings(oaLibDefList                *obj,
                           const oaString              &msg,
                           oaLibDefListWarningTypeEnum type);
};

// oaLibDefList observer construction
oasLibDefListObserver::oasLibDefListObserver(oaUInt4       priorityIn, 
                                           oaBoolean     enabledIn)
										   
:	oaObserver<oaLibDefList>(priorityIn, enabledIn)            // The constructor passes the                     
{                                                                  // priority and enabled status.
}

// oaLibDefList observer to store load errors
oaBoolean oasLibDefListObserver::onLoadWarnings(oaLibDefList *obj,
                                                const oaString &msg,
                                                oaLibDefListWarningTypeEnum type)
{
  oaString fmt_msg;
  if (!obj) {
    // Do not throw an exception if there is not yet a lib.defs file.
    // It is a perfectly ordinary situation.
    // The working group has decided that not even a warning should be issued.
    // printf("WARNING: Trying to open lib.defs file: %s\n", (const oaChar *)msg);
    return true;
  } else {
    oaString libDefPath;
    obj->getPath(libDefPath);
    switch(type) {
    case oacSyntaxErrorLibDefListWarning:
      fmt_msg.format("ERROR: %s", (const oaChar *)msg);
      break;
    default:
      fmt_msg.format("ERROR: in %s: %s", (const oaChar *)libDefPath, (const oaChar *)msg);
      break;
    }
  }
  LibDefListErrors.append(fmt_msg);
  return true;
}

%}

%init %{
  // Register the oaLibDefList observer for registering any errors found in opening
  // the lib.defs file.
  static oasLibDefListObserver ldObserver(1);
%}

// Macro for exception handling of openLibs and open
%define LIBDEFEXCP(method)
%exception method {

  // Clear lib.def errors in case there were some last time
  LibDefListErrors.clear();

  try
  {
    $action
  }
  catch(OpenAccess_4::oaException& excp) {
    LANG_THROW_OAEXCP(excp);
  }

  // Check to see if any errors in the lib.defs file were found
  if (LibDefListErrors.hasErrors())
  {
    // Throw exception for errors in lib.defs file
    oaString msg;
    LibDefListErrors.getString(msg);
  #if SWIGCSHARP == 1
    SWIG_CSharpSetPendingExceptionCustom((const char*) msg);
  #else
    // C# and Java do not have SWIG_exception_fail()
    SWIG_exception_fail(SWIG_RuntimeError, (const char *) msg);
  #endif
  }

}
%enddef

// Define special exception handlers for openLibs and open
LIBDEFEXCP(OpenAccess_4::oaLibDefList::openLibs)
LIBDEFEXCP(OpenAccess_4::oaLibDefList::openLibs(const oaString&))
LIBDEFEXCP(OpenAccess_4::oaLibDefList::open)
