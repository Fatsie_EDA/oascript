/* -*- mode: c++ -*- */
/*
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%inline %{

class oaUUBox
{
public:
    oaUUBox(const OpenAccess_4::oaBox& box, OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mBox(box)
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    oaUUBox(OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mBox()
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    oaUUBox(OpenAccess_4::oaDouble leftIn, OpenAccess_4::oaDouble bottomIn,
            OpenAccess_4::oaDouble rightIn, OpenAccess_4::oaDouble topIn,
            OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
      : mBox(tech->uuToDBU(viewType, leftIn), tech->uuToDBU(viewType, bottomIn),
             tech->uuToDBU(viewType, rightIn), tech->uuToDBU(viewType, topIn))
      , mTech(tech)
      , mViewType(viewType)
    {
    }

    oaUUBox(const oaUUPoint& lowerLeftIn, const oaUUPoint& upperRightIn,
            OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
      : mBox(lowerLeftIn.getPoint(), upperRightIn.getPoint())
      , mTech(tech)
      , mViewType(viewType)
    {
    }

    oaUUBox(const oaUUBox& box, const OpenAccess_4::oaTransform& xform)
      : mBox(box.mBox, xform)
      , mTech(box.mTech)
      , mViewType(box.mViewType)
    {
    }

    oaUUPoint lowerLeft() const
    {
        checkTech();
        OpenAccess_4::oaDouble left = mTech->dbuToUU(mViewType, mBox.left());
        OpenAccess_4::oaDouble bottom = mTech->dbuToUU(mViewType, mBox.bottom());
        return oaUUPoint(left, bottom, mTech, mViewType);
    }

    oaUUPoint upperRight() const
    {
        checkTech();
        OpenAccess_4::oaDouble right = mTech->dbuToUU(mViewType, mBox.right());
        OpenAccess_4::oaDouble top = mTech->dbuToUU(mViewType, mBox.top());
        return oaUUPoint(right, top, mTech, mViewType);
    }

    OpenAccess_4::oaDouble left() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mBox.left());
    }

    OpenAccess_4::oaDouble bottom() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mBox.bottom());
    }

    OpenAccess_4::oaDouble right() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mBox.right());
    }

    OpenAccess_4::oaDouble top() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mBox.top());
    }

    void set(OpenAccess_4::oaDouble leftIn, OpenAccess_4::oaDouble bottomIn,
             OpenAccess_4::oaDouble rightIn, OpenAccess_4::oaDouble topIn)
    {
        checkTech();
        OpenAccess_4::oaCoord l = mTech->uuToDBU(mViewType, leftIn);
        OpenAccess_4::oaCoord b = mTech->uuToDBU(mViewType, bottomIn);
        OpenAccess_4::oaCoord r = mTech->uuToDBU(mViewType, rightIn);
        OpenAccess_4::oaCoord t = mTech->uuToDBU(mViewType, topIn);
        mBox.set(l, b, r, t);
    }

    void set(const oaUUPoint& lowerLeftIn, const oaUUPoint& upperRightIn)
    {
        checkTech();
        mBox.set(lowerLeftIn.getPoint(), upperRightIn.getPoint());
    }

    void set(const oaUUPoint& center, const OpenAccess_4::oaDouble size)
    {
        checkTech();
        mBox.set(center.getPoint(), mTech->uuToDBUDistance(mViewType, size));
    }

    void set(const oaUUBox& box, const OpenAccess_4::oaTransform& xform)
    {
        mBox = OpenAccess_4::oaBox(box.getBox(), xform);
    }

    OpenAccess_4::oaDouble getWidth() const
    {
        checkTech();
        return mTech->dbuToUUDistance(mViewType, mBox.getWidth());
    }

    OpenAccess_4::oaDouble getHeight() const
    {
        checkTech();
        return mTech->dbuToUUDistance(mViewType, mBox.getHeight());
    }

    oaUUPoint getCenter() const
    {
        OpenAccess_4::oaPoint point;
        mBox.getCenter(point);
        return oaUUPoint(point, mTech, mViewType);
    }

    oaUUPoint getLowerRight() const
    {
        OpenAccess_4::oaPoint point;
        mBox.getLowerRight(point);
        return oaUUPoint(point, mTech, mViewType);
    }

    oaUUPoint getUpperLeft() const
    {
        OpenAccess_4::oaPoint point;
        mBox.getUpperLeft(point);
        return oaUUPoint(point, mTech, mViewType);
    }

    OpenAccess_4::oaBoolean hasNoArea() const
    {
        return mBox.hasNoArea();
    }

    OpenAccess_4::oaBoolean isInverted() const
    {
        return mBox.isInverted();
    }

    OpenAccess_4::oaBoolean overlaps(const oaUUBox& box, OpenAccess_4::oaBoolean incEdges=true) const
    {
        return mBox.overlaps(box.mBox, incEdges);
    }

    OpenAccess_4::oaBoolean overlaps(const oaUUSegment& segment, OpenAccess_4::oaBoolean incEdges=true) const
    {
        return mBox.overlaps(segment.getSegment(), incEdges);
    }

    OpenAccess_4::oaBoolean contains(const oaUUBox& box, OpenAccess_4::oaBoolean incEdges=true) const
    {
        return mBox.contains(box.mBox, incEdges);
    }

    OpenAccess_4::oaBoolean contains(const oaUUPoint& point, OpenAccess_4::oaBoolean incEdges=true) const
    {
        return mBox.contains(point.getPoint(), incEdges);
    }

    OpenAccess_4::oaDouble distanceFrom2(const oaUUPoint& point) const
    {
        checkTech();
        oaDouble d = mBox.distanceFrom2(point.getPoint());
        return d / mTech->getDBUPerUU(mViewType);
    }

    void fix(oaUUBox& result) const
    {
        mBox.fix(result.mBox);
    }

    void fix()
    {
        mBox.fix();
    }

    void transform(const OpenAccess_4::oaTransform& xform, oaUUBox& result) const
    {
        mBox.transform(xform, result.mBox);
    }

    void transform(const OpenAccess_4::oaTransform& xform)
    {
        mBox.transform(xform);
    }

    void transform(const oaUUPoint& offset, oaUUBox& result) const
    {
        mBox.transform(offset.getPoint(), result.mBox);
    }

    void transform(const oaUUPoint& offset)
    {
        mBox.transform(offset.getPoint());
    }

    void transform(OpenAccess_4::oaDouble scale, OpenAccess_4::oaDouble angle, oaUUBox& result) const
    {
        mBox.transform(scale, angle, result.mBox);
    }

    void transform(OpenAccess_4::oaDouble scale, OpenAccess_4::oaDouble angle)
    {
        mBox.transform(scale, angle);
    }

    void scale(OpenAccess_4::oaFloat scale)
    {
        mBox.scale(scale);
    }

    void merge(const oaUUBox& box, oaUUBox& result) const
    {
        mBox.merge(box.mBox, result.mBox);
    }

    void merge(const oaUUBox& box)
    {
        mBox.merge(box.mBox);
    }

    void merge(const oaUUPoint& point, oaUUBox& result) const
    {
        mBox.merge(point.getPoint(), result.mBox);
    }

    void merge(const oaUUPoint& point)
    {
        mBox.merge(point.getPoint());
    }

    void intersection(const oaUUBox& box, oaUUBox& result) const
    {
        mBox.intersection(box.mBox, result.mBox);
    }

    void intersection(const oaUUBox& box)
    {
        mBox.intersection(box.mBox);
    }

    void makeZero()
    {
        mBox.makeZero();
    }

    void makeInvertedZero()
    {
        mBox.makeInvertedZero();
    }

    void init()
    {
        mBox.init();
    }

    const OpenAccess_4::oaBox& getBox() const
    {
        return mBox;
    }

    OpenAccess_4::oaTech* getTech() const
    {
        return mTech;
    }

    OpenAccess_4::oaViewType* getViewType() const
    {
        return mViewType;
    }

private:
    void checkTech() const
    {
        if (!mTech->isValid()) {
            // make this into an exception!
            printf("invalid tech!\n");
        }
    }

private:
    OpenAccess_4::oaBox mBox;
    OpenAccess_4::oaTech* mTech;
    OpenAccess_4::oaViewType* mViewType;
};

%}

