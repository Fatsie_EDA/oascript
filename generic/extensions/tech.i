/* -*- mode: c++ -*- */
/*
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%include <generic/extensions/macros.i>

GETSTRING(oaAnalysisLib, getName);
GETSTRING(oaAnalysisLib, getFormat);
GETSCALARNAME(oaAnalysisLib, getFileName);
GETSCALARNAME(oaAnalysisLib, getCellName);
GETSCALARNAME(oaAnalysisLib, getViewName);
GETSTRINGWITHNS(oaCustomViaDef, getLibName);
GETSTRINGWITHNS(oaCustomViaDef, getCellName);
GETSTRINGWITHNS(oaCustomViaDef, getViewName);
GETSTRING(oaDerivedLayerDef, getName);
GETSTRING(oaDerivedLayerParamDef, getName);
GETSTRING(oaLayer, getName);
GETSTRING(oaOpPoint, getName);
GETSTRING(oaPurpose, getName);
GETSTRING(oaSiteDef, getName);
GETSTRINGWITHNS(oaTech, getLibName);
GETSTRING(oaViaDef, getName);
GETSTRING(oaViaTopology, getName);
GETSTRING(oaViaVariant, getName);
