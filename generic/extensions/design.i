/* -*- mode: c++ -*- */
/*
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%include <generic/extensions/macros.i>

GETSTRING(oaAnalysisPoint, getName);
GETSTRING(oaAreaBoundary, getName);
GETSTRINGWITHNS(oaArrayInst, getName);
GETSTRING(oaAssignment, getName);
GETSTRINGWITHNS(oaAttrDisplay, getText);
GETBOX(oaBlock, getBBox);
GETUUBOX(oaBlock, getBBox, getUUBBox);
GETSTRING(oaClusterBoundary, getName);
GETSTRINGWITHNS(oaCustomVia, getLibName);
GETSTRINGWITHNS(oaCustomVia, getCellName);
GETSTRINGWITHNS(oaCustomVia, getViewName);
GETSTRINGWITHNS(oaCustomViaHeader, getLibName);
GETSTRINGWITHNS(oaCustomViaHeader, getCellName);
GETSTRINGWITHNS(oaCustomViaHeader, getViewName);
GETSTRINGWITHNS(oaDesign, getLibName);
GETSTRINGWITHNS(oaDesign, getCellName);
GETSTRINGWITHNS(oaDesign, getViewName);
GETSTRING(oaDesign, getPcellEvaluatorName);
GETSTRINGWITHNS(oaBundleNet, getName);
GETSTRINGWITHNS(oaBundleTerm, getName);
GETSTRINGWITHNS(oaBusNetBit, getName);
GETSTRINGWITHNS(oaBusNetDef, getName);
GETSTRINGWITHNS(oaBusNet, getName);
GETSTRINGWITHNS(oaBusTermBit, getName);
GETSTRINGWITHNS(oaBusTermDef, getName);
GETSTRINGWITHNS(oaBusTerm, getName);
GETSTRING(oaCluster, getName);
GETSTRING(oaDevice, getName);
GETSTRING(oaEvalText, getExpression);
GETSTRING(oaEvalText, getEvaluatorName);
GETSTRINGWITHNS(oaFigGroup, getName);
GETSTRINGWITHNS(oaInst, getName);
GETSTRINGWITHNS(oaInst, getLibName);
GETSTRINGWITHNS(oaInst, getCellName);
GETSTRINGWITHNS(oaInst, getViewName);
GETSTRINGWITHNS(oaInstAttrDisplay, getText);
GETSTRINGWITHNS(oaInstHeader, getLibName);
GETSTRINGWITHNS(oaInstHeader, getCellName);
GETSTRINGWITHNS(oaInstHeader, getViewName);
GETSTRINGWITHNS(oaInstTerm, getTermName);
GETSTRING(oaMarker, getMsg);
GETSTRING(oaMarker, getShortMsg);
GETSTRING(oaMarker, getTool);
GETSTRING(oaModAssignment, getName);
GETSTRINGWITHNS(oaModBundleNet, getName);
GETSTRINGWITHNS(oaModBundleTerm, getName);
GETSTRINGWITHNS(oaModBusNetBit, getName);
GETSTRINGWITHNS(oaModBusNetDef, getName);
GETSTRINGWITHNS(oaModBusNet, getName);
GETSTRINGWITHNS(oaModBusTermBit, getName);
GETSTRINGWITHNS(oaModBusTermDef, getName);
GETSTRINGWITHNS(oaModBusTerm, getName);
GETSTRINGWITHNS(oaModDesignInst, getLibName);
GETSTRINGWITHNS(oaModDesignInst, getCellName);
GETSTRINGWITHNS(oaModDesignInst, getViewName);
GETSTRINGWITHNS(oaModInst, getName);
GETSTRINGWITHNS(oaModInstHeader, getLibName);
GETSTRINGWITHNS(oaModInstHeader, getCellName);
GETSTRINGWITHNS(oaModInstHeader, getViewName);
GETSTRINGWITHNS(oaModInstTerm, getTermName);
GETSTRINGWITHNS(oaModModuleScalarInst, getName);
GETSTRINGWITHNS(oaModModuleVectorInst, getName);
GETSTRINGWITHNS(oaModModuleVectorInstBit, getName);
GETSTRINGWITHNS(oaModNet, getName);
GETSTRINGWITHNS(oaModScalarInst, getName);
GETSTRINGWITHNS(oaModScalarNet, getName);
GETSTRINGWITHNS(oaModScalarTerm, getName);
GETSTRINGWITHNS(oaModTerm, getName);
GETSTRINGWITHNS(oaModule, getName);
GETSTRINGWITHNS(oaModVectorInstBit, getName);
GETSTRINGWITHNS(oaModVectorInstDef, getName);
GETSTRINGWITHNS(oaModVectorInst, getName);
GETSTRINGWITHNS(oaNet, getName);
GETSTRING(oaNode, getName);
GETSTRINGWITHNS(oaOccArrayInst, getName);
GETSTRINGWITHNS(oaOccArrayInst, getPathName);
GETSTRING(oaOccAssignment, getName);
GETSTRINGWITHNS(oaOccAttrDisplay, getText);
GETSTRINGWITHNS(oaOccBundleNet, getName);
GETSTRINGWITHNS(oaOccBundleNet, getPathName);
GETSTRINGWITHNS(oaOccBundleTerm, getName);
GETSTRINGWITHNS(oaOccBusNetBit, getName);
GETSTRINGWITHNS(oaOccBusNetBit, getPathName);
GETSTRINGWITHNS(oaOccBusNetDef, getName);
GETSTRINGWITHNS(oaOccBusNetDef, getPathName);
GETSTRINGWITHNS(oaOccBusNet, getName);
GETSTRINGWITHNS(oaOccBusNet, getPathName);
GETSTRINGWITHNS(oaOccBusTermBit, getName);
GETSTRINGWITHNS(oaOccBusTermDef, getName);
GETSTRINGWITHNS(oaOccBusTerm, getName);
GETSTRINGWITHNS(oaOccDesignInst, getLibName);
GETSTRINGWITHNS(oaOccDesignInst, getCellName);
GETSTRINGWITHNS(oaOccDesignInst, getViewName);
GETSTRINGWITHNS(oaOccInst, getName);
GETSTRINGWITHNS(oaOccInst, getPathName);
GETSTRINGWITHNS(oaOccInstAttrDisplay, getText);
GETSTRINGWITHNS(oaOccInstHeader, getLibName);
GETSTRINGWITHNS(oaOccInstHeader, getCellName);
GETSTRINGWITHNS(oaOccInstHeader, getViewName);
GETSTRINGWITHNS(oaOccInstTerm, getTermName);
GETSTRINGWITHNS(oaOccModuleInstHeader, getName);
GETSTRINGWITHNS(oaOccModuleScalarInst, getName);
GETSTRINGWITHNS(oaOccModuleScalarInst, getPathName);
GETSTRINGWITHNS(oaOccModuleVectorInst, getName);
GETSTRINGWITHNS(oaOccModuleVectorInst, getPathName);
GETSTRINGWITHNS(oaOccModuleVectorInstBit, getName);
GETSTRINGWITHNS(oaOccModuleVectorInstBit, getPathName);
GETSTRINGWITHNS(oaOccNet, getName);
GETSTRINGWITHNS(oaOccNet, getPathName);
GETSTRINGWITHNS(oaOccScalarInst, getName);
GETSTRINGWITHNS(oaOccScalarInst, getPathName);
GETSTRINGWITHNS(oaOccScalarNet, getName);
GETSTRINGWITHNS(oaOccScalarNet, getPathName);
GETSTRINGWITHNS(oaOccScalarTerm, getName);
GETSTRINGWITHNS(oaOccTerm, getName);
GETSTRING(oaOccTextDisplay, getText);
GETSTRING(oaOccText, getText);
GETSTRINGWITHNS(oaOccVectorInstBit, getName);
GETSTRINGWITHNS(oaOccVectorInstBit, getPathName);
GETSTRINGWITHNS(oaOccVectorInstDef, getName);
GETSTRINGWITHNS(oaOccVectorInstDef, getPathName);
GETSTRINGWITHNS(oaOccVectorInst, getName);
GETSTRINGWITHNS(oaOccVectorInst, getPathName);
GETSTRING(oaParasiticNetwork, getName);
GETSTRING(oaPin, getName);
GETSTRING(oaRow, getName);
GETSTRINGWITHNS(oaScalarInst, getName);
GETSTRINGWITHNS(oaScalarNet, getName);
GETSTRINGWITHNS(oaScalarTerm, getName);
GETSTRING(oaScanChain, getName);
GETSTRING(oaSubNetwork, getName);
GETSTRINGWITHNS(oaTerm, getName);
GETSTRING(oaText, getText);
GETSTRING(oaTextDisplay, getText);
GETSTRINGWITHNS(oaVectorInstBit, getName);
GETSTRINGWITHNS(oaVectorInstDef, getName);
GETSTRINGWITHNS(oaVectorInst, getName);
GETBOX(oaFig, getBBox);
GETUUBOX(oaFig, getBBox, getUUBBox);
GETBOX(oaOccDonut, getHoleBBox);
GETUUBOX(oaOccDonut, getHoleBBox, getHoleUUBBox);
GETBOX(oaOccShape, getBBox);
GETUUBOX(oaOccShape, getBBox, getUUBBox);
GETBOX(oaParasiticNetwork, getBBox);
GETUUBOX(oaParasiticNetwork, getBBox, getUUBBox);
GETBOX(oaRefHeader, getMasterBBox);
GETUUBOX(oaRefHeader, getMasterBBox, getMasterUUBBox);
GETBOX(oaRefHeader, getBBox);
GETUUBOX(oaRefHeader, getBBox, getUUBBox);
GETBOX(oaRowHeader, getBBox);
GETUUBOX(oaRowHeader, getBBox, getUUBBox);
GETPOINT(oaFigGroup, getOrigin);
GETUUPOINT(oaFigGroup, getOrigin, getUUOrigin);
GETPOINT(oaOccDonut, getCenter);
GETUUPOINT(oaOccDonut, getCenter, getUUCenter);
GETPOINT(oaOccTextDisplay, getOrigin);
GETUUPOINT(oaOccTextDisplay, getOrigin, getUUOrigin);
GETPOINT(oaOccText, getOrigin);
GETUUPOINT(oaOccText, getOrigin, getUUOrigin);
GETPOINT(oaRef, getOrigin);
GETUUPOINT(oaRef, getOrigin, getUUOrigin);
GETPOINT(oaRow, getOrigin);
GETUUPOINT(oaRow, getOrigin, getUUOrigin);
GETPOINT(oaTextDisplay, getOrigin);
GETUUPOINT(oaTextDisplay, getOrigin, getUUOrigin);
GETPOINT(oaText, getOrigin);
GETUUPOINT(oaText, getOrigin, getUUOrigin);
GETTRANSFORM(oaFigGroup, getTransform);
GETTRANSFORM(oaHierPath, getTransform);
GETTRANSFORM(oaRef, getTransform);
GETPOINTARRAY(oaBoundary, getPoints);

/// one of a kind
%extend OpenAccess_4::oaArrayInst {
        oaBox getBBox(oaUInt4 row, oaUInt4 col) const
        {
                oaBox result;
                self->getBBox(row, col, result);
                return result;
        }

        oaTransform getTransform(oaUInt4 row, oaUInt4 col) const
        {
                oaTransform result;
                self->getTransform(row, col, result);
                return result;
        }
}

