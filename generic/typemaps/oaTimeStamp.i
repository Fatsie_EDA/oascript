/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2011 Intel Corp.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%ignore OpenAccess_4::oaTimeStamp;

%typemap(in) OpenAccess_4::oaTimeStamp
{
  if (LANG_INTP($input)) {
    $1.set((oaUInt4) LANG_NUM_TO_INT($input));
  } else {
    SWIG_exception_fail(SWIG_ArgError(SWIG_ERROR), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
  }
}

%typemap(in) OpenAccess_4::oaTimeStamp& (oaTimeStamp tmpTS)
{
    $1 = tmpTS;
}

%typemap(in) const OpenAccess_4::oaTimeStamp& (oaTimeStamp tmpTS)
{
  if (LANG_INTP($input)) {
    tmpTS.set((oaUInt4) LANG_NUM_TO_INT($input));
    *$1 = tmpTS;
  } else {
    SWIG_exception_fail(SWIG_ArgError(SWIG_ERROR), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
  }
}

%typecheck(0) OpenAccess_4::oaTimeStamp&
{
  $1 = (LANG_INTP($input));
}

%typecheck(0) const OpenAccess_4::oaTimeStamp& = (OpenAccess_4::oaTimeStamp&);

%typemap(argout) OpenAccess_4::oaTimeStamp&
{
  $1 = LANG_NUM_TO_INT($input);
}

%typemap(argout) const OpenAccess_4::oaTimeStamp&
{
}

%typemap(out) OpenAccess_4::oaTimeStamp
{
  $result = LANG_INT_TO_NUM((oaUInt4) $1);
}

%typemap(out) OpenAccess_4::oaTimeStamp&
{
  $result = LANG_INT_TO_NUM((oaUInt4) *$1);
}
