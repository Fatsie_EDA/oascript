/* ****************************************************************************

   Copyright 2010 Intel Corporation
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     05/18/2010: Initial typemaps to accept a string in place of an input
                 const oaScalarName.  The default namespace is used
		 (oaNativeNS).
     11/07/2010: Updating common typemap to remove string_to_oaScalarName
                 function and add better error message reporting.
     04/19/2011: Moved to a new filename and Included typemaps for all of the
                 other "name" type classes

 *************************************************************************** */

%define OA_NAME_INPUT_TYPEMAP(ns, class, getFunc)
%typemap(in) const ns::class& (ns::class tmpSN)
{
  int res;
  void *argp;
  
  if (SWIG_IsOK(SWIG_ConvertPtr($input, &argp, $1_descriptor,  0)) && argp != NULL) {
    $1 = reinterpret_cast<$1_basetype *>(argp);
  } else if (LANG_STRINGP($input)) {
    oaNameSpace* defns = getDefaultNameSpace();
    LANG_OA_TRY(tmpSN.init(*defns, (const oaChar*)LANG_STR_TO_CSTR($input)));
    $1 = &tmpSN;
  } else {
    SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
  }
}

%typecheck(0) const ns::class& {
  void *tmpVoidPtr;
  $1 = 0;
  if (SWIG_IsOK(SWIG_ConvertPtr($input, &tmpVoidPtr, $1_descriptor, 0))) {
    $1 = 1;
  } else if (LANG_STRINGP($input)) {
    oaNameSpace* defns = getDefaultNameSpace();
    oaName tmpName;
    try {
        tmpName.init(*defns, (const oaChar*)LANG_STR_TO_CSTR($input));
        if (tmpName.getFunc()) {
            $1 = 1;
        }
    } catch (const ns::oaException& e) {
    }
  }
}
%enddef

%define COMPOSITE_OA_NAME_INPUT_TYPEMAP(ns, class)
%typemap(in) const ns::class& (ns::class tmpSN)
{
  int res;
  void *argp;
  
  if (SWIG_IsOK(SWIG_ConvertPtr($input, &argp, $1_descriptor,  0)) && argp != NULL) {
    $1 = reinterpret_cast<$1_basetype *>(argp);
  } else if (LANG_STRINGP($input)) {
    oaNameSpace* defns = getDefaultNameSpace();
    LANG_OA_TRY(tmpSN.init(*defns, (const oaChar*)LANG_STR_TO_CSTR($input)));
    $1 = &tmpSN;
  } else {
    SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
  }
}

%typecheck(0) const ns::class& {
  void *tmpVoidPtr;
  $1 = (SWIG_IsOK(SWIG_ConvertPtr($input, &tmpVoidPtr, $1_descriptor, 0)) || LANG_STRINGP($input));
}
%enddef

OA_NAME_INPUT_TYPEMAP(OpenAccess_4, oaBundleName, getBundle)
OA_NAME_INPUT_TYPEMAP(OpenAccess_4, oaScalarName, getScalar)
OA_NAME_INPUT_TYPEMAP(OpenAccess_4, oaVectorName, getVector)
OA_NAME_INPUT_TYPEMAP(OpenAccess_4, oaVectorBitName, getVectorBit)
COMPOSITE_OA_NAME_INPUT_TYPEMAP(OpenAccess_4, oaSimpleName)
COMPOSITE_OA_NAME_INPUT_TYPEMAP(OpenAccess_4, oaName)

