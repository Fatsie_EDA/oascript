/*
   Copyright 2009 Advanced Micro Devices, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <oaCommon.h>
#include <oaBase.h>
#include <oaPlugIn.h>
#include <oaDM.h>
#include <oaDesignDB.h>
#include <oaTechDB.h>
#include <oaWaferDB.h>
#include <oaUtil.h>

// temporary until we can find the right way to make these appear in the tu dump
// -----------------------------------------------------------------------------
static oa::oaCdbaNS foo1;
static oa::oaSpiceNS foo2;
static oa::oaSpefNS foo3;
static oa::oaSpfNS foo4;
static oa::oaUnixNS foo5;
static oa::oaWinNS foo6;
static oa::oaVerilogNS foo7;
static oa::oaVhdlNS foo8;
static oa::oaNativeNS foo9;
static oa::oaLefNS foo10;
static oa::oaVCOperation foo11(oa::oacVCOperationGetStatus);
static oa::oaVCMessageType foo12(oa::oacVCMsgTypeInfo);
static oa::oaVCObserverResult foo13(oa::oacVCObserverResultOk);
static oa::oaVCCap foo14(oa::oacSupportsGetStatusVCCap);
// -----------------------------------------------------------------------------


// A dummy main() function, just so you can link if you want.
int
main(int argc, char **argv)
{
	oa::oaString dummy("dummy");
}
